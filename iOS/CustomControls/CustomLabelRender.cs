﻿using System;
using OneJob;
using OneJob.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomLabel), typeof(CustomLabelRender))]
namespace OneJob.iOS
{
	public class CustomLabelRender : LabelRenderer
	{
		public CustomLabelRender(){}

		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);
			try
			{

				CustomLabel element = Element as CustomLabel;
				if (e.NewElement != null)
				{
					element = Element as CustomLabel;
				}
				else
				{
					element = e.OldElement as CustomLabel;
				}

				if (Control != null)
				{

					Control.ExclusiveTouch = true;
					//var element = Element as CustomLabel;
					if (!string.IsNullOrWhiteSpace(element.CustomFontColor))
					{
						//Control.TextColor = Color.FromHex(element.CustomFontColor).ToUIColor();
						//Control.TextColor = Color.FromHex("#00A6CC").ToUIColor();
						//Control.TextColor = Color.FromHex(element.CustomFontColor).ToCGColor();
					}
					//Control.TextColor = UIColor.Black;
					//for place holder
					//var entry1 = new Entry();
					//Control.Layer.BorderColor = Color.FromHex("#0000").ToCGColor();
					//Control.Layer.BorderWidth = 0;
					//entry1.Layer.BorderWidth = 1f;
					if (element.CustomFontFamily == "Avenir65")
					{
						Control.Font = UIFont.FromName("AvenirLTStd-Medium.ttf", 16);
					}
					else if (element.CustomFontFamily == "Avenir45")
					{
						Control.Font = UIFont.FromName("AvenirLTStd-Book.ttf", 16);
					}
					else
					{
					}
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
		}
	}
}

