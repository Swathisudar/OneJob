﻿using System;
using CoreGraphics;
using OneJob;
using OneJob.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomFrame), typeof(CustomFrameRenderer))]
namespace OneJob.iOS
{
	public class CustomFrameRenderer : FrameRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged(e);
			this.Layer.CornerRadius = 20;
			//this.Layer.Bounds.Inset(1, 1);
			Layer.BorderColor = UIColor.White.CGColor;
			Layer.BackgroundColor = UIColor.White.CGColor;
			Layer.BorderWidth = 2;
			//Layer.BackgroundColor = Color.Transparent.ToCGColor();
		}
	}
}
