﻿using System;
using OneJob;
using OneJob.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MyListView), typeof(MyListViewRender))]
namespace OneJob.iOS
{
	public class MyListViewRender : ListViewRenderer
	{
		public MyListViewRender(){}

		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);
			Control.SectionIndexColor = UIKit.UIColor.Brown;
			Control.AllowsSelection = false;
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			Control.SectionIndexColor = UIKit.UIColor.Brown;
		}
	}
}

