﻿using System;
using OneJob.iOS;
using OneJob;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace OneJob.iOS
{
	public class CustomEditorRenderer : EditorRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged(e);
			try
			{
				CustomEditor element = Element as CustomEditor;
				if (e.NewElement != null)
				{
					element = Element as CustomEditor;
				}
				else
				{
					element = e.OldElement as CustomEditor;
				}

				if (Control != null)
				{
					// do whatever you want to the UITextField here
					//var element = Element as CustomEditor;

					Control.Layer.CornerRadius = 10;
					Control.ExclusiveTouch = true;
					//Control.MinimumFontSize = 15f;

					Control.TextColor = UIColor.Black;//for place holder
													  //var entry1 = new Entry();
													  //Control.Layer.BorderColor = Color.FromHex("#0000").ToCGColor();
													  //Control.Layer.BorderWidth = 0;
													  //entry1.Layer.BorderWidth = 1f;

				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			try
			{
				CustomEditor element = Element as CustomEditor;
				if (Control != null)
				{
					// do whatever you want to the UITextField here
					//var element = Element as CustomEditor;

					//Control.Layer.CornerRadius = 10;
					Control.ExclusiveTouch = true;
					//Control.MinimumFontSize = 15f;

					Control.TextColor = UIColor.Black;
					Control.BackgroundColor = UIColor.Clear;//for place holder
													  //var entry1 = new Entry();
													  //Control.Layer.BorderColor = Color.FromHex("#0000").ToCGColor();
													  //Control.Layer.BorderWidth = 0;
													  //entry1.Layer.BorderWidth = 1f;

				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}
	}
}