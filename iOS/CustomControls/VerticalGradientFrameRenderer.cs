﻿using System;
using CoreAnimation;
using CoreGraphics;
using OneJob;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomVerticalGradientFrame), typeof(VerticalGradientFrameRenderer))]
namespace OneJob
{
	public class VerticalGradientFrameRenderer : VisualElementRenderer<Frame>
	{
		public override void Draw(CGRect rect)
		{
			base.Draw(rect);
			CustomVerticalGradientFrame stack = (CustomVerticalGradientFrame)this.Element;
			CGColor startColor = stack.StartColor.ToCGColor();
			CGColor endColor = stack.EndColor.ToCGColor();
			var gradientLayer = new CAGradientLayer();
			gradientLayer.Frame = rect;
			gradientLayer.Colors = new CGColor[] { startColor, endColor };
			NativeView.Layer.InsertSublayer(gradientLayer, 0);
		}
	}
}
