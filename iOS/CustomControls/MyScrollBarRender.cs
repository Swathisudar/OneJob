﻿using System;
using System.ComponentModel;
using OneJob;
using OneJob.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly : ExportRenderer(typeof(MyScrollBar), typeof(MyScrollBarRender))]
namespace OneJob.iOS
{
	public class MyScrollBarRender : ScrollViewRenderer
	{
		public MyScrollBarRender(){}

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || this.Element == null)
			{
				return;
			}

			if (e.OldElement != null)
			{
				e.OldElement.PropertyChanged -= OnElementPropertyChanged;
			}
			this.ShowsHorizontalScrollIndicator = false;
			this.ShowsVerticalScrollIndicator = false;
			e.NewElement.PropertyChanged += OnElementPropertyChanged;

		}

		void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			this.ShowsHorizontalScrollIndicator = false;
			this.ShowsVerticalScrollIndicator = false;
		}
}
}

