﻿using System;
using System.ComponentModel;
using CoreGraphics;
using OneJob;
using OneJob.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomBoxViewCircle), typeof(CustomBoxViewCircleRender))]
namespace OneJob.iOS
{
	public class CustomBoxViewCircleRender : BoxRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<BoxView> e)
		{
			base.OnElementChanged(e);

			if (Element == null)
				return;

			Layer.MasksToBounds = true;
			Layer.CornerRadius = (float)((CustomBoxViewCircle)this.Element).CornerRadius / 2.0f;
		}

	}
}

