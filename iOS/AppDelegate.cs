﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using TK.CustomMap.iOSUnified;
using UIKit;
using Xamarin;
using XLabs.Platform.Device;

namespace OneJob.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			#region For Screen Height & Width

			BaseContentPage.screenWidth = (int)UIScreen.MainScreen.Bounds.Width;
			BaseContentPage.screenHeight = (int)UIScreen.MainScreen.Bounds.Height;

			#endregion

			#region For Maps

			FormsMaps.Init();
			TKCustomMapRenderer.InitMapRenderer();
			//NativePlacesApi.Init();

			#endregion

			SlideOverKit.iOS.SlideOverKit.Init();

			SetIoc();

			LoadApplication(new App(false));
			//UIApplication.SharedApplication.SetStatusBarStyle.te.Appearance.BarTintColor = UIColor.White;

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
								   UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
								   new NSSet());

				UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
				UIApplication.SharedApplication.RegisterForRemoteNotifications();
			}
			else {
				UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
				UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
			}

			return base.FinishedLaunching(app, options);
		}




		#region Camera and Gallery

		private void SetIoc()
		{
			var resolverContainer = new XLabs.Ioc.SimpleContainer();
			resolverContainer.Register<XLabs.Platform.Device.IDevice>(t => AppleDevice.CurrentDevice)
				.Register<XLabs.Platform.Device.IDisplay>(t => t.Resolve<XLabs.Platform.Device.IDevice>().Display)
			.Register<XLabs.Ioc.IDependencyContainer>(t => resolverContainer);
			XLabs.Ioc.Resolver.SetResolver(resolverContainer.GetResolver());
		}
		#endregion


		#region for RemoteNotifications


		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
			//if (CrossPushNotification.Current is IPushNotificationHandler)
			//{
			//	((IPushNotificationHandler)CrossPushNotification.Current).OnErrorReceived(error);
			//}
		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			//if (CrossPushNotification.Current is IPushNotificationHandler)
			//{
			//	((IPushNotificationHandler)CrossPushNotification.Current).OnRegisteredSuccess(deviceToken);
			//}
			// REGISTER DEVICE TOKEN IN SERVER.


			var DeviceToken = deviceToken.Description;
			if (!string.IsNullOrWhiteSpace(DeviceToken))
			{
				DeviceToken = DeviceToken.Trim('<').Trim('>');
			}

			// Get previous device token
			var oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");

			// Has the token changed?
			if (string.IsNullOrEmpty(oldDeviceToken) || !oldDeviceToken.Equals(DeviceToken))
			{
				// Add custom implementation here as needed.
				DBMethods objDB = new DBMethods();
				var deviceUDID = new DeviceUDID().GetDeviceUDID();
				objDB.SaveDeviceToken(DeviceToken, deviceUDID);
				//var userInfo = objDB.GetUserInfo();
				//var deviceInfo = objDB.GetDeviceToken();
				//if (userInfo != null)
				//{
				//	IDeviceIdBAL objIDeviceBAL = new DeviceIdBAL();
				//	objIDeviceBAL.SendDeviceId(new DeviceIdReq { device_type = "ios", udid = DeviceToken, user_id = userInfo.UserID });
				//}
			}

			// Save new device token 
			NSUserDefaults.StandardUserDefaults.SetString(DeviceToken, "PushDeviceToken");
		}

		public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
		{
			application.RegisterForRemoteNotifications();
		}


		public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
		{
			//if (CrossPushNotification.Current is IPushNotificationHandler)
			//{
			//	//Console.WriteLine (userInfo);
			//	((IPushNotificationHandler)CrossPushNotification.Current).OnMessageReceived(userInfo);
			//}
		}

		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{

			try
			{
				UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

				NSDictionary aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;
				var notificationTitle = (aps[new NSString("title")] as NSString).ToString();
				var notificationMessage = (aps[new NSString("job_id")] as NSString).ToString();
				var NotificationId = (aps[new NSString("notification_id")] as NSString).ToString();

				//var notificationMessage = "89";
				//var NotificationId = "107";



				if (UIApplication.SharedApplication.ApplicationState.Equals(UIApplicationState.Active))
				{
					//App is in foreground. Act on it.
				}
				else
				{
					NotificationsPage.noteclick = "ok";
					App.Current.MainPage = new AppliedJobDetails(null, notificationMessage, NotificationId);
				}

				//				if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
				//{
				//					
				//				}



				//MessageingCenter objMessageCenterIOS = new MessageingCenter();
				//IDatabaseMethods iDatabaseMethods = new DatabaseMethods(); 

				//if (isPostNotification != null)
				//{
				//	Utilities.ToastMessage.SetToast(Utilities.ToastOptions.Notification, notificationText, PostTitle);
				//	iDatabaseMethods.AddItemCount(1, "PC");
				//	objMessageCenterIOS.NotifyPostMessage();
				//	var postNotificationCount = iDatabaseMethods.GetItemCount("PC");
				//	//UIApplication.SharedApplication.ApplicationIconBadgeNumber = postNotificationCount;
				//}
				//else {
				//	Utilities.ToastMessage.SetToast(Utilities.ToastOptions.Notification, notificationText, PostTitle);
				//}
			}
			catch (Exception ex)
			{
				//LogInfo.ReportErrorInfo(ex.Message, ex.StackTrace, "AppDelegate-DidReceiveRemoteNotification");

			}

		}

		#endregion
	}
}
