﻿using System;
using OneJob.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ICropService))]
namespace OneJob.iOS
{
	public class ICropService : ICrop
	{
		string pageTest;

		public void GetPhotoRegister(byte[] img2, string path, UploadPicture img, string testPage)
		{

			UIWindow window = UIApplication.SharedApplication.KeyWindow;
			UIViewController viewController = window.RootViewController;
			if (viewController != null)
			{
				while (viewController.PresentedViewController != null)
					viewController = viewController.PresentedViewController;
				viewController.PresentViewController(new ViewCrop(img2, testPage), true, null);
			}
		}


		public void GetPhotoEditProfile(byte[] img2, string path, EditProfile imgUpload, string testPage)
		{

			UIWindow window = UIApplication.SharedApplication.KeyWindow;
			UIViewController viewController = window.RootViewController;
			if (viewController != null)
			{
				while (viewController.PresentedViewController != null)
					viewController = viewController.PresentedViewController;
				viewController.PresentViewController(new ViewCrop(img2, testPage), true, null);
			}
		}


		public static void OnResultUpload(byte[] resultCode)
		{
			UploadPicture.imgUploadPicture.FinalUploadImg(resultCode);
		}



		public static void OnResultEditProfile(byte[] resultCode)
		{
			EditProfile.imgEditProfile.FinalUpdateImg(resultCode);
		}


		public static void OnResultEditProfileImg(string resultCode)
		{
			EditProfile.imgEditProfile.FinalEditImg(resultCode);
		}

	}
}
