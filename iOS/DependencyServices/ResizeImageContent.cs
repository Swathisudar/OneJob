﻿using System;
using System.Drawing;
using CoreGraphics;
using OneJob.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ResizeImageContent))]
namespace OneJob.iOS
{
	public class ResizeImageContent : ImageResizer
	{
		#region ImageResizer implementation

		public byte[] ResizeImage(byte[] imageData, float width, float height)
		{
			UIImage originalImage = ImageFromByteArray(imageData);
			byte[] resizedImageContent;
			//create a 24bit RGB image
			using (CGBitmapContext context = new CGBitmapContext(IntPtr.Zero,
												 (int)width, (int)height, 8,
												 (int)(4 * width), CGColorSpace.CreateDeviceRGB(),
												 CGImageAlphaInfo.PremultipliedFirst))
			{

				RectangleF imageRect = new RectangleF(0, 0, width, height);

				// draw the image 
				switch (originalImage.Orientation)
				{
					case UIImageOrientation.Left:
						context.RotateCTM((float)Math.PI / 2);
						context.TranslateCTM(0, -height);
						break;
					case UIImageOrientation.Right:
						context.RotateCTM(-((float)Math.PI / 2));
						context.TranslateCTM(-width, 0);
						break;
					case UIImageOrientation.Up:
						break;
					case UIImageOrientation.Down:
						context.TranslateCTM(width, height);
						context.RotateCTM(-(float)Math.PI);
						break;
				}

				context.DrawImage(imageRect, originalImage.CGImage);


				UIKit.UIImage resizedImage = UIKit.UIImage.FromImage(context.ToImage());

				// save the image as a jpeg
				resizedImageContent = resizedImage.AsPNG().ToArray();
			}
			return resizedImageContent;
		}

		#endregion

		public static UIKit.UIImage ImageFromByteArray(byte[] data)
		{
			if (data == null)
			{
				return null;
			}

			UIKit.UIImage image;
			try
			{
				image = new UIKit.UIImage(Foundation.NSData.FromArray(data));
			}
			catch (Exception e)
			{
				Console.WriteLine("Image load failed: " + e.Message);
				return null;
			}
			return image;
		}

		public ResizeImageContent()
		{
		}
	}
}
