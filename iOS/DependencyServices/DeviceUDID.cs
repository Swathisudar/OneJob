using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using OneJob.iOS;

[assembly: Dependency(typeof(DeviceUDID))]
namespace OneJob.iOS
{
	using System.Diagnostics;
	using System.IO;
	using System.Runtime.InteropServices;
	using OneJob;
	public class DeviceUDID : IDeviceUDID
	{
		[DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
		private static extern uint IOServiceGetMatchingService(uint masterPort, IntPtr matching);

		[DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
		private static extern IntPtr IOServiceMatching(string s);

		[DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
		private static extern IntPtr IORegistryEntryCreateCFProperty(uint entry, IntPtr key, IntPtr allocator, uint options);

		[DllImport("/System/Library/Frameworks/IOKit.framework/IOKit")]
		private static extern int IOObjectRelease(uint o);
		public string GetDeviceUDID()
		{
			string serial = string.Empty;
			uint platformExpert = IOServiceGetMatchingService(0, IOServiceMatching("IOPlatformExpertDevice"));
			if (platformExpert != 0)
			{
				NSString key = (NSString)"IOPlatformSerialNumber";
				IntPtr serialNumber = IORegistryEntryCreateCFProperty(platformExpert, key.Handle, IntPtr.Zero, 0);
				if (serialNumber != IntPtr.Zero)
				{
					serial = NSString.FromHandle(serialNumber);
				}

				IOObjectRelease(platformExpert);
			}

			//return serial;
			NSUuid identifier = UIKit.UIDevice.CurrentDevice.IdentifierForVendor;
			return identifier.AsString();

		}
		private const string NotificationKey = "LocalNotificationKey";
		public void CancelID(int id)
		{
			UIApplication.SharedApplication.CancelAllLocalNotifications();
			var notifications = UIApplication.SharedApplication.ScheduledLocalNotifications;
			var notification = notifications.Where(n => n.UserInfo.ContainsKey(NSObject.FromObject(NotificationKey)))
				.FirstOrDefault(n => n.UserInfo[NotificationKey].Equals(NSObject.FromObject(id)));
			UIApplication.SharedApplication.CancelAllLocalNotifications();
			if (notification != null)
			{
				UIApplication.SharedApplication.CancelLocalNotification(notification);
				UIApplication.SharedApplication.CancelAllLocalNotifications();
			}
		}
	}
}