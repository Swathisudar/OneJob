﻿using System;
using CoreGraphics;
using UIKit;
using OneJob;
using Wapps.TOCrop;
using Xamarin.Forms;
using System.IO;
using Foundation;
using System.Collections.ObjectModel;

namespace OneJob.iOS
{
	public partial class ViewCrop : UIViewController
	{

		UIImageView ImageView { get; set; }
		byte[] img;
		UIButton BtnEdit { get; set; }

		string testpage;
		public ViewCrop(byte[] img1, string page)
		{
			img = img1;
			testpage = page;
		}

		public override void ViewDidLoad()
		{

			base.ViewDidLoad();

			this.View.BackgroundColor = UIColor.White;
			var data = NSData.FromArray(img);
			ImageView = new UIImageView(UIImage.LoadFromData(data));
			ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.View.AddSubview(ImageView);
		}

		public override void ViewWillLayoutSubviews()
		{

			base.ViewWillLayoutSubviews();
			this.ImageView.Frame = this.View.Bounds;
			var cropVC = new TOCropViewController(TOCropViewCroppingStyle.Circular, ImageView.Image);
			cropVC.Delegate = new CropVCDelegate(this, testpage);
			this.PresentViewController(cropVC, true, null);
		}

		public class CropVCDelegate : TOCropViewControllerDelegate
		{

			ViewCrop owner;
			WeakReference<ViewCrop> _owner;
			public byte[] finalimg;
			UIViewController objUIViewController;
			string pageTest;

			public CropVCDelegate(ViewCrop owner, string page)
			{
				pageTest = page;
				this.objUIViewController = owner;
				_owner = new WeakReference<ViewCrop>(owner);
			}
			public async override void DidFinishCancelled(TOCropViewController cropViewController, bool cancelled)
			{
				try
				{
					if (pageTest == "uploadPicture")
					{

						var uploadalertimg = UploadPicture.finalimg;

						if (uploadalertimg != null)
						{
							ICropService.OnResultUpload(uploadalertimg);
						}
						else
						{
							ICropService.OnResultUpload(finalimg);
						}

						objUIViewController.PresentingViewController.DismissViewController(true, null);

					}
					else
					{
						var editProfileImg = EditProfile.finalimg;

						var databaseImg = EditProfile.updateimg;

						if (editProfileImg != null)
						{
							ICropService.OnResultEditProfile(editProfileImg);
						}
						else
						{
							if (databaseImg != null)
							{
								ICropService.OnResultEditProfileImg(databaseImg);
							}
							else
							{
								ICropService.OnResultEditProfile(finalimg);
							}
						}
						objUIViewController.PresentingViewController.DismissViewController(true, null);
					}
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			}
			public async override void DidCropImageToRect(TOCropViewController cropViewController, CGRect cropRect, nint angle)
			{
				try
				{
					cropViewController.PresentingViewController.DismissViewController(true, null);

					if (_owner.TryGetTarget(out owner))
					{
						owner.ImageView.Image = cropViewController.FinalImage;
						NSData data = null;

						data = cropViewController.FinalImage.AsPNG();
						finalimg = data.ToArray();
						String str = Convert.ToBase64String(finalimg);
					}

					if (pageTest == "uploadPicture")
					{
						ICropService.OnResultUpload(finalimg);
						owner.PresentingViewController.DismissViewController(true, null);
					}
					else
					{
						ICropService.OnResultEditProfile(finalimg);
						owner.PresentingViewController.DismissViewController(true, null);
					}
					owner.PresentingViewController.DismissViewController(true, null);

				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			}
		}
	}
}

