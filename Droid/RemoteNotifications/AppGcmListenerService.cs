﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Gms.Gcm;
using Android.Util;
using Xamarin.Forms;
using System;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.Generic;
using System.Threading;
//using OneJob.Droid.Connection;

namespace OneJob.Droid
{
	[Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
	public class AppGcmListenerService : GcmListenerService
	{
		static string title;
		static string description;
		static string jobId;
		static string NotificationId;

		//public PostComments objSavePostComments;
		public override void OnMessageReceived(string from, Bundle objData)
		{

			try
			{
				if (objData != null)
				{
					//string title = objData.GetString("title");
					//string description = objData.GetString("data");
					//string alertId = objData.GetString("registration_ids");

					title = "OneJob!";
					description = objData.GetString("title");
					jobId = objData.GetString("job_id");
					NotificationId = objData.GetString("notification_id");




					//Newtonsoft.Json.Linq.JToken values = Newtonsoft.Json.Linq.JObject.Parse(objData.GetString("Bundle")); 
					SendNotification(title, description, jobId, NotificationId);
					//SendNotification("OneJob", "Notification from OneJob");
				}
			}
			catch (Exception ex)
			{
				//LogInfo.ReportErrorInfo(ex.Message, ex.StackTrace, "AppGcmListenerService-OnMessageReceived");

			}

		}

		// Use Notification Builder to create and launch the notification:

		void SendNotification(string NotificationTitle, string NotificationMessage, string jobId, string NotificationId)
		{

			try
			{

				var intent = new Intent(this, typeof(MainActivity));
				//intent.PutExtra("alertNotificationId", NotificationAlertId);
				//intent.PutExtra("JobID", "74");
				//intent.PutExtra("NotificationId", "74");
				intent.PutExtra("JobID", jobId);
				intent.PutExtra("NotificationId", NotificationId);

				intent.AddFlags(ActivityFlags.ClearTop);

				Random random = new Random();
				int pushCount = random.Next(9999 - 1000) + 1000;

				//TaskStackBuilder stackBuilder = TaskStackBuilder.Create(this);
				//stackBuilder.AddNextIntent(intent);
				//PendingIntent resultPendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);


				intent.AddFlags(ActivityFlags.ClearTop);
				var pendingIntent = PendingIntent.GetActivity(this, pushCount, intent, PendingIntentFlags.Immutable);


				//var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.UpdateCurrent);

				var notificationBuilder = new Notification.Builder(this)
					.SetSmallIcon(Resource.Drawable.OneIcon)
					.SetContentTitle(NotificationTitle)
					.SetContentText(NotificationMessage)
					.SetAutoCancel(true)
					.SetDefaults(NotificationDefaults.Sound)
					.SetContentIntent(pendingIntent);

				var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
				notificationManager.Notify(pushCount, notificationBuilder.Build());


			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//LogInfo.ReportErrorInfo(ex.Message, ex.StackTrace, "AppGcmListenerService-SendNotification");

			}
		}

	}
}
