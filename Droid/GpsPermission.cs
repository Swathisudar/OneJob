﻿using System;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.Provider;
using OneJob.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(GpsPermission))]
namespace OneJob.Droid
{
	[Activity(Label = "OneJob", MainLauncher = false)]
	public class GpsPermission : Activity, IGps
	{
		public void GetGps()
		{
			LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

			if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
			{
				Intent gpsSettingIntent = new Intent(Settings.ActionLocationSourceSettings);
				Forms.Context.StartActivity(gpsSettingIntent);
			}
		}
	}
}
