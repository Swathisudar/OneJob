﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using OneJob.Droid;
using Java.Util;
using Xamarin.Forms;

[assembly: Dependency(typeof(ICropService))]
namespace OneJob.Droid
{
	public class ICropService : Activity, ICrop
	{
		string page;
		private Android.Net.Uri mImageCaptureUri;
		public void GetPhotoRegister(byte[] img2, string path, UploadPicture img, string testPage)
		{
			page = testPage;
			var intent = new Intent(Android.App.Application.Context, typeof(CropActivity));
			intent.PutExtra("mypage", page);

			intent.SetFlags(ActivityFlags.NewTask);
			Android.App.Application.Context.StartActivity(intent);
		}

		public void GetPhotoEditProfile(byte[] img2, string path, EditProfile imgUpload, string testPage)
		{
			page = testPage;
			var intent = new Intent(Android.App.Application.Context, typeof(CropActivity));

			intent.PutExtra("mypage", page);
			intent.SetFlags(ActivityFlags.NewTask);
			Android.App.Application.Context.StartActivity(intent);
		}



		public static void OnResultUpload(byte[] resultCode)
		{
			UploadPicture.imgUploadPicture.FinalUploadImg(resultCode);
		}

		public static void OnResultEditProfile(byte[] resultCode)
		{
			EditProfile.imgEditProfile.FinalUpdateImg(resultCode);
		}


	}
}
