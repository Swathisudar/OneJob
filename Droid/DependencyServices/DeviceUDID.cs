using Xamarin.Forms;
using Android.Provider;
using OneJob.Droid;
using Android.Telephony;
using System;
using Java.Util;
using OneJob;
using Android.App;
using Android.Support.V4.App;
using Android.Content;

[assembly: Dependency(typeof(DeviceUDID))]
namespace OneJob.Droid
{
	public class DeviceUDID : IDeviceUDID
	{
		public string GetDeviceUDID()
		{
			//            return Settings.Secure.GetString(Forms.Context.ContentResolver, Settings.Secure.AndroidId);
			//
			//
			//
			//			var telephonyDeviceID = string.Empty;
			//			var telephonySIMSerialNumber = string.Empty;
			//			TelephonyManager telephonyManager = (TelephonyManager)activity.ApplicationContext.GetSystemService(Context.TelephonyService);
			//			if (telephonyManager != null)
			//			{
			//				if(!string.IsNullOrEmpty(telephonyManager.DeviceId))
			//					telephonyDeviceID = telephonyManager.DeviceId;
			//				if(!string.IsNullOrEmpty(telephonyManager.SimSerialNumber))
			//					telephonySIMSerialNumber = telephonyManager.SimSerialNumber;
			//			}
			//			var androidID = Android.Provider.Settings.Secure.GetString(activity.ApplicationContext.ContentResolver, Android.Provider.Settings.Secure.AndroidId);
			//			var deviceUuid = new UUID(androidID.GetHashCode(), ((long)telephonyDeviceID.GetHashCode() << 32) | telephonySIMSerialNumber.GetHashCode());
			//			var deviceID = deviceUuid.ToString();



			//TelephonyManager TelephonyMgr = (TelephonyManager)Android.Content.Context.GetSystemService("TELEPHONY_SERVICE"); 
			//String m_deviceId = TelephonyMgr.GetDeviceId(); 

			return UUID.RandomUUID().ToString();

			//return Android.OS.Build.Serial.ToString();
			//return Settings.Secure.GetString(Forms.Context.ContentResolver, Settings.Secure.AndroidId);

		}

		public void CancelID(int id)
		{
			Java.Util.Random random = new Java.Util.Random();
			int pushCount = random.NextInt(9999 - 1000) + 1000;

			var intent = CreateIntent(id);
			var pendingIntent = PendingIntent.GetBroadcast(Android.App.Application.Context, id, intent, PendingIntentFlags.CancelCurrent);
			var alarmManager = GetAlarmManager();
			alarmManager.Cancel(pendingIntent);
			var notificationManager = NotificationManagerCompat.From(Android.App.Application.Context);
			notificationManager.CancelAll();
			//notificationManager.Cancel(pushCount);
		}
		private AlarmManager GetAlarmManager()
		{
			var alarmManager = Android.App.Application.Context.GetSystemService(Context.AlarmService) as AlarmManager;
			return alarmManager;
		}

		private Intent CreateIntent(int id)
		{
			return new Intent(Android.App.Application.Context, typeof(AppGcmListenerService))
				.SetAction("LocalNotifierIntent" + id);
		}
	}
}