﻿using System;
using System.IO;
using Android.Graphics;
using OneJob.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ResizeImageContent))]
namespace OneJob.Droid
{
	public class ResizeImageContent : ImageResizer
	{
		public ResizeImageContent()
		{
		}

		#region ImageResizer implementation

		public byte[] ResizeImage(byte[] imageData, float width, float height)
		{
			byte[] resizedImageContent;


			Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);


			Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)width, (int)height, false);

			using (MemoryStream ms = new MemoryStream())
			{
				resizedImage.Compress(Bitmap.CompressFormat.Png, 100, ms);
				resizedImageContent = ms.ToArray();
			}
			return resizedImageContent;
		}

		#endregion
	}
}
