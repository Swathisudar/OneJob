﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using XLabs.Platform.Device;
using Plugin.Permissions;
using Android.Gms.Common;
using System.Threading;

namespace OneJob.Droid
{
	[Activity(Label = "OneJob", Icon = "@drawable/OneIcon", Theme = "@style/Theme.Splash", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);
			Thread.Sleep(30);

			global::Xamarin.Forms.Forms.Init(this, bundle);

			#region For screen Height & Width

			var pixels = Resources.DisplayMetrics.WidthPixels;
			var scale = Resources.DisplayMetrics.Density;

			var dps = (double)((pixels - 0.5f) / scale);

			var ScreenWidth = (int)dps;
			BaseContentPage.screenWidth = ScreenWidth;

			RequestedOrientation = ScreenOrientation.Portrait;

			pixels = Resources.DisplayMetrics.HeightPixels;
			dps = (double)((pixels - 0.5f) / scale);

			var ScreenHeight = (int)dps;
			BaseContentPage.screenHeight = ScreenHeight;

			#endregion

			#region For Maps

			Xamarin.FormsMaps.Init(this, bundle);

			#endregion

			SetIoc();

			#region for local notifactions
			string JobId = Intent.GetStringExtra("JobID");
			string NotificationId = Intent.GetStringExtra("NotificationId");
			if (JobId == null || NotificationId == null)
			{
				LoadApplication(new App(false));
			}
			else
			{

				//App.isNotified = "89";
				//App.isNotifiedId = "107";
				App.isNotified = JobId;
				App.isNotifiedId = NotificationId;

				NotificationsPage.noteclick = "ok";
				LoadApplication(new App(true));
			}
			#endregion

			//LoadApplication(new App());

			if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
			{
				Window.ClearFlags(WindowManagerFlags.TranslucentStatus);
				Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
				Window.SetStatusBarColor(Android.Graphics.Color.Rgb(75, 0, 0));
			}
			#region for PushNotifications method call
			if (IsPlayServicesAvailable())
			{
				var intent = new Intent(this, typeof(RegistrationIntentService));
				StartService(intent);
			}

			#endregion
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}


		#region for PushNotifications
		public bool IsPlayServicesAvailable()
		{
			int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
			if (resultCode != ConnectionResult.Success)
			{
				if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
				{
					//msgText.Text = GoogleApiAvailability.Instance.GetErrorString(resultCode);
				}
				else
				{
					//msgText.Text = "Sorry, this device is not supported";
					Finish();
				}
				return false;
			}
			else
			{
				//msgText.Text = "Google Play Services is available.";
				return true;
			}
		}
		#endregion


		#region Camera and Gallery

		private void SetIoc()
		{
			try
			{
				var resolverContainer = new XLabs.Ioc.SimpleContainer();
				resolverContainer.Register<XLabs.Platform.Device.IDevice>(t => AndroidDevice.CurrentDevice)
				.Register<XLabs.Platform.Device.IDisplay>(t => t.Resolve<XLabs.Platform.Device.IDevice>().Display)
				.Register<XLabs.Ioc.IDependencyContainer>(t => resolverContainer);
				XLabs.Ioc.Resolver.SetResolver(resolverContainer.GetResolver());
				//XLabs.Ioc.Resolver.Resolve<XLabs.Platform.Device.IDevice>
			}
			catch (Exception ex)
			{
				//exception
			}
		}

		#endregion
	}
}
