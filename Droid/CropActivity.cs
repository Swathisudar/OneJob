﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OneJob.Droid
{
	[Activity(Label = "OneJob", MainLauncher = false)]
	public class CropActivity : Activity
	{

		string pageTest;
		const int CAMERA_CAPTURE = 1;
		//keep track of cropping intent
		const int PIC_CROP = 2;
		//captured picture uri
		private Android.Net.Uri picUri;
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			//SetContentView (Resource.Layout.Main);

			pageTest = Intent.GetStringExtra("mypage");

			string str = App.Current.Properties["startimg"] as String;
			if (str == "Gallery")
			{
				performCrop();
			}
			else {
				Intent captureIntent = new Intent(MediaStore.ActionImageCapture);
				StartActivityForResult(captureIntent, CAMERA_CAPTURE);
			}
		}

		protected async override void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Android.App.Result.Ok)
			{
				//user is returning from capturing an image using the camera
				if (requestCode == CAMERA_CAPTURE)
				{
					//get the Uri for the captured image
					picUri = data.Data;
					//carry out the crop operation
					performCrop();
				}
				//user is returning from cropping the image
				else if (requestCode == PIC_CROP)
				{
					//get the returned data
					Bundle extras = data.Extras;
					//get the cropped bitmap
					Bitmap thePic = (Android.Graphics.Bitmap)extras.GetParcelable("data");

					byte[] imgbyte;
					using (var stream = new MemoryStream())
					{
						thePic.Compress(Bitmap.CompressFormat.Png, 0, stream);

						imgbyte = stream.ToArray();
						var str = Convert.ToBase64String(imgbyte);
					}

					try
					{
						if (pageTest == "uploadPicture")
						{
							ICropService.OnResultUpload(imgbyte);
						}
						else {
							ICropService.OnResultEditProfile(imgbyte);
						}
					}
					catch (Exception ex)
					{

					}

					Finish();

				}
			}
			else {

				if (pageTest == "uploadPicture")
				{
					ICropService.OnResultUpload(null);
				}
				else {
					ICropService.OnResultEditProfile(null);
				}

				Finish();
			}
		}

		private void performCrop()
		{
			//take care of exceptions
			try
			{

				//call the standard crop action intent (the user device may not support it)
				Intent cropIntent = new Intent("com.android.camera.action.CROP");
				//indicate image type and Uri
				cropIntent.SetDataAndType(picUri, "image/*");

				cropIntent.PutExtra("crop", "true");
				//indicate aspect of desired crop
				cropIntent.PutExtra("aspectX", 1);
				cropIntent.PutExtra("aspectY", 1);
				//indicate output X and Y
				cropIntent.PutExtra("outputX", 256);
				cropIntent.PutExtra("outputY", 256);
				//retrieve data on return
				cropIntent.PutExtra("return-data", true);
				//start the activity - we handle returning in onActivityResult
				StartActivityForResult(cropIntent, PIC_CROP);
				//StartActivityForResult(cropIntent, CAMERA_CAPTURE);

			}

			//respond to users whose devices do not support the crop action
			catch (ActivityNotFoundException anfe)
			{
				//display an error message
				String errorMessage = "Whoops - your device doesn't support the crop action!";
				Toast toast = Toast.MakeText(this, errorMessage, ToastLength.Short);
				toast.Show();
			}
		}

		//public override void OnBackPressed()
		//{
		//	Finish();

		//}

	}
}
