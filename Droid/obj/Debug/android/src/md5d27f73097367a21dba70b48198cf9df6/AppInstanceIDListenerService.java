package md5d27f73097367a21dba70b48198cf9df6;


public class AppInstanceIDListenerService
	extends com.google.android.gms.iid.InstanceIDListenerService
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onTokenRefresh:()V:GetOnTokenRefreshHandler\n" +
			"";
		mono.android.Runtime.register ("OneJob.Droid.AppInstanceIDListenerService, OneJob.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", AppInstanceIDListenerService.class, __md_methods);
	}


	public AppInstanceIDListenerService () throws java.lang.Throwable
	{
		super ();
		if (getClass () == AppInstanceIDListenerService.class)
			mono.android.TypeManager.Activate ("OneJob.Droid.AppInstanceIDListenerService, OneJob.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onTokenRefresh ()
	{
		n_onTokenRefresh ();
	}

	private native void n_onTokenRefresh ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
