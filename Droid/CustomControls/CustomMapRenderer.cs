﻿using System;
using System.ComponentModel;
using Android.Gms.Maps;
using OneJob;
using TK.CustomMap.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]

namespace OneJob
{
	public class CustomMapRenderer : TKCustomMapRenderer
	{
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			var map = ((MapView)Control).Map;
			map.UiSettings.ZoomControlsEnabled = false;
			map.UiSettings.MyLocationButtonEnabled = false;
		}
	}
}
