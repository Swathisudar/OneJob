﻿using System;
using OneJob;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomVerticalGradientFrame), typeof(VerticalGradientFrameRenderer))]


namespace OneJob
{
	public class VerticalGradientFrameRenderer : VisualElementRenderer<Frame>
	{
		private Color StartColor { get; set; }
		private Color EndColor { get; set; }

		protected override void DispatchDraw(global::Android.Graphics.Canvas canvas)
		{

			var gradient = new Android.Graphics.LinearGradient(0, 0, 0, Height,
				this.StartColor.ToAndroid(),
				this.EndColor.ToAndroid(),
				Android.Graphics.Shader.TileMode.Mirror);

			var paint = new Android.Graphics.Paint()
			{
				Dither = true,
			};
			paint.SetShader(gradient);
			canvas.DrawPaint(paint);
			base.DispatchDraw(canvas);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || Element == null)
			{
				return;
			}
			try
			{
				var stack = e.NewElement as CustomVerticalGradientFrame;
				this.StartColor = stack.StartColor;
				this.EndColor = stack.EndColor;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(@"ERROR:", ex.Message);
			}
		}

	}
}