﻿using System;
using System.ComponentModel;
using OneJob;
using OneJob.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MyScrollBar), typeof(MyScrollBarRender))]
namespace OneJob.Droid
{
	public class MyScrollBarRender : ScrollViewRenderer
	{
		public MyScrollBarRender(){}

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || this.Element == null)
			{
				return;
			}

			if (e.OldElement != null)
			{
				e.OldElement.PropertyChanged -= OnElementPropertyChanged;
			}
			this.HorizontalScrollBarEnabled = false;
			this.VerticalScrollBarEnabled = false;
			e.NewElement.PropertyChanged += OnElementPropertyChanged;

		}

		void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			this.HorizontalScrollBarEnabled = false;
			this.VerticalScrollBarEnabled = false;
		}
}
}

