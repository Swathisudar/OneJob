﻿using System;
using OneJob;
using OneJob.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomBadge), typeof(CustomBadgeRenderer))]
namespace OneJob.Droid
{
	public class CustomBadgeRenderer : FrameRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged(e);
			this.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.Badge_Rect));
		}
	}
}
