﻿using System;
using OneJob;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(HorizontalGradientButton), typeof(VerticalGradientButtonRenderer))]


namespace OneJob
{
	public class VerticalGradientButtonRenderer : ButtonRenderer
	{
		private Color StartColor { get; set; }
		private Color EndColor { get; set; }

		protected override void DispatchDraw(global::Android.Graphics.Canvas canvas)
		{
			#region for Vertical Gradient  
			var gradient = new Android.Graphics.LinearGradient(0, 0, 0, Height,
			#endregion

			//#region for Horizontal Gradient  
			//var gradient = new Android.Graphics.LinearGradient(0, 0, Width, 0,

			this.StartColor.ToAndroid(),
			this.EndColor.ToAndroid(),
			Android.Graphics.Shader.TileMode.Mirror);

			//#endregion
			var paint = new Android.Graphics.Paint()
			{
				Dither = true,
			};
			paint.SetShader(gradient);
			canvas.DrawPaint(paint);
			base.DispatchDraw(canvas);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || Element == null)
			{
				return;
			}
			try
			{
				var stack = e.NewElement as HorizontalGradientButton;
				this.StartColor = stack.StartColor;
				this.EndColor = stack.EndColor;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(@"ERROR:", ex.Message);
			}
		}
	}
}