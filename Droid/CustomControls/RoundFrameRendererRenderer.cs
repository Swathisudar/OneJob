﻿using System;
using OneJob;
using OneJob.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(RoundFrame), typeof(RoundFrameRenderer))]
namespace OneJob.Droid
{
	public class RoundFrameRenderer : FrameRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged(e);
			this.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.RoundFrame));
		}
	}
}
