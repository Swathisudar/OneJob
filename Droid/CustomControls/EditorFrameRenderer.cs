﻿using System;
using OneJob;
using OneJob.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EditorFrame), typeof(EditorFrameRenderer))]
namespace OneJob.Droid
{
	public class EditorFrameRenderer : FrameRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged(e);
			this.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.EditorFrame));
		}
	}
}
