﻿using System;
using OneJob;
using OneJob.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(MyListView), typeof(MyListViewRender))]
namespace OneJob.Droid
{
	public class MyListViewRender : ListViewRenderer
	{
		public MyListViewRender(){}

		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);
			Control.Selected = false;
			//var element = e.NewElement as ListView;
			//var element2 = new Android.Widget.ListView(Context);
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
		}
	}
}

