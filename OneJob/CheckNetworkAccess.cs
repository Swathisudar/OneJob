﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneJob
{
	public static class CheckNetworkAccess
	{
		public static bool IsNetworkConnected()
		{
			bool retVal = false;

			try
			{
				retVal = CrossConnectivity.Current.IsConnected;
				return retVal;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				throw ex;
			}
		}

		public static async Task<bool> IsHostReached()
		{
			bool retVal = false;
			try
			{
				retVal = await CrossConnectivity.Current.IsReachable("http://www.google.com/");
				return retVal;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				throw ex;
			}
		}
	}
}
