﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;
using System.Linq;

namespace OneJob
{

	#region ISignInBAL interface Implementation. Here we get SignIn User details 

	public class JobDetailsBAL : IJobDetailsBAL
	{
		JobDetailsList objJobDetails;

		public async Task<JobDetailsList> jobDetailsData(JobDetailsReq request)
		{
			JobdetailsRes JSONResponse;

			List<JobsListInfo> objJobdetailsnfo = new List<JobsListInfo>();

			try
			{
				var ResponseStr = await HttpClientSource<JobDetailsReq>.CreateOrUpdateItemWithPostAsync("detailedjob", request);
				JSONResponse = JsonConvert.DeserializeObject<JobdetailsRes>(ResponseStr);


				if (JSONResponse.status == 0)
				{

					objJobdetailsnfo.Add(new JobsListInfo()
					{
						jobId = JSONResponse.jobdatails.id,
						Providerid = JSONResponse.jobdatails.provider_id,
						jobTitle = JSONResponse.jobdatails.job_title,
						workTypeID = JSONResponse.jobdatails.worktype_id,
						Experience = JSONResponse.jobdatails.experience,
						noOfPositions = JSONResponse.jobdatails.no_of_positions,
						Compensention = JSONResponse.jobdatails.compensation,
						jobDescription = JSONResponse.jobdatails.job_description,
						jobAddress = JSONResponse.jobdatails.job_address,
						jobCity = JSONResponse.jobdatails.job_city,
						jobStateId = JSONResponse.jobdatails.job_state_id,
						jobCountryId = JSONResponse.jobdatails.job_country_id,
						jobZipCode = JSONResponse.jobdatails.job_zipcode,
						Status = JSONResponse.jobdatails.status,
						createdDate = JSONResponse.jobdatails.created_date,
						modifiedDate = JSONResponse.jobdatails.modified_date,
						startDate = JSONResponse.jobdatails.start_date,
						endDate = JSONResponse.jobdatails.end_date,
						Latitude = JSONResponse.jobdatails.latitude,
						Longitude = JSONResponse.jobdatails.longitude,
						workTypeName = JSONResponse.jobdatails.worktype_name,
						companyName = JSONResponse.jobdatails.company_name,
						Favourite = JSONResponse.jobdatails.favourite,
						Applied = JSONResponse.jobdatails.applied,
						profile_pic = JSONResponse.jobdatails.profile_pic,
						providerPhoneNumber = JSONResponse.jobdatails.provider_phone,
						ProviderRating = JSONResponse.jobdatails.provider_rating,
						seekerStatus = JSONResponse.jobdatails.seeker_status,
						isInterview = JSONResponse.jobdatails.is_interview,
						amountType = JSONResponse.jobdatails.amount_type,
						countryName = JSONResponse.jobdatails.country_name,
						stateName = JSONResponse.jobdatails.state_name,
						Distance = JSONResponse.jobdatails.distance,
						RatingTest = JSONResponse.jobdatails.rating,
						perHourDay = JSONResponse.jobdatails.per_hour_day


						//Distance = JSONResponse.jobdatails.d,
						//FaceBook = JSONResponse.jobdatails.latitude,
						//firstName = JSONResponse.jobdatails.longitude,
						//jobEmail = JSONResponse.jobdatails.worktype_name,
						//lastName = JSONResponse.jobdatails.company_name,
						// linkesIn = JSONResponse.jobdatails.end_date,
						//perHourDay = JSONResponse.jobdatails.latitude,
						//Twitter = JSONResponse.jobdatails.longitude,


						//UDID = JSONResponse.user_details.,
						//deviceType = JSONResponse.user_details.device_type,
						//deviceInfo = JSONResponse.user_details.device_info,
						//createdDate = JSONResponse.user_details.created_date,
						//modifiedDate = JSONResponse.user_details.modified_date,
						//seekerStatus = JSONResponse.user_details.seeker_status
					});


					objJobDetails = new JobDetailsList()
					{
						Status = JSONResponse.status,
						JobDetails = objJobdetailsnfo,

					};
				}
				else
				{
					return null;
				}

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objJobDetails;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
