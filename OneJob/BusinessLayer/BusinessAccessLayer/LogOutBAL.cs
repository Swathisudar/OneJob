﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;

namespace OneJob
{

	#region ILogOutBAL interface Implementation. Here we get SignIn User details 

	public class LogOutBAL : ILogOutBAL
	{
		AppLogOut objLogOut;

		public async Task<AppLogOut> UserLogOut(LogOutReq request)
		{
			LogOutRes JSONResponse;


			try
			{
				var ResponseStr = await HttpClientSource<LogOutReq>.CreateOrUpdateItemWithPostAsync("clearpushnotifications", request);
				JSONResponse = JsonConvert.DeserializeObject<LogOutRes>(ResponseStr);

				if (JSONResponse.status == 0)
				{
					objLogOut = new AppLogOut()
					{
						Message = JSONResponse.message,
						Status = JSONResponse.status
					};
				}
				else {

					objLogOut = new AppLogOut()
					{
						Message = JSONResponse.message,
						Status = JSONResponse.status
					};
				}


			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objLogOut;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
