﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public class CountryBAL : ICountryBAL
	{
		public CountryBAL()
		{

		}

		public async Task<List<CountryData>> GetCountries()
		{
			List<CountryData> CountryItemsList = new List<CountryData>();

			var CountriesList = await HttpClientSource<CountryList>.RetriveDataWithPostAsync("getcountries");

			CountryItemsList = CountriesList.countries.Select(x => new CountryData
			{
				countryID = x.country_id,
				coutryName = x.country_name,
				countryDailCode = x.country_with_dial_code,
				dailCode = x.dial_code,
				coutryFlag = x.country_flag,
				iso2 = x.iso2

			}).ToList();

			return CountryItemsList;

		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
