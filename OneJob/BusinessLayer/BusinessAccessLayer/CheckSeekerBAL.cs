﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneJob
{
	public class CheckSeekerBAL : ICheckSeekerBAL
	{
		public CheckSeekerBAL()
		{
		}

		public async Task<CheckSeeker> CheckingSeeker(CheckSeekerReq  )
		{
			CheckSeeker checkSeeker = new CheckSeeker();
			try
			{
				var ResponseStr = await HttpClientSource<CheckSeekerReq>.CreateOrUpdateItemWithPostAsync("checkseekerexistornot", request);
				var JSONResponse = JsonConvert.DeserializeObject<CheckSeekerRes>(ResponseStr);
				checkSeeker.Message = JSONResponse.message;
				checkSeeker.Status = JSONResponse.status;
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
			return checkSeeker;
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
