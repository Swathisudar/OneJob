﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;

namespace OneJob
{

	#region ISignUpBAL interface Implementation. Here we get SignUp users details 


	public class SignUpBAL : ISignUpBAL
	{

		User objSignUp;

		public async Task<User> UserSignUp(SignUpReq request)
		{
			//User signupinfo;
			SignUpRes JSONResponse;

			try
			{
				var ResponseStr = await HttpClientSource<SignUpReq>.CreateOrUpdateItemWithPostAsync("registerprocess", request);
				JSONResponse = JsonConvert.DeserializeObject<SignUpRes>(ResponseStr);

				if (JSONResponse.error_count == "0")
				{
					objSignUp = new User()
					{
						errorCount = JSONResponse.error_count,
						successMessage = JSONResponse.success_message,
						register_id = JSONResponse.register_id.ToString(),
						dailID = JSONResponse.dial_code
						//responseInfo = null

					};
				}
				else {

					objSignUp = new User()
					{
						errorCount = JSONResponse.error_count,
						successMessage = "Phone Number is already exist.",//JSONResponse.success_message,
						register_id = JSONResponse.register_id.ToString(),
						dailID = JSONResponse.dial_code
						//responseInfo = null

					};
				}

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objSignUp;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
