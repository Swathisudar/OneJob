﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;
using System.Linq;

namespace OneJob
{

	#region ISelectWorkTypeBAL interface Implementation. Here we get SignIn User details 

	public class JobListBAL : IJobListBAL
	{
		JobList JSONResponse; 		public async Task<List<JobsListInfo>> GetJobList(JobListReq request) 		{ 			List<JobsListInfo> jobItemsList = new List<JobsListInfo>();  			try 			{
				//var jobListData = await HttpClientSource<JobList>.CreateOrUpdateItemWithPostAsync("listofjobs", request);
				var ResponseStr = await HttpClientSource<JobListReq>.CreateOrUpdateItemWithPostAsync("listofjobs", request); 				JSONResponse = JsonConvert.DeserializeObject<JobList>(ResponseStr);  				jobItemsList = JSONResponse.jobs.Select(x => new JobsListInfo 				{  					jobId = x.id, 					Providerid = x.provider_id, 					jobTitle = x.job_title, 					workTypeID = x.worktype_id, 					Experience = x.experience, 					noOfPositions = x.no_of_positions, 					Compensention = x.compensation, 					jobDescription = x.job_description, 					jobAddress = x.job_address, 					jobCity = x.job_city, 					jobStateId = x.job_state_id, 					jobCountryId = x.job_country_id, 					jobZipCode = x.job_zipcode, 					Status = x.status, 					createdDate = x.created_date, 					modifiedDate = x.modified_date, 					startDate = x.start_date, 					endDate = x.end_date, 					Latitude = x.latitude, 					Longitude = x.longitude, 					workTypeName = x.worktype_name, 					companyName = x.company_name, 					amountType = x.amount_type, 					FaceBook = x.facebook, 					firstName = x.first_name, 					jobEmail = x.job_email, 					lastName = x.last_name, 					linkesIn = x.linkedin, 					perHourDay = x.per_hour_day, 					Twitter = x.twitter, 					Distance = x.distance,
					Name = x.name,
					profile_pic = x.profile_pic,
					providerPhoneNumber = x.provider_phone,
					ProviderRating = x.provider_rating,
					Favourite = x.favourite 				}).ToList();  			} 			catch (Exception ex) 			{  			}  			return jobItemsList;   		} 

		public async Task<GoogleSearch> GetAreaDetails(string strSearch)
		{
			try
			{

				var result = await new HttpClientSource<GoogleSearch>().GetAreaDetailsList(strSearch, "POST");
				if (result != null)
				{
					return result;
				}
				else
				{

				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
			return null;
		}


		public async Task<LocationRootObject> GetAreaLocations(string str)
		{
			try
			{

				var result = await new HttpClientSource<LocationRootObject>().GetAreaDetailsLocation(str, "GET");
				if (result != null)
				{
					return result;
				}
				else
				{

				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
			return null;
		}

		#endregion


		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
