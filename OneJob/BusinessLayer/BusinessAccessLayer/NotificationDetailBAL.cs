﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;
using System.Linq;

namespace OneJob
{

	#region INotificationDetailBAL interface Implementation. Here we get NotificationDetails data  details 

	public class NotificationDetailBAL : INotificationDetailBAL
	{
		JobDetailsList objJobDetails;

		public async Task<JobDetailsList> NotificationDetailsData(NotificationDetailReq request)
		{
			NotificationDetailsRes JSONResponse;

			List<JobsListInfo> objJobdetailsnfo = new List<JobsListInfo>();

			try
			{
				var ResponseStr = await HttpClientSource<NotificationDetailReq>.CreateOrUpdateItemWithPostAsync("notificationdetailedjob", request);
				JSONResponse = JsonConvert.DeserializeObject<NotificationDetailsRes>(ResponseStr);


				if (JSONResponse.status == 0)
				{

					objJobdetailsnfo.Add(new JobsListInfo()
					{
						jobId = JSONResponse.notificationjobdatails.id,
						Providerid = JSONResponse.notificationjobdatails.provider_id,
						jobTitle = JSONResponse.notificationjobdatails.job_title,
						workTypeID = JSONResponse.notificationjobdatails.worktype_id,
						Experience = JSONResponse.notificationjobdatails.experience,
						noOfPositions = JSONResponse.notificationjobdatails.no_of_positions,
						Compensention = JSONResponse.notificationjobdatails.compensation,
						jobDescription = JSONResponse.notificationjobdatails.job_description,
						jobAddress = JSONResponse.notificationjobdatails.job_address,
						jobCity = JSONResponse.notificationjobdatails.job_city,
						jobStateId = JSONResponse.notificationjobdatails.job_state_id,
						jobCountryId = JSONResponse.notificationjobdatails.job_country_id,
						jobZipCode = JSONResponse.notificationjobdatails.job_zipcode,
						Status = JSONResponse.notificationjobdatails.status,
						createdDate = JSONResponse.notificationjobdatails.created_date,
						modifiedDate = JSONResponse.notificationjobdatails.modified_date,
						startDate = JSONResponse.notificationjobdatails.start_date,
						endDate = JSONResponse.notificationjobdatails.end_date,
						Latitude = JSONResponse.notificationjobdatails.latitude,
						Longitude = JSONResponse.notificationjobdatails.longitude,
						workTypeName = JSONResponse.notificationjobdatails.worktype_name,
						companyName = JSONResponse.notificationjobdatails.company_name,
						Favourite = JSONResponse.notificationjobdatails.favourite,
						Applied = JSONResponse.notificationjobdatails.applied,
						profile_pic = JSONResponse.notificationjobdatails.profile_pic,
						providerPhoneNumber = JSONResponse.notificationjobdatails.provider_phone,
						ProviderRating = JSONResponse.notificationjobdatails.provider_rating,
						seekerStatus = JSONResponse.notificationjobdatails.seeker_status,
						isInterview = JSONResponse.notificationjobdatails.is_interview,
						amountType = JSONResponse.notificationjobdatails.amount_type,
						countryName = JSONResponse.notificationjobdatails.country_name,
						stateName = JSONResponse.notificationjobdatails.state_name,
						Distance = JSONResponse.notificationjobdatails.distance.ToString(),
						RatingTest = JSONResponse.notificationjobdatails.rating,
						perHourDay = JSONResponse.notificationjobdatails.per_hour_day,
						statusMessage = JSONResponse.notificationjobdatails.message,


						//	FaceBook = JSONResponse.notificationjobdatails.f,
						//	firstName = JSONResponse.notificationjobdatails.state_name,
						//	jobEmail = JSONResponse.notificationjobdatails.distance.ToString(),
						//	lastName = JSONResponse.notificationjobdatails.rating,
						//	linkesIn = JSONResponse.notificationjobdatails.per_hour_day,
						//	Name = JSONResponse.notificationjobdatails.n,
						//Twitter = JSONResponse.notificationjobdatails.message,

					});


					objJobDetails = new JobDetailsList()
					{
						Status = JSONResponse.status,
						JobDetails = objJobdetailsnfo,

					};
				}
				else
				{
					return null;
				}

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objJobDetails;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
