﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;
using System.Linq;

namespace OneJob
{

	#region ISelectWorkTypeBAL interface Implementation. Here we get SignIn User details 

	public class SelectWorkTypeBAL : ISelectWorkTypeBAL
	{
		public SelectWorkTypeBAL()
		{

		}

		public async Task<List<SelectWorkType>> GetSelectWorkType()
		{
			List<SelectWorkType> WorkTypeItemsList = new List<SelectWorkType>();

			var objWorkTypeList = await HttpClientSource<SelectedWorks>.RetriveDataWithPostAsync("getworktype");

			WorkTypeItemsList = objWorkTypeList.worktypeget.Select(x => new SelectWorkType
			{
				ID = x.id,
				worktypeName = x.worktype_name,
				Status = x.status,
				createsDate = x.created_date,
				modifiedDate = x.modified_date,

			}).ToList();

			return WorkTypeItemsList;

		}
		#endregion


		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
