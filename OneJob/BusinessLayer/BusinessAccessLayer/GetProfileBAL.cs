﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneJob
{
	public class GetProfileBAL : IGetProfileBAL
	{
		GetProfileData objProfileInfo;

		List<GetprofileWorktypeList> objworktype = new List<GetprofileWorktypeList>();
		List<GetProileLanguagesList> objLanguages = new List<GetProileLanguagesList>();

		public async Task<GetProfileInfoList> GetProfileData(GetProfileReq request)
		{
			GetProfileInfoList GetProfileItemsList = new GetProfileInfoList();

			var ResponseStr = await HttpClientSource<GetProfileReq>.CreateOrUpdateItemWithPostAsync("getprofile", request);
			var JSONResponse = JsonConvert.DeserializeObject<GetProfileResData>(ResponseStr);


			if (JSONResponse.error_count == 0)
			{


				objworktype = JSONResponse.getprofile_details.getprofile_worktype.Select(x => new GetprofileWorktypeList
				{
					Experience = x.experience,
					worktypeId = x.worktype_id,
					worktyeName = x.worktype_name,

				}).ToList();


				objLanguages = JSONResponse.getprofile_details.getprofile_languageids.Select(x => new GetProileLanguagesList
				{
					languageCode = x.language_code,
					languageId = x.id,
					languageName = x.language_name,
					Status = x.status,

				}).ToList();


				objProfileInfo = new GetProfileData()
				{
					id = JSONResponse.getprofile_details.id,
					firstName = JSONResponse.getprofile_details.first_name,
					lastName = JSONResponse.getprofile_details.last_name,
					dateOfBirth = JSONResponse.getprofile_details.date_of_birth,
					optionalPhone = JSONResponse.getprofile_details.optional_phone,
					Address = JSONResponse.getprofile_details.address,
					Gender = JSONResponse.getprofile_details.gender,
					Latitude = JSONResponse.getprofile_details.latitude,
					Longitude = JSONResponse.getprofile_details.longitude,
					getprofileWorkTypeList = objworktype,
					getProfileLanguagesList = objLanguages,
					modifiedDate = JSONResponse.getprofile_details.modified_date,
					jobDescription = JSONResponse.getprofile_details.job_description,
					Location = JSONResponse.getprofile_details.location,
					optionalCoutryCode = JSONResponse.getprofile_details.optional_dial_code,
					City = JSONResponse.getprofile_details.city,
					countryId = JSONResponse.getprofile_details.country_id,
					createdDate = JSONResponse.getprofile_details.created_date,


					dailCode = JSONResponse.getprofile_details.dial_code,
					DeviceInfo = JSONResponse.getprofile_details.device_info,
					deviceType = JSONResponse.getprofile_details.device_type,
					Email = JSONResponse.getprofile_details.email,

					languageId = JSONResponse.getprofile_details.language_id,
					Password = JSONResponse.getprofile_details.password,
					primaryPhone = JSONResponse.getprofile_details.phone,
					profilePic = JSONResponse.getprofile_details.profile_pic,
					seekerRating = JSONResponse.getprofile_details.seeker_rating,
					seekerStatus = JSONResponse.getprofile_details.seeker_status,
					stateId = JSONResponse.getprofile_details.state_id,
					UDID = JSONResponse.getprofile_details.udid,
					zipCode = JSONResponse.getprofile_details.zipcode

				};

				GetProfileItemsList = new GetProfileInfoList()
				{
					//Message = JSONResponse.message,
					errorCount = JSONResponse.error_count,
					userDetails = objProfileInfo

				};
			}

			return GetProfileItemsList;

		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
