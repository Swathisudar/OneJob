﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;

namespace OneJob
{

	#region ISignInBAL interface Implementation. Here we get SignIn User details 

	public class SignInBAL : ISignInBAL
	{
		User objSingIn;

		public async Task<User> UserSignIn(SignInReq request)
		{
			SignInRes JSONResponse;
			UserInfo signupinfo;
			List<UserInfo> objUserInfo = new List<UserInfo>();
			List<WorkTypeDetails> workData = new List<WorkTypeDetails>();

			try
			{
				var ResponseStr = await HttpClientSource<SignInReq>.CreateOrUpdateItemWithPostAsync("loginaccess", request);
				JSONResponse = JsonConvert.DeserializeObject<SignInRes>(ResponseStr);

				if (JSONResponse.error_count != null)
				{

					if (JSONResponse.error_count != 1)
					{
						foreach (var item in JSONResponse.user_details.worktype)
						{
							workData.Add(new WorkTypeDetails
							{
								Experience = item.experience.ToString(),
								WorkTypeId = item.worktype_id,
								WorkTypeName = item.worktype_name
							});
						}
						objUserInfo.Add(new UserInfo()
						{
							UserID = JSONResponse.user_details.id,
							firstName = JSONResponse.user_details.first_name,
							lastName = JSONResponse.user_details.last_name,
							dateOfBirth = JSONResponse.user_details.date_of_birth,
							emailID = JSONResponse.user_details.email,
							phoneNumber = JSONResponse.user_details.phone,
							optionalPhoneNumber = JSONResponse.user_details.optional_phone,
							Password = JSONResponse.user_details.password,
							Adddress = JSONResponse.user_details.address,
							languageId = JSONResponse.user_details.language_id,
							cityName = JSONResponse.user_details.city,
							stateID = JSONResponse.user_details.state_id,
							countryId = JSONResponse.user_details.country_id,
							zipCode = JSONResponse.user_details.zipcode,
							Gender = JSONResponse.user_details.gender,
							//workTypeId = JSONResponse.user_details.worktype_id,
							//Experience = JSONResponse.user_details.experience,
							jobDescription = JSONResponse.user_details.job_description,
							profilePic = JSONResponse.user_details.profile_pic,
							workType = workData,
							optionalDialCode = JSONResponse.user_details.optional_dial_code,
							UDID = JSONResponse.user_details.udid,
							deviceType = JSONResponse.user_details.device_type,
							deviceInfo = JSONResponse.user_details.device_info,
							createdDate = JSONResponse.user_details.created_date,
							modifiedDate = JSONResponse.user_details.modified_date,
							seekerStatus = JSONResponse.user_details.seeker_status,
							Location = JSONResponse.user_details.location,
							seekerRating = JSONResponse.user_details.seeker_rating,
							Latitude = JSONResponse.user_details.latitude,
							Longitude = JSONResponse.user_details.longitude,
							dailCode = JSONResponse.user_details.dial_code
						});


						objSingIn = new User()
						{
							errorCount = JSONResponse.error_count.ToString(),
							responseInfo = objUserInfo,
							successMessage = JSONResponse.error_message
						};
					}
					else
					{
						objSingIn = new User()
						{
							errorCount = JSONResponse.error_count.ToString(),
							responseInfo = objUserInfo,
							successMessage = "Invalid mobile number/password"
						};
					}
				}

				else
				{
					return null;
				}

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objSingIn;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
