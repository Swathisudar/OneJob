﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;

namespace OneJob
{

	#region ISignInBAL interface Implementation. Here we get SignIn User details 

	public class UpdateProfileBAL : IUpdateProfileBAL
	{
		UpdateInfo objUpdateProfile;

		public async Task<UpdateInfo> UpdateUserProfile(UpdateProfileReq request)
		{
			UpdateProfileRes JSONResponse;

			UpdateProfileData objUpdateProfileInfo = new UpdateProfileData();

			try
			{
				var ResponseStr = await HttpClientSource<UpdateProfileReq>.CreateOrUpdateItemWithPostAsync("updateprofile", request);
				JSONResponse = JsonConvert.DeserializeObject<UpdateProfileRes>(ResponseStr);

				if (JSONResponse.error_count == 0)
				{

					objUpdateProfileInfo = new UpdateProfileData()
					{
						UserID = JSONResponse.user_details.id,
						firstName = JSONResponse.user_details.first_name,
						lastName = JSONResponse.user_details.last_name,
						dateOfBirth = JSONResponse.user_details.date_of_birth,
						optionalNumber = JSONResponse.user_details.optional_phone,
						Address = JSONResponse.user_details.address,
						//countryId = JSONResponse.user_details.country_id,
						Gender = JSONResponse.user_details.gender,
						Latitude = JSONResponse.user_details.latitude,
						Longitude=JSONResponse.user_details.longitude,
						WorkType = JSONResponse.user_details.worktype,
						languagesId = JSONResponse.user_details.language_id,
						modifiedDate = JSONResponse.user_details.modified_date,
						jobDescription = JSONResponse.user_details.job_description,
						LocationNam=JSONResponse.user_details.location,
						optionalDialCode = JSONResponse.user_details.optional_dial_code
					
						//profilePic = JSONResponse.user_details.profile_pic,

						//UDID = JSONResponse.user_details.,
						//deviceType = JSONResponse.user_details.device_type,
						//deviceInfo = JSONResponse.user_details.device_info,
						//createdDate = JSONResponse.user_details.created_date,
						//modifiedDate = JSONResponse.user_details.modified_date,
						//seekerStatus = JSONResponse.user_details.seeker_status
					};


					objUpdateProfile = new UpdateInfo()
					{
						Message = JSONResponse.message,
						errorCount = JSONResponse.error_count,
						userDetails = objUpdateProfileInfo

					};
				}
				else {
					return null;
				}

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objUpdateProfile;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
