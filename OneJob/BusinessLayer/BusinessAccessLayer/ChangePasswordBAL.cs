﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneJob
{

	#region IChangePasswordBAL interface Implementation. Here we get changepassword details 

	public class ChangePasswordBAL : IChangePasswordBAL
	{

		UserChangePassword objChangePassword;

		public async Task<UserChangePassword> ChangePwd(ChangePwdReq request)
		{
			ChangePwdRes JSONResponse;

			try
			{
				var ResponseStr = await HttpClientSource<ChangePwdReq>.CreateOrUpdateItemWithPostAsync("changepassword", request);

				JSONResponse = JsonConvert.DeserializeObject<ChangePwdRes>(ResponseStr);

				if (JSONResponse != null)
				{
					objChangePassword = new UserChangePassword
					{

						Message = JSONResponse.message,
						Status = JSONResponse.status,
					};
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return objChangePassword;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
