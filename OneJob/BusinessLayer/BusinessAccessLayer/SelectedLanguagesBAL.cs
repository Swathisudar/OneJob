﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace OneJob
{
	#region ISelectedLanguaguesBAL interface Implementation. Here we get SignIn User details 


	public class SelectedLanguagesBAL : ISelectedLanguaguesBAL
	{
		public SelectedLanguagesBAL()
		{

		}

		public async Task<List<SelectedLanguages<Type>>> UserSpeakLanguages()
		{
			List<SelectedLanguages<Type>> LanguagesItemsList = new List<SelectedLanguages<Type>>();

			var LanguagesList = await HttpClientSource<LaguageList>.RetriveDataWithPostAsync("getlanguages");

			LanguagesItemsList = LanguagesList.laguages.Select(x => new SelectedLanguages<Type>
			{
				ID = x.id,
				Status = x.status,
				LanguageName = x.language_name,
				languagesCode = x.language_code,
			}).ToList();

			return LanguagesItemsList;
			#endregion
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
