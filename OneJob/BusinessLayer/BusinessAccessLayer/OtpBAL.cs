﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneJob;

namespace OneJob
{

	#region IOtpBAL interface Implementation. Here we get SignUp users details 


	public class OtpBAL : IOtpBAL
	{

		OTPApplyJobInfo otpinfo;

		public async Task<OTPApplyJobInfo> mobileOTP(OTPReq request)
		{

			OTPRes JSONResponse;

			try
			{
				var ResponseStr = await HttpClientSource<OTPReq>.CreateOrUpdateItemWithPostAsync("send_otp", request);
				JSONResponse = JsonConvert.DeserializeObject<OTPRes>(ResponseStr);

				if (JSONResponse.status != null)
				{
					otpinfo = new OTPApplyJobInfo()
					{
						Status = JSONResponse.status,
						Message = JSONResponse.message
					};
				}
				else {
					otpinfo = null;
				}

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				//JSONResponse = null;
			}
			return otpinfo;
		}

		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
