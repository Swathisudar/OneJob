﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneJob
{
	public class SeekerRelatedJobsBAL : ISeekerRelatedJobsBAL
	{
		public SeekerRelatedJobsBAL()
		{
		}

		public async Task<SeekerRelatedJobs> SeekerRelatedJob(SeekerRelatedJobsReq request)
		{
			SeekerRelatedJobs checkSeeker = new SeekerRelatedJobs();
			List<SeekerRelatedJobsList> jobDetails = new List<SeekerRelatedJobsList>();

			try
			{
				var ResponseStr = await HttpClientSource<SeekerRelatedJobsReq>.CreateOrUpdateItemWithPostAsync("seekerrelatedjobs", request);
				var JSONResponse = JsonConvert.DeserializeObject<SeekerRelatedJobsRes>(ResponseStr);
				if (JSONResponse.jobdetails != null)
				{
					foreach (var item in JSONResponse.jobdetails)
					{
						jobDetails.Add(new SeekerRelatedJobsList()
						{
							AmountType = item.amount_type,
							CompanyName = item.company_name,
							Status = item.status,
							Compensation = item.compensation,
							CreatedDate = item.created_date,
							EndDate = item.end_date,
							Experience = item.experience,
							Facebook = item.facebook,
							Id = item.id,
							JobAddress = item.job_address,
							JobCity = item.job_city,
							JobCountryId = item.job_country_id,
							JobDescription = item.job_description,
							JobEmail = item.job_email,
							JobStateId = item.job_state_id,
							JobTitle = item.job_title,
							JobZipcode = item.job_zipcode,
							Latitude = item.latitude,
							Linkedin = item.linkedin,
							Longitude = item.longitude,
							ModifiedDate = item.modified_date,
							NoOfPositions = item.no_of_positions,
							PerHourDay = item.per_hour_day,
							ProviderId = item.provider_id,
							StartDate = item.start_date,
							Twitter = item.twitter,
							WorktypeId = item.worktype_id,
							WorktypeName = item.worktype_name,
							firstName = item.first_name,
							lastName = item.last_name,
							providerPhone = item.provider_phone,
							providerRating = item.provider_rating,
							seekerStatus = item.seeker_status,
							userProfile = item.profile_pic,
							Distance = item.distance
						});
					}
					checkSeeker.JobDetails = jobDetails;
					checkSeeker.Status = JSONResponse.status;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
			return checkSeeker;
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~CountryBAL() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion
	}
}
