﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneJob
{
	public class NotificationBAL : INotificationBAL
	{

		public async Task<NotificationListData> NotifiList(NotificationReq request)
		{
			NotificationCls JSONResponse;


			NotificationListData NotifyList = new NotificationListData();

			List<NotificationsJob> jobDetails = new List<NotificationsJob>();

			try
			{
				var ResponseStr = await HttpClientSource<NotificationReq>.CreateOrUpdateItemWithPostAsync("listofnotifications", request);
				JSONResponse = JsonConvert.DeserializeObject<NotificationCls>(ResponseStr);
				if (JSONResponse.notificationjobs != null)
				{
					foreach (var item in JSONResponse.notificationjobs)
					{
						jobDetails.Add(new NotificationsJob()
						{
							createdDate = item.created_date,
							Status = item.status,
							jobId = item.job_id,
							Message = item.message,
							NotificationID = item.notification_id,
							seekerId = item.seeker_id

						});
					}
					NotifyList.Status = JSONResponse.status;
					NotifyList.notificationJobs = jobDetails;

				}
				//return NotifyList;

			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}

			return NotifyList;

		}

		private bool disposedValue = false;
		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
				}

				disposedValue = true;
			}
		}


		public void Dispose()
		{
			Dispose(true);
		}
	}

}
