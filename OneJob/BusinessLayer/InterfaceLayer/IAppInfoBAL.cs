﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IAppInfoBAL : IDisposable
	{
		Task<AppInfoData> GetAppContent();
	}
}
