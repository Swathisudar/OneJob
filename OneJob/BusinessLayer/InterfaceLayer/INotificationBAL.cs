﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface INotificationBAL : IDisposable
	{
		Task<NotificationListData> NotifiList(NotificationReq request);
	}
}
