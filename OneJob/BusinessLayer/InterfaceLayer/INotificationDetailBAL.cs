﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface INotificationDetailBAL : IDisposable
	{
		Task<JobDetailsList> NotificationDetailsData(NotificationDetailReq request);
	}
}
