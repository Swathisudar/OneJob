﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IJobListBAL : IDisposable
	{
		Task<List<JobsListInfo>> GetJobList(JobListReq request);

		Task<GoogleSearch> GetAreaDetails(string strSearch);

		Task<LocationRootObject> GetAreaLocations(string strLocationSearch);
	}
}
