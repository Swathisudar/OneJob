﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IApplyJobBAL : IDisposable
	{

		Task<OTPApplyJobInfo> applyJOB(ApplyJobReq request);
	}
}
