﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface ISelectedLanguaguesBAL : IDisposable
	{
		Task<List<SelectedLanguages<Type>>> UserSpeakLanguages();

	}
}
