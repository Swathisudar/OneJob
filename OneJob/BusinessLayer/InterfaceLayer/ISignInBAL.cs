﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	/* SingIn Service interface */

	public interface ISignInBAL : IDisposable
	{
		Task<User> UserSignIn(SignInReq request);
	}
}
