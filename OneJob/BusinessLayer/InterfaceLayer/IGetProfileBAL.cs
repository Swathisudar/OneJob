﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IGetProfileBAL : IDisposable
	{

		Task<GetProfileInfoList> GetProfileData(GetProfileReq request);
		//Task<GetProfileInfoList> GetProfileData();

	}
}
