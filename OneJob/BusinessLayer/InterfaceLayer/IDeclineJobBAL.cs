﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IDeclineJobBAL : IDisposable
	{
		Task<DeclineJob> DeclineThisJob(DeclineJobReq request);
	}
}
