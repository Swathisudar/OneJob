﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface ISelectWorkTypeBAL : IDisposable
	{
		Task<List<SelectWorkType>> GetSelectWorkType();
	}
}
