﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	/* ChangePassword Service interface */

	public interface IChangePasswordBAL : IDisposable
	{
		Task<UserChangePassword> ChangePwd(ChangePwdReq request);
	}
}
