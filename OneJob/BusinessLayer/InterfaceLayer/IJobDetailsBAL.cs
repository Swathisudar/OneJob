﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IJobDetailsBAL : IDisposable
	{
		Task<JobDetailsList> jobDetailsData(JobDetailsReq request);
	}
}
