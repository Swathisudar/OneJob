﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IOtpBAL : IDisposable
	{
		Task<OTPApplyJobInfo> mobileOTP(OTPReq request);
	}
}
