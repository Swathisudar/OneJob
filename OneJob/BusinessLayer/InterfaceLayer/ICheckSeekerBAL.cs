﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface ICheckSeekerBAL : IDisposable
	{
		Task<CheckSeeker> CheckingSeeker(CheckSeekerReq request);
	}
}
