﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	/* UpdateProfile Service interface */

	public interface IUpdateProfileBAL : IDisposable
	{
		Task<UpdateInfo> UpdateUserProfile(UpdateProfileReq request);
	}
}
