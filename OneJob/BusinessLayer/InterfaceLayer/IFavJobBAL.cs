﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IFavJobBAL : IDisposable
	{
		Task<OTPApplyJobInfo> GetFavJob(FavJobReq request);
	}
}
