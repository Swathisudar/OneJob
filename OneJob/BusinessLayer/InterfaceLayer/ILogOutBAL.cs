﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	/* LogOut Service interface */

	public interface ILogOutBAL : IDisposable
	{
		Task<AppLogOut> UserLogOut(LogOutReq request);
	}
}
