﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface ISelectCountriesBAL : IDisposable
	{
		Task<List<Country>> GetCountries();
	}
}
