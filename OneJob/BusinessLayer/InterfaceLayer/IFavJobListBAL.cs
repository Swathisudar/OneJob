﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IFavJobListBAL : IDisposable
	{
		Task<List<JobsListInfo>> GetFavJobList(FavJobListReq request);
	}
}
