﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	/* SignUp Service interface */

	public interface ISignUpBAL : IDisposable
	{
		Task<User> UserSignUp(SignUpReq request);
	}
}
