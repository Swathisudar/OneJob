﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneJob
{
	/* ForgetPassword Service interface */

	public interface IForgetPasswordBAL : IDisposable
	{
		Task<PasswordChange> getForgetPwd(ForgetPwdReq request);
	}
}
