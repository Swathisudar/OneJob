﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface ISeekerRatingBAL : IDisposable
	{
		Task<SeekerRating> RateThisJob(SeekerRatingReq request);
	}
}
