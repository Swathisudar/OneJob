﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IProfileImgBAL : IDisposable
	{
		Task<ProfileImg> UserProfile(ProfileImgReq request);
	}
}
