﻿using System;
using System.Threading.Tasks;

namespace OneJob
{
	public interface ISeekerRelatedJobsBAL : IDisposable
	{
		Task<SeekerRelatedJobs> SeekerRelatedJob(SeekerRelatedJobsReq request);
	}
}
