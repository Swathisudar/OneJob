﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace OneJob
{
	public interface ICountryBAL : IDisposable
	{
		Task<List<CountryData>> GetCountries();
	}
}
