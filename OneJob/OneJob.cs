﻿using System;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace OneJob
{
	public class App : Application
	{
		public static string isNotified;
		public static string isNotifiedId;

		public bool navigating;
		public App(bool shallNavigate)
		{
			Application.Current.Properties["Detail"] = null;
			Application.Current.Properties["SelectedCountry"] = null;
			Application.Current.Properties["FavCount"] = "0";
			Application.Current.Properties["AllpiedCount"] = "0";


			try
			{
				IGeolocator locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				if (locator.IsGeolocationEnabled && locator.IsGeolocationAvailable)
				{

				}
				else
				{

				}
			}
			catch (Exception ex)
			{

			}

			navigating = shallNavigate;

			//var info = new DBMethods().GetUserInfo();
			//if (shallNavigate == false)
			//{
			//	if (info != null)
			//	{
			//		MainPage = new HomeMasterPage();
			//	}
			//	else {
			//		MainPage = new Carousel();
			//	}
			//}
			//else
			//{
			//	MainPage = new AppliedJobDetails(null, isNotified);
			//}

			//MainPage = new UserProfile();
			//The root page of your application         
		}


		protected override void OnStart()
		{
			OnAppStart();// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}

		private async void OnAppStart()
		{
			//BaseContentPage bcp = new BaseContentPage();
			//if (CheckNetworkAccess.IsNetworkConnected())
			//{
			//	bcp.PageLoading.IsVisible = true;
			//	DBMethods objDB = new DBMethods();
			//	var getLocalDB = objDB.GetUserInfo();
			//	var getLocalLang = objDB.GetLaguages();
			//	var getLocalWorkType = objDB.GetWorktype();

			//	if (getLocalDB != null && getLocalLang != null && getLocalWorkType != null)
			//	{
			//		App.Current.MainPage = new HomeMasterPage();
			//		bcp.PageLoading.IsVisible = false;
			//	}
			//	else
			//	{
			//		MainPage = new Carousel() { };
			//		bcp.PageLoading.IsVisible = false;
			//	}
			//}
			//else {
			//	await App.Current.MainPage.DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");

			//	bcp.PageLoading.IsVisible = false;
			//}


			#region PushNotication

			try
			{
				var objUserInfo = new DBMethods().GetUserInfo();
				if (navigating == false)
				{
					//var objUserInfo = new DatabaseMethods().GetUserInfo();
					if (objUserInfo != null)
					{
						//Constants.ServerSecurityCode = objUserInfo.SecurityCode;
						if (Device.OS == TargetPlatform.iOS)
						{
							MainPage = new HomeMasterPage();
						}
						else
						{
							if (Constants.IsNotified != true)
							{
								MainPage = new HomeMasterPage();
							}
							else
							{
								if (objUserInfo != null)
								{
									MainPage = new AppliedJobDetails(null, Constants.rem_Ids, " ");
									Constants.IsNotified = false;
								}
								else
								{
									MainPage = new Carousel();
								}
							}
						}
					}
					else
					{
						MainPage = new Carousel();
					}
				}
				else
				{
					if (objUserInfo != null)
					{
						//Constants.ServerSecurityCode = objUserInfo.SecurityCode;
						MainPage = new AppliedJobDetails(null, isNotified, isNotifiedId);
					}
					else
					{
						MainPage = new Carousel();
					}
				}


			}
			catch (Exception ex)
			{
				//System.Diagnostics.Debug.WriteLine ("Error => " + ex.Message);
				//System.Diagnostics.Debug.WriteLine ("StackTrace =>" + ex.StackTrace);
				throw new Exception(ex.Message);
			}

			#endregion

			// Handle when your app starts
		}
	}

	//public class OneJob : ContentPage
	//{
	//	public OneJob()
	//	{
	//	}
	//}
}

