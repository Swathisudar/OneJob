﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services.Media;
using System.Linq.Expressions;

namespace OneJob
{
	public partial class UploadPicture : BaseContentPage
	{
		ImageCircle imgProfile;
		string finalImg;

		public static string str;
		public MediaFile mediaFile { get; set; }
		byte[] resizedImage;
		private readonly TaskScheduler _scheduler = TaskScheduler.FromCurrentSynchronizationContext();
		public static byte[] finalimg;
		public static string[] entrydata;

		public static UploadPicture imgUploadPicture;

		public static string databaseimg;

		string pagePicture;

		public UploadPicture(string testPage)
		{
			InitializeComponent();

			imgUploadPicture = this;

			pagePicture = testPage;

			BackgroundColor = Color.FromHex("#E0E0E0");

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 7) / 100;
			var entryWidth = (width * 76.36) / 100;
			var headerHeight = (height * 8) / 100;
			var heightRequest = screenHeight / 9.55;
			var header_Height = (height * 7.3) / 100;
			var image_Height = (screenHeight * 6) / 100;
			var PicImgHeight = screenHeight / 5;
			var headerChildrenHeight = height * 7;
			Image menuImage = new Image()
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Source = "imgBack.png",
				HeightRequest = screenHeight / 14.72,
				WidthRequest = screenWidth / 8.28,
				Margin = new Thickness(screenHeight / 73.6, 0, 0, 0)
			};
			Label headerTitle = new Label()
			{
				Text = "Upload Picture",
				FontSize = screenHeight / 30,
				TextColor = Color.White,
				WidthRequest = (BaseContentPage.screenHeight * 68) / 100,
				HeightRequest = headerChildrenHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End,
				Margin = new Thickness(0, 2, 0, 0)
			};

			TapGestureRecognizer menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {
			   if (pagePicture == "editProfile")
			   {
				   Navigation.PushModalAsync(new EditProfile("editProfile"));
			   }
			   else
			   {
				   Navigation.PopModalAsync();
			   }

		   };
			menuImage.GestureRecognizers.Add(menuClicked);
			BoxView _space = new BoxView()
			{
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HeightRequest = headerHeight,
				WidthRequest = header_Height,
				HorizontalOptions = LayoutOptions.End
			};
			HorizontalGradientStack stackHeader = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { menuImage, headerTitle, _space },
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Padding = new Thickness(1, 0, 10, 0),
				BackgroundColor = Color.FromHex("#707070"),
				HeightRequest = heightRequest,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};


			imgProfile = new ImageCircle()
			{
				//Source = ImageSource.FromFile ("Avatar.png"),
				HeightRequest = PicImgHeight,
				WidthRequest = PicImgHeight,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Transparent,
				Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png")
			};


			var tgimgProfile = new TapGestureRecognizer { };
			tgimgProfile.Tapped += async (sender, args) =>
			{
				try
				{
					IList<String> buttons = new List<String>();
					buttons.Add(ChooseImageFrom.Gallery.ToString());
					buttons.Add(ChooseImageFrom.Camera.ToString());

					var action = await DisplayActionSheet("Choose photo from", "Cancel", null, buttons.ToArray());
					var _mediaPicker = DependencyService.Get<IMediaPicker>() ?? Resolver.Resolve<IDevice>().MediaPicker;

					if (action == ChooseImageFrom.Gallery.ToString())
					{
						if (Device.OS == TargetPlatform.Android)
						{
							Application.Current.Properties["startimg"] = "Gallery";
							DependencyService.Get<ICrop>().GetPhotoRegister(null, null, imgUploadPicture, "uploadPicture");
						}
						else
						{
							mediaFile = await _mediaPicker.SelectPhotoAsync(new CameraMediaStorageOptions
							{
								DefaultCamera = XLabs.Platform.Services.Media.CameraDevice.Front,
								MaxPixelDimension = 350
							});
							byte[] imageData;
							using (var streamReader = new MemoryStream())
							{
								mediaFile.Source.CopyTo(streamReader);
								imageData = streamReader.ToArray();
								str = mediaFile.Path.ToString();
							}
							resizedImage = DependencyService.Get<ImageResizer>().ResizeImage(imageData, 250, 250);
							DependencyService.Get<ICrop>().GetPhotoRegister(resizedImage, str, imgUploadPicture, "uploadPicture");
							str = Convert.ToBase64String(resizedImage);
							//imgProfile.Source = ImageSource.FromStream (() => new MemoryStream (resizedImage));
						}
					}
					else if (action == ChooseImageFrom.Camera.ToString())
					{
						if (Device.OS == TargetPlatform.Android)
						{
							Application.Current.Properties["startimg"] = "Camera";
							DependencyService.Get<ICrop>().GetPhotoRegister(null, null, imgUploadPicture, "uploadPicture");
						}
						else
						{
							await _mediaPicker.TakePhotoAsync(new CameraMediaStorageOptions
							{
								DefaultCamera = XLabs.Platform.Services.Media.CameraDevice.Front,
								MaxPixelDimension = 250
							}).ContinueWith(t =>
							{
								if (t.IsFaulted)
								{
									var s = t.Exception.InnerException.ToString();
								}
								else if (t.IsCanceled)
								{
									var canceled = true;
								}
								else
								{

									mediaFile = t.Result;

									byte[] imageData;
									using (var streamReader = new MemoryStream())
									{
										mediaFile.Source.CopyTo(streamReader);
										imageData = streamReader.ToArray();
										str = mediaFile.Path.ToString();
									}
									Application.Current.Properties["startimg"] = str;

									resizedImage = DependencyService.Get<ImageResizer>().ResizeImage(imageData, 250, 250);
									DependencyService.Get<ICrop>().GetPhotoRegister(resizedImage, str, imgUploadPicture, "uploadPicture");
									str = Convert.ToBase64String(resizedImage);
									//imgProfile.Source = ImageSource.FromStream (() => new MemoryStream (resizedImage));
									return mediaFile;
								}
								return null;
							}, _scheduler);

						}
					}
				}
				catch (TaskCanceledException ex)
				{
				}
				catch (Exception ex)
				{

					throw;
				}
			};
			imgProfile.GestureRecognizers.Add(tgimgProfile);


			Label lblProfile = new Label()
			{
				Text = "Profile picture",
				TextColor = AppGlobalVariables.fontMediumThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
			};

			Label lblTxt = new Label()
			{
				Text = "Add a picture to make the first best impression.",
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				HorizontalOptions = LayoutOptions.Center,
				TextColor = AppGlobalVariables.fontLessThick,
				FontFamily = AppGlobalVariables.fontFamily65,
				HorizontalTextAlignment = TextAlignment.Center
			};

			StackLayout bodyStack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = { imgProfile, lblProfile, lblTxt }
			};

			var btnHeight = (height * 8.5) / 100;


			Image btnBackground = new Image()
			{
				Source = "imgLoginButton.png",
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			CustomLabel imgLogin = new CustomLabel()
			{
				Text = "Submit",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Grid ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},

				WidthRequest = entryWidth,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);

			TapGestureRecognizer btnSubmitTap = new TapGestureRecognizer();
			btnSubmitTap.Tapped += btnSubmit_Click;

			ImgButton.GestureRecognizers.Add(btnSubmitTap);


			BoxView spaceFooter = new BoxView()
			{
				HeightRequest = (entryHeight * 2) / 4,
				BackgroundColor = Color.Transparent
			};


			Grid gridMain = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Star},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = 4,

				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			gridMain.Children.Add(stackHeader, 0, 0);
			gridMain.Children.Add(bodyStack, 0, 1);
			gridMain.Children.Add(ImgButton, 0, 2);
			gridMain.Children.Add(spaceFooter, 0, 3);

			PageControlsStackLayout.Children.Add(gridMain);
		}


		public void FinalUploadImg(byte[] filepath)
		{
			finalimg = filepath;

			if (finalimg != null)
			{
				String androidImg = Convert.ToBase64String(finalimg);
				imgProfile.Source = ImageSource.FromStream(() => new MemoryStream(finalimg));
			}
			else
			{
				imgProfile.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			}
		}

		public void FinalDatabaseImg(string filepath)
		{
			finalImg = filepath;
			//imgDate = filepath;
			if (finalImg != null)
			{
				imgProfile.Source = new UriImageSource()
				{
					Uri = new Uri(finalImg),
					CachingEnabled = false
				};
			}
			else
			{
				imgProfile.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			}
		}


		private async void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{

				DBMethods objDatabase = new DBMethods();

				var getLocalDB = objDatabase.GetUserInfo();

				ProfileImgReq ProfileImgReqObj = new ProfileImgReq();

				ProfileImgReqObj.seeker_id = getLocalDB.UserID;
				if (finalimg != null)
				{
					String androidImg = Convert.ToBase64String(finalimg);

					ProfileImgReqObj.seeker_image = androidImg;

				}
				else
				{
					ProfileImgReqObj.seeker_image = str;
				}

				PageLoading.IsVisible = true;


				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (IProfileImgBAL profiledetails = new ProfileImgBAL())
					{
						var uploadcheck = await profiledetails.UserProfile(ProfileImgReqObj);

						if (uploadcheck != null)
						{
							if (uploadcheck.Status == 0)
							{
								#region Save UserInfo
								//var saveUserDeatils = new userInfo
								//{

								var saveUserDeatils = new UsersInfo
								{
									UserProfilePic = uploadcheck.ProfileURL,

									Address = getLocalDB.Address,
									dateOFBirth = getLocalDB.dateOFBirth,
									Gender = getLocalDB.Gender,
									//jobDescription = getLocalDB.jobDescription,
									LocationName = getLocalDB.LocationName,
									//languageId = getLocalDB.languagesId,
									OptionalMobile = getLocalDB.OptionalMobile,
									UserFirstName = getLocalDB.UserFirstName,
									UserLastName = getLocalDB.UserLastName,
									UserID = getLocalDB.UserID,
									CountryCode = getLocalDB.CountryCode,
									OptionalCountryCode = getLocalDB.OptionalCountryCode,
									SecurityCode = "",
									UserPassword = "",
									jobDescription = " ",
									MobileNumber = getLocalDB.MobileNumber

								};
								objDatabase.DeleteUserInfo();
								objDatabase.SaveUserInfo(saveUserDeatils);
								var data = objDatabase.GetUserInfo();
								#endregion

								await DisplayAlert("OneJob", uploadcheck.Message, "Ok");
								if (pagePicture == "editProfile")
								{
									App.Current.MainPage = new HomeMasterPage();
									//await Navigation.PushModalAsync(new ProfileViewPage());
								}
								else
								{
									App.Current.MainPage = new HomeMasterPage();
								}


								PageLoading.IsVisible = false;

								//securityCode = signupcheck.ResponseInfo.SecurityCode;
								//isRegistered = "success";
							}
							else
							{

								await DisplayAlert("OneJob", uploadcheck.Message, "Ok");
								PageLoading.IsVisible = false;

							}
						}

						else
						{
							await DisplayAlert("OneJob", "Upload picture failed please try again!", "Ok");
							App.Current.MainPage = new HomeMasterPage();
							PageLoading.IsVisible = false;

						}
					}

				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}

			}
			catch (Exception ex)
			{

				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}
		#region Camera && Gallery
	}
	public static class ImageUpload
	{
		public static IMediaPicker _mediaPicker;

		public static MediaFile mediaFile { get; set; }

		public static IDevice _device;

		public static string Status { get; set; }

		public static string _path;
		private static readonly TaskScheduler scheduler = TaskScheduler.FromCurrentSynchronizationContext();

		////////////////////////////////// Choose Image From Gallery //////////////////////////////////
		public static async Task<MediaFile> ChoosePicture(int maxPixel, string imageChosenType)
		{
			mediaFile = null;
			try
			{
				//DisplayActivityIndicator.SetAcivityIndicatorState(true);
				if (imageChosenType != null)
				{
					_mediaPicker = DependencyService.Get<IMediaPicker>() ?? Resolver.Resolve<IDevice>().MediaPicker;
					await Task.Delay(1000);
					if (imageChosenType == Convert.ToString(ChooseImageFrom.Gallery))
					{
						var picfromgallery = new CameraMediaStorageOptions
						{
							Directory = "Sample",
							Name = new Random().Next().ToString("D10") + ".jpg",
							MaxPixelDimension = 400,
						};
						mediaFile = await _mediaPicker.SelectPhotoAsync(picfromgallery);
					}
					else
					{
						var picfromcamera = new CameraMediaStorageOptions
						{
							DefaultCamera = CameraDevice.Front,
							MaxPixelDimension = maxPixel,
						};
						mediaFile = await _mediaPicker.TakePhotoAsync(picfromcamera);
					}
				}
				return mediaFile;
			}
			catch (TaskCanceledException ex)
			{
				throw ex;
			}
			catch (Exception exp)
			{
				string errMsg = exp.Message;
				return null;
			}
		}
	}

	public enum ChooseImageFrom
	{
		Camera,
		Gallery
	}
	#endregion
}
