﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace OneJob
{
	public partial class HomeMenuPage : BaseContentPage
	{
		public static OptionsViewCell opv;

		#region Global Variables Declaration

		ImgCircleBlue imgProfile;
		//IDatabaseMethods objDatabase;
		Label lblusername;
		string deviceType;

		#endregion

		DBMethods objDatabase = new DBMethods();
		public HomeMenuPage()
		{
			InitializeComponent();
			opv = new OptionsViewCell();
			this.BackgroundColor = Color.White;
			Title = "HomeMaster";
			double width = (BaseContentPage.screenWidth * 1) / 100;


			#region Detect Device Type

			if (Device.OS == TargetPlatform.Android)
			{
				deviceType = "android";
			}
			else
			{
				deviceType = "ios";
			}

			#endregion

			#region for rating stars


			var ratingIcons1 = new Image() { };
			var ratingIcons2 = new Image() { };
			var ratingIcons3 = new Image() { };
			var ratingIcons4 = new Image() { };
			var ratingIcons5 = new Image() { };

			var getLocalDB = objDatabase.GetUserInfo();

			#region Rating Logic

			try
			{
				var rate = Double.Parse(getLocalDB.userRating);
				if (rate != null)
				{
					if (rate <= 0.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 0.5 || rate <= 1.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 1.5 || rate <= 2.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 2.5 || rate <= 3.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 3.5 || rate <= 4.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate >= 4.5)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingDone");
					}

				}
				else
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}

			}
			catch (Exception ex)
			{
				ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
			}

			#endregion

			Grid ratingHolder = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
				Padding = new Thickness(0, 0, 0, 0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				//WidthRequest = width * 15.62,
				WidthRequest = width * 25,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			ratingHolder.Children.Add(ratingIcons1, 0, 0);
			ratingHolder.Children.Add(ratingIcons2, 1, 0);
			ratingHolder.Children.Add(ratingIcons3, 2, 0);
			ratingHolder.Children.Add(ratingIcons4, 3, 0);
			ratingHolder.Children.Add(ratingIcons5, 4, 0);

			#endregion

			#region List Creation

			List<Options> options = new List<Options> {
				new Options { Img = "imgProfile.png", Title = "Profile",Notify=""},
				new Options { Img = "imgWork.png", Title = "Work type",Notify="" },
				new Options { Img = "imgFavourite.png", Title = "Favourites" ,Notify=""},
				new Options { Img = "imgApplied.png", Title = "Applied Jobs",Notify="" },
				new Options { Img = "imgComplete.png", Title = "Job Completed",Notify="" },
				new Options { Img = "imgBlackNotification.png", Title = "Notifications",Notify="yes"},
				new Options { Img = "imgLogout.png", Title = "Log Out",Notify=""},
			};

			#endregion

			#region  Page Header


			int height = screenHeight;

			var imageHeight = (height * 13.19) / 100;

			imgProfile = new ImgCircleBlue()
			{
				//Source = ImageSource.FromFile ("Avatar.png"),
				HeightRequest = imageHeight * 0.85,
				WidthRequest = imageHeight * 0.85,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Color.Transparent,
				Source = Device.OnPlatform("imgUpload.png", "imgUpload.png", "imgUpload.png")
			};


			if (getLocalDB == null)
			{
				imgProfile.Source = Device.OnPlatform("imgUpload.png", "imgUpload.png", "imgUpload.png");
			}

			else if (getLocalDB.UserProfilePic != null && getLocalDB.UserProfilePic != "")
			{
				imgProfile.Source = new UriImageSource()
				{
					Uri = new Uri(getLocalDB.UserProfilePic),
					CachingEnabled = false
				};
			}
			else
			{
				imgProfile.Source = Device.OnPlatform("imgUpload.png", "imgUpload.png", "imgUpload.png");
			}

			lblusername = new Label
			{
				FontSize = 18,
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.FromHex("#004491"),
				HorizontalOptions = LayoutOptions.Center,
			};


			if (getLocalDB == null)
			{
				lblusername.Text = "User Name";

			}
			else if (getLocalDB.UserFirstName == null && getLocalDB.UserLastName == null)
			{
				lblusername.Text = "User Name";
			}
			else
			{
				lblusername.Text = getLocalDB.UserFirstName + " " + getLocalDB.UserLastName;
			}




			var Header = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				BackgroundColor = Color.White,
				Padding = new Thickness(20, 10, 10, 10),
				Spacing = 10,
				Children = { imgProfile,
					new StackLayout
					{
						Orientation=StackOrientation.Vertical,
						Padding=new Thickness(0,10,0,0),
						Children=
						{
							lblusername,ratingHolder

						}
					}

					 }
			};

			var lstMenu = new ListView()
			{
				ItemsSource = options,
				HasUnevenRows = true,
				BackgroundColor = Color.FromHex("#E9E4E6"),
				SeparatorVisibility = SeparatorVisibility.None,
				ItemTemplate = new DataTemplate(typeof(OptionsViewCell)),
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			#endregion

			#region  Selected Page Navigaton

			string pageType;

			lstMenu.ItemSelected += async (sender, e) =>
			 {
				 try
				 {
					 if (e.SelectedItem == null)
						 return;
					 var parentDetailView = (MasterDetailPage)this.Parent;
					 var item = (Options)e.SelectedItem;
					 parentDetailView.IsPresented = false;

					 switch (item.Title)
					 {
						 case "Profile":
							 parentDetailView.Detail = new ProfileViewPage() { BackgroundColor = Color.White, };
							 break;
						 case "Work type":
							 App.Current.Properties["WorkFlow"] = "Home";
							 parentDetailView.Detail = new RegisterWorkType(null, null, pageType = "HomeMenu", null, 0.0, 0.0, null, null) { BackgroundColor = Color.White, };
							 break;
						 case "Favourites":
							 parentDetailView.Detail = new FavouriteJobList() { BackgroundColor = Color.White, };
							 break;
						 case "Applied Jobs":
							 //await DisplayAlert("OneJob", "InProgress.", "Ok");
							 parentDetailView.Detail = new AppliedJobs() { BackgroundColor = Color.White, };
							 break;
						 case "Job Completed":
							 parentDetailView.Detail = new JobCompleted() { BackgroundColor = Color.White, };
							 break;
						 case "Notifications":
							 //await DisplayAlert("OneJob", "InProgress.", "Ok");
							 parentDetailView.Detail = new NotificationsPage() { BackgroundColor = Color.White, };
							 break;
						 case "Log Out":


							 try
							 {
								 LogOutReq objLogOutReq = new LogOutReq();
								 if (getLocalDB != null)
								 {
									 objLogOutReq.seeker_id = getLocalDB.UserID;
								 }

								 PageLoading.IsVisible = true;

								 if (CheckNetworkAccess.IsNetworkConnected())
								 {
									 PageLoading.IsVisible = true;
									 using (ILogOutBAL getChangePwdDetails = new LogOutBAL())
									 {
										 var logoutRes = await getChangePwdDetails.UserLogOut(objLogOutReq);
										 if (logoutRes != null)
										 {
											 objDatabase.DeleteUserInfo();
											 objDatabase.DeleteWorkType();
											 objDatabase.DeleteLanguages();
											 // objDatabase.DeleteDeviceTokens();

											 App.Current.MainPage = new Carousel();

											 PageLoading.IsVisible = false;
										 }
										 else
										 {
											 await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
											 PageLoading.IsVisible = false;
										 }
									 }
								 }
								 else
								 {
									 await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
									 PageLoading.IsVisible = false;
								 }

							 }
							 catch (Exception ex)
							 {
								 var msg = ex.Message;
								 PageLoading.IsVisible = false;
							 }
							 PageLoading.IsVisible = false;



							 break;


					 }
				 }
				 catch (Exception ex)
				 {
					 System.Diagnostics.Debug.WriteLine("Error => " + ex.Message);
					 System.Diagnostics.Debug.WriteLine("StackTrace => " + ex.StackTrace);
					 throw new Exception(ex.Message);
				 }
				((ListView)sender).SelectedItem = null;
			 };

			var stackinfo = new StackLayout
			{
				Padding = new Thickness(20, 10, 10, 10),
				BackgroundColor = Color.FromHex("#E9E4E6"),
				Spacing = 40,
				Orientation = StackOrientation.Horizontal,
				Children =
				{
					new Image
							{
								HorizontalOptions=LayoutOptions.Start,
								VerticalOptions=LayoutOptions.Center,
								Source="imgInfo.png"
							},
							new Label
							{
								Text="info",
								HorizontalOptions=LayoutOptions.Start,
								VerticalOptions=LayoutOptions.Center,
								FontFamily = Device.OnPlatform("Roboto-Light.ttf", null, null),
								TextColor = Color.FromHex("#505050"),
					FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
							}
				}
			};


			TapGestureRecognizer stackinfoTap = new TapGestureRecognizer();
			stackinfoTap.Tapped += async (sender, e) =>
			{
				await Navigation.PushModalAsync(new InfoPage());
			};
			stackinfo.GestureRecognizers.Add(stackinfoTap);


			Content = new StackLayout
			{
				Spacing = 0,
				Padding = new Thickness(0, Device.OnPlatform(20, 30, 0), 0, 0),
				Orientation = StackOrientation.Vertical,
				Children = { Header, lstMenu ,
					stackinfo
				}
			};
		}
		#endregion

		#region UDID Service

		/*	userInfo getLocalDB;

		   public async void DeviceTokenUDID ()
		   {

			   getLocalDB = objDatabase.GetUserInfo ();

			   var objDeviceToken = objDatabase.GetDeviceToken ();

			   DeviceIdReq udidReq = new DeviceIdReq ();
			   udidReq.user_id = getLocalDB.UserId;
			   udidReq.device_type = deviceType;
			   udidReq.udid = objDeviceToken.DeviceToken;

			   PageLoading.IsVisible = true;

			   using (IDeviceIdBAL objDeviceId = new DeviceIdBAL ()) {
				   var objobjDeviceIdcCheck = await objDeviceId.SendDeviceId (udidReq);
				   if (objobjDeviceIdcCheck != null) {
					   if (objobjDeviceIdcCheck.Status == 1) {
						   ToastMessage.SetToast (ToastOptions.Info, objobjDeviceIdcCheck.Message);
					   } else {
						   ToastMessage.SetToast (ToastOptions.Error, "Invalid Email_ID!");
					   }
				   } else {
					   ToastMessage.SetToast (ToastOptions.Error, "Invalid Email_ID!");
				   }
			   }
			   PageLoading.IsVisible = false;
		   }

			   */

		#endregion

		#region  ListView ViewCell 

		public class OptionsViewCell : ViewCell
		{
			Label badgelabel;
			protected override void OnBindingContextChanged()
			{
				base.OnBindingContextChanged();
				dynamic c = BindingContext;
				var imgicon = new Image
				{
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Center,
					//WidthRequest = 40,
					//HeightRequest = 40
				};
				imgicon.SetBinding(Image.SourceProperty, "Img");

				var lblTitle = new Label
				{
					TextColor = Color.FromHex("#505050"),
					FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
					VerticalOptions = LayoutOptions.Center,
					HorizontalOptions = LayoutOptions.Start,
					VerticalTextAlignment = TextAlignment.Center,

					FontFamily = Device.OnPlatform("Roboto-Light.ttf", null, null),
				};
				lblTitle.SetBinding(Label.TextProperty, "Title");

				badgelabel = new Label()
				{

					TextColor = Color.White,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};

				CustomBadge badge = new CustomBadge()
				{
					Content = badgelabel,
					//BackgroundColor = AppGlobalVariables.lightMarron,
					HeightRequest = screenHeight / 26.28,
					WidthRequest = screenHeight / 18.4,
					HorizontalOptions = LayoutOptions.EndAndExpand,
					VerticalOptions = LayoutOptions.Center,
					HasShadow = false,
					Padding = 0
				};

				//var btnNotifications = new Button
				//{
				//	BackgroundColor = AppGlobalVariables.lightMarron,
				//	Text = "12",
				//	TextColor = Color.White,
				//	HeightRequest = 30,
				//	WidthRequest = 30,
				//	BorderRadius = 30,
				//	HorizontalOptions = LayoutOptions.EndAndExpand,
				//	VerticalOptions = LayoutOptions.Center
				//};

				if (c.Notify == "yes")
				{
					badge.IsVisible = true;
					opv = this;
					GetNotificationlist();
				}
				else
				{
					badge.IsVisible = false;
				}
				//btnNotifications.SetBinding(Button.TextProperty, "Notify");


				var stackRow = new StackLayout
				{
					Padding = new Thickness(6, 2, 0, 2),
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					Orientation = StackOrientation.Horizontal,
					Spacing = 15,
					Children = {
						new StackLayout {
							HorizontalOptions = LayoutOptions.Start,
							WidthRequest = 45,
							//HeightRequest = 32,
							VerticalOptions=LayoutOptions.Center,
							Padding = new Thickness(7,0,0,0),
							Children = {
								imgicon
							}
						} ,
						lblTitle,
						badge
					}
				};
				View = new StackLayout
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Padding = new Thickness(10, 0, 10, 0),
					Children =
					{
						new BoxView{HorizontalOptions=LayoutOptions.FillAndExpand,HeightRequest=0.8},
						new Frame
						{
							//BackgroundColor = Color.FromRgb(224,224,224),
							BackgroundColor = Color.Transparent,
							Content = stackRow,
							Padding = 0,
							HasShadow=false,
							//OutlineColor = Color.FromRgb(219,197,197),
							OutlineColor = Color.Transparent,
							VerticalOptions = LayoutOptions.CenterAndExpand,
							HorizontalOptions = LayoutOptions.FillAndExpand
						},
					}
				};
			}

			#region for notification count service
			public async void GetNotificationlist()
			{
				try
				{
					DBMethods objDatabase = new DBMethods();
					var getLocaDB = objDatabase.GetUserInfo();

					NotificationReq obj = new NotificationReq();
					obj.seeker_id = getLocaDB.UserID;
					//obj.seeker_id = "93";
					BaseContentPage bp = new BaseContentPage();
					bp.PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (INotificationBAL JList = new NotificationBAL())
						{
							var joblist = await JList.NotifiList(obj);
							if (joblist != null)
							{
								int i = 0;
								foreach (var item in joblist.notificationJobs)
								{
									//if (item.Status != "rejected")
									//{
									i += 1;
									//}
								}
								badgelabel.Text = i.ToString();
								//badgelabel.Text = joblist.notificationJobs.Count.ToString();
								bp.PageLoading.IsVisible = false;
							}
							else
							{
								//await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								bp.PageLoading.IsVisible = false;
							}

						}
						bp.PageLoading.IsVisible = false;
					}
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			}
			#endregion
		}
	}

	#endregion

	public class Options
	{
		public string Title { get; set; }

		public string Img { get; set; }

		public string Notify { get; set; }
	}


}
