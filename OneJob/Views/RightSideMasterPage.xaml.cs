﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using SlideOverKit;
using Xamarin.Forms;

namespace OneJob
{
	public partial class RightSideMasterPage : SlideMenuView
	{
		public ObservableCollection<SelectedLanguages<Type>> LanguagesList;
		ObservableCollection<selectedLanguages> selectedLanguagesList;
		selectedLanguages languages;
		public static RightSideMasterPage rsmp;
		ListView langagueslistView;
		public static string data;
		public static string str = null;
		public static int languageId;
		public RightSideMasterPage()
		{
			rsmp = this;
			Languages();
			selectedLanguagesList = new ObservableCollection<selectedLanguages>();
			LanguagesList = new ObservableCollection<SelectedLanguages<Type>>();

			langagueslistView = new ListView() 			{ 				HorizontalOptions = LayoutOptions.FillAndExpand, 				VerticalOptions = LayoutOptions.FillAndExpand, 				BackgroundColor = Color.Transparent, 				SeparatorVisibility = SeparatorVisibility.None, 				HasUnevenRows = true, 				Margin = new Thickness(BaseContentPage.screenHeight / 24.53, BaseContentPage.screenHeight / 49.06, BaseContentPage.screenHeight / 24.53, 0) 			};


			langagueslistView.ItemTemplate = new DataTemplate(typeof(cellview));

			langagueslistView.ItemSelected += (sender, e) =>
			{
				if (((ListView)sender).SelectedItem == null)
				{
					return;
				}
				var item = ((ListView)sender).SelectedItem as SelectedLanguages<Type>;

				if (item.IsTick == true)
				{
					item.Tick = false;
					LanguagesList.Remove(item);
				}

				else
				{
					item.Tick = true;
					LanguagesList.Add(item);
				}

				/*
				if (item.Tick == true)
				{
					languages = new selectedLanguages();
					languages.LanguageId = Convert.ToInt32(item.ID);
					languages.Language = item.LanguageName;

					selectedLanguagesList.Add(languages);

					foreach (var i in selectedLanguagesList)
					{
						if (str != null)
						{
							
							str = str + ", " + i.Language;
							languageId = i.LanguageId;
							//MessagingCenter.Send<RightSideMasterPage, int>(this, "LanguagesId", languageId);


							data = i.Language +"@"+ i.LanguageId;
							//MessagingCenter.Send<RightSideMasterPage, string>(this, "Hi",data);

						}
						else
						{
							str = i.Language;
						}
					}

					//MessagingCenter.Send<RightSideMasterPage, string>(this, "LanguagesData", str);

				}

				else
				{
					try
					{
						foreach (var j in selectedLanguagesList)
						{
							if (j.Language == item.LanguageName)
							{
								selectedLanguagesList.Remove(j);
							}

						}

					}
					catch (Exception ecx)
					{

					};

					foreach (var i in selectedLanguagesList)
					{
						if (str != null)
						{
							str = str + "," + i.Language;
						}
						else
						{
							str = i.Language;
						}
					}

					//MessagingCenter.Send<RightSideMasterPage, string>(this, "LanguagesData", str);
				}

				*/

				MessagingCenter.Send<RightSideMasterPage, string>(this, "Hi", "GoGetData");
				str = null;

				((ListView)sender).SelectedItem = null;
			};


			// InitializeComponent ();
			// You must set IsFullScreen in this case, 
			// otherwise you need to set HeightRequest, 
			// just like the QuickInnerMenu sample
			this.IsFullScreen = true;
			// You must set WidthRequest in this case
			this.WidthRequest = BaseContentPage.screenWidth / 1.27;

			//this.HeightRequest = 250;
			this.MenuOrientations = MenuOrientation.RightToLeft;

			// You must set BackgroundColor, 
			// and you cannot put another layout with background color cover the whole View
			// otherwise, it cannot be dragged on Android
			//this.BackgroundColor = Color.Red;

			// This is shadow view color, you can set a transparent color
			// this.BackgroundViewColor = Color.FromHex ("#CE766C");
			//this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.3);
			this.BackgroundColor = Color.FromRgba(0, 0, 0, 0.1);

			Label lbl = new Label()
			{
				Text = "Select Languages you speak",
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontSize = BaseContentPage.screenHeight / 36.8,
				Margin = new Thickness(BaseContentPage.screenHeight / 24.53, BaseContentPage.screenHeight / 18.4, 0, 0)
			};

			var btnHeight = (BaseContentPage.screenHeight * 8.5) / 100;


			Button donebutton = new Button()
			{
				BackgroundColor = Color.White,
				Text = "Done",
				TextColor = Color.Gray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
				HeightRequest = btnHeight,

				Margin = new Thickness(BaseContentPage.screenHeight / 24.53, 0, BaseContentPage.screenHeight / 24.53, BaseContentPage.screenHeight / 49.06)

			};

			donebutton.Clicked += (sender, e) =>
			{
				HideWithoutAnimations();
			};

			VerticalGradientStack stackSpeakLanguages = new VerticalGradientStack()
			{
				//Padding=new Thickness(0,10,0,0),
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { lbl, langagueslistView, donebutton }
			};
			Content = stackSpeakLanguages;
		}


		public async void Languages() 		{ 			try 			{
				if (LanguagesList == null)
				{
					LanguagesList = new ObservableCollection<SelectedLanguages<Type>>();
				}
				else
				{
					LanguagesList.Clear();
				} 				BaseContentPage basePage = new BaseContentPage(); 				basePage.PageLoading.IsVisible = true;
				DBMethods objDatabase = new DBMethods();
				var languageData = objDatabase.GetLaguages();
				if (languageData.Count > 0)
				{
					//foreach (var ite in languageData)
					//{
					//	LanguagesList.Add(ite);
					//}
					//SelectedLanguages
				}



 				using (ISelectedLanguaguesBAL languagesList = new SelectedLanguagesBAL()) 				{ 					var objLanguagesList = await languagesList.UserSpeakLanguages();
					//var tempObjLanguagesList = objLanguagesList;
					if (languageData.Count > 0)
					{
						foreach (var item in languageData)
						{
							foreach (var items in objLanguagesList)
							{
								if (item.Id == items.ID)
								{
									items.IsTick = true;
									LanguagesList.Add(items);
								}
							}
						}
					}
					else
					{

					}
					//langagueslistView.ItemsSource = objLanguagesList.Select(x => x.languagesName).ToList();
					//foreach (var item in objLanguagesList)
					//{
					//	LanguagesList.Add(new SelectedLanguages<Type> { LanguageName = item.LanguageName });
					//}
					langagueslistView.ItemsSource = objLanguagesList; 				} 				basePage.PageLoading.IsVisible = false; 			} 			catch (Exception ex) 			{ 				var msg = ex.Message; 			} 		} 
	}

	public class cellview : ViewCell
	{
		public cellview()
		{

			Label languagename = new Label()
			{
				TextColor = Color.White,
				FontSize = BaseContentPage.screenHeight / 36.8,
			};

			languagename.SetBinding(Label.TextProperty, new Binding("LanguageName"));

			Image checkimg = new Image()
			{
				Source = "imgCheckBoxWhite.png",
			};
			checkimg.SetBinding(Image.IsVisibleProperty, new Binding("true"));

			Image whitecheckimg = new Image()
			{
				Source = "imgCheckBoxLanWhite.png",
			};
			whitecheckimg.SetBinding(Image.IsVisibleProperty, new Binding("Tick"));

			Grid maingrid = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(4, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(0.5, GridUnitType.Star)},
				},
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			maingrid.Children.Add(languagename, 0, 0);
			maingrid.Children.Add(checkimg, 1, 0);
			maingrid.Children.Add(whitecheckimg, 1, 0);

			BoxView box = new BoxView()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 1,
				BackgroundColor = Color.FromRgb(141, 60, 79),
				Margin = new Thickness(0, BaseContentPage.screenHeight / 61.33, 0, 0)
			};
			BoxView spacebox = new BoxView()
			{
				HeightRequest = BaseContentPage.screenHeight / 36.8,
				BackgroundColor = Color.Transparent,
			};

			StackLayout mainstack = new StackLayout()
			{
				Children = { spacebox, maingrid, box },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};
			StackLayout mainstack1 = new StackLayout()
			{
				Children = { mainstack },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(0, 0, BaseContentPage.screenHeight / 73.6, 0)
			};

			View = mainstack1;
		}
	}

	//public class LanguageDetails<T> : INotifyPropertyChanged
	//{
	//	public string LanguageName { get; set; }
	//	public int LanguageId { get; set; }
	//	public bool IsTick = false;
	//	public bool Tick
	//	{
	//		get
	//		{
	//			return IsTick;
	//		}
	//		set
	//		{
	//			if (IsTick != value)
	//			{
	//				IsTick = value;
	//				PropertyChanged(this, new PropertyChangedEventArgs("Tick"));
	//			}
	//		}
	//	}

	//	public event PropertyChangedEventHandler PropertyChanged = delegate { };
	//}

	public class selectedLanguages
	{
		public string Language { get; set; }
		public int LanguageId { get; set; }
	}
}

