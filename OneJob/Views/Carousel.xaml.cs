﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Plugin.Geolocator;

namespace OneJob
{
	public partial class Carousel : BaseContentPage
	{

		public DotButtonsLayout dotLayout;
		public CarouselView carousel;
		public static bool skip = false;

		public class Details
		{
			public int position { get; set; }
			public string Header { get; set; }
			public string Content { get; set; }
		}
		public Carousel()
		{

			try
			{
				IGeolocator locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				if (locator.IsGeolocationEnabled && locator.IsGeolocationAvailable)
				{

				}
				else
				{
					DisplayAlert("Message", "Please enable Gps for locations.", "Ok");
				}
			}
			catch (Exception ex)
			{

			}
			ObservableCollection<Details> collection = new ObservableCollection<Details>{
			new Details{Header="Lorem Ipsum", Content="Lorem ipsum dolor sit amet, conselectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore." },
			new Details{Header="Tempor Labore", Content="Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mimim veni." },
			new Details{Header="Aliquip Veniam", Content="Ut anim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."},
		};
			BackgroundColor = Color.FromHex("#FFFFFF");


			var iosstatusbarstack = new StackLayout()
			{
				BackgroundColor = Color.FromHex("#4B0000"),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = screenHeight / 28.30,
				IsVisible = Device.OnPlatform(true, false, false)
			};

			var lblSkip = new Label()
			{
				Text = "SKIP",
				FontSize = screenHeight / 36.8,
				TextColor = AppGlobalVariables.fontMediumThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			var imgSkip = new Image()
			{
				HeightRequest = screenHeight / 19.36,
				WidthRequest = screenHeight / 19.36,
				Source = "imgRightArrow.png",
				VerticalOptions = LayoutOptions.CenterAndExpand
			};


			//var lblSkipTap = new TapGestureRecognizer();
			//lblSkipTap.Tapped += (s, e) =>
			//{
			//	skip = true;
			//	App.Current.MainPage = new HomeMasterPage();
			//};
			//lblSkip.GestureRecognizers.Add(lblSkipTap);


			//var imgSkipTap = new TapGestureRecognizer();
			//imgSkipTap.Tapped += (s, e) =>
			//{
			//	skip = true;
			//	App.Current.MainPage = new HomeMasterPage();
			//	//Navigation.PushModalAsync(new HomePage(true));
			//};
			//imgSkip.GestureRecognizers.Add(imgSkipTap);



			var stackSkip = new StackLayout()
			{
				Spacing = 0,
				IsEnabled = true,
				Orientation = StackOrientation.Horizontal,
				Margin = new Thickness(0, screenHeight / 73.6, screenHeight / 73.6, 0),
				HorizontalOptions = LayoutOptions.EndAndExpand,
				Children = { lblSkip, imgSkip }
			};

			TapGestureRecognizer TapstackSkip = new TapGestureRecognizer();
			TapstackSkip.NumberOfTapsRequired = 1;
			TapstackSkip.Tapped += (object sender, EventArgs e) =>
			{
				PageLoading.IsVisible = true;
				skip = true;
				App.Current.MainPage = new HomeMasterPage();
				PageLoading.IsVisible = false;
			};
			stackSkip.GestureRecognizers.Add(TapstackSkip);



			var downstack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			carousel = new CarouselView()
			{
				BackgroundColor = Color.Transparent,
				HeightRequest = screenHeight / 1.41,
			};


			DataTemplate template = new DataTemplate(() =>
			{
				BoxView box = new BoxView()
				{
					HeightRequest = screenHeight / 2.09
				};

				var stacksample = new StackLayout()
				{
					Spacing = 0,
					Padding = new Thickness(screenHeight / 21.02, 0, screenHeight / 21.02, 0),
					HorizontalOptions = LayoutOptions.FillAndExpand
				};

				var lblName = new Label()
				{
					TextColor = AppGlobalVariables.fontMediumThick,
					Margin = new Thickness(0, 0, 0, screenHeight / 49.06),
					FontSize = screenHeight / 29.44,
					FontFamily = AppGlobalVariables.fontFamily45
				};
				lblName.HorizontalOptions = LayoutOptions.Center;
				lblName.SetBinding(Label.TextProperty, "Header");

				var lblDescription = new Label()
				{
					TextColor = Color.Gray,
					FontSize = screenHeight / 43.29,
					FontFamily = AppGlobalVariables.fontFamily45,
					HorizontalTextAlignment = TextAlignment.Center
				};

				lblName.HorizontalOptions = LayoutOptions.Center;
				lblDescription.SetBinding(Label.TextProperty, "Content");

				stacksample.Children.Add(box);
				stacksample.Children.Add(lblName);
				stacksample.Children.Add(lblDescription);


				return stacksample;
			});

			carousel.ItemTemplate = template;


			carousel.PositionSelected += pageChanged;




			//Label loginlabel = new Label()
			//{
			//	Text = "Login",
			//	TextColor = Color.FromHex("#FFFFFF"),
			//	FontFamily = AppGlobalVariables.fontFamily45,
			//	FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
			//	//FontSize = screenHeight / 43.29,
			//	HorizontalOptions = LayoutOptions.CenterAndExpand,
			//	VerticalOptions = LayoutOptions.CenterAndExpand
			//};

			//CustomVerticalGradientFrame btnLogin = new CustomVerticalGradientFrame()
			//{
			//	StartColor = AppGlobalVariables.lightMarron,
			//	EndColor = AppGlobalVariables.darkMarron,
			//	Content = loginlabel,
			//	HasShadow = false,
			//	OutlineColor = Color.White,
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//	Padding = 0,
			//	HeightRequest = screenHeight / 14

			//};
			int height = screenHeight; //568;
			var entryHeight = (height * 8) / 100;


			var btnBackground = new Image()
			{
				Source = "imgButtonGradient.png",
				HeightRequest = entryHeight,
				//WidthRequest = entryWidth,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			var imgLogin = new CustomLabel()
			{
				Text = "Register",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			var ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				//BackgroundColor = Color.Blue,
				//RowSpacing = 10,
				//WidthRequest = entryWidth,
				////Padding = new Thickness(20, 20, 10, 0),
				////HorizontalOptions = LayoutOptions.FillAndExpand,
				////VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);


			var btnRegisterTap = new TapGestureRecognizer();
			btnRegisterTap.Tapped += (s, e) =>
			{

				//Application.Current.Properties["Detail"] = "Reg";
				//App.Current.MainPage = new CountryMasterPage();


				Application.Current.Properties["Detail"] = "Reg";
				//Application.Current.Properties["Detail"] = "EditProfileLocation";
				App.Current.MainPage = new CountryMasterPage();

				//Navigation.PushModalAsync(new UserRegistration());
			};
			ImgButton.GestureRecognizers.Add(btnRegisterTap);

			var imgButtonLogin = new Image()
			{
				Source = "imgButtonGradient.png",
				HeightRequest = entryHeight,
				//WidthRequest = entryWidth,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			var imglblLogin = new CustomLabel()
			{
				Text = "Login",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			var ImgButtonLogin = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				//BackgroundColor = Color.Blue,
				//RowSpacing = 10,
				//WidthRequest = entryWidth,
				////Padding = new Thickness(20, 20, 10, 0),
				////HorizontalOptions = LayoutOptions.FillAndExpand,
				////VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButtonLogin.Children.Add(imgButtonLogin, 0, 0);
			ImgButtonLogin.Children.Add(imglblLogin, 0, 0);




			var btnLoginTap = new TapGestureRecognizer();
			btnLoginTap.Tapped += (s, e) =>
			{

				//Application.Current.Properties["Detail"] = "Log";
				//App.Current.MainPage = new CountryMasterPage();

				Navigation.PushModalAsync(new UserLogin());
			};
			ImgButtonLogin.GestureRecognizers.Add(btnLoginTap);

			var buttonstack = new StackLayout()
			{
				Spacing = screenHeight / 73.6,
				Padding = new Thickness(screenHeight / 24.53, screenHeight / 36.8, screenHeight / 24.53, screenHeight / 24.53),
				Children = { ImgButton, ImgButtonLogin },
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			carousel.ItemsSource = collection;

			downstack.Children.Add(carousel);

			dotLayout = new DotButtonsLayout(collection.Count, AppGlobalVariables.lightBlue, 11);

			foreach (DotButton dot in dotLayout.dots)

				dot.Clicked += dotClicked;

			downstack.Children.Add(dotLayout);

			StackLayout stack = new StackLayout()
			{
				Children = { iosstatusbarstack, stackSkip, downstack, buttonstack },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			PageControlsStackLayout.Children.Add(stack);




		}
		private void pageChanged(object sender, SelectedPositionChangedEventArgs e)
		{
			var position = (int)(e.SelectedPosition);
			for (int i = 0; i < dotLayout.dots.Length; i++)
				if (position == i)
				{
					dotLayout.dots[i].Opacity = 1;
				}
				else
				{
					dotLayout.dots[i].Opacity = 0.2;
				}
		}

		protected override bool OnBackButtonPressed()
		{
			return false;
			//return base.OnBackButtonPressed();
		}

		private void dotClicked(object sender)
		{
			var button = (DotButton)sender;
			int index = button.index;
			carousel.Position = index;
		}
	}

	public class DotButtonsLayout : StackLayout
	{
		public DotButton[] dots;
		public DotButtonsLayout(int dotCount, Color dotColor, int dotSize)
		{

			dots = new DotButton[dotCount];

			Orientation = StackOrientation.Horizontal;
			VerticalOptions = LayoutOptions.Center;
			HorizontalOptions = LayoutOptions.Center;
			Spacing = BaseContentPage.screenHeight / 36.8;

			for (int i = 0; i < dotCount; i++)
			{
				dots[i] = new DotButton
				{
					HeightRequest = dotSize,
					WidthRequest = dotSize,
					BackgroundColor = dotColor,
					Opacity = 0.2
				};
				dots[i].index = i;
				dots[i].layout = this;
				Children.Add(dots[i]);
			}
			dots[0].Opacity = 1;
		}
	}


	public class DotButton : CustomBoxViewCircle
	{
		public int index;
		public DotButtonsLayout layout;
		public event ClickHandler Clicked;
		public delegate void ClickHandler(DotButton sender);
		public DotButton()
		{
			CornerRadius = Device.OnPlatform(10, 5, 0);
			var clickCheck = new TapGestureRecognizer()
			{
				Command = new Command(() =>
					{
						if (Clicked != null)
						{
							Clicked(this);
						}
					})
			};
			GestureRecognizers.Add(clickCheck);
		}
	}
}