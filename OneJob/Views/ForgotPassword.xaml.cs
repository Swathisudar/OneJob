﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace OneJob
{
	public partial class ForgotPassword : BaseContentPage
	{
		CustomLabel mobileNoLbl;
		CustomEntry mobileNo;
		public static CustomLabel lblCountryCode;

		public ForgotPassword()
		{
			InitializeComponent();

			//BackgroundColor = Color.FromHex("#E0E0E0");


			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 8) / 100;
			var entryWidth = (width * 75.36) / 100;


			#region Page Header
			Image headerImg = new Image()
			{
				HorizontalOptions = LayoutOptions.Start,
				Source = Device.OnPlatform("navigationBarBack.png", "imgBack.png", "navigationBarBack.png"),
				HeightRequest = screenHeight / 15.33,
				WidthRequest = screenHeight / 15.33,
				Margin = new Thickness(screenHeight / 147.2, screenHeight / 24, 0, 0)
			};
			BoxView space = new BoxView()
			{
				HeightRequest = screenHeight / 21.02
			};
			BoxView space1 = new BoxView()
			{
				HeightRequest = screenHeight / 14.72
			};

			CustomLabel lblOTPVerification = new CustomLabel()
			{
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//HorizontalTextAlignment = TextAlignment.Center,
				FontSize = screenHeight / 28.30,
				//FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				Text = "Password Recovery",
				TextColor = Color.White
			};


			#endregion

			#region PageBody
			lblCountryCode = new CustomLabel() 			{
				TextColor = Color.White, 				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Start,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				//WidthRequest = (entryWidth) / 4,
				//Margin = new Thickness(8, 0, 0, 0),
				VerticalTextAlignment = TextAlignment.Center,
				Margin = new Thickness(screenHeight / 24.53, 0, 0, 0), 			}; 			var Ccdoe = Application.Current.Properties["SelectedCountry"];
			if (Ccdoe == null)
			{
				lblCountryCode.Text = "+91";
			}
			else
			{
				lblCountryCode.Text = Ccdoe.ToString();
			}

			var lblCountryCodeTap = new TapGestureRecognizer();
			lblCountryCodeTap.Tapped += (s, e) =>
			{
				var ParentPage = (MasterDetailPage)this.Parent;
				ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			};
			lblCountryCode.GestureRecognizers.Add(lblCountryCodeTap);

			mobileNo = new CustomEntry()
			{
				Placeholder = "Mobile Number",
				//CustomFontSize = fontsize,//15,
				TextColor = Color.White,
				PlaceholderColor = Color.White,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Numeric,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			if (Device.OS == TargetPlatform.Android)
			{
				mobileNo.Margin = new Thickness(0, 5, 0, 0);
			}

			mobileNoLbl = new CustomLabel()
			{
				Text = "Mobile number",
				TextColor = Color.White,
				//TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};
			mobileNo.TextChanged += mobileNoChanged;

			mobileNo.Focused += (object sender, FocusEventArgs e) =>
			{
				mobileNoLbl.IsVisible = true;
				mobileNoLbl.TextColor = Color.White;
				mobileNoLbl.Text = "Mobile number";
				mobileNo.PlaceholderColor = Color.Transparent;

			};
			mobileNo.Unfocused += (object sender, FocusEventArgs e) =>
			{
				mobileNoLbl.IsVisible = true;

				if (!string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					var textValue = mobileNo.Text;
					if (textValue.Length >= 10 || textValue.Length == 0)
					{
						mobileNoLbl.IsVisible = true;
						mobileNoLbl.TextColor = Color.White;
						mobileNoLbl.Text = "Mobile number";
					}
					else
					{
						mobileNoLbl.IsVisible = true;
						mobileNoLbl.TextColor = Color.White;
						mobileNoLbl.Text = "Mobile number must be 10 digits";
					}
					mobileNo.PlaceholderColor = Color.Transparent;

				}
				else
				{

					mobileNoLbl.IsVisible = false;
					mobileNoLbl.Text = "Mobile number";
					mobileNo.PlaceholderColor = Color.White;
					mobileNo.Placeholder = "Mobile number";
				}
				if (string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					mobileNo.Placeholder = "Mobile number";
				}
			};

			StackLayout entryMobileNo = new StackLayout() 			{

				HorizontalOptions = LayoutOptions.Center,
				WidthRequest = entryWidth,
				Spacing = 10, 				Orientation = StackOrientation.Horizontal, 				Children = { lblCountryCode, mobileNo }, 			}; 

			BoxView OTPULine = new BoxView()
			{
				HeightRequest = 0.8,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				Opacity = 0.4,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout OTPULineStack = new StackLayout()
			{
				Children = { OTPULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};


			Image imgCountryCode = new Image()
			{
				Source = "imgPickerWhite.png",
				Margin = new Thickness(screenHeight / 49.06, 0, 0, 0),
			};

			Grid entry = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},

				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			entry.Children.Add(lblCountryCode, 0, 0);
			entry.Children.Add(imgCountryCode, 1, 0);
			entry.Children.Add(entryMobileNo, 2, 0);


			pageBody.Children.Add(mobileNoLbl, 0, 0);
			pageBody.Children.Add(OTPULineStack, 0, 0);
			pageBody.Children.Add(entry, 0, 0);


			Label lblTxt = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				Text = "Enter registered mobile number",
				TextColor = Color.White,
				Opacity = 0.5,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))


			};
			Label lblTxtNumber = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				Text = "your number.",
				Opacity = 0,
				IsEnabled = false
			};
			StackLayout txtStack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { pageBody,
					new ContentView{
						Padding=new Thickness(30,0,30,0),
						Content=lblTxt
					},
					 lblTxtNumber }
			};



			CustomButton btnSubmit = new CustomButton()
			{
				Text = "Submit",
				TextColor = AppGlobalVariables.fontMediumThick,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomButton)),
				FontFamily = AppGlobalVariables.fontFamily45,
				BorderRadius = 2,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End,

				HeightRequest = screenHeight / 14,

				Margin = new Thickness(screenHeight / 24.53, screenHeight / 76.3, screenHeight / 24.53, 0),
			};

			btnSubmit.Clicked += btnSubmit_Click;


			StackLayout bodyStack = new StackLayout()
			{
				Spacing = 12,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { txtStack, btnSubmit }
			};

			StackLayout headerStack = new StackLayout()
			{
				Padding = new Thickness(5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { headerImg, space, lblOTPVerification, space1, bodyStack }
			};


			#endregion


			VerticalGradientStack MainStack = new VerticalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { headerStack }
			};



			PageControlsStackLayout.Children.Add(MainStack);

			var imgNavigationTap = new TapGestureRecognizer();
			imgNavigationTap.Tapped += (s, e) =>
			{
				Navigation.PushModalAsync(new UserLogin());
			};
			headerImg.GestureRecognizers.Add(imgNavigationTap);

		}

		#region event to handle when mobile number is changed
		void mobileNoChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 10)
			{
				mobileNo.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 10)
			{
				mobileNoLbl.TextColor = Color.White;
				mobileNoLbl.Text = "Mobile number";
			}
			else
			{
				mobileNoLbl.TextColor = Color.White;
				mobileNoLbl.Text = "Mobile number must be 10 digits";
			}
		}






		#endregion


		private async void btnSubmit_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(mobileNo.Text))
			{
				mobileNoLbl.IsVisible = true;
				mobileNoLbl.TextColor = Color.White;
				mobileNoLbl.Text = "Please enter Mobile number";
			}

			else if (mobileNo.Text.Length != 10)
			{
				mobileNoLbl.IsVisible = true;
				mobileNoLbl.TextColor = Color.White;
				mobileNoLbl.Text = "Mobile number must be 10 digits";
			}
			else if (!Regex.IsMatch(mobileNo.Text, @"^([1-9])([0-9]*)$"))
			{
				mobileNoLbl.IsVisible = true;
				mobileNoLbl.TextColor = Color.White;
				mobileNoLbl.Text = "Mobile number must be 10 digits";
			}

			else
			{
				PageLoading.IsVisible = true;

				CheckSeekerReq csRequest = new CheckSeekerReq();
				csRequest.phone = mobileNo.Text;

				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (ICheckSeekerBAL checkUser = new CheckSeekerBAL())
					{
						var checkUserResponse = await checkUser.CheckingSeeker(csRequest);
						if (checkUserResponse != null)
						{
							if (checkUserResponse.Status == 1)
							{
								await DisplayAlert("OneJob", checkUserResponse.Message, "Ok");
								PageLoading.IsVisible = false;
							}
							else
							{
								//await DisplayAlert("OneJob", checkUserResponse.Message, "Ok");
								Random generator = new Random();
								String randomNumber = generator.Next(100000, 999999).ToString("D6");
								OTPReq objOtpReq = new OTPReq();
								objOtpReq.otp = randomNumber;
								string str = lblCountryCode.Text + mobileNo.Text;
								//string str = "+" + lblCountryCode.Text + mobileNo.Text;
								objOtpReq.phone_number = str; //otpmobilenumberTest

								objOtpReq.otp_for = "ForgetPassword";


								string pageInfo = "forgetpassword";

								//string[] forgetdata = new string[] { pageInfo, randomNumber, str };
								string[] forgetdata = new string[] { pageInfo, randomNumber, mobileNo.Text, lblCountryCode.Text };
								//string[] registerdata = new string[] { pageInfo, randomNumber, mobileNo.Text, password.Text, lblCountryCode.Text };

								PageLoading.IsVisible = true;
								if (CheckNetworkAccess.IsNetworkConnected())
								{
									using (IOtpBAL getOtpDetails = new OtpBAL())
									{
										var otpCheck = await getOtpDetails.mobileOTP(objOtpReq);

										if (otpCheck != null)
										{
											if (otpCheck.Status == 1)
											{
												await DisplayAlert("OneJob", otpCheck.Message, "Ok");
												await Navigation.PushModalAsync(new OTPVerification(forgetdata));
											}
											else
											{
												await DisplayAlert("OneJob", otpCheck.Message, "Ok");
												PageLoading.IsVisible = false;
											}

											PageLoading.IsVisible = false;
										}
										else
										{
											//await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");

											await DisplayAlert("OneJob", "Please check country code", "Ok");
											PageLoading.IsVisible = false;
										}
									}
								}
								else
								{
									await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
									PageLoading.IsVisible = false;
								}

								//	PageLoading.IsVisible = false;

							}
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}

					}

				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
		}
	}
}
