﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace OneJob
{
	public partial class HomeMasterPage : MasterDetailPage
	{
		public HomeMasterPage()
		{
			this.Icon = null;
			this.Title = "Menu";

			NavigationPage.SetHasNavigationBar(this, false);

			//InitializeComponent();


			Application.Current.Properties["ParentPage"] = this;

			Master = new HomeMenuPage();
			Detail = new HomePage() { BackgroundColor = Color.White, };
		}
	}
}
