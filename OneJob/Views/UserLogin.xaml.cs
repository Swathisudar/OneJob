﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OneJob
{
	public partial class UserLogin : BaseContentPage
	{
		CustomEntry mobileNo, password, confirmPassword;
		CustomLabel mobileNoLbl, passwordLbl, confirmPasswordLbl;
		public static CustomLabel logCountryCode;
		string deviceType;

		protected override bool OnBackButtonPressed()
		{
			App.Current.MainPage = new Carousel();
			return true;
		}

		public UserLogin()
		{
			//InitializeComponent();

			BackgroundColor = Color.FromHex("#FFFFFF");

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 8) / 100;

			var entryWidth = (width * 81) / 100;
			#region Detect Device Type
			if (Device.OS == TargetPlatform.Android)
			{
				deviceType = "android";
			}
			else
			{
				deviceType = "iphone";
			}
			#endregion

			logCountryCode = new CustomLabel()
			{
				Text = "+91",
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Margin = new Thickness((entryWidth) / 9.5, 0, 0, 0),
				VerticalTextAlignment = TextAlignment.Center
			};

			//var Ccdoe = Application.Current.Properties["SelectedCountry"];
			//if (Ccdoe == null)
			//{
			//	logCountryCode.Text = "+91";
			//}
			//else
			//{
			//	logCountryCode.Text = "+" + Ccdoe.ToString();
			//}

			var logCountryCodeTap = new TapGestureRecognizer();
			logCountryCodeTap.Tapped += (s, e) =>
			{
				var ParentPage = (MasterDetailPage)this.Parent;
				ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			};
			logCountryCode.GestureRecognizers.Add(logCountryCodeTap);


			mobileNo = new CustomEntry()
			{
				Placeholder = " Mobile Number",
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Telephone,
				HeightRequest = entryHeight,
				//WidthRequest = (entryWidth * 2) / 2.2,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};

			if (Device.OS == TargetPlatform.Android)
			{
				mobileNo.Margin = new Thickness(0, 3, 0, 0);
			}

			Image imgCountryCode = new Image()
			{
				Source = "imgPicker.png"
			};

			mobileNoLbl = new CustomLabel()
			{
				Text = "Mobile number",
				TextColor = AppGlobalVariables.fontLessThick,
				//TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			mobileNo.TextChanged += mobileNoChanged;

			Grid entry = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = entryWidth,
				ColumnSpacing = 8,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			entry.Children.Add(logCountryCode, 0, 0);
			entry.Children.Add(imgCountryCode, 1, 0);
			entry.Children.Add(mobileNo, 2, 0);


			var imgCountryCodeTap = new TapGestureRecognizer();
			imgCountryCodeTap.Tapped += (s, e) =>
			{
				var ParentPage = (MasterDetailPage)this.Parent;
				ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			};
			imgCountryCode.GestureRecognizers.Add(imgCountryCodeTap);




			mobileNo.Focused += (object sender, FocusEventArgs e) =>
			{
				//mobileNoLbl.FontSize = 10;
				//mobileNoLbl.IsVisible = true;
				//var textValue = mobileNo.Text;
				//if (textValue.Length == 10 || textValue.Length == 0)
				//{
				mobileNoLbl.IsVisible = true;
				mobileNoLbl.TextColor = AppGlobalVariables.fontLessThick;
				mobileNoLbl.Text = "Mobile number";
				//}
				//else {
				//	mobileNoLbl.TextColor = Color.Red;
				//	mobileNoLbl.Text = "Mobile number must be 10 digits";
				//}

				//mobileNoLbl.Text = "Mobile number";
				mobileNo.PlaceholderColor = Color.Transparent;
				//mobileNo.Placeholder = string.Empty;
			};
			mobileNo.Unfocused += (object sender, FocusEventArgs e) =>
			{
				mobileNoLbl.IsVisible = true;

				if (!string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					//passwordLbl.FontSize = 10;


					var textValue = mobileNo.Text;
					if (textValue.Length >= 10 || textValue.Length == 0)
					{
						mobileNoLbl.IsVisible = true;
						mobileNoLbl.TextColor = AppGlobalVariables.fontLessThick;
						mobileNoLbl.Text = "Mobile number";
					}
					else
					{
						mobileNoLbl.IsVisible = true;
						mobileNoLbl.TextColor = Color.Red;
						mobileNoLbl.Text = "Mobile number must be 10 digits";
					}


					mobileNo.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					mobileNoLbl.IsVisible = false;
					mobileNoLbl.Text = "Mobile number";
					mobileNo.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					mobileNo.Placeholder = "Mobile number";
				}
				if (string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					mobileNo.Placeholder = "Mobile number";
				}
			};



			password = new CustomEntry()
			{
				Placeholder = "Password",
				//CustomFontSize = fontsize,//15,
				IsCustomPassword = true,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				IsPassword = true,
				Keyboard = Keyboard.Email,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};

			password.TextChanged += pwdChanged;

			passwordLbl = new CustomLabel()
			{
				Text = "Password",
				//TextColor = AppGlobalVariables.fontLessThick,
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			password.Focused += (object sender, FocusEventArgs e) =>
			{

				//var textValue = password.Text;
				//if (textValue.Length >= 8)
				//{
				//	passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				//	passwordLbl.Text = "Password";
				//}
				//else {
				passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				passwordLbl.Text = "Password";
				//}

				passwordLbl.IsVisible = true;
				password.PlaceholderColor = Color.Transparent;
				//password.Placeholder = string.Empty;
			};
			password.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(password.Text))
				{
					//passwordLbl.FontSize = 10;
					passwordLbl.IsVisible = true;

					var textValue = password.Text;
					if (textValue.Length >= 8 || textValue.Length == 0)
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
						passwordLbl.Text = "Password";
					}
					else
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = Color.Red;
						passwordLbl.Text = "Password must be 8 characters";
					}


					password.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					passwordLbl.IsVisible = false;
					passwordLbl.Text = "Password";
					password.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					password.Placeholder = "Password";
				}
				if (string.IsNullOrWhiteSpace(password.Text))
				{
					password.Placeholder = "Password";
				}
			};




			BoxView mobileNoULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout mobileNoULineStack = new StackLayout()
			{
				Children = { mobileNoULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView passwordULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout passwordULineStack = new StackLayout()
			{
				Children = { passwordULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			//var btnHeight = (height * 7) / 100;

			//HorizontalGradientButton btnLogin = new HorizontalGradientButton()
			//{
			//	Text = "Login",
			//	BorderRadius = 10,
			//	StartColor = AppGlobalVariables.lightMarron,
			//	EndColor = AppGlobalVariables.darkMarron,
			//	FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
			//	HeightRequest = btnHeight,
			//	TextColor = Color.FromHex("#FFFFFF"),
			//	FontFamily = AppGlobalVariables.fontFamily45,
			//	HorizontalOptions = LayoutOptions.Center,
			//	BackgroundColor = Color.Transparent,
			//	WidthRequest = entryWidth,
			//	VerticalOptions = LayoutOptions.End
			//};

			//btnLogin.Clicked += BtnLogin_Clicked;


			CustomLabel lblpwd = new CustomLabel()
			{
				Text = "Password",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.Start,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};
			var lblpwdTap = new TapGestureRecognizer();
			lblpwdTap.Tapped += (s, e) =>
			{
				Application.Current.Properties["Detail"] = "For";
				App.Current.MainPage = new CountryMasterPage();
				//Navigation.PushModalAsync(new ForgotPassword());
			};
			lblpwd.GestureRecognizers.Add(lblpwdTap);

			BoxView lineBox = new BoxView()
			{
				WidthRequest = 1,
				HeightRequest = screenHeight / 29.44,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				BackgroundColor = AppGlobalVariables.underLineColor,
			};

			CustomLabel lblRegister = new CustomLabel()
			{
				Text = "Register",
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				TextColor = AppGlobalVariables.fontLessThick,
				HorizontalOptions = LayoutOptions.EndAndExpand
			};

			var lblRegisterTap = new TapGestureRecognizer();
			lblRegisterTap.Tapped += (s, e) =>
			{
				Application.Current.Properties["Detail"] = "Reg";
				App.Current.MainPage = new CountryMasterPage();

			};
			lblRegister.GestureRecognizers.Add(lblRegisterTap);


			StackLayout stackFooter = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(entryWidth / 4, 0, entryWidth / 4, 0),
				//WidthRequest = (entryWidth * 2) / 2,
				Children = { lblpwd, lineBox, lblRegister }
			};

			HorizontalGradientStack iosstatusbarstack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = Color.FromHex("#4B0000"),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = screenHeight / 28.30,
				IsVisible = Device.OnPlatform(true, false, false)
			};


			Image joboneimg = new Image()
			{
				Source = "imgLogoLoginRegister.png",
				HeightRequest = screenHeight / 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Image btnBackground = new Image()
			{
				Source = "imgLoginButton.png",
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			CustomLabel imgLogin = new CustomLabel()
			{
				Text = "Login",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Grid ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				//BackgroundColor = Color.Blue,
				//RowSpacing = 10,
				//WidthRequest = entryWidth,
				////Padding = new Thickness(20, 20, 10, 0),
				////HorizontalOptions = LayoutOptions.FillAndExpand,
				WidthRequest = entryWidth,
				////Padding = new Thickness(20, 20, 10, 0),
				////HorizontalOptions = LayoutOptions.FillAndExpand,
				////VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);

			TapGestureRecognizer btnLoginTap = new TapGestureRecognizer();
			btnLoginTap.Tapped += BtnLogin_Clicked;

			ImgButton.GestureRecognizers.Add(btnLoginTap);



			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = entryHeight / 10,
				WidthRequest = entryWidth,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			BoxView space = new BoxView()
			{
				Opacity = 0,
				HeightRequest = (entryHeight * 2) / 5,
				WidthRequest = entryHeight,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};

			BoxView space1 = new BoxView()
			{
				Opacity = 0,
				HeightRequest = (entryHeight * 4.8) / 4,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};



			pageBody.Children.Add(mobileNoLbl, 0, 0);
			pageBody.Children.Add(mobileNoULineStack, 0, 0);
			pageBody.Children.Add(mobileNo, 0, 0);

			pageBody.Children.Add(passwordLbl, 0, 1);
			pageBody.Children.Add(passwordULineStack, 0, 1);
			pageBody.Children.Add(password, 0, 1);

			pageBody.Children.Add(space1, 0, 2);
			pageBody.Children.Add(ImgButton, 0, 2);
			pageBody.Children.Add(space, 0, 3);
			pageBody.Children.Add(stackFooter, 0, 4);


			StackLayout bodyHolder = new StackLayout()
			{
				Children = { pageBody },
				Spacing = (entryHeight * 3) / 4,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			//ScrollView scrollHolder = new ScrollView()
			//{
			//	Content = bodyHolder,
			//	//VerticalOptions = LayoutOptions.End

			//};
			BoxView spaceButton = new BoxView()
			{
				Opacity = 0,
				HeightRequest = entryWidth / 7.3,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};

			BoxView imgTopspace = new BoxView()
			{
				Opacity = 0,
				HeightRequest = screenHeight / 13,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};

			StackLayout stackMain = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { iosstatusbarstack, imgTopspace, joboneimg, spaceButton, bodyHolder }
			};


			PageControlsStackLayout.Children.Add(stackMain);

		}


		void mobileNoChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 10)
			{
				mobileNo.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 10)
			{
				mobileNoLbl.TextColor = AppGlobalVariables.fontLessThick;
				mobileNoLbl.Text = "Mobile number";
			}
			else
			{
				mobileNoLbl.TextColor = Color.Red;
				mobileNoLbl.Text = "Mobile number must be 10 digits";
			}
		}

		void pwdChanged(object sender, TextChangedEventArgs e)
		{
			var textValue = password.Text;
			if (textValue.Length >= 8)
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				passwordLbl.Text = "Password";
			}
			else
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = Color.Red;
				passwordLbl.Text = "Password must be 8 characters";
			}

		}

		string data;
		UsersInfo saveUserDeatils;
		private async void BtnLogin_Clicked(object sender, EventArgs e)
		{
			mobileNo.Unfocus();
			password.Unfocus();
			try
			{
				if (string.IsNullOrWhiteSpace(mobileNo.Text) && string.IsNullOrWhiteSpace(password.Text))
				{
					passwordLbl.IsVisible = true;
					mobileNoLbl.IsVisible = true;
					mobileNoLbl.Text = "Please enter mobile number";
					passwordLbl.Text = "Please enter password";
					mobileNoLbl.TextColor = Color.Red;
					passwordLbl.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(mobileNo.Text))
				{

					mobileNoLbl.TextColor = Color.Red;
					mobileNoLbl.Text = "Please enter mobile number";
				}

				else if (mobileNo.Text.Length != 10)
				{
					mobileNoLbl.TextColor = Color.Red;
					mobileNoLbl.Text = "Mobile number must be 10 digits";
				}
				else if (!Regex.IsMatch(mobileNo.Text, @"^([1-9])([0-9]*)$"))
				{
					mobileNoLbl.TextColor = Color.Red;
					mobileNoLbl.Text = "Mobile number must be 10 digits";
				}
				else if (string.IsNullOrWhiteSpace(password.Text))
				{
					passwordLbl.IsVisible = true;
					passwordLbl.TextColor = Color.Red;
					passwordLbl.Text = "Please enter password";
				}
				else
				{
					DBMethods objDB = new DBMethods();

					var userInfo = objDB.GetUserInfo();
					var objDeviceToken = objDB.GetDeviceToken();

					SignInReq objSignInReq = new SignInReq();
					objSignInReq.phone = mobileNo.Text;
					objSignInReq.password = password.Text;
					objSignInReq.device_type = deviceType;
					//objSignInReq.device_info = " ";

					if (objDeviceToken != null)
					{
						objSignInReq.device_info = objDeviceToken.DeviceToken;
						objSignInReq.udid = objDeviceToken.DeviceUDID;
					}
					else
					{
						objSignInReq.udid = "";
					}

					//objSignInReq.udid = " ";
					PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{


						using (ISignInBAL getLoginDetails = new SignInBAL())
						{
							var loginUserCheck = await getLoginDetails.UserSignIn(objSignInReq);
							if (loginUserCheck != null)
							{
								List<SelectedLanguages<Type>> languageResponse = new List<SelectedLanguages<Type>>();
								List<LanguageId> languagesList = new List<LanguageId>();
								if (loginUserCheck.errorCount == "0")
								{
									using (ISelectedLanguaguesBAL languageDetails = new SelectedLanguagesBAL())
									{
										languageResponse = await languageDetails.UserSpeakLanguages();
									}
									foreach (var item in loginUserCheck.responseInfo)
									{
										saveUserDeatils = new UsersInfo
										{
											Address = item.Adddress,
											dateOFBirth = item.dateOfBirth,
											Gender = item.Gender,
											MobileNumber = item.phoneNumber,
											UserFirstName = item.firstName,
											UserLastName = item.lastName,
											UserID = item.UserID,
											LocationName = item.Location,
											CountryCode = item.dailCode,
											UserProfilePic = item.profilePic,
											userRating = item.seekerRating,
											_localLog = item.Longitude.ToString(),
											_localLat = item.Latitude.ToString(),
											SecurityCode = "",
											UserPassword = "",
											jobDescription = " ",
											OptionalMobile = item.optionalPhoneNumber,
											//OptionalCountryCode = item.optionalDialCode ?? "+91"
											OptionalCountryCode = item.optionalDialCode

										};

										var langId = item.languageId;
										var langList = langId.Split(',');
										foreach (var langItems in langList)
										{
											foreach (var items in languageResponse)
											{
												if (langItems == items.ID)
												{
													languagesList.Add(new LanguageId()
													{
														id = items.ID,
														language_code = items.languagesCode,
														language_name = items.LanguageName,
														status = items.Status
													});
												}
											}
											//var dataCollected = 
										}
									}

									//var 
									List<Worktypereq> objworktype = new List<Worktypereq>();
									foreach (var item in loginUserCheck.responseInfo)
									{
										foreach (var items in item.workType)
										{
											objworktype.Add(new Worktypereq() { worktype_id = Convert.ToInt32(items.WorkTypeId), experience = Convert.ToInt32(items.Experience) });
										}
									}

									DBMethods objDatabase = new DBMethods();

									//objDatabase.DeleteUserInfo();
									objDatabase.SaveUserInfo(saveUserDeatils);

									objDatabase.DeleteLanguages();
									objDatabase.SaveUserLanguages(languagesList);

									objDatabase.DeleteWorkType();
									objDatabase.SaveUserWorkType(objworktype);

									//await DisplayAlert("OneJob", "Login successfully.", "Ok");
									App.Current.MainPage = new HomeMasterPage();
									PageLoading.IsVisible = false;
								}
								else
								{
									await DisplayAlert("OneJob", loginUserCheck.successMessage, "Ok");
									//await DisplayAlert("OneJob", "Invalid User!", "Ok");
									PageLoading.IsVisible = false;
								}
							}
							else
							{
								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								PageLoading.IsVisible = false;
							}
						}
					}
					else
					{ 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					}

				}

			}
			catch (Exception ex)
			{

				//App.Current.MainPage = new HomeMasterPage();
				PageLoading.IsVisible = false;
				var msg = ex.Message;

			}
		}
	}
}
