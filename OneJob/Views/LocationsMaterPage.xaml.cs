﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace OneJob
{
	public partial class LocationsMaterPage : MasterDetailPage
	{
		public LocationsMaterPage()
		{
			InitializeComponent();

			this.Icon = null;
			this.Title = "Menu";

			NavigationPage.SetHasNavigationBar(this, false);


			Application.Current.Properties["ParentPage"] = this;
			var detail = Application.Current.Properties["Detail"].ToString();
			Master = new LocationsMenuPage();
			if (detail == "ProfileLocation")
			{
				Detail = new UserProfile() { BackgroundColor = Color.White, };
			}
			else if (detail == "Pro")
			{
				Detail = new UserProfile() { BackgroundColor = Color.White, };
			}

			if (detail == "EditProfileLocation")
			{
				Detail = new EditProfile(null) { BackgroundColor = Color.White, };
			}
			else if (detail == "EditPro")
			{
				Detail = new EditProfile(null) { BackgroundColor = Color.White, };
			}

			else {

			}

		}
	}
}
