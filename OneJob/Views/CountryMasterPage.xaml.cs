﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace OneJob
{
	public partial class CountryMasterPage : MasterDetailPage
	{
		public CountryMasterPage()
		{
			this.Icon = null;
			this.Title = "Menu";

			NavigationPage.SetHasNavigationBar(this, false);


			Application.Current.Properties["ParentPage"] = this;
			var detail = Application.Current.Properties["Detail"].ToString();
			Master = new CountryMenuPage();
			if (detail == "Reg")
			{
				Detail = new UserRegistration() { BackgroundColor = Color.White, };
			}
			else if (detail == "For")
			{
				Detail = new ForgotPassword() { BackgroundColor = Color.White, };
			}
			else if (detail == "Pro")
			{
				Detail = new EditProfile(null) { BackgroundColor = Color.White, };
			}
			else if (detail == "Profile")
			{
				Detail = new UserProfile() { BackgroundColor = Color.White, };
			}
			else
			{
				//Detail = new UserLogin() { BackgroundColor = Color.White, };
			}



		}
	}
}
