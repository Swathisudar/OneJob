﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using XLabs.Platform.Services.Media;
using XLabs.Ioc;
using XLabs.Platform.Device;
using System.IO;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace OneJob
{
	public partial class EditProfile : BaseContentPage
	{

		#region for Declaring User Variables
		public static string updateimg;

		CustomLabel lblDateofBirth, lblFirstName, lblLastName, lblLanguages, entryLanguage, lblLocation;
		CustomDatePicker dateOfBirth;
		CustomEntry entryFirstName, entryLastName, entryMobileNo, entrySecondaryMobileNo;
		public static CustomLabel entryLocation;
		EditorCtrl editorAddress;
		List<LanguageId> languaguesClassData;
		CustomLabel lblCountryCode;
		public static CustomLabel lblCountryCode1;
		ImageCircle loadImage;
		string finalImg;
		Image imgFemale, imgMale;

		public static byte[] finalimg;

		HorizontalGradientButton btnUpdate;
		bool isDateSelected = false, isGenderSelected = false;
		public static EditProfile imgEditProfile;
		public static string str;

		public MediaFile mediaFile { get; set; }
		byte[] resizedImage;
		private readonly TaskScheduler _scheduler = TaskScheduler.FromCurrentSynchronizationContext();

		public static string[] entrydata;


		CustomLabel entityNameLbl, entityTypeLbl, entityStateLbl, regDateLbl, alertDateLbl, alertTimeLbl;
		Dictionary<string, string> statesListData, entityListData;

		#endregion

		//	string profileStr;

		CustomLabel lblMobileNo;
		CustomLabel lblSecondaryMobileNo;
		String genderTest;
		string requiredDate;
		List<int> objLanguages;

		bool testProfile;

		public GoogleSearch Arearesponse;
		public ListView searchlistview;
		public string searchcomplete = null;
		public static LocationRootObject loc;
		public static double _latitude;
		public static double _longitude;

		string objpageTest;
		public EditProfile(string pageTest)
		{
			InitializeComponent();
			imgEditProfile = this;

			AbsoluteLayout ablayout = new AbsoluteLayout();
			//GetProfileData();
			objpageTest = pageTest;

			//DBMethods database = new DBMethods();
			//var getdata = database.GetUserInfo();
			//if (getdata != null)
			//{

			//	updateimg = getdata.UserProfilePic;
			//}


			//profileStr = profileImg;
			BackgroundColor = Color.FromHex("#FFFFFF");

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 7) / 100;


			var entryWidth = (width * 80.5) / 100;


			var entry_Width = (width * 75.36) / 100;
			var headerHeight = (height * 7.5) / 100;

			var topheaderHeight = (height * 9) / 100;
			var fontsize = Convert.ToSingle((height * 2.4647) / 100);


			#region Page header
			DBMethods objDatabase = new DBMethods();
			var getLocalDB = objDatabase.GetUserInfo();

			var image_Height = (screenHeight * 6) / 100;
			var imaPicHeight = screenHeight / 6;

			Image menuImage = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				HeightRequest = image_Height,
				WidthRequest = image_Height,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End,
				Margin = new Thickness(entryWidth / 34, 0, 0, entryWidth / 45)
			};
			CustomLabel headerTitle = new CustomLabel()
			{
				Margin = new Thickness(0, 0, 0, entryWidth / 19),
				Text = "Edit Profile",
				//TextColor = AppGlobalVariables.lightGray,
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.End
			};

			Image cameraPic = new Image()
			{
				Source = ImageSource.FromFile("cameraIcon.png")
			};


			Image rgightImg = new Image()
			{
				Source = ImageSource.FromFile("imgDone.png"),
				HeightRequest = image_Height,
				WidthRequest = image_Height,
				HorizontalOptions = LayoutOptions.End
			};



			TapGestureRecognizer menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {
			   App.Current.MainPage = new HomeMasterPage();
			   //Navigation.PopModalAsync(false);
		   };
			menuImage.GestureRecognizers.Add(menuClicked);



			loadImage = new ImageCircle()
			{
				HeightRequest = imaPicHeight,
				WidthRequest = imaPicHeight,
				//Source = ImageSource.FromFile("Avatar.png"),
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			//if (profileStr != null)
			//{
			//	loadImage.Source = new UriImageSource()
			//	{
			//		Uri = new Uri(profileStr),
			//		CachingEnabled = false
			//	};
			//}
			//else {
			//	loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			//}
			//if (getLocalDB != null)
			//{

			//	if (getLocalDB.UserProfilePic != null && getLocalDB.UserProfilePic != "")
			//	{
			//		loadImage.Source = new UriImageSource()
			//		{
			//			Uri = new Uri(getLocalDB.UserProfilePic),
			//			CachingEnabled = false
			//		};
			//	}
			//	else
			//	{
			//		loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			//	}
			//}
			//else
			//{
			//	loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			//}


			//TapGestureRecognizer loadImageTap = new TapGestureRecognizer();
			//loadImageTap.NumberOfTapsRequired = 1;

			var tgimgProfile = new TapGestureRecognizer { };
			tgimgProfile.Tapped += async (sender, args) =>
			{
				try
				{
					IList<String> buttons = new List<String>();
					buttons.Add(ChooseImageFrom.Gallery.ToString());
					buttons.Add(ChooseImageFrom.Camera.ToString());

					var action = await DisplayActionSheet("Choose photo from", "Cancel", null, buttons.ToArray());
					var _mediaPicker = DependencyService.Get<IMediaPicker>() ?? Resolver.Resolve<IDevice>().MediaPicker;

					if (action == ChooseImageFrom.Gallery.ToString())
					{
						if (Device.OS == TargetPlatform.Android)
						{
							Application.Current.Properties["startimg"] = "Gallery";
							DependencyService.Get<ICrop>().GetPhotoEditProfile(null, null, imgEditProfile, "editProfile");
						}
						else
						{
							mediaFile = await _mediaPicker.SelectPhotoAsync(new CameraMediaStorageOptions
							{
								DefaultCamera = XLabs.Platform.Services.Media.CameraDevice.Front,
								MaxPixelDimension = 350
							});
							byte[] imageData;
							using (var streamReader = new MemoryStream())
							{
								mediaFile.Source.CopyTo(streamReader);
								imageData = streamReader.ToArray();
								str = mediaFile.Path.ToString();
							}
							resizedImage = DependencyService.Get<ImageResizer>().ResizeImage(imageData, 250, 250);
							DependencyService.Get<ICrop>().GetPhotoEditProfile(resizedImage, str, imgEditProfile, "editProfile");
							str = Convert.ToBase64String(resizedImage);
							//imgProfile.Source = ImageSource.FromStream (() => new MemoryStream (resizedImage));
						}
					}
					else if (action == ChooseImageFrom.Camera.ToString())
					{
						if (Device.OS == TargetPlatform.Android)
						{
							Application.Current.Properties["startimg"] = "Camera";
							DependencyService.Get<ICrop>().GetPhotoEditProfile(null, null, imgEditProfile, "editProfile");
						}
						else
						{
							await _mediaPicker.TakePhotoAsync(new CameraMediaStorageOptions
							{
								DefaultCamera = XLabs.Platform.Services.Media.CameraDevice.Front,
								MaxPixelDimension = 250
							}).ContinueWith(t =>
							{
								if (t.IsFaulted)
								{
									var s = t.Exception.InnerException.ToString();
								}
								else if (t.IsCanceled)
								{
									var canceled = true;
								}
								else
								{

									mediaFile = t.Result;

									byte[] imageData;
									using (var streamReader = new MemoryStream())
									{
										mediaFile.Source.CopyTo(streamReader);
										imageData = streamReader.ToArray();
										str = mediaFile.Path.ToString();
									}
									Application.Current.Properties["startimg"] = str;

									resizedImage = DependencyService.Get<ImageResizer>().ResizeImage(imageData, 250, 250);
									DependencyService.Get<ICrop>().GetPhotoEditProfile(resizedImage, str, imgEditProfile, "editProfile");
									str = Convert.ToBase64String(resizedImage);
									//imgProfile.Source = ImageSource.FromStream (() => new MemoryStream (resizedImage));
									return mediaFile;
								}
								return null;
							}, _scheduler);

						}
					}
				}
				catch (TaskCanceledException ex)
				{
				}
				catch (Exception ex)
				{

					throw;
				}
			};
			loadImage.GestureRecognizers.Add(tgimgProfile);
			cameraPic.GestureRecognizers.Add(tgimgProfile);



			AbsoluteLayout imageAbsLayout = new AbsoluteLayout()
			{
				//WidthRequest = width / 4,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(loadImage, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(loadImage, AbsoluteLayoutFlags.All);
			imageAbsLayout.Children.Add(loadImage);

			AbsoluteLayout.SetLayoutBounds(cameraPic, new Rectangle(1.4, 1, 0.5, 0.5));
			AbsoluteLayout.SetLayoutFlags(cameraPic, AbsoluteLayoutFlags.All);
			imageAbsLayout.Children.Add(cameraPic);

			HorizontalGradientStack imgback = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { menuImage, headerTitle, rgightImg },
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(1, 10, 10, 0),
				BackgroundColor = Color.FromHex("#707070"),
				HeightRequest = topheaderHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};


			HorizontalGradientStack stackLogo = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { imageAbsLayout },
				//Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Padding = new Thickness(1, 0, 10, 0),
				BackgroundColor = Color.FromHex("#707070"),
				HeightRequest = headerHeight * 2.6,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};


			entryFirstName = new CustomEntry()
			{
				Placeholder = "First name",
				//Text = getLocalDB.UserFirstName,
				//CustomFontSize = fontsize,//15,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};
			lblFirstName = new CustomLabel()
			{
				Text = "First name",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			entryFirstName.Focused += (object sender, FocusEventArgs e) =>
			{

				lblFirstName.IsVisible = true;
				entryFirstName.PlaceholderColor = Color.Transparent;
				//name.Placeholder = string.Empty;
			};
			entryFirstName.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entryFirstName.Text))
				{
					lblFirstName.Text = "First name";
					lblFirstName.IsVisible = true;
					lblFirstName.TextColor = AppGlobalVariables.fontLessThick;
					entryFirstName.Placeholder = "First name";
				}
				else
				{

					entryFirstName.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entryFirstName.Placeholder = "First name";
					lblFirstName.Text = "Enter first name";
					lblFirstName.TextColor = Color.Red;
					lblFirstName.IsVisible = true;
				}

			};


			entryLastName = new CustomEntry()
			{
				Placeholder = "Last name",
				//Text = getLocalDB.UserLastName,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Text,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			lblLastName = new CustomLabel()
			{
				Text = "Last name",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			entryLastName.Focused += (object sender, FocusEventArgs e) =>
			{

				lblLastName.IsVisible = true;
				entryLastName.PlaceholderColor = Color.Transparent;
			};

			entryLastName.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entryLastName.Text))
				{
					lblLastName.Text = "Last name";
					lblLastName.IsVisible = true;
					lblLastName.TextColor = AppGlobalVariables.fontLessThick;
					entryLastName.Placeholder = "Last name";

				}
				else
				{

					entryLastName.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entryLastName.Placeholder = "Last name";
					lblLastName.Text = "Enter last name";
					lblLastName.TextColor = Color.Red;
					lblLastName.IsVisible = true;

				}

			};


			dateOfBirth = new CustomDatePicker()
			{
				//EnterText = getLocalDB.dateOFBirth,
				Date = DateTime.Now.Date,
				CustomFontSize = fontsize,//15,
				CustomFontFamily = "Avenir45",
				Format = "MM-dd-yyyy",
				//MaximumDate = DateTime.Now,
				//MinimumDate = DateTime.Now,
				MaximumDate = DateTime.Now,
				TextColor = AppGlobalVariables.fontVeryThick,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,//entryWidth,
										  //BackgroundColor = Color.Green,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};
			dateOfBirth.DateSelected += (object sender, DateChangedEventArgs e) =>
			{
				isDateSelected = true;
				var selectedDate = dateOfBirth.Date;
				requiredDate = selectedDate.Year.ToString() + "-" + selectedDate.Month.ToString() + "-" + selectedDate.Day.ToString();
				//requiredDate = selectedDate.Month.ToString() + "-" + selectedDate.Day.ToString() + "-" + selectedDate.Year.ToString();
			};
			lblDateofBirth = new CustomLabel()
			{
				Text = "Date of birth",
				TextColor = AppGlobalVariables.fontLessThick,
				//FontSize = fontsize2,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				//IsVisible = false,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start
			};
			dateOfBirth.Focused += (sender, e) =>
			{

			};
			dateOfBirth.Unfocused += (object sender, FocusEventArgs e) =>
			{

			};


			CustomLabel lblGender = new CustomLabel()
			{
				HorizontalOptions = LayoutOptions.Start,
				Text = "Gender:",
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				VerticalTextAlignment = TextAlignment.Center,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};


			CustomLabel lblMale = new CustomLabel()
			{
				Text = "Male",
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalTextAlignment = TextAlignment.Center,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};


			imgMale = new Image()
			{
				Source = "imgGenderNone.png",
				HeightRequest = image_Height,
				WidthRequest = image_Height
			};

			StackLayout stackMale = new StackLayout()
			{
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Children = { imgMale, lblMale }
			};


			CustomLabel lblFemale = new CustomLabel()
			{
				Text = "Female",
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};

			imgFemale = new Image()
			{
				Source = "imgGenderNone.png",
				HeightRequest = image_Height,
				WidthRequest = image_Height
			};

			var imgMaleTap = new TapGestureRecognizer();
			imgMaleTap.Tapped += (s, e) =>
			{
				imgFemale.Source = "imgGenderNone.png";
				imgMale.Source = "imgGenderDone.png";
				genderTest = "Male";
				isGenderSelected = true;
			};

			imgMale.GestureRecognizers.Add(imgMaleTap);


			var imgFemaleTap = new TapGestureRecognizer();
			imgFemaleTap.Tapped += (s, e) =>
			{
				imgMale.Source = "imgGenderNone.png";
				imgFemale.Source = "imgGenderDone.png";
				genderTest = "Female";
				isGenderSelected = true;
			};

			imgFemale.GestureRecognizers.Add(imgFemaleTap);

			StackLayout stackFemale = new StackLayout()
			{
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = { imgFemale, lblFemale },
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};

			StackLayout stackGender = new StackLayout()
			{
				Spacing = 10,
				WidthRequest = entryWidth,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Children = { lblGender, stackMale, stackFemale }
			};

			lblCountryCode = new CustomLabel()
			{
				//Text = "+91",
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				//Margin = new Thickness((entryWidth) / 8, 0, 0, 0),
				VerticalTextAlignment = TextAlignment.Center
			};

			if (getLocalDB != null)
			{
				lblCountryCode.Text = getLocalDB.CountryCode;
			}



			entryMobileNo = new CustomEntry()
			{
				//Text = getLocalDB.MobileNumber,
				//Text = getLocalDB.optionalMobileNumber,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.lightGray,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Numeric,
				HeightRequest = entryHeight,
				IsEnabled = false,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			//entryMobileNo.TextChanged += mobileNoChanged;

			lblMobileNo = new CustomLabel()
			{
				Text = "Primary mobile number",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			//entryMobileNo.TextChanged += mobileNoChanged;
			entryMobileNo.Focused += (object sender, FocusEventArgs e) =>
			{
				//mobileNoLbl.FontSize = 10;
				lblMobileNo.IsVisible = true;
				entryMobileNo.PlaceholderColor = Color.Transparent;
				//mobileNo.Placeholder = string.Empty;
			};
			entryMobileNo.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entryMobileNo.Text))
				{
					//mobileNoLbl.FontSize = 10;
					lblMobileNo.IsVisible = true;
					entryMobileNo.PlaceholderColor = Color.Transparent;
					//mobileNo.Placeholder = string.Empty;
				}
				else
				{
					//mobileNoLbl.FontSize = 15;
					lblMobileNo.IsVisible = false;
					entryMobileNo.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entryMobileNo.Placeholder = "Primary mobile number";
				}
				if (string.IsNullOrWhiteSpace(entryMobileNo.Text))
				{
					entryMobileNo.Placeholder = "Primary mobile number";
				}
			};


			Image imgCountryCode = new Image()
			{
				Source = "imgPicker.png"
			};


			Grid entry = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = entryWidth,
				ColumnSpacing = 8,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			entry.Children.Add(lblCountryCode, 0, 0);
			entry.Children.Add(imgCountryCode, 1, 0);
			entry.Children.Add(entryMobileNo, 2, 0);


			entrySecondaryMobileNo = new CustomEntry()
			{
				Placeholder = "Secondary mobile number",
				//Text = getLocalDB.optionalMobileNumber,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Numeric,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			lblSecondaryMobileNo = new CustomLabel()
			{
				Text = "Secondary mobile number",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};

			entrySecondaryMobileNo.TextChanged += mobileSecNoChanged;

			//entryMobileNo.TextChanged += mobileNoChanged;
			entrySecondaryMobileNo.Focused += (object sender, FocusEventArgs e) =>
			{
				//mobileNoLbl.FontSize = 10;
				lblSecondaryMobileNo.IsVisible = true;
				entryMobileNo.PlaceholderColor = Color.Transparent;
				//mobileNo.Placeholder = string.Empty;
			};
			entrySecondaryMobileNo.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entrySecondaryMobileNo.Text))
				{
					//mobileNoLbl.FontSize = 10;
					lblSecondaryMobileNo.IsVisible = true;
					entrySecondaryMobileNo.PlaceholderColor = Color.Transparent;
					//mobileNo.Placeholder = string.Empty;
				}
				else
				{
					//mobileNoLbl.FontSize = 15;
					lblSecondaryMobileNo.IsVisible = false;
					entrySecondaryMobileNo.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entrySecondaryMobileNo.Placeholder = "Secondary mobile number";
				}
				if (string.IsNullOrWhiteSpace(entrySecondaryMobileNo.Text))
				{
					entrySecondaryMobileNo.Placeholder = "Secondary mobile number";
				}
			};



			Image imgCuntryCode1 = new Image()
			{
				Source = "imgPicker.png"
			};

			lblCountryCode1 = new CustomLabel()
			{
				Text = "+91",
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				//Margin = new Thickness((entryWidth) / 8, 0, 0, 0),
				VerticalTextAlignment = TextAlignment.Center
			};

			//var Ccdoe = Application.Current.Properties["SelectedCountry"];
			//if (Ccdoe == null)
			//{
			//	lblCountryCode1.Text = "+91";
			//}
			//else
			//{
			//	lblCountryCode1.Text = "+" + Ccdoe.ToString();
			//}
			//if (getLocalDB != null)
			//{
			//	if (getLocalDB.OptionalCountryCode != "" && getLocalDB.OptionalCountryCode != null)
			//	{
			//		lblCountryCode1.Text = getLocalDB.OptionalCountryCode;
			//	}
			//	else
			//	{
			//		lblCountryCode1.Text = "+91";
			//	}

			//}
			//else
			//{
			//	lblCountryCode1.Text = "+91";
			//}

			Grid entrySecondaryFrid = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = entryWidth,
				ColumnSpacing = 8,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			entrySecondaryFrid.Children.Add(lblCountryCode1, 0, 0);
			entrySecondaryFrid.Children.Add(imgCuntryCode1, 1, 0);
			entrySecondaryFrid.Children.Add(entrySecondaryMobileNo, 2, 0);




			var taplblCountryCode1 = new TapGestureRecognizer();
			taplblCountryCode1.Tapped += (s, e) =>
			{
				try
				{
					MessagingCenter.Send<EditProfile, string>(this, "Code", "userProfile");
					Application.Current.Properties["Detail"] = "EditPro";
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{

				}
			};
			lblCountryCode1.GestureRecognizers.Add(taplblCountryCode1);


			var tapimgCuntryCode1 = new TapGestureRecognizer();
			tapimgCuntryCode1.Tapped += (s, e) =>
			{
				try
				{
					MessagingCenter.Send<EditProfile, string>(this, "Code", "userProfile");
					Application.Current.Properties["Detail"] = "EditPro";
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{

				}

			};
			imgCuntryCode1.GestureRecognizers.Add(tapimgCuntryCode1);


			//string landata = "";
			//var getLanDB = objDatabase.GetLaguages();
			//foreach (var item in getLanDB)
			//{
			//	if (landata == "")
			//	{
			//		landata = item.LanguageName;
			//	}
			//	else
			//	{
			//		landata = landata + "," + item.LanguageName;
			//	}
			//}
			entryLanguage = new CustomLabel()
			{

				//Text = "Languagues you speak",
				//Text = landata + ".",
				//CustomFontSize = fontsize,//15,
				TextColor = AppGlobalVariables.fontVeryThick,
				//PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HeightRequest = (entryHeight * 2) / 2.6,
				WidthRequest = entryWidth,
				LineBreakMode = LineBreakMode.NoWrap,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};
			//ScrollView entryLanguagesScroll = new ScrollView()
			//{
			//	Content = entryLanguage
			//};
			languaguesClassData = new List<LanguageId>();
			objLanguages = new List<int>();

			//MessagingCenter.Subscribe<RightSideMasterPage, int>(this, "LanguagesId", (s, arg) =>
			//{
			//	entryLanguage.Text = string.Empty;
			//	if (arg == 0)
			//	{
			//		entryLanguage.Text = "Languagues you speak";
			//	}
			//	else if (entryLanguage.Text != "Languagues you speak")
			//	{
			//		objLanguages.Add(arg);

			//	}
			//	else
			//	{
			//		entryLanguage.Text = arg + ".";
			//	}
			//});

			MessagingCenter.Subscribe<RightSideMasterPage, string>(this, "Hi", async (s, arg) =>
			{
				if (arg != null)
				{
					await UpdateLanguageData();
				}

			});

			//MessagingCenter.Subscribe<RightSideMasterPage, string>(this, "LanguagesData", (s, e) =>
			//{
			//	entryLanguage.Text = string.Empty;
			//	if (e == null)
			//	{
			//		entryLanguage.Text = "Languagues you speak";
			//	}
			//	else if (entryLanguage.Text != "Languagues you speak")
			//	{

			//		lblLanguages.IsVisible = true;
			//		entryLanguage.Text = e + ".";
			//	}
			//	else
			//	{
			//		entryLanguage.Text = e + ".";
			//	}

			//});

			lblLanguages = new CustomLabel()
			{
				Text = "Languagues you speak",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				LineBreakMode = LineBreakMode.NoWrap,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};


			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				this.ShowMenu();
			};
			entryLanguage.GestureRecognizers.Add(tapGestureRecognizer);

			this.SlideMenu = new RightSideMasterPage();


			entryLocation = new CustomLabel()
			{
				Text = "Location",
				//CustomFontSize = fontsize,//15,
				TextColor = AppGlobalVariables.fontVeryThick,
				//PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HeightRequest = (entryHeight * 2) / 2.6,
				WidthRequest = entryWidth,
				LineBreakMode = LineBreakMode.NoWrap,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
				//BackgroundColor = Color.Red
			};

			var tapGestureRecognizerLocation = new TapGestureRecognizer();
			tapGestureRecognizerLocation.Tapped += (s, e) =>
			{
				try
				{
					MessagingCenter.Send<EditProfile, string>(this, "Location", "userProfile");

					Application.Current.Properties["Detail"] = "EditProfileLocation";
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			entryLocation.GestureRecognizers.Add(tapGestureRecognizerLocation);

			//if (getLocalDB != null)
			//{
			//	entryLocation.Text = getLocalDB.LocationName;
			//	//entryLocation.Placeholder = getLocalDB.LocationName;
			//}
			//else
			//{
			//	//entryLocation.Placeholder = "Location";
			//}

			lblLocation = new CustomLabel()
			{
				Text = "Location",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};



			entryLocation.Focused += (object sender, FocusEventArgs e) =>
			{
				lblLocation.IsVisible = true;
				//entryLocation.PlaceholderColor = Color.Transparent;
			};


			int rowheight1 = Convert.ToInt16(screenHeight / 14.72);




			entryLocation.Unfocused += (object sender, FocusEventArgs e) =>
			{

				if (!string.IsNullOrWhiteSpace(entryLocation.Text))
				{
					lblLocation.Text = "Location";
					lblLocation.IsVisible = true;
					lblLocation.TextColor = AppGlobalVariables.fontLessThick;
					//entryLocation.Placeholder = "Location";
				}
				else
				{
					//entryLocation.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					//entryLocation.Placeholder = "Location";
					lblLocation.Text = "Please select location";
					lblLocation.TextColor = Color.Red;
					lblLocation.IsVisible = true;
				}

			};







			CustomLabel lbladdress = new CustomLabel()
			{
				Text = "Contact address",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//HeightRequest = entryHeight/2,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};

			editorAddress = new EditorCtrl()
			{
				BackgroundColor = Color.Transparent,
				HeightRequest = (entryHeight * 3.7) / 2,
				TextColor = AppGlobalVariables.fontVeryThick,
				//Text = getLocalDB.Address,
			};

			StackLayout stackAddress = new StackLayout()
			{
				Spacing = 10,
				Children = { lbladdress, editorAddress }
			};

			Image btnBackground = new Image()
			{
				Source = "imgLoginButton.png",
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				//Scale = 1.1,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				btnBackground.Scale = 1.1;
			}
			else
			{
				btnBackground.Scale = 1.17;
			}

			CustomLabel imgLogin = new CustomLabel()
			{
				Text = "Update",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Grid ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				WidthRequest = entryWidth,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);

			TapGestureRecognizer btnLoginTap = new TapGestureRecognizer();
			btnLoginTap.Tapped += btnUpdate_Clicked;

			ImgButton.GestureRecognizers.Add(btnLoginTap);

			TapGestureRecognizer imgDoneTap = new TapGestureRecognizer();
			imgDoneTap.Tapped += btnUpdate_Clicked;

			rgightImg.GestureRecognizers.Add(imgDoneTap);

			GetProfileData();

			BoxView FirstNameULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout FirstNameULineStack = new StackLayout()
			{
				Children = { FirstNameULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView LastNameULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout LastNameULineStack = new StackLayout()
			{
				Children = { LastNameULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView dateULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};

			StackLayout dateULineStack = new StackLayout()
			{
				Children = { dateULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView mobileNoULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout mobileNoULineStack = new StackLayout()
			{
				Children = { mobileNoULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView secmobileNoULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout secmobileNoULineStack = new StackLayout()
			{
				Children = { secmobileNoULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView LanguagesULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout LanguagesULineStack = new StackLayout()
			{
				Children = { LanguagesULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView LocationULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout LocationULineStack = new StackLayout()
			{
				Children = { LocationULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView space = new BoxView()
			{
				Opacity = 0,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};

			BoxView space1 = new BoxView()
			{
				Opacity = 0,
				HeightRequest = 5,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};
			Grid nameGrid = new Grid()
			{
				ColumnSpacing = 5,
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				}
			};

			nameGrid.Children.Add(lblFirstName, 0, 0);
			nameGrid.Children.Add(FirstNameULineStack, 0, 0);
			nameGrid.Children.Add(entryFirstName, 0, 0);

			nameGrid.Children.Add(lblLastName, 1, 0);
			nameGrid.Children.Add(LastNameULineStack, 1, 0);
			nameGrid.Children.Add(entryLastName, 1, 0);


			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = 5,
				Padding = new Thickness(entry_Width / 8, 0, entry_Width / 8, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			BoxView spaceFooter = new BoxView()
			{
				HeightRequest = 20,
				BackgroundColor = Color.Transparent
			};

			BoxView spaceFooter1 = new BoxView()
			{
				HeightRequest = (entryHeight) / 20,
				BackgroundColor = Color.Transparent
			};

			pageBody.Children.Add(nameGrid, 0, 0);

			pageBody.Children.Add(lblDateofBirth, 0, 1);
			pageBody.Children.Add(dateULineStack, 0, 1);
			pageBody.Children.Add(dateOfBirth, 0, 1);

			pageBody.Children.Add(space, 0, 2);
			pageBody.Children.Add(stackGender, 0, 2);

			pageBody.Children.Add(lblMobileNo, 0, 3);
			pageBody.Children.Add(mobileNoULineStack, 0, 3);
			pageBody.Children.Add(entry, 0, 3);

			pageBody.Children.Add(lblSecondaryMobileNo, 0, 4);
			pageBody.Children.Add(secmobileNoULineStack, 0, 4);
			pageBody.Children.Add(entrySecondaryFrid, 0, 4);


			pageBody.Children.Add(lblLanguages, 0, 5);
			pageBody.Children.Add(LanguagesULineStack, 0, 5);
			pageBody.Children.Add(entryLanguage, 0, 5);

			pageBody.Children.Add(lblLocation, 0, 6);
			pageBody.Children.Add(LocationULineStack, 0, 6);
			pageBody.Children.Add(entryLocation, 0, 6);

			pageBody.Children.Add(space1, 0, 7);
			pageBody.Children.Add(stackAddress, 0, 8);

			pageBody.Children.Add(spaceFooter, 0, 9);

			pageBody.Children.Add(ImgButton, 0, 10);

			pageBody.Children.Add(spaceFooter1, 0, 11);

			/*
			if (getLocalDB != null)
			{
				string landata = "";
				var getLanDB = objDatabase.GetLaguages();
				if (getLanDB != null)
				{
					foreach (var item in getLanDB)
					{
						if (landata == "")
						{
							landata = item.LanguageName;
						}
						else
						{
							landata = landata + "," + item.LanguageName;
						}
					}
					lblLanguages.IsVisible = true;
					//entryLanguage.Text = landata.Remove(landata.Length - 1) + ".";
					if (landata.Length > 1)
					{
						entryLanguage.Text = landata + ".";
					}
				}
				entryFirstName.Text = getLocalDB.UserFirstName;
				entryLastName.Text = getLocalDB.UserLastName;
				dateOfBirth.EnterText = getLocalDB.dateOFBirth;
				entryMobileNo.Text = getLocalDB.MobileNumber;


				if (getLocalDB.Gender == "Male" || getLocalDB.Gender == "male")
				{
					imgMale.Source = "imgGenderDone.png";
					imgFemale.Source = "imgGenderNone.png";

				}
				else
				{
					imgMale.Source = "imgGenderNone.png";
					imgFemale.Source = "imgGenderDone.png";
				}
				entrySecondaryMobileNo.Text = getLocalDB.OptionalMobile;
				if (!string.IsNullOrEmpty(entrySecondaryMobileNo.Text))
				{
					lblSecondaryMobileNo.IsVisible = true;
				}
				//entryLocation.Text = getLocalDB.;
				editorAddress.Text = getLocalDB.Address;
				lblFirstName.IsVisible = true;
				lblLastName.IsVisible = true;
				lblMobileNo.IsVisible = true;
				lblLocation.IsVisible = true;
			}
			else
			{

				//entryFirstName.Text = "";
				//entryLastName.Text = "";
				//dateOfBirth.EnterText = "";
				imgMale.Source = "imgGenderNone.png";
				imgFemale.Source = "imgGenderNone.png";
				//entrySecondaryMobileNo.Text = "";
				entryLanguage.Text = "Languagues you speak";
				//entryLocation.Text = "";
				//editorAddress.Text = "";
			}
			*/

			#endregion

			StackLayout bodyHolder = new StackLayout()
			{
				Children = { stackLogo, pageBody },
				Spacing = entryHeight / 4,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			ScrollView scrollHolder = new ScrollView()
			{
				Content = bodyHolder
			};


			StackLayout holder = new StackLayout()
			{
				Children = { imgback, scrollHolder },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			AbsoluteLayout.SetLayoutBounds(holder, new Rectangle(1, 1, 1, 1));
			AbsoluteLayout.SetLayoutFlags(holder, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(holder);

			//AbsoluteLayout.SetLayoutBounds(fra, new Rectangle(0.5, Device.OnPlatform(0.4, 0.4, 0.188), 0.83, 0.3));
			//AbsoluteLayout.SetLayoutFlags(fra, AbsoluteLayoutFlags.All);
			//ablayout.Children.Add(fra);

			PageControlsStackLayout.Children.Add(ablayout);

		}

		public void FinalEditImg(string filepath)
		{
			string finalImg = filepath;

			if (finalImg != null)
			{
				loadImage.Source = new UriImageSource()
				{
					Uri = new Uri(finalImg),
					CachingEnabled = false
				};
			}
			else
			{
				loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			}
		}

		public void FinalUpdateImg(byte[] filepath)
		{
			finalimg = filepath;

			if (finalimg != null)
			{
				String androidImg = Convert.ToBase64String(finalimg);
				loadImage.Source = ImageSource.FromStream(() => new MemoryStream(finalimg));
			}
			else
			{
				loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			}
		}

		async Task<bool> UpdateLanguageData()
		{
			try
			{
				languaguesClassData.Clear();
				objLanguages.Clear();
				string langName = null;
				var joggedData = RightSideMasterPage.rsmp.LanguagesList;

				if (joggedData.Count == 0)
				{
					entryLanguage.Text = "Languagues you speak";
				}
				else
				{
					if (joggedData.Count == 1)
					{
						langName = joggedData[0].LanguageName + "," + ",";
						objLanguages.Add(Convert.ToInt32(joggedData[0].ID));
						languaguesClassData.Add(new LanguageId() { id = joggedData[0].ID, language_name = joggedData[0].LanguageName });
					}
					else
					{
						foreach (var item in joggedData)
						{
							langName = langName + item.LanguageName + ", ";
							objLanguages.Add(Convert.ToInt32(item.ID));
							languaguesClassData.Add(new LanguageId() { id = item.ID, language_name = item.LanguageName });
						}
					}
					var myString = "";
					if (langName.Length > 1)
					{
						myString = langName.Remove(langName.Length - 2) + ".";
					}
					entryLanguage.Text = myString;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
			return true;
		}

		void mobileNoChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 10)
			{
				entryMobileNo.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 10)
			{
				lblMobileNo.TextColor = AppGlobalVariables.fontLessThick;
				lblMobileNo.Text = "Mobile number";
			}
			else
			{
				lblMobileNo.TextColor = Color.Red;
				lblMobileNo.Text = "Mobile number must be 10 digits";
			}
		}

		void mobileSecNoChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 10)
			{
				entrySecondaryMobileNo.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 10)
			{
				lblSecondaryMobileNo.TextColor = AppGlobalVariables.fontLessThick;
				lblSecondaryMobileNo.Text = "Mobile number";
			}
			else
			{
				lblSecondaryMobileNo.TextColor = Color.Red;
				lblSecondaryMobileNo.Text = "Mobile number must be 10 digits";
			}
		}

		ProfileImg uploadcheck;

		#region Profile image service
		public async void UpdateProfileService()
		{
			try
			{

				DBMethods objDatabase = new DBMethods();
				var getLocalDB = objDatabase.GetUserInfo();

				ProfileImgReq ProfileImgReqObj = new ProfileImgReq();
				if (getLocalDB != null)
				{
					ProfileImgReqObj.seeker_id = getLocalDB.UserID;
				}

				if (finalimg != null)
				{
					String androidImg = Convert.ToBase64String(finalimg);

					ProfileImgReqObj.seeker_image = androidImg;

				}
				else
				{
					ProfileImgReqObj.seeker_image = str;
				}

				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{
					using (IProfileImgBAL profiledetails = new ProfileImgBAL())
					{
						testProfile = true;
						uploadcheck = await profiledetails.UserProfile(ProfileImgReqObj);


						//UsersInfo saveUserDeatils;
						//DBMethods objDatabase = new DBMethods();
						//var getLocalDB = objDatabase.GetUserInfo();

						if (uploadcheck != null)
						{
							if (uploadcheck.Status == 0)
							{
								#region Save UserInfo
								testProfile = true;
								var saveUserDeatils = new UsersInfo
								{
									UserProfilePic = uploadcheck.ProfileURL,

									Address = getLocalDB.Address,
									dateOFBirth = getLocalDB.dateOFBirth,
									Gender = getLocalDB.Gender,
									//jobDescription = getLocalDB.jobDescription,
									LocationName = getLocalDB.LocationName,
									//languageId = getLocalDB.languagesId,
									OptionalMobile = getLocalDB.OptionalMobile,
									UserFirstName = getLocalDB.UserFirstName,
									UserLastName = getLocalDB.UserLastName,
									UserID = getLocalDB.UserID,
									CountryCode = getLocalDB.CountryCode,
									SecurityCode = "",
									UserPassword = "",
									jobDescription = " ",
									userRating = getLocalDB.userRating,
									MobileNumber = getLocalDB.MobileNumber,
									OptionalCountryCode = lblCountryCode1.Text ?? getLocalDB.OptionalCountryCode

								};
								objDatabase.DeleteUserInfo();
								objDatabase.SaveUserInfo(saveUserDeatils);
								var data = objDatabase.GetUserInfo();
								#endregion

								PageLoading.IsVisible = false;

							}
							else
							{
								testProfile = false;
								await DisplayAlert("OneJob", uploadcheck.Message, "Ok");
								PageLoading.IsVisible = false;

							}
						}

						else
						{
							testProfile = false;
							//await DisplayAlert("OneJob", "Upload Picture failed try again !", "Ok");
							await Navigation.PopModalAsync();
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}
					}
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{

				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}

		}
		#endregion

		#region GetProfileData from Service
		public async void GetProfileData()
		{
			try
			{
				DBMethods objDB = new DBMethods();
				var getLocalDB = objDB.GetUserInfo();

				GetProfileReq objGetProfioeReq = new GetProfileReq();
				if (getLocalDB != null)
				{
					objGetProfioeReq.seeker_id = getLocalDB.UserID;
				}


				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{
					using (IGetProfileBAL selectWorkTypeList = new GetProfileBAL())
					{
						var objGetProfile = await selectWorkTypeList.GetProfileData(objGetProfioeReq);
						if (objGetProfile != null)
						{
							if (objGetProfile.errorCount == 0)
							{
								PageLoading.IsVisible = false;
								entryFirstName.Text = objGetProfile.userDetails.firstName;
								lblFirstName.IsVisible = true;
								entryLastName.Text = objGetProfile.userDetails.lastName;
								lblLastName.IsVisible = true;
								entryMobileNo.Text = objGetProfile.userDetails.primaryPhone;
								lblMobileNo.IsVisible = true;
								entrySecondaryMobileNo.Text = objGetProfile.userDetails.optionalPhone;
								lblSecondaryMobileNo.IsVisible = true;
								var dob = objGetProfile.userDetails.dateOfBirth.Split('-');
								entryLocation.Text = objGetProfile.userDetails.Location;
								lblLocation.IsVisible = true;
								editorAddress.Text = objGetProfile.userDetails.Address;
								//lblCountryCode.Text = objGetProfile.userDetails.optionalCoutryCode;
								//lblCountryCode1.Text = objGetProfile.userDetails.optionalCoutryCode;
								if (objGetProfile.userDetails.optionalPhone != "" && objGetProfile.userDetails.optionalPhone != null)
								{
									entrySecondaryMobileNo.Text = objGetProfile.userDetails.optionalPhone;
									lblSecondaryMobileNo.IsVisible = true;
								}
								else
								{
									entrySecondaryMobileNo.Placeholder = "Secondary mobile number";
								}
								if (objGetProfile.userDetails.optionalCoutryCode != "" && objGetProfile.userDetails.optionalCoutryCode != null)
								{
									lblCountryCode1.Text = objGetProfile.userDetails.optionalCoutryCode;
								}
								else
								{
									lblCountryCode1.Text = "+91";
								}

								string languages = "";
								objLanguages = new List<int>();
								foreach (var item in objGetProfile.userDetails.getProfileLanguagesList)
								{

									objLanguages.Add(Convert.ToInt32(item.languageId));
									if (languages == "")
									{
										languages = item.languageName;
									}
									else
									{
										languages = languages + "," + item.languageName;
									}
								}
								lblLanguages.IsVisible = true;
								//entryLanguage.Text = landata.Remove(landata.Length - 1) + ".";
								if (languages.Length > 1)
								{
									entryLanguage.Text = languages + ".";
									lblLanguages.IsVisible = true;
								}

								if (objGetProfile.userDetails.Gender == "Male" || objGetProfile.userDetails.Gender == "male")
								{
									genderTest = "Male";
									imgMale.Source = "imgGenderDone.png";
									imgFemale.Source = "imgGenderNone.png";

								}
								else
								{
									genderTest = "Female";
									imgMale.Source = "imgGenderNone.png";
									imgFemale.Source = "imgGenderDone.png";
								}
								requiredDate = dob[2] + "-" + dob[0] + "-" + dob[1];
								entrySecondaryMobileNo.Text = getLocalDB.OptionalMobile;
								if (!string.IsNullOrEmpty(entrySecondaryMobileNo.Text))
								{
									lblSecondaryMobileNo.IsVisible = true;
								}


								if (objGetProfile.userDetails.profilePic != null && objGetProfile.userDetails.profilePic != "")
								{
									loadImage.Source = new UriImageSource()
									{
										Uri = new Uri(objGetProfile.userDetails.profilePic),
										CachingEnabled = false
									};
								}
								else
								{
									loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
								}
								dateOfBirth.Date = Convert.ToDateTime(objGetProfile.userDetails.dateOfBirth);
							}

						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}
					}
					PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				PageLoading.IsVisible = false;
			}
			PageLoading.IsVisible = false;
		}

		#endregion

		#region to handle button click to call update service
		private async void btnUpdate_Clicked(object sender, EventArgs e)
		{

			try
			{
				if (string.IsNullOrWhiteSpace(entryFirstName.Text) && string.IsNullOrWhiteSpace(entryLastName.Text) && string.IsNullOrWhiteSpace(entryLocation.Text) && string.IsNullOrWhiteSpace(entryLanguage.Text))
				{
					lblFirstName.IsVisible = true;
					lblFirstName.Text = "Enter First name";
					lblFirstName.TextColor = Color.Red;

					lblLastName.IsVisible = true;
					lblLastName.Text = "Enter Last name";
					lblLastName.TextColor = Color.Red;

					lblLanguages.IsVisible = true;
					lblLanguages.Text = "Please select languages";
					lblLanguages.TextColor = Color.Red;

					lblLocation.IsVisible = true;
					lblLocation.Text = "Please select location";
					lblLocation.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(entryFirstName.Text))
				{
					lblFirstName.IsVisible = true;
					lblFirstName.Text = "Enter First name";//
					lblFirstName.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(entryLastName.Text))
				{
					lblLastName.IsVisible = true;
					lblLastName.Text = "Enter Last name";
					lblLastName.TextColor = Color.Red;
				}

				else if (string.IsNullOrWhiteSpace(entryLocation.Text))
				{
					lblLocation.IsVisible = true;
					lblLocation.Text = "Please select Location";
					lblLocation.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(entryLanguage.Text) || entryLanguage.Text == "Languagues you speak")
				{
					lblLanguages.IsVisible = true;
					lblLanguages.Text = "Please select languages";
					lblLanguages.TextColor = Color.Red;
				}
				else
				{

					UpdateProfileService();


					//if (testProfile == true)
					//{
					DBMethods objDatabase = new DBMethods();
					var getLocalDB = objDatabase.GetUserInfo();
					await UpdateLanguageData();

					PageLoading.IsVisible = true;


					#region Service for Edit Profile

					try
					{
						List<Worktypereq> objworktype = new List<Worktypereq>();
						var workItems = objDatabase.GetWorktype();
						if (workItems.Count > 0)
						{
							foreach (var tem in workItems)
							{
								objworktype.Add(new Worktypereq() { worktype_id = tem.ID, experience = Convert.ToInt32(tem.Experience) });
							}
						}
						objDatabase.DeleteWorkType();
						objDatabase.SaveUserWorkType(objworktype);
						var worktypwe = objDatabase.GetWorktype();

						if (isDateSelected != true)
						{
							requiredDate = getLocalDB.dateOFBirth;
						}
						if (isGenderSelected != true)
						{
							genderTest = getLocalDB.Gender;
						}

						UpdateProfileReq objUpdateProfileReq = new UpdateProfileReq();
						objUpdateProfileReq.id = getLocalDB.UserID;
						//objUpdateProfileReq.country_id = getLocalDB.CountryCode;
						objUpdateProfileReq.address = editorAddress.Text;
						objUpdateProfileReq.date_of_birth = requiredDate;
						objUpdateProfileReq.first_name = entryFirstName.Text;
						objUpdateProfileReq.last_name = entryLastName.Text;
						objUpdateProfileReq.optional_dial_code = lblCountryCode1.Text;

						if (!string.IsNullOrEmpty(entryLocation.Text))
						{
							objUpdateProfileReq.location = entryLocation.Text;
						}
						else if (getLocalDB != null)
						{
							objUpdateProfileReq.location = getLocalDB.LocationName;
						}
						else
						{
							objUpdateProfileReq.location = "";
						}
						objUpdateProfileReq.longitude = _longitude.ToString();
						objUpdateProfileReq.latitude = _latitude.ToString();
						//objUpdateProfileReq.e = "8";
						objUpdateProfileReq.job_description = "Chef";
						objUpdateProfileReq.optional_phone = entrySecondaryMobileNo.Text;
						objUpdateProfileReq.worktypereq = objworktype;
						objUpdateProfileReq.gender = genderTest;
						objUpdateProfileReq.language_id = objLanguages;


						if (CheckNetworkAccess.IsNetworkConnected())
						{

							using (IUpdateProfileBAL objUpdateProfile = new UpdateProfileBAL())
							{
								var responeUpdateProfile = await objUpdateProfile.UpdateUserProfile(objUpdateProfileReq);
								if (responeUpdateProfile != null)
								{
									if (responeUpdateProfile.errorCount == 0)
									{
										testProfile = false;
										var saveUserDeatils = new UsersInfo
										{

											Address = responeUpdateProfile.userDetails.Address,
											dateOFBirth = responeUpdateProfile.userDetails.dateOfBirth,
											Gender = responeUpdateProfile.userDetails.Gender,
											OptionalMobile = responeUpdateProfile.userDetails.optionalNumber,
											UserFirstName = responeUpdateProfile.userDetails.firstName,
											UserLastName = responeUpdateProfile.userDetails.lastName,
											UserID = getLocalDB.UserID,
											CountryCode = getLocalDB.CountryCode,
											LocationName = responeUpdateProfile.userDetails.LocationNam,
											UserProfilePic = getLocalDB.UserProfilePic,
											userRating = getLocalDB.userRating,
											_localLog = responeUpdateProfile.userDetails.Longitude,
											_localLat = responeUpdateProfile.userDetails.Latitude,
											//UserProfilePic = uploadcheck.ProfileURL,
											//UserProfilePic = responeUpdateProfile.,
											SecurityCode = "",
											UserPassword = "",
											jobDescription = " ",
											MobileNumber = getLocalDB.MobileNumber,
											OptionalCountryCode = responeUpdateProfile.userDetails.optionalDialCode ?? getLocalDB.OptionalCountryCode
										};

										objDatabase.DeleteUserInfo();

										objDatabase.SaveUserInfo(saveUserDeatils);

										objDatabase.SaveUserLanguages(languaguesClassData);

										var getData = objDatabase.GetUserInfo();

										await DisplayAlert("OneJob", responeUpdateProfile.Message, "Ok");
										//await Navigation.PopModalAsync();
										App.Current.MainPage = new HomeMasterPage();
										PageLoading.IsVisible = false;
									}
									else
									{
										await DisplayAlert("OneJob", responeUpdateProfile.Message, "Ok");
										PageLoading.IsVisible = false;
									}
								}

								else
								{
									await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
									PageLoading.IsVisible = false;
								}

							}

						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
							PageLoading.IsVisible = false;
						}
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
						PageLoading.IsVisible = false;
					}
					PageLoading.IsVisible = false;

					#endregion

				}

			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;

			}
		}
		#endregion

	}
}
