﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using Plugin.Geolocator.Abstractions;
using Plugin.Geolocator;

namespace OneJob
{
	public partial class UserProfile : BaseContentPage
	{

		#region for Declaring User Variables

		CustomLabel lblDateofBirth, lblFirstName, lblLastName, lblLanguages, lblLocation, entryLanguage;
		CustomDatePicker dateOfBirth;
		CustomEntry entryFirstName, entryLastName, entryMobileNo;
		public static CustomLabel entryLocation;
		EditorCtrl editorAddress;

		CustomLabel lblMobileNo;
		String genderTest;

		#endregion

		public static CustomLabel profileCountryCode;



		List<int> objLanguages;
		string requiredDate;
		string LanName;
		string LanId;
		List<LanguageId> languaguesClassData;
		public GoogleSearch Arearesponse;
		public ListView searchlistview;
		public string searchcomplete = null;
		public LocationRootObject loc;
		public static double _latitude;
		public static double _longitude;
		public static string locationName;

		public UserProfile()
		{
			InitializeComponent();

			AbsoluteLayout ablayout = new AbsoluteLayout();

			BackgroundColor = Color.FromHex("#FFFFFF");

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 7) / 100;
			//var widthEntry = (width * 76.36) / 100;
			var entryWidth = (width * 81) / 100;

			var entry_Width = (width * 75.36) / 100;
			var topheaderHeight = (height * 9.5) / 100;
			var headerHeight = (height * 7.5) / 100;
			var header_Height = (height * 7.3) / 100;

			var fontsize = Convert.ToSingle((height * 2.4647) / 100);
			var fontsize2 = Convert.ToSingle((height * 2) / 100);

			#region Page header
			var image_Height = (screenHeight * 6) / 100;
			Image menuImage = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				HeightRequest = image_Height,
				WidthRequest = image_Height,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End,
				Margin = new Thickness(entryWidth / 34, 0, 0, entryWidth / 45)
			};
			CustomLabel headerTitle = new CustomLabel()
			{
				Margin = new Thickness(0, 0, 0, entryWidth / 19),
				Text = "Create your Profile",
				//TextColor = AppGlobalVariables.lightGray,
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.End
			};

			TapGestureRecognizer menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {
			   // App.Current.MainPage = new HomeMasterPage();
			   Navigation.PushModalAsync(new UserRegistration());
			   // Navigation.PopModalAsync();
			   //var ParentPage = (MasterDetailPage)this.Parent;
			   //ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;

		   };
			menuImage.GestureRecognizers.Add(menuClicked);
			BoxView _space = new BoxView()
			{
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HeightRequest = headerHeight,
				WidthRequest = header_Height,
				HorizontalOptions = LayoutOptions.End
			};

			HorizontalGradientStack imgback = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { menuImage, headerTitle, _space },
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(1, 0, 10, 0),
				BackgroundColor = Color.FromHex("#707070"),
				HeightRequest = topheaderHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};

			entryFirstName = new CustomEntry()
			{
				Placeholder = "First name",
				//CustomFontSize = fontsize,//15,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};
			lblFirstName = new CustomLabel()
			{
				Text = "First name",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			entryFirstName.Focused += (object sender, FocusEventArgs e) =>
			{

				lblFirstName.IsVisible = true;
				entryFirstName.PlaceholderColor = Color.Transparent;
				//name.Placeholder = string.Empty;
			};

			entryFirstName.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entryFirstName.Text))
				{
					lblFirstName.Text = "First name";
					lblFirstName.IsVisible = true;
					lblFirstName.TextColor = AppGlobalVariables.fontLessThick;
					entryFirstName.Placeholder = "First name";
				}
				else
				{

					entryFirstName.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entryFirstName.Placeholder = "First name";
					lblFirstName.Text = "Enter first name";
					lblFirstName.TextColor = Color.Red;
					lblFirstName.IsVisible = true;
				}

			};


			entryLastName = new CustomEntry()
			{
				Placeholder = "Last name",
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Text,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			lblLastName = new CustomLabel()
			{
				Text = "Last name",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			entryLastName.Focused += (object sender, FocusEventArgs e) =>
			{

				lblLastName.IsVisible = true;
				entryLastName.PlaceholderColor = Color.Transparent;
			};

			entryLastName.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entryLastName.Text))
				{
					lblLastName.Text = "Last name";
					lblLastName.IsVisible = true;
					lblLastName.TextColor = AppGlobalVariables.fontLessThick;
					entryLastName.Placeholder = "Last name";

				}
				else
				{

					entryLastName.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entryLastName.Placeholder = "Last name";
					lblLastName.Text = "Enter last name";
					lblLastName.TextColor = Color.Red;
					lblLastName.IsVisible = true;

				}

			};

			dateOfBirth = new CustomDatePicker()
			{
				EnterText = "Date of birth",
				Date = DateTime.Now.Date,
				CustomFontSize = fontsize,//15,
				CustomFontFamily = "Avenir45",
				Format = "MM-dd-yyyy",
				//MinimumDate = DateTime.Now,
				//MinimumDate = DateTime.Now.AddDays(1),
				MaximumDate = DateTime.Now,
				TextColor = AppGlobalVariables.fontVeryThick,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,//entryWidth,
										  //BackgroundColor = Color.Green,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};


			dateOfBirth.DateSelected += (object sender, DateChangedEventArgs e) =>
			{
				var selectedDate = dateOfBirth.Date;
				requiredDate = selectedDate.Year.ToString() + "-" + selectedDate.Month.ToString() + "-" + selectedDate.Day.ToString();
				//requiredDate = selectedDate.Month.ToString() + "/" + selectedDate.Day.ToString() + "/" + selectedDate.Year.ToString();
			};
			lblDateofBirth = new CustomLabel()
			{
				Text = "Date of birth",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = fontsize2,
				//FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				//IsVisible = false,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start
			};
			dateOfBirth.DateSelected += (object sender, DateChangedEventArgs e) =>
			{

			};
			dateOfBirth.Focused += (sender, e) =>
			{

			};
			dateOfBirth.Unfocused += (object sender, FocusEventArgs e) =>
			{

			};

			CustomLabel lblGender = new CustomLabel()
			{
				HorizontalOptions = LayoutOptions.Start,
				Text = "Gender:",
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};


			CustomLabel lblMale = new CustomLabel()
			{
				Text = "Male",
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};

			Image imgMale = new Image()
			{
				Source = "imgGenderDone.png",
				HeightRequest = image_Height,
				WidthRequest = image_Height
			};
			genderTest = "Male";

			StackLayout stackMale = new StackLayout()
			{
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				Children = { imgMale, lblMale }
			};


			CustomLabel lblFemale = new CustomLabel()
			{
				Text = "Female",
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};

			Image imgFemale = new Image()
			{
				Source = "imgGenderNone.png",
				HeightRequest = image_Height,
				WidthRequest = image_Height
			};

			var imgMaleTap = new TapGestureRecognizer();
			imgMaleTap.Tapped += (s, e) =>
			{
				imgFemale.Source = "imgGenderNone.png";
				imgMale.Source = "imgGenderDone.png";
				genderTest = "Male";

			};

			imgMale.GestureRecognizers.Add(imgMaleTap);


			var imgFemaleTap = new TapGestureRecognizer();
			imgFemaleTap.Tapped += (s, e) =>
			{
				imgMale.Source = "imgGenderNone.png";
				imgFemale.Source = "imgGenderDone.png";
				genderTest = "FeMale";

			};

			imgFemale.GestureRecognizers.Add(imgFemaleTap);





			StackLayout stackFemale = new StackLayout()
			{
				Spacing = 3,
				Orientation = StackOrientation.Horizontal,
				Children = { imgFemale, lblFemale },
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};

			StackLayout stackGender = new StackLayout()
			{
				Spacing = 10,
				WidthRequest = entryWidth,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Children = { lblGender, stackMale, stackFemale }
			};

			profileCountryCode = new CustomLabel()
			{
				Text = "+91",
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				//Margin = new Thickness((entryWidth) / 8, 0, 0, 0),
				VerticalTextAlignment = TextAlignment.Center
			};

			entryMobileNo = new CustomEntry()
			{
				Placeholder = "Optional Mobile number",
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Telephone,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			entryMobileNo.TextChanged += mobileNoChanged;

			Image imgCountryCode = new Image()
			{
				Source = "imgPicker.png"
			};

			Grid entry = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = entryWidth,
				ColumnSpacing = 8,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			entry.Children.Add(profileCountryCode, 0, 0);
			entry.Children.Add(imgCountryCode, 1, 0);
			entry.Children.Add(entryMobileNo, 2, 0);

			var tapimgCuntryCode1 = new TapGestureRecognizer();
			tapimgCuntryCode1.Tapped += (s, e) =>
			{
				try
				{
					MessagingCenter.Send<UserProfile, string>(this, "Code", "userProfile");
					Application.Current.Properties["Detail"] = "Profile";
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			imgCountryCode.GestureRecognizers.Add(tapimgCuntryCode1);




			var taplblCountryCode1 = new TapGestureRecognizer();
			taplblCountryCode1.Tapped += (s, e) =>
			{
				MessagingCenter.Send<UserProfile, string>(this, "Code", "userProfile");
				Application.Current.Properties["Detail"] = "Profile";
				var ParentPage = (MasterDetailPage)this.Parent;
				ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			};
			profileCountryCode.GestureRecognizers.Add(taplblCountryCode1);


			lblMobileNo = new CustomLabel()
			{
				Text = "Mobile number",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			//entryMobileNo.TextChanged += mobileNoChanged;
			entryMobileNo.Focused += (object sender, FocusEventArgs e) =>
			{
				//mobileNoLbl.FontSize = 10;
				lblMobileNo.IsVisible = true;
				entryMobileNo.PlaceholderColor = Color.Transparent;
				//mobileNo.Placeholder = string.Empty;
			};
			entryMobileNo.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(entryMobileNo.Text))
				{
					//mobileNoLbl.FontSize = 10;
					lblMobileNo.IsVisible = true;
					entryMobileNo.PlaceholderColor = Color.Transparent;
					//mobileNo.Placeholder = string.Empty;
				}
				else
				{
					//mobileNoLbl.FontSize = 15;
					lblMobileNo.IsVisible = false;
					entryMobileNo.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					entryMobileNo.Placeholder = "Optional Mobile number";
				}
				if (string.IsNullOrWhiteSpace(entryMobileNo.Text))
				{
					entryMobileNo.Placeholder = "Optional Mobile number";
				}
			};



			entryLanguage = new CustomLabel()
			{
				Text = "Languagues you speak",
				//CustomFontSize = fontsize,//15,
				TextColor = AppGlobalVariables.fontVeryThick,
				//PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HeightRequest = (entryHeight * 2) / 2.6,
				WidthRequest = entryWidth,
				LineBreakMode = LineBreakMode.NoWrap,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};
			//ScrollView entryLanguagesScroll = new ScrollView()
			//{
			//	Content = entryLanguage
			//};

			languaguesClassData = new List<LanguageId>();
			objLanguages = new List<int>();

			//MessagingCenter.Subscribe<RightSideMasterPage, int>(this, "LanguagesId", (s, arg) =>
			//{


			//    entryLanguage.Text = string.Empty;
			//    if (arg == 0)
			//    {
			//        entryLanguage.Text = "Languagues you speak";
			//    }
			//    else if (entryLanguage.Text != "Languagues you speak")
			//    {
			//        objLanguages.Add(arg);
			//        //languaguesClassData.Add(new LanguageId() { id = arg.ToString() });

			//    }
			//    else
			//    {
			//        entryLanguage.Text = arg + ".";
			//    }

			//});



			MessagingCenter.Subscribe<RightSideMasterPage, string>(this, "Hi", (s, arg) =>
			{
				if (arg != null)
				{
					objLanguages.Clear();
					languaguesClassData.Clear();
					string langName = null;
					var joggedData = RightSideMasterPage.rsmp.LanguagesList;
					//if (joggedData.Count == 0)
					//{
					//    entryLanguage.Text = "Languagues you speak";
					//}
					//else if (entryLanguage.Text != "Languagues you speak")
					//{
					//    lblLanguages.IsVisible = true;
					//    entryLanguage.Text = arg + ".";
					//    //languaguesClassData.Add(new LanguageId() { language_name = arg });
					//}
					//else
					//{
					//    entryLanguage.Text = arg + ".";
					//}
					if (joggedData.Count == 0)
					{
						entryLanguage.Text = "Languagues you speak";
					}
					else
					{
						if (joggedData.Count == 1)
						{
							langName = joggedData[0].LanguageName + "," + ",";
							objLanguages.Add(Convert.ToInt32(joggedData[0].ID));
							languaguesClassData.Add(new LanguageId() { id = joggedData[0].ID, language_name = joggedData[0].LanguageName });
						}
						else
						{
							foreach (var item in joggedData)
							{
								langName = langName + item.LanguageName + ", ";
								objLanguages.Add(Convert.ToInt32(item.ID));
								languaguesClassData.Add(new LanguageId() { id = item.ID, language_name = item.LanguageName });
							}
						}

						var myString = langName.Remove(langName.Length - 2) + ".";

						entryLanguage.Text = myString;
					}
					//entryLanguage.Text = langName + ".";
					//var datas = arg.Split('@');
					//LanId = datas[1];
					//LanName = datas[0];
					//languaguesClassData.Add(new LanguageId() { id = LanId, language_name = LanName });
				}

			});




			//MessagingCenter.Subscribe<RightSideMasterPage, string>(this, "LanguagesData", (s, arg) =>
			//{
			//    if (arg == null)
			//    {
			//        entryLanguage.Text = "Languagues you speak";
			//    }
			//    else if (entryLanguage.Text != "Languagues you speak")
			//    {

			//        lblLanguages.IsVisible = true;
			//        entryLanguage.Text = arg + ".";

			//        //languaguesClassData.Add(new LanguageId() { language_name = arg });
			//    }
			//    else
			//    {
			//        entryLanguage.Text = arg + ".";
			//    }

			//});




			lblLanguages = new CustomLabel()
			{
				Text = "Languagues you speak",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				LineBreakMode = LineBreakMode.NoWrap,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};




			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				//MessagingCenter.Send<UserProfile, string>(this, "Hii", "Location");
				this.ShowMenu();
			};
			entryLanguage.GestureRecognizers.Add(tapGestureRecognizer);

			this.SlideMenu = new RightSideMasterPage();


			entryLocation = new CustomLabel()
			{
				Text = "Location",
				//CustomFontSize = fontsize,//15,
				TextColor = AppGlobalVariables.fontVeryThick,
				//PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HeightRequest = (entryHeight * 2) / 2.6,
				WidthRequest = entryWidth,
				LineBreakMode = LineBreakMode.NoWrap,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};
			lblLocation = new CustomLabel()
			{
				Text = "Location",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};

			var tapGestureRecognizerLocation = new TapGestureRecognizer();
			tapGestureRecognizerLocation.Tapped += (s, e) =>
			{
				try
				{
					MessagingCenter.Send<UserProfile, string>(this, "Location", "userProfile");
					Application.Current.Properties["Detail"] = "ProfileLocation";
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			entryLocation.GestureRecognizers.Add(tapGestureRecognizerLocation);





			//var tapGestureRecognizerLocation = new TapGestureRecognizer();
			//tapGestureRecognizerLocation.Tapped += (s, e) =>
			//{
			//	this.ShowMenu();
			//	MessagingCenter.Send<UserProfile, string>(this, "Hi", "Location");
			//};
			//entryLocation.GestureRecognizers.Add(tapGestureRecognizerLocation);

			this.SlideMenu = new RightSideMasterPage();


			entryLocation.Focused += (object sender, FocusEventArgs e) =>
			{
				//confirmPasswordLbl.FontSize = 10;
				lblLocation.IsVisible = true;

				//entryLocation.PlaceholderColor = Color.Transparent;
				//confirmPassword.Placeholder = string.Empty;
			};

			int rowheight1 = Convert.ToInt16(screenHeight / 14.72);

			searchlistview = new ListView()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				SeparatorVisibility = SeparatorVisibility.None,
				//IsVisible = false,
				//SeparatorColor = Color.Black,
				BackgroundColor = Color.White,
				RowHeight = rowheight1
				//HasUnevenRows = true
				//BackgroundColor = Color.Red
				//Margin = new Thickness(0, screenHeight / 10.98, 0, 0),
				//RowHeight = rowheight
			};

			Frame fra = new Frame()
			{
				Content = searchlistview,
				IsVisible = false,
				OutlineColor = AppGlobalVariables.darkMarron,
				HasShadow = false,
				Padding = 0,
			};

			entryLocation.Unfocused += (object sender, FocusEventArgs e) =>
			{
				try
				{

					fra.IsVisible = false;

					if (!string.IsNullOrWhiteSpace(entryLocation.Text))
					{
						lblLocation.Text = "Location";
						lblLocation.IsVisible = true;
						lblLocation.TextColor = AppGlobalVariables.fontLessThick;
						//entryLocation.Placeholder = "Location";
					}
					else
					{

						//entryLocation.PlaceholderColocationNamelor = AppGlobalVariables.fontVeryThick;
						//entryLocation.Placeholder = "Location";
						lblLocation.Text = "Please select location";
						lblLocation.TextColor = Color.Red;
						lblLocation.IsVisible = true;
					}
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}


			};




			searchlistview.ItemTemplate = new DataTemplate(typeof(searchcell));

			searchlistview.ItemSelected += async (sender, e) =>
			{
				try
				{
					fra.IsVisible = false;
					if (((ListView)sender).SelectedItem == null)
					{
						return;
					}
					var item = ((ListView)sender).SelectedItem as Prediction;
					searchcomplete = "ok";
					if (item.description != null)
					{
						entryLocation.Text = item.description;
						locationName = item.description;
						loc = await new JobListBAL().GetAreaLocations(item.place_id);
						_latitude = loc.result.geometry.location.lat;
						_longitude = loc.result.geometry.location.lng;
					}
					else
					{
						entryLocation.Text = "";
						locationName = "";
						//loc = await new JobListBAL().GetAreaLocations(item.place_id);
						try
						{
							IGeolocator locator;
							locator = CrossGeolocator.Current;
							locator.DesiredAccuracy = 50;
							if (locator.IsGeolocationEnabled)
							{
								PageLoading.IsVisible = true;
								var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
								PageLoading.IsVisible = false;
								_latitude = position.Latitude;
								_longitude = position.Longitude;
							}
							else
							{
								_latitude = 0.0;
								_longitude = 0.0;
							}
						}
						catch (Exception ex)
						{
							var msg = ex.Message;
						}
						PageLoading.IsVisible = false;
					}
					/*
					entryLocation.Text = item.description;
					locationName = item.description;
					loc = await new JobListBAL().GetAreaLocations(item.place_id);
					_latitude = loc.result.geometry.location.lat;
					_longitude = loc.result.geometry.location.lng;
					*/
					((ListView)sender).SelectedItem = null;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};

			//entryLocation.TextChanged += async (sender, e) =>
			//{
			//	try
			//	{
			//		if (entryLocation.Text == string.Empty)
			//		{
			//			fra.IsVisible = false;
			//		}
			//		else
			//		{
			//			if (searchcomplete != "ok")
			//			{
			//				Arearesponse = await new JobListBAL().GetAreaDetails(entryLocation.Text);
			//				searchlistview.ItemsSource = Arearesponse.predictions.ToList();
			//				fra.IsVisible = true;

			//			}
			//			else
			//			{
			//				fra.IsVisible = false;
			//			}
			//			searchcomplete = null;
			//		}
			//	}

			//	catch (Exception ex)
			//	{
			//		var msg = ex.Message;
			//	}
			//};

			CustomLabel lbladdress = new CustomLabel()
			{
				Text = "Contact address",
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//HeightRequest = entryHeight/2,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};

			editorAddress = new EditorCtrl()
			{
				BackgroundColor = Color.Transparent,
				HeightRequest = (entryHeight * 3.7) / 2
				//HorizontalOptions = LayoutOptions.Start,
			};


			StackLayout stackAddress = new StackLayout()
			{
				Spacing = 10,
				Children = { lbladdress, editorAddress }
			};

			var btnHeight = (height * 8.5) / 100;



			Image btnBackground = new Image()
			{
				Source = "imgLoginButton.png",
				HeightRequest = entryHeight,
				//WidthRequest = entryWidth,
				//Scale = 1,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				btnBackground.Scale = 1.1;
			}
			else
			{
				btnBackground.Scale = 1.17;
			}


			CustomLabel imgLogin = new CustomLabel()
			{
				Text = "Next",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Grid ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				//HeightRequest = screenHeight / 14,
				//Padding = new Thickness(10),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);

			TapGestureRecognizer btnLoginTap = new TapGestureRecognizer();
			btnLoginTap.Tapped += btnNext_Clicked;

			ImgButton.GestureRecognizers.Add(btnLoginTap);


			//BoxView entityNameULine = new BoxView()
			//{
			//    HeightRequest = boxHeight,
			//    WidthRequest = boxWidth,
			//    Color = AppGlobalVariables.underLineColor,
			//    BackgroundColor = AppGlobalVariables.underLineColor,
			//    HorizontalOptions = LayoutOptions.Center,
			//    VerticalOptions = LayoutOptions.EndAndExpand
			//};


			BoxView FirstNameULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout FirstNameULineStack = new StackLayout()
			{
				Children = { FirstNameULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView LastNameULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout LastNameULineStack = new StackLayout()
			{
				Children = { LastNameULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView dateULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};

			StackLayout dateULineStack = new StackLayout()
			{
				Children = { dateULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView mobileNoULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout mobileNoULineStack = new StackLayout()
			{
				Children = { mobileNoULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView LanguagesULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout LanguagesULineStack = new StackLayout()
			{
				Children = { LanguagesULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView LocationULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout LocationULineStack = new StackLayout()
			{
				Children = { LocationULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView space = new BoxView()
			{
				Opacity = 0,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};

			BoxView space1 = new BoxView()
			{
				Opacity = 0,
				HeightRequest = 3,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};
			Grid nameGrid = new Grid()
			{
				ColumnSpacing = 12,
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				}
			};

			nameGrid.Children.Add(lblFirstName, 0, 0);
			nameGrid.Children.Add(FirstNameULineStack, 0, 0);
			nameGrid.Children.Add(entryFirstName, 0, 0);

			nameGrid.Children.Add(lblLastName, 1, 0);
			nameGrid.Children.Add(LastNameULineStack, 1, 0);
			nameGrid.Children.Add(entryLastName, 1, 0);


			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				//RowSpacing = entryWidth / 20,
				RowSpacing = entryWidth / 31,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				Padding = new Thickness(entry_Width / 8, 0, entry_Width / 8, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			BoxView spaceFooter = new BoxView()
			{
				//    HeightRequest = (entryHeight * 3) / 15,
				HeightRequest = 15,
				BackgroundColor = Color.Transparent
			};

			BoxView spaceFooter1 = new BoxView()
			{
				HeightRequest = (entryHeight) / 20,
				BackgroundColor = Color.Transparent
			};


			pageBody.Children.Add(imgback, 0, 0);

			pageBody.Children.Add(nameGrid, 0, 1);

			pageBody.Children.Add(lblDateofBirth, 0, 2);
			pageBody.Children.Add(dateULineStack, 0, 2);
			pageBody.Children.Add(dateOfBirth, 0, 2);

			pageBody.Children.Add(space, 0, 3);
			pageBody.Children.Add(stackGender, 0, 3);

			pageBody.Children.Add(lblMobileNo, 0, 4);
			pageBody.Children.Add(mobileNoULineStack, 0, 4);
			pageBody.Children.Add(entry, 0, 4);

			pageBody.Children.Add(lblLanguages, 0, 5);
			pageBody.Children.Add(LanguagesULineStack, 0, 5);
			pageBody.Children.Add(entryLanguage, 0, 5);

			pageBody.Children.Add(lblLocation, 0, 6);
			pageBody.Children.Add(LocationULineStack, 0, 6);
			pageBody.Children.Add(entryLocation, 0, 6);

			pageBody.Children.Add(space1, 0, 7);
			pageBody.Children.Add(stackAddress, 0, 8);

			pageBody.Children.Add(spaceFooter, 0, 9);

			//pageBody.Children.Add(ImgButton, 0, 10);

			//pageBody.Children.Add(spaceFooter1, 0, 11);


			#endregion

			StackLayout bodyHolder = new StackLayout()
			{
				Children = { pageBody, ImgButton, spaceFooter1 },
				//Spacing = (entryHeight * 3) / 4,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			ScrollView scrollHolder = new ScrollView()
			{
				Content = bodyHolder
			};


			StackLayout holder = new StackLayout()
			{
				Children = { imgback, scrollHolder },
				//Spacing = (entryHeight) / 2,
				Spacing = entryHeight / 4,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			AbsoluteLayout.SetLayoutBounds(holder, new Rectangle(1, 1, 1, 1));
			AbsoluteLayout.SetLayoutFlags(holder, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(holder);

			AbsoluteLayout.SetLayoutBounds(fra, new Rectangle(0.5, Device.OnPlatform(0.4, 0.4, 0.188), 0.83, 0.3));
			AbsoluteLayout.SetLayoutFlags(fra, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(fra);

			PageControlsStackLayout.Children.Add(ablayout);

		}

		void mobileNoChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 10)
			{
				entryMobileNo.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 10)
			{
				lblMobileNo.TextColor = AppGlobalVariables.fontLessThick;
				lblMobileNo.Text = "Mobile number";
			}
			else
			{
				lblMobileNo.TextColor = Color.Red;
				lblMobileNo.Text = "Mobile number must be 10 digits";
			}
		}

		private async void btnNext_Clicked(object sender, EventArgs e)
		{


			try
			{
				if (string.IsNullOrWhiteSpace(entryFirstName.Text) && string.IsNullOrWhiteSpace(entryLastName.Text) && string.IsNullOrWhiteSpace(entryLocation.Text) && string.IsNullOrWhiteSpace(entryLanguage.Text))
				{
					lblFirstName.IsVisible = true;
					lblFirstName.Text = "Enter First name";
					lblFirstName.TextColor = Color.Red;

					lblLastName.IsVisible = true;
					lblLastName.Text = "Enter Last name";
					lblLastName.TextColor = Color.Red;

					lblLanguages.IsVisible = true;
					lblLanguages.Text = "Please select languages";
					lblLanguages.TextColor = Color.Red;

					lblLocation.IsVisible = true;
					lblLocation.Text = "Please select location";
					lblLocation.TextColor = Color.Red;


				}
				else if (string.IsNullOrWhiteSpace(entryFirstName.Text))
				{
					lblFirstName.IsVisible = true;
					lblFirstName.Text = "Enter First name";
					lblFirstName.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(entryLastName.Text))
				{
					lblLastName.IsVisible = true;
					lblLastName.Text = "Enter Last name";
					lblLastName.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(entryLocation.Text) || entryLocation.Text == "Location")
				{
					lblLocation.IsVisible = true;
					lblLocation.Text = "Please select location";
					lblLocation.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(entryLanguage.Text) || entryLanguage.Text == "Languagues you speak")
				{
					lblLanguages.IsVisible = true;
					lblLanguages.Text = "Please select Languages";
					lblLanguages.TextColor = Color.Red;
				}
				else
				{
					DBMethods objDatabase = new DBMethods();
					var getLocalDB = objDatabase.GetUserInfo();

					try
					{
						Application.Current.Properties["WorkFlow"] = "Profile";
						string[] registerdata = new string[] { getLocalDB.UserID, profileCountryCode.Text, editorAddress.Text, requiredDate, entryFirstName.Text, entryLastName.Text, entryMobileNo.Text, genderTest };

						await Navigation.PushModalAsync(new RegisterWorkType(registerdata, objLanguages, "UserProfile", languaguesClassData, _latitude, _longitude, locationName, "profileTest"));

					}
					catch (Exception ex)
					{
						var msg = ex.Message;

					}
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;

			}
		}
	}
}