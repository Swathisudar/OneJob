﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace OneJob
{
	public partial class LocationsMenuPage : BaseContentPage
	{
		public List<CountryData> countries { get; set; }
		CustomLabel lblTitle;
		//public CustomEntry schSearchItems;
		public ListView listView;
		//public SearchBar schSearchItems;
		//public ListView listView;
		public AbsoluteLayout contentLayout;
		public ActivityIndicator ai;
		public StackLayout stackOverlay;
		public HorizontalGradientStack stackContent;
		public CustomEntry schSearchItems;
		//public ActivityIndicator ai;
		//public StackLayout stackOverlay;
		//public HorizontalGradientStack stackContent;
		public string searchcomplete = null;
		public GoogleSearch Arearesponse;
		public double _latitude;
		public double _longitude;

		public static string pageTest;
		public LocationRootObject loc;

		public LocationsMenuPage()
		{

			//InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);
			this.Icon = null;
			this.Title = "Countries";
			//this.BackgroundColor = Color.FromHex("#F8C03E");
			countries = new List<CountryData>();
			if (Device.OS == TargetPlatform.Android)
			{
				listView = new ListView(ListViewCachingStrategy.RecycleElement)
				{
					//HasUnevenRows = true,
					RowHeight = 50,
					SeparatorVisibility = SeparatorVisibility.Default,
					SeparatorColor = Color.Black,
					BackgroundColor = Color.White,
				};
			}
			else
			{
				listView = new ListView()
				{
					//HasUnevenRows = true,
					RowHeight = 50,
					SeparatorVisibility = SeparatorVisibility.Default,
					SeparatorColor = Color.Black,
					BackgroundColor = Color.White,
				};
			}






			schSearchItems = new CustomEntry
			{
				Placeholder = "Please enter your location",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				TextColor = Color.Black,
				BackgroundColor = Color.Transparent,
			};



			ai = new ActivityIndicator
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Color = Color.FromHex("#F8C03E"),
				IsEnabled = true,
				IsVisible = true,
				IsRunning = true,
			};


			contentLayout = new AbsoluteLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			stackOverlay = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				IsVisible = false,
				Children = { ai }
			};


			BaseContentPage bcp = new BaseContentPage();

			listView.ItemSelected += async (sender, e) =>
			  {
				  try
				  {
					  if (e.SelectedItem == null)
					  {
						  return;
					  }


					  if (pageTest != "Code")
					  {
						  var item = ((ListView)sender).SelectedItem as Prediction;
						  var parentDetailView = (MasterDetailPage)this.Parent;
						  parentDetailView.IsPresented = false;
						  schSearchItems.Text = "";
						  Application.Current.Properties["ParentPage"] = this;
						  var detail = Application.Current.Properties["Detail"].ToString();
						  if (detail == "ProfileLocation")
						  {

							  searchcomplete = "ok";
							  if (item.description != null)
							  {
								  schSearchItems.Text = item.description;
								  UserProfile.locationName = item.description;
								  UserProfile.entryLocation.Text = item.description.ToString();
								  loc = await new JobListBAL().GetAreaLocations(item.place_id);
								  UserProfile._latitude = loc.result.geometry.location.lat;
								  UserProfile._longitude = loc.result.geometry.location.lng;
								  schSearchItems.Text = "";
							  }
							  else
							  {
								  schSearchItems.Text = "";
								  UserProfile.locationName = "";
								  //loc = await new JobListBAL().GetAreaLocations(item.place_id);
								  try
								  {
									  IGeolocator locator;
									  locator = CrossGeolocator.Current;
									  locator.DesiredAccuracy = 50;
									  if (locator.IsGeolocationEnabled)
									  {

										  bcp.PageLoading.IsVisible = true;
										  var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
										  bcp.PageLoading.IsVisible = false;
										  _latitude = position.Latitude;
										  _longitude = position.Longitude;
									  }
									  else
									  {
										  _latitude = 0.0;
										  _longitude = 0.0;
									  }
								  }
								  catch (Exception ex)
								  {
									  var msg = ex.Message;
								  }
								  bcp.PageLoading.IsVisible = false;
							  }
						  }
						  else
						  {


							  searchcomplete = "ok";
							  if (item.description != null)
							  {
								  schSearchItems.Text = item.description;
								  // EditProfile.locationName = item.description;
								  EditProfile.entryLocation.Text = item.description.ToString();
								  loc = await new JobListBAL().GetAreaLocations(item.place_id);
								  EditProfile._latitude = loc.result.geometry.location.lat;
								  EditProfile._longitude = loc.result.geometry.location.lng;
								  schSearchItems.Text = "";
							  }
							  else
							  {
								  schSearchItems.Text = "";
								  //EditProfile.locationName = "";
								  //loc = await new JobListBAL().GetAreaLocations(item.place_id);
								  try
								  {
									  IGeolocator locator;
									  locator = CrossGeolocator.Current;
									  locator.DesiredAccuracy = 50;
									  if (locator.IsGeolocationEnabled)
									  {

										  bcp.PageLoading.IsVisible = true;
										  var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
										  bcp.PageLoading.IsVisible = false;
										  _latitude = position.Latitude;
										  _longitude = position.Longitude;
									  }
									  else
									  {
										  _latitude = 0.0;
										  _longitude = 0.0;
									  }
								  }
								  catch (Exception ex)
								  {
									  var msg = ex.Message;
								  }
								  bcp.PageLoading.IsVisible = false;
							  }

						  }
						  HideMenu();


					  }
					  else
					  {
						  if (e.SelectedItem == null)
						  {
							  return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
						  }



						  var country = (CountryData)e.SelectedItem;
						  var parentDetailView = (MasterDetailPage)this.Parent;
						  parentDetailView.IsPresented = false;
						  schSearchItems.Text = "";
						  Application.Current.Properties["SelectedCountry"] = "+91";//country.dailCode; 

						  Application.Current.Properties["ParentPage"] = this;
						  var detail = Application.Current.Properties["Detail"].ToString();

						  if (detail == "EditPro")
						  {
							  EditProfile.lblCountryCode1.Text = "+" + country.dailCode;
						  }
						  else if (detail == "Profile")
						  {
							  UserProfile.profileCountryCode.Text = "+" + country.dailCode;
						  }
						  else
						  {
							  //UserLogin.logCountryCode.Text = "+" + country.dailCode;
						  }

						  HideMenu();
					  }
					  //await Navigation.PushModalAsync(new CountryMasterPage());
				  }
				  catch (Exception ex)
				  {
				  }
				  ((ListView)sender).SelectedItem = null;

			  };

			MessagingCenter.Subscribe<UserProfile, string>(this, "Code", (sender1, arg) =>
				{
					GetContriesListView();
					pageTest = "Code";
					lblTitle.Text = "Select Country";
					schSearchItems.Placeholder = "Search";
					schSearchItems.Text = "";
					listView.RowHeight = 50;
				});


			MessagingCenter.Subscribe<EditProfile, string>(this, "Code", (sender1, arg) =>
				{
					GetContriesListView();
					pageTest = "Code";
					lblTitle.Text = "Select Country";
					schSearchItems.Text = "";
					schSearchItems.Placeholder = "Search";
					listView.RowHeight = 50;

				});


			MessagingCenter.Subscribe<EditProfile, string>(this, "Location", (sender1, arg) =>
				{
					//GetContriesListView();//pageTest = "Code";
					GetLocations();
					pageTest = "Location";
					lblTitle.Text = "Select Location";
					schSearchItems.Text = "";
					schSearchItems.Placeholder = "Please enter your location";
					listView.RowHeight = 50;

					//listView.HasUnevenRows = true;
				});


			MessagingCenter.Subscribe<UserProfile, string>(this, "Location", (sender1, arg) =>
				{
					//GetContriesListView();//pageTest = "Code";
					GetLocations();
					pageTest = "Location";
					lblTitle.Text = "Select Location";
					schSearchItems.Text = "";
					schSearchItems.Placeholder = "Please enter your location";
					//listView.HasUnevenRows = true;
					listView.RowHeight = 50;

					//listView.ItemsSource = Arearesponse.predictions;
				});


			schSearchItems.TextChanged += async (sender, args) =>
			{


				if (pageTest != "Code")
				{


					try
					{
						if (schSearchItems.Text == string.Empty)
						{
							//fra.IsVisible = false;
						}
						else
						{
							if (searchcomplete != "ok")
							{
								if (schSearchItems.Text != null && schSearchItems.Text != "")
								{

									stackOverlay.IsVisible = true;
									Arearesponse = await new JobListBAL().GetAreaDetails(schSearchItems.Text);

									listView.ItemTemplate = new DataTemplate(typeof(LocationDataCell));
									listView.ItemsSource = Arearesponse.predictions;
									stackOverlay.IsVisible = false;
									//foreach(var item in Arearesponse.predictions)
									//{
									//	listView.ItemsSource =;
									//}
									// fra.IsVisible = true;
								}
								else
								{
									HideMenu();
								}
								stackOverlay.IsVisible = false;
								//foreach(var item in Arearesponse.predictions)
								//{
								//	listView.ItemsSource =;
								//}
								// fra.IsVisible = true;

							}
							else
							{
								//fra.IsVisible = false;
							}
							searchcomplete = null;
							stackOverlay.IsVisible = false;
						}


						if (schSearchItems.Text.Length <= 0)
						{
							listView.ItemsSource = null;
							schSearchItems.Placeholder = "Please enter your location";
							schSearchItems.Unfocus();
						}
					}

					catch (Exception ex)
					{
						var msg = ex.Message;
					}

				}
				else
				{
					try
					{


						if (countries != null)
						{
							if (string.IsNullOrWhiteSpace(args.NewTextValue))
							{
								listView.ItemsSource = countries.ToList();
							}
							//listView.ItemsSource = countries.Result.result.OrderBy(x => x.CountryName).ToList();

							else
							{
								if (schSearchItems.Text != null && schSearchItems.Text != "")
								{
									listView.ItemsSource = countries.Where(i => i.coutryName.ToLower().Contains(args.NewTextValue.ToLower()));
								}


							}

						}
						else
						{

						}


						if (schSearchItems.Text.Length <= 0)
						{
							//listView.ItemsSource = null;
							schSearchItems.Placeholder = "Search...";
							schSearchItems.Unfocus();
						}
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					}
				}


			};

			//lblTitle = new Label()
			//{
			//	Text = "Select Location",
			//	FontSize = Device.OnPlatform(20, 22, 26),
			//	TextColor = Color.White,
			//	HorizontalOptions = LayoutOptions.CenterAndExpand,
			//	VerticalOptions = LayoutOptions.Center
			//};

			#region Page Header
			Image headerImg = new Image()
			{
				HorizontalOptions = LayoutOptions.Start,
				//HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.Center,
				Source = Device.OnPlatform("navigationBarBack.png", "imgBack.png", "navigationBarBack.png"),
				HeightRequest = screenHeight / 15.33,
				WidthRequest = screenHeight / 15.33,
				Margin = new Thickness(screenHeight / 147.2, 0, 0, 0)
			};
			BoxView space = new BoxView()
			{
				HeightRequest = screenHeight / 25.02
			};
			BoxView space1 = new BoxView()
			{
				HeightRequest = screenHeight / 14.72
			};

			//CustomLabel lblOTPVerification = new CustomLabel()
			//{
			//	HorizontalOptions = LayoutOptions.Center,
			//	//FontAttributes = FontAttributes.Bold,

			//	//HorizontalTextAlignment = TextAlignment.Center,

			//	//FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
			//	Text = "Password Recovery",
			//	TextColor = Color.White
			//};




			lblTitle = new CustomLabel()
			{
				Text = "Select Location",
				FontSize = screenHeight / 35.30,
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.Center,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
			};

			StackLayout headerStack = new StackLayout()
			{
				//Padding = new Thickness(5),
				Padding = new Thickness(0, Device.OnPlatform(10, 20, 0), 0, 0),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
				Children = { headerImg, space, lblTitle, space1 }
			};
			#endregion
			var imgNavigationTap = new TapGestureRecognizer();
			imgNavigationTap.Tapped += (s, e) =>
			{
				var parentDetailView = (MasterDetailPage)this.Parent;
				parentDetailView.IsPresented = false;
			};
			headerImg.GestureRecognizers.Add(imgNavigationTap);

			//schSearchItems = new CustomEntry()
			//{
			//	Placeholder = "Search",
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//	VerticalOptions = LayoutOptions.CenterAndExpand
			//};
			Image searchiconimg = new Image()
			{
				Source = "imgSearchicon.png",
				HorizontalOptions = LayoutOptions.Start,
				HeightRequest = screenHeight / 18.4,
				WidthRequest = screenHeight / 18.4,

			};
			StackLayout searchstack = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { searchiconimg, schSearchItems },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};
			CustomFrame searchframe = new CustomFrame()
			{
				Content = searchstack,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(screenHeight / 147.2, 0, screenHeight / 35.04, 0),
				HasShadow = false,
				//HeightRequest = screenHeight / 30
			};
			StackLayout searchStackFrame = new StackLayout()
			{
				//Orientation = StackOrientation.Horizontal,
				Children = { searchframe },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(8, 0, 8, 0),
				HeightRequest = 10
			};


			StackLayout headerframe = new StackLayout()
			{
				//Padding = new Thickness(5),
				Padding = new Thickness(0, Device.OnPlatform(0, 20, 0), 0, 0),
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { headerStack, searchStackFrame },
				Spacing = 0,
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				headerframe.HeightRequest = screenHeight / 6;
			}

			stackContent = new HorizontalGradientStack
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				//Spacing = 7,
				Spacing = 0,
				Padding = new Thickness(0, Device.OnPlatform(5, 10, 0), 0, 0),
				Children = {
					//headerStack,
					//searchStackFrame,
					headerframe,
					listView
				}
			};

			AbsoluteLayout.SetLayoutFlags(stackContent, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(stackContent, new Rectangle(0, 0, 1, 1));
			contentLayout.Children.Add(stackContent);

			AbsoluteLayout.SetLayoutFlags(stackOverlay, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(stackOverlay, new Rectangle(0.5, 0.5, 1, 1));
			contentLayout.Children.Add(stackOverlay);

			Content = contentLayout;
		}

		private async void GetLocations()
		{

			if (searchcomplete != "ok")
			{

				//Arearesponse = await new JobListBAL().GetAreaDetails(schSearchItems.Text);

				listView.ItemsSource = null;
				//searchcomplete = "ok";
				//foreach(var item in Arearesponse.predictions)
				//{
				//	listView.ItemsSource =;
				//}
				// fra.IsVisible = true;

			}
			else
			{

				//fra.IsVisible = false;
			}
			searchcomplete = "";

		}
		private async void GetContriesListView()
		{
			try
			{
				PageLoading.IsVisible = true;
				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (ICountryBAL countryList = new CountryBAL())
					{
						stackOverlay.IsVisible = true;
						//countries.Clear();

						try
						{
							countries = new List<CountryData>();
							//await Task.Delay(1000);
							countries = await countryList.GetCountries();
							//await Task.Delay(1000);
							//countries = countries1;
						}

						catch (Exception fgfg)
						{

						}


						//countries = CountriesList.ToList();

						if (countries != null)
						{
							//var listcountries = countries.result.OrderBy(x => x.CountryName).ToList();
							//var listcountries = countries.result.ToList();
							listView.ItemsSource = countries;
							listView.ItemTemplate = new DataTemplate(typeof(CountryDataCell));
							stackOverlay.IsVisible = false;
							schSearchItems.IsEnabled = true;
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
							stackOverlay.IsVisible = false;
							schSearchItems.IsEnabled = false;
						}
					}

				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}



		public class LocationDataCell : ViewCell
		{
			protected override void OnBindingContextChanged()
			{
				base.OnBindingContextChanged();

				dynamic item = BindingContext;

				if (item != null)
				{

					try
					{
						var lblCountryName = new Label()
						{
							HorizontalOptions = LayoutOptions.FillAndExpand,
							//FontSize = Device.OnPlatform(18, 22, 26),
							FontSize = BaseContentPage.screenHeight / 36.8,
							TextColor = Color.Black,
							VerticalTextAlignment = TextAlignment.Center,
							//Text = item.description
							//Text = "Select Location"
						};

						string input = item.description;
						if (input == null)
						{
							lblCountryName.SetBinding(Label.TextProperty, new Binding("description"));
						}
						else
						{
							if (input.Length >= 48)
							{
								string sub = input.Substring(0, 48);
								lblCountryName.Text = sub + "...";
							}
							else
							{
								lblCountryName.Text = item.description;
							}
						}
						//lblCountryName.SetBinding(Label.TextProperty, "description");

						View = new StackLayout()
						{
							HorizontalOptions = LayoutOptions.FillAndExpand,
							Padding = new Thickness(25, 5, 5, 5),
							Children = { lblCountryName }
						};
					}



					catch (Exception ex)
					{
					}

				}

			}
		}
	}
}
