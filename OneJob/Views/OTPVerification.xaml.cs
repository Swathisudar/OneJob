﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace OneJob
{
	public partial class OTPVerification : BaseContentPage
	{
		CustomLabel entryOTPLbl;
		CustomEntry entryOTP;

		string otptestPage;
		string randomNumber;
		string mobileNumber;
		string passwordRegister;
		string lblCountryCode;

		string[] dataTest;

		string deviceType;

		DBMethods objDatabase;
		public OTPVerification(string[] arrayData)
		{
			objDatabase = new DBMethods();
			var info = objDatabase.GetUserInfo();
			InitializeComponent();
			//otpNumber = otp;
			BackgroundColor = Color.FromHex("#4B0000");

			dataTest = arrayData;
			#region Detect Device Type
			if (Device.OS == TargetPlatform.Android)
			{
				deviceType = "android";
			}
			else
			{
				deviceType = "iphone";
			}
			#endregion


			if (dataTest != null)
			{
				otptestPage = dataTest[0];

				if (otptestPage == "forgetpassword")
				{
					randomNumber = dataTest[1];
					mobileNumber = dataTest[2];
					lblCountryCode = dataTest[3];
				}
				else
				{
					randomNumber = dataTest[1];
					mobileNumber = dataTest[2];
					passwordRegister = dataTest[3];
					lblCountryCode = dataTest[4];
				}

			}

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 8) / 100;
			var entryWidth = (width * 81) / 100;


			#region Page Header
			Image headerImg = new Image()
			{
				HorizontalOptions = LayoutOptions.Start,
				Source = Device.OnPlatform("navigationBarBack.png", "imgBack.png", "navigationBarBack.png"),
				HeightRequest = screenHeight / 15.33,
				WidthRequest = screenHeight / 15.33,
				Margin = new Thickness(screenHeight / 147.2, screenHeight / 30, 0, 0)
			};
			BoxView space = new BoxView()
			{
				HeightRequest = screenHeight / 21.02
			};
			BoxView space1 = new BoxView()
			{
				HeightRequest = screenHeight / 14.72
			};

			Label lblOTPVerification = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
				//HorizontalTextAlignment = TextAlignment.Center,
				//FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				Text = "OTP Verification",
				FontSize = screenHeight / 28.30
			};


			#endregion

			#region PageBody


			entryOTP = new CustomEntry()
			{
				Placeholder = "                   Enter OTP",
				//CustomFontSize = fontsize,//15,
				//IsCustomPassword = true,
				TextColor = Color.White,
				PlaceholderColor = Color.White,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				//	IsPassword = true,
				Keyboard = Keyboard.Numeric,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};

			if (Device.OS == TargetPlatform.Android)
			{
				entryOTP.Margin = new Thickness(0, 5, 0, 0);
			}

			entryOTP.TextChanged += entryOTPChanged;

			entryOTPLbl = new CustomLabel()
			{
				Text = "Enter OTP",
				//TextColor = Color.White,
				TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};

			entryOTP.Focused += (object sender, FocusEventArgs e) =>
			{
				//entryOTPLbl.FontSize = 10;
				//entryOTPLbl.IsVisible = true;
				//var textValue = entryOTP.Text;
				//if (textValue.Length == 10 || textValue.Length == 0)
				//{
				entryOTPLbl.IsVisible = true;
				entryOTPLbl.TextColor = Color.White;
				entryOTPLbl.Text = "Enter OTP";
				//}
				//else {
				//	entryOTPLbl.TextColor = Color.White;
				//	entryOTPLbl.Text = "Mobile number must be 10 digits";
				//}

				//entryOTPLbl.Text = "Mobile number";
				entryOTP.PlaceholderColor = Color.Transparent;
				//entryOTP.Placeholder = string.Empty;
			};
			entryOTP.Unfocused += (object sender, FocusEventArgs e) =>
			{
				entryOTPLbl.IsVisible = true;

				if (!string.IsNullOrWhiteSpace(entryOTP.Text))
				{
					//passwordLbl.FontSize = 10;


					var textValue = entryOTP.Text;
					if (textValue.Length >= 4 || textValue.Length == 0)
					{
						entryOTPLbl.IsVisible = true;
						entryOTPLbl.TextColor = Color.White;
						entryOTPLbl.Text = "Enter OTP";
					}
					else
					{
						entryOTPLbl.IsVisible = true;
						entryOTPLbl.TextColor = Color.White;
						entryOTPLbl.Text = "OTP must be 6 digits";
					}


					entryOTP.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					entryOTPLbl.IsVisible = false;
					entryOTPLbl.Text = "Enter OTP";
					entryOTP.PlaceholderColor = Color.White;
					entryOTP.Placeholder = "                     Enter OTP";
				}
				if (string.IsNullOrWhiteSpace(entryOTP.Text))
				{
					entryOTP.Placeholder = "                     Enter OTP";
				}
			};

			BoxView OTPULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout OTPULineStack = new StackLayout()
			{
				Children = { OTPULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};





			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = screenHeight / 73.6,
				//WidthRequest = entryWidth,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};


			pageBody.Children.Add(entryOTPLbl, 0, 0);
			pageBody.Children.Add(OTPULineStack, 0, 0);
			pageBody.Children.Add(entryOTP, 0, 0);

			Label lblTxt = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				Text = "An OTP has been sent. Please verify",
				TextColor = Color.White,
				Opacity = 0.5,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
			};
			Label lblTxtNumber = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
				Opacity = 0.5,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				Text = "your number."
			};
			StackLayout txtStack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { pageBody,

					new ContentView{
						Padding=new Thickness(screenHeight/21.02,0,screenHeight/21.02,0),
						Content=lblTxt
					},
					new ContentView{
						Padding=new Thickness(screenHeight/76.3,0,screenHeight/76.3,0),
						Content=lblTxtNumber
					}
				}

			};

			var btnHeight = (height * 8.5) / 100;

			CustomButton btnVerify = new CustomButton()
			{
				Text = "Verify",
				TextColor = AppGlobalVariables.fontMediumThick,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomButton)),
				FontFamily = AppGlobalVariables.fontFamily45,
				WidthRequest = entryWidth,
				//HeightRequest = btnHeight,
				BorderRadius = 2,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,

				HeightRequest = screenHeight / 14,

				//Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
			};

			btnVerify.Clicked += btnVerify_Click;


			StackLayout bodyStack = new StackLayout()
			{
				Spacing = entryHeight / 2,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = { txtStack, btnVerify }
			};

			StackLayout headerStack = new StackLayout()
			{
				Padding = new Thickness(5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { headerImg, space, lblOTPVerification, space1, bodyStack }
			};


			#endregion



			#region PageFooter

			Label lblNotReceiveOTP = new Label()
			{
				Text = "Not yet received OTP?",
				Opacity = 0.5,
				TextColor = AppGlobalVariables.lightGray,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = screenHeight / 36.8
				//FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
			};

			Label lblResendOTP = new Label()
			{
				Text = "Resend OTP",
				FontFamily = AppGlobalVariables.fontFamily45,
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold,
				FontSize = screenHeight / 36.8
				//FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
			};



			BoxView spaceFooter = new BoxView()
			{
				HeightRequest = (entryHeight * 2) / 4,
				BackgroundColor = Color.Transparent
			};

			StackLayout stackFooter = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { lblNotReceiveOTP, lblResendOTP },
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			#endregion

			VerticalGradientStack MainStack = new VerticalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,


				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { headerStack, stackFooter, spaceFooter }
			};

			PageControlsStackLayout.Children.Add(MainStack);

			var imgNavigationTap = new TapGestureRecognizer();
			imgNavigationTap.Tapped += (s, e) =>
			{
				if (otptestPage == "forgetpassword")
				{
					Navigation.PushModalAsync(new ForgotPassword());
				}
				else
				{
					Navigation.PushModalAsync(new UserRegistration());
				}
			};
			headerImg.GestureRecognizers.Add(imgNavigationTap);

			var lblResentOTap = new TapGestureRecognizer();
			lblResentOTap.Tapped += async (s, e) =>
			{
				try
				{
					OTPReq objOtpReq = new OTPReq();

					Random generator = new Random();
					String randomumber = generator.Next(100000, 999999).ToString("D6");
					objOtpReq.otp = randomumber;
					randomNumber = randomumber;

					objOtpReq.phone_number = lblCountryCode + mobileNumber;

					objOtpReq.otp_for = otptestPage;

					PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (IOtpBAL getOtpDetails = new OtpBAL())
						{
							var otpCheck = await getOtpDetails.mobileOTP(objOtpReq);
							if (otpCheck != null)
							{
								if (otpCheck.Status == 1)
								{
									await DisplayAlert("OneJob", "Please check your mobile for OTP", "Ok");
									PageLoading.IsVisible = false;
								}
								else
								{
									await DisplayAlert("OneJob", otpCheck.Message, "Ok");
									PageLoading.IsVisible = false;
								}
							} 							else 							{ 								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok"); 								PageLoading.IsVisible = false; 							}

							PageLoading.IsVisible = false;
						};

					}
					else
					{ 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					}
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
				PageLoading.IsVisible = false;
			};

			lblResendOTP.GestureRecognizers.Add(lblResentOTap);
		}

		void entryOTPChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 6)
			{
				entryOTP.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 6)
			{
				entryOTPLbl.TextColor = Color.White;
				entryOTPLbl.Text = "Enter OTP";
			}
			else
			{
				entryOTPLbl.TextColor = Color.White;
				entryOTPLbl.Text = "OTP must be 6 digits";
			}
		}

		private async void btnVerify_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(entryOTP.Text))
			{
				entryOTPLbl.IsVisible = true;
				entryOTPLbl.TextColor = Color.White;
				entryOTPLbl.Text = "Please enter OTP";
			}

			else if (entryOTP.Text.Length != 6)
			{
				entryOTPLbl.IsVisible = true;
				entryOTPLbl.TextColor = Color.White;
				entryOTPLbl.Text = "OTP must be 6 digits";
			}
			else if (!Regex.IsMatch(entryOTP.Text, @"^([1-9])([0-9]*)$"))
			{
				entryOTPLbl.IsVisible = true;
				entryOTPLbl.TextColor = Color.White;
				entryOTPLbl.Text = "OTP must be 6 digits";
			}

			else
			{

				DBMethods objDB = new DBMethods();


				var objDeviceToken = objDB.GetDeviceToken();

				SignUpReq signupReqObj = new SignUpReq();
				signupReqObj.phone = mobileNumber;
				signupReqObj.password = passwordRegister;
				signupReqObj.dial_code = lblCountryCode;

				signupReqObj.device_type = deviceType;

				//signupReqObj.device_info = " ";

				if (objDeviceToken != null)
				{
					signupReqObj.device_info = objDeviceToken.DeviceToken;
					signupReqObj.udid = objDeviceToken.DeviceUDID;
				}
				else
				{
					signupReqObj.udid = "";
				}

				//signupReqObj.udid = deviceInfo.DeviceUDID;

				PageLoading.IsVisible = true;


				if (entryOTP.Text == randomNumber)
				{
					if (otptestPage == "forgetpassword")
					{
						try
						{
							await Navigation.PushModalAsync(new ChangePassword(mobileNumber));

						}
						catch (Exception ex)
						{
							PageLoading.IsVisible = false;
							var msg = ex.Message;
						}
					}
					else
					{
						if (CheckNetworkAccess.IsNetworkConnected())
						{
							using (ISignUpBAL signupdetails = new SignUpBAL())
							{
								var signupcheck = await signupdetails.UserSignUp(signupReqObj);

								if (signupcheck != null)
								{
									if (signupcheck.errorCount == "0")
									{


										var saveUserDeatils = new UsersInfo
										{

											UserID = signupcheck.register_id,
											CountryCode = lblCountryCode,
											MobileNumber = signupReqObj.phone

										};

										objDatabase.SaveUserInfo(saveUserDeatils);

										await DisplayAlert("OneJob", signupcheck.successMessage, "Ok");

										Application.Current.Properties["Detail"] = "ProfileLocation";
										App.Current.MainPage = new LocationsMaterPage();

										//await Navigation.PushModalAsync(new UserProfile());
										PageLoading.IsVisible = false;

									}
									else
									{
										await DisplayAlert("OneJob", signupcheck.successMessage, "Ok");
										PageLoading.IsVisible = false;
									}

								}
								else
								{
									await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
									PageLoading.IsVisible = false;
								}

								PageLoading.IsVisible = false;
							}

						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
							PageLoading.IsVisible = false;
						}
					}

					PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", "Invalid OTP", "Ok");
					PageLoading.IsVisible = false;
				}
			}

		}
	}

}
