﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OneJob
{
	public partial class ChangePassword : BaseContentPage
	{

		CustomLabel passwordLbl, rePasswordLbl;
		CustomEntry password, rePassword;
		string MobileNumber;

		public ChangePassword(string mobileNo)
		{
			InitializeComponent();

			MobileNumber = mobileNo;

			BackgroundColor = Color.FromHex("#4B0000");


			int height = screenHeight; //568;
			int width = screenWidth; //320;
			var stackHeight = (height * 6.5) / 100;
			var entryHeight = (height * 8) / 100;
			var entryWidth = (width * 76.36) / 100;
			var headerHeight = (height * 7.5) / 100;
			var header_Height = (height * 7.3) / 100;

			#region Page Header
			Image headerImg = new Image()
			{
				HorizontalOptions = LayoutOptions.Start,
				Source = Device.OnPlatform("navigationBarBack.png", "imgBack.png", "navigationBarBack.png"),
				HeightRequest = screenHeight / 15.33,
				WidthRequest = screenHeight / 15.33,
				Margin = new Thickness(screenHeight / 147.2, screenHeight / 23, 0, 0)
			};
			BoxView space = new BoxView()
			{
				HeightRequest = screenHeight / 21.02
			};
			BoxView space1 = new BoxView()
			{
				HeightRequest = screenHeight / 49.06
			};

			CustomLabel lblOTPVerification = new CustomLabel()
			{
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				TextColor = Color.White,
				//HorizontalTextAlignment = TextAlignment.Center,
				FontSize = screenHeight / 28.30,
				Text = "Setup new Password"
			};


			#endregion

			#region PageBody

			password = new CustomEntry()
			{
				Placeholder = "                  New Password",
				//CustomFontSize = fontsize,//15,
				IsCustomPassword = true,
				TextColor = Color.White,
				PlaceholderColor = Color.White,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				IsPassword = true,
				Keyboard = Keyboard.Email,
				HeightRequest = entryHeight,
				//WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				VerticalOptions = LayoutOptions.End,
			};

			password.TextChanged += pwdChanged;

			passwordLbl = new CustomLabel()
			{
				Text = "New Password",
				//TextColor = Color.White,
				TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				//WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				VerticalOptions = LayoutOptions.Start
			};
			password.Focused += (object sender, FocusEventArgs e) =>
			{

				//var textValue = password.Text;
				//if (textValue.Length >= 8)
				//{
				//	passwordLbl.TextColor = Color.White;
				//	passwordLbl.Text = "Password";
				//}
				//else {
				passwordLbl.TextColor = Color.White;
				passwordLbl.Text = "New Password";
				//}

				passwordLbl.IsVisible = true;
				password.PlaceholderColor = Color.Transparent;
				//password.Placeholder = string.Empty;
			};
			password.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(password.Text))
				{
					//passwordLbl.FontSize = 10;
					passwordLbl.IsVisible = true;

					var textValue = password.Text;
					if (textValue.Length >= 8 || textValue.Length == 0)
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = Color.White;
						passwordLbl.Text = "New Password";
					}
					else
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = Color.White;
						passwordLbl.Text = "New Password must be 8 characters";
					}


					password.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					passwordLbl.IsVisible = false;
					passwordLbl.Text = "New Password";
					password.PlaceholderColor = Color.White;
					password.Placeholder = "                  New Password";
				}
				if (string.IsNullOrWhiteSpace(password.Text))
				{
					password.Placeholder = "                  New Password";
				}
			};



			rePassword = new CustomEntry()
			{
				Placeholder = "              Re-enter new Password",
				//CustomFontSize = fontsize,//15,
				IsCustomPassword = true,
				TextColor = Color.White,
				PlaceholderColor = Color.White,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				IsPassword = true,
				Keyboard = Keyboard.Email,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				VerticalOptions = LayoutOptions.End,
			};


			rePassword.TextChanged += confirmPwdChanged;

			rePasswordLbl = new CustomLabel()
			{
				Text = "Re-enter new Password",
				//TextColor = Color.White,
				TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				//WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				VerticalOptions = LayoutOptions.Start
			};
			rePassword.Focused += (object sender, FocusEventArgs e) =>
			{

				//var textValue = password.Text;
				//if (textValue.Length >= 8)
				//{
				//	passwordLbl.TextColor = Color.White;
				//	passwordLbl.Text = "Password";
				//}
				//else {
				rePasswordLbl.TextColor = Color.White;
				rePasswordLbl.Text = "Re-enter new Password";
				//}

				rePasswordLbl.IsVisible = true;
				rePassword.PlaceholderColor = Color.Transparent;
				//password.Placeholder = string.Empty;
			};
			rePassword.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(password.Text))
				{
					//passwordLbl.FontSize = 10;
					rePasswordLbl.IsVisible = true;

					var textValue = rePassword.Text;
					if (textValue.Length >= 8 || textValue.Length == 0)
					{
						rePasswordLbl.IsVisible = true;
						rePasswordLbl.TextColor = Color.White;
						rePasswordLbl.Text = "Re-enter new Password";
					}
					else
					{
						rePasswordLbl.IsVisible = true;
						rePasswordLbl.TextColor = Color.White;
						rePasswordLbl.Text = "Re-enter new Password must be 8 characters";
					}


					rePassword.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					rePasswordLbl.IsVisible = false;
					rePasswordLbl.Text = "Re-enter new Password";
					rePassword.PlaceholderColor = Color.White;
					rePassword.Placeholder = "                 Re-enter new Password";
				}
				if (string.IsNullOrWhiteSpace(rePassword.Text))
				{
					rePassword.Placeholder = "                 Re-enter new Password";
				}
			};


			BoxView OTPULine = new BoxView()
			{
				HeightRequest = 0.8,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				//WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};




			StackLayout OTPULineStack = new StackLayout()
			{
				Children = { OTPULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};


			BoxView RePWdULine = new BoxView()
			{
				HeightRequest = 0.8,
				Margin = new Thickness(screenHeight / 24.53, 0, screenHeight / 24.53, 0),
				//WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};




			StackLayout RePWdULineStack = new StackLayout()
			{
				Children = { RePWdULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};




			Label lblTxt = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				Text = "An OTP has been sent. Please verify",
				Opacity = 0,
				IsEnabled = false
			};
			Label lblTxtNumber = new Label()
			{
				HorizontalOptions = LayoutOptions.Center,
				Text = "your number.",
				Opacity = 0,
				IsEnabled = false
			};


			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = 0,
				//WidthRequest = entryWidth,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};



			pageBody.Children.Add(passwordLbl, 0, 0);
			pageBody.Children.Add(OTPULineStack, 0, 0);
			pageBody.Children.Add(password, 0, 0);

			pageBody.Children.Add(rePasswordLbl, 0, 1);
			pageBody.Children.Add(RePWdULineStack, 0, 1);
			pageBody.Children.Add(rePassword, 0, 1);




			StackLayout txtStack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { pageBody }
			};

			var btnHeight = (height * 8.5) / 100;

			CustomButton btnVerify = new CustomButton()
			{
				Text = "Save",
				TextColor = AppGlobalVariables.fontMediumThick,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomButton)),
				FontFamily = AppGlobalVariables.fontFamily45,
				//WidthRequest = entryWidth,
				//HeightRequest = btnHeight,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End,

				HeightRequest = screenHeight / 14,

				Margin = new Thickness(screenHeight / 24.53, screenHeight / 24.53, screenHeight / 24.53, 0),
			};

			btnVerify.Clicked += btnSave_Click;


			StackLayout bodyStack = new StackLayout()
			{
				Spacing = 12,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = { txtStack, btnVerify }
			};

			StackLayout headerStack = new StackLayout()
			{
				Padding = new Thickness(5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { headerImg, space, lblOTPVerification, space1, bodyStack }
			};


			#endregion

			#region PageFooter

			Label lblNotReceiveOTP = new Label()
			{
				Text = "Not yet received OTP?"
			};

			Label lblResendOTP = new Label()
			{
				Text = "Resend OTP",
				FontAttributes = FontAttributes.Bold
			};


			#endregion

			VerticalGradientStack MainStack = new VerticalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { headerStack }
			};


			PageControlsStackLayout.Children.Add(MainStack);

			var imgNavigationTap = new TapGestureRecognizer();
			imgNavigationTap.Tapped += (s, e) =>
			{
				Navigation.PushModalAsync(new UserLogin());
			};
			headerImg.GestureRecognizers.Add(imgNavigationTap);

			var lblResentOTPTap = new TapGestureRecognizer();
			lblResentOTPTap.Tapped += (s, e) =>
			{
				Navigation.PushModalAsync(new UserLogin());
			};
			lblResendOTP.GestureRecognizers.Add(lblResentOTPTap);

		}
		#region event to handle when Password is changed
		void pwdChanged(object sender, TextChangedEventArgs e)
		{
			var textValue = password.Text;
			if (textValue.Length >= 8)
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = Color.White;
				passwordLbl.Text = "New Password";
			}
			else
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = Color.White;
				passwordLbl.Text = "New Password must be 8 characters";
			}

		}

		void confirmPwdChanged(object sender, TextChangedEventArgs e)
		{
			var textValue = rePassword.Text;
			if (textValue.Length >= 8)
			{
				rePasswordLbl.IsVisible = true;
				rePasswordLbl.TextColor = Color.White;
				rePasswordLbl.Text = "Re-enter new Password";
			}
			else
			{
				rePasswordLbl.IsVisible = true;
				rePasswordLbl.TextColor = Color.White;
				rePasswordLbl.Text = "Re-enter new Password must be 8 characters";
			}

		}

		#endregion


		private async void btnSave_Click(object sender, EventArgs e)
		{

			if (string.IsNullOrWhiteSpace(password.Text))
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = Color.White;
				passwordLbl.Text = "Please enter New Password";
			}

			else if (string.IsNullOrWhiteSpace(rePassword.Text))
			{
				rePasswordLbl.IsVisible = true;
				rePasswordLbl.TextColor = Color.White;
				rePasswordLbl.Text = "Please enter Re-enter new Password";
			}
			else if (rePassword.Text != password.Text)
			{
				await Task.Delay(100);
				var textValue = rePassword.Text;
				var pwdValue = password.Text;

				if (textValue.Length >= 8 && pwdValue.Length >= 8 && rePassword.Text != password.Text)
				{
					rePasswordLbl.IsVisible = true;
					rePasswordLbl.TextColor = Color.White;
					rePasswordLbl.Text = "Password's did't match";
				}
				else
				{
					if (password.Text.Length < 8 || rePassword.Text.Length < 8)
					{
						if (password.Text.Length < 8)
						{
							passwordLbl.IsVisible = true;
							passwordLbl.TextColor = Color.White;
							passwordLbl.Text = "Password must be 8 characters";
						}
						else
						{
							rePasswordLbl.IsVisible = true;
							rePasswordLbl.TextColor = Color.White;
							rePasswordLbl.Text = "Re-enter new Password must be 8 characters";
						}
					}
				}

				rePasswordLbl.IsVisible = true;
			}
			else if (password.Text.Length < 8 || rePassword.Text.Length < 8)
			{
				if (password.Text.Length < 8)
				{
					passwordLbl.IsVisible = true;
					passwordLbl.TextColor = Color.White;
					passwordLbl.Text = "Password must be 8 characters";
				}
				else
				{
					rePasswordLbl.IsVisible = true;
					rePasswordLbl.TextColor = Color.White;
					rePasswordLbl.Text = "Confirm password must be 8 characters";
				}
			}
			else
			{


				#region ForgetPassword

				try
				{

					ForgetPwdReq forgetReq = new ForgetPwdReq();
					forgetReq.phone = MobileNumber;
					forgetReq.password = password.Text;

					PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{
						using (IForgetPasswordBAL objforgetpwddetails = new ForgetPasswordBAL())
						{
							var objforgetpwdchek = await objforgetpwddetails.getForgetPwd(forgetReq);
							if (objforgetpwdchek != null)
							{
								if (objforgetpwdchek.Status != 1)
								{
									await DisplayAlert("OneJob", objforgetpwdchek.Message, "Ok");
									await Navigation.PushModalAsync(new UserLogin());
									PageLoading.IsVisible = false;
								}
								else
								{
									await DisplayAlert("OneJob", objforgetpwdchek.Message, "Ok");
									PageLoading.IsVisible = false;
								}
							}
							else 							{ 								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok"); 								PageLoading.IsVisible = false; 							} 
						}

					}
					else
					{ 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					}
				}
				catch (Exception ex)
				{
					PageLoading.IsVisible = false;
					var msg = ex.Message;
				}

				#endregion


				#region ChangePWd Service Response 
				//		ChangePwdReq changepwdReq = new ChangePwdReq();

				//		DBMethods objDatabase = new DBMethods();
				//		var getLocalDB = objDatabase.GetUserInfo();
				//		changepwdReq.id = getLocalDB.UserID;

				//		changepwdReq.password = password.Text;

				//		PageLoading.IsVisible = true;
				//		//if (CheckNetworkAccess.IsNetworkConnected())
				//		//{
				//		using (IChangePasswordBAL changepwddetails = new ChangePasswordBAL())
				//		{
				//			var changepwdcheck = await changepwddetails.ChangePwd(changepwdReq);

				//			#region Save UserInfo

				//			//objDatabase = new DatabaseMethods();

				//			//getLocalDB = objDatabase.GetUserInfo();

				//			//var saveUserDeatils = new userInfo
				//			//{

				//			//	UserId = getLocalDB.UserId,
				//			//	Name = getLocalDB.Name,
				//			//	MobileNumber = getLocalDB.MobileNumber,
				//			//	UserProfileImageUrl = getLocalDB.UserProfileImageUrl,
				//			//	EmailId = getLocalDB.EmailId,
				//			//	SecurityCode = getLocalDB.SecurityCode,
				//			//	OldPassword = newpassword.Text
				//			//};
				//			//objDatabase.DeleteUserInfo();

				//			//getLocalDB = objDatabase.GetUserInfo();
				//			//objDatabase.SaveUserInfo(saveUserDeatils);

				//			//getLocalDB = objDatabase.GetUserInfo();

				//			#endregion

				//			if (changepwdcheck != null)
				//			{
				//				if (changepwdcheck.Status != 1)
				//				{
				//					await DisplayAlert("OneJob", changepwdcheck.Message, "Ok");
				//					//App.Current.MainPage = new HomeMasterPage();
				//					//ToastMessage.SetToast(ToastOptions.Info, changepwdcheck.Message);
				//					await Navigation.PushModalAsync(new UserLogin());
				//					PageLoading.IsVisible = false;
				//				}
				//				else {
				//					await DisplayAlert("OneJob", changepwdcheck.Message, "Ok");
				//				}
				//			}
				//			else {
				//				//ToastMessage.SetToast(ToastOptions.Info, changepwdcheck.Message);
				//			}
				//		}

				//		PageLoading.IsVisible = false;
				//	}
				//	//else
				//	//{
				//	//	await Task.Delay(1000);
				//	//	PageLoading.IsVisible = false;
				//	//	ToastMessage.SetToast(ToastOptions.Info, "Please check your internet connection");
				//	//	//await Task.Delay(1500);
				//	//	//PageLoading.IsVisible = false;
				//	//}

				//}

				//		//App.Current.MainPage = new HomeMasterPage();

				#endregion
			}
		}
	}
}
