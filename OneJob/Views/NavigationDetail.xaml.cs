﻿using System;
using Xamarin.Forms;
using TK.CustomMap.Overlays;
using System.Collections.ObjectModel;
using TK.CustomMap;

/* This page is done by Gopi */
namespace OneJob
{
	public partial class NavigationDetail : BaseContentPage
	{

		/* global declarations */

		public ObservableCollection<TKCustomMapPin> _Pins { get; set; }
		public ObservableCollection<TKRoute> _Routes { get; set; }

		public NavigationDetail(Xamarin.Forms.Maps.Position to)
		{
			InitializeComponent();

			Image navigationBack = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
				HeightRequest = BaseContentPage.screenHeight / 15,
				WidthRequest = BaseContentPage.screenHeight / 15,
			};
			TapGestureRecognizer goback = new TapGestureRecognizer();

			goback.Tapped += (object sender, EventArgs e) =>
			{
				Navigation.PopModalAsync();
			};
			navigationBack.GestureRecognizers.Add(goback);

			HorizontalGradientStack headerstack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HeightRequest = screenHeight / 9.55,
				Children =
				{
					navigationBack
				}
			};

			WebView browser = new WebView
			{
				//Source = "http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			PageLoading.IsVisible = true;
			browser.Source = "http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "";
			PageLoading.IsVisible = false;

			StackLayout stack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children =
				{
					headerstack,
					//mapview
					browser
				},
				Spacing = 0
			};

			Content = stack;
		}
	}
}
