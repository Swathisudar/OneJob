﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace OneJob
{
	public partial class CountryMenuPage : BaseContentPage
	{
		public List<CountryData> countries { get; set; }

		//public SearchBar schSearchItems;
		public ListView listView;
		public AbsoluteLayout contentLayout;
		public ActivityIndicator ai;
		public StackLayout stackOverlay;
		public HorizontalGradientStack stackContent;
		CustomEntry schSearchItems;
		public CountryMenuPage()
		{

			//InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);
			this.Icon = null;
			this.Title = "Countries";
			//this.BackgroundColor = Color.FromHex("#F8C03E");

			listView = new ListView(ListViewCachingStrategy.RecycleElement)
			{
				//HasUnevenRows = true,
				SeparatorVisibility = SeparatorVisibility.Default,
				SeparatorColor = Color.Black,
				BackgroundColor = Color.White,
				RowHeight = 50
			};


			//schSearchItems = new SearchBar
			//{
			//	Placeholder = "Search",
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//	TextColor = Color.Black,
			//	BackgroundColor = Color.Transparent,
			//};


			schSearchItems = new CustomEntry()
			{
				Placeholder = "Search",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Image searchiconimg = new Image()
			{
				Source = "imgSearchicon.png",
				HorizontalOptions = LayoutOptions.Start,
				HeightRequest = screenHeight / 18.4,
				WidthRequest = screenHeight / 18.4,

			};
			StackLayout searchstack = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { searchiconimg, schSearchItems },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};
			CustomFrame searchframe = new CustomFrame()
			{
				Content = searchstack,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(screenHeight / 147.2, 0, screenHeight / 35.04, 0),
				HasShadow = false,
				//HeightRequest = screenHeight / 30
			};
			StackLayout searchStackFrame = new StackLayout()
			{
				//Orientation = StackOrientation.Horizontal,
				Children = { searchframe },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(10, 0, 10, 0),
				HeightRequest = 10
			};

			ai = new ActivityIndicator
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Color = Color.FromHex("#F8C03E"),
				IsEnabled = true,
				IsVisible = true,
				IsRunning = true,
			};


			contentLayout = new AbsoluteLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			stackOverlay = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				IsVisible = false,
				Children = { ai }
			};

			GetContriesListView(listView);

			listView.ItemSelected += (sender, e) =>
			 {
				 try
				 {
					 if (e.SelectedItem == null)
					 {
						 return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
					 }
					 var country = (CountryData)e.SelectedItem;
					 var parentDetailView = (MasterDetailPage)this.Parent;
					 parentDetailView.IsPresented = false;
					 schSearchItems.Text = "";
					 //Application.Current.Properties["SelectedCountry"] = "+91";//country.dailCode; 

					 //Application.Current.Properties["ParentPage"] = this;

					 var detail = Application.Current.Properties["Detail"].ToString();

					 if (detail == "Reg")
					 {
						 UserRegistration.regCountryCode.Text = "+" + country.dailCode;
					 }
					 else if (detail == "For")
					 {
						 ForgotPassword.lblCountryCode.Text = "+" + country.dailCode;
					 }
					 else if (detail == "Pro")
					 {
						 EditProfile.lblCountryCode1.Text = "+" + country.dailCode;
					 }
					 else if (detail == "Profile")
					 {
						 UserProfile.profileCountryCode.Text = "+" + country.dailCode;
					 }
					 else
					 {
						 // UserLogin.logCountryCode.Text = "+" + country.dailCode;
					 }

					 HideMenu();
					 //await Navigation.PushModalAsync(new CountryMasterPage());
				 }
				 catch (Exception ex)
				 {
				 }
				 ((ListView)sender).SelectedItem = null;

			 };

			schSearchItems.TextChanged += (sender, args) =>
		   {
			   try
			   {
				   //listView.BeginRefresh();
				   if (countries != null)
				   {
					   if (string.IsNullOrWhiteSpace(args.NewTextValue))
						   //listView.ItemsSource = countries.Result.result.OrderBy(x => x.CountryName).ToList();
						   listView.ItemsSource = countries.ToList();
					   else
						   listView.ItemsSource = countries.Where(i => i.coutryName.ToLower().Contains(args.NewTextValue.ToLower()));
				   }
				   else
				   {

				   }
			   }
			   catch (Exception ex)
			   {
				   var msg = ex.Message;
			   }


		   };


			#region Page Header
			Image headerImg = new Image()
			{
				HorizontalOptions = LayoutOptions.Start,
				//HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.Center,
				Source = Device.OnPlatform("navigationBarBack.png", "imgBack.png", "navigationBarBack.png"),
				HeightRequest = screenHeight / 15.33,
				WidthRequest = screenHeight / 15.33,
				Margin = new Thickness(screenHeight / 147.2, 0, 0, 0)
			};
			BoxView space = new BoxView()
			{
				HeightRequest = screenHeight / 25.02
			};
			BoxView space1 = new BoxView()
			{
				HeightRequest = screenHeight / 14.72
			};

			//CustomLabel lblOTPVerification = new CustomLabel()
			//{
			//	HorizontalOptions = LayoutOptions.Center,
			//	//FontAttributes = FontAttributes.Bold,

			//	//HorizontalTextAlignment = TextAlignment.Center,

			//	//FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
			//	Text = "Password Recovery",
			//	TextColor = Color.White
			//};




			CustomLabel lblTitle = new CustomLabel()
			{
				Text = "Select Country",
				FontSize = screenHeight / 30,
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.Center,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
			};

			StackLayout headerStack = new StackLayout()
			{
				//Padding = new Thickness(5),
				Padding = new Thickness(0, Device.OnPlatform(10, 20, 0), 0, 0),
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
				Children = { headerImg, lblTitle }
			};
			#endregion
			var imgNavigationTap = new TapGestureRecognizer();
			imgNavigationTap.Tapped += (s, e) =>
			{
				var parentDetailView = (MasterDetailPage)this.Parent;
				parentDetailView.IsPresented = false;
			};
			headerImg.GestureRecognizers.Add(imgNavigationTap);

			StackLayout headerframe = new StackLayout()
			{
				//Padding = new Thickness(5),
				Padding = new Thickness(0, Device.OnPlatform(0, 20, 0), 0, 0),
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { headerStack, searchStackFrame },
				Spacing = 0,
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				headerframe.HeightRequest = screenHeight / 6;
			}

			stackContent = new HorizontalGradientStack
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				//Spacing = 7,
				Spacing = 0,
				Padding = new Thickness(0, Device.OnPlatform(5, 10, 0), 0, 0),
				Children = {
					//headerStack,
					//searchStackFrame,
					headerframe,
					listView
				}
			};

			AbsoluteLayout.SetLayoutFlags(stackContent, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(stackContent, new Rectangle(1, 1, 1, 1));
			contentLayout.Children.Add(stackContent);

			AbsoluteLayout.SetLayoutFlags(stackOverlay, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(stackOverlay, new Rectangle(0.5, 0.5, 1, 1));
			contentLayout.Children.Add(stackOverlay);

			Content = contentLayout;
		}

		//**** Retrieve Countries to bind select country picker ****//

		private async void GetContriesListView(ListView listView)
		{
			try
			{
				PageLoading.IsVisible = true;
				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (ICountryBAL countryList = new CountryBAL())
					{
						stackOverlay.IsVisible = true;
						countries = await countryList.GetCountries();
						//countries = CountriesList.ToList();

						if (countries != null)
						{
							//var listcountries = countries.result.OrderBy(x => x.CountryName).ToList();
							//var listcountries = countries.result.ToList();
							listView.ItemsSource = countries;
							listView.ItemTemplate = new DataTemplate(typeof(CountryDataCell));
							stackOverlay.IsVisible = false;
							schSearchItems.IsEnabled = true;
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
							stackOverlay.IsVisible = false;
							schSearchItems.IsEnabled = false;
						}
					}

				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}

	}
	public class CountryDataCell : ViewCell
	{
		public CountryDataCell()
		{
			try
			{
				var lblCountryName = new Label()
				{
					HorizontalOptions = LayoutOptions.Start,
					FontSize = BaseContentPage.screenHeight / 36.8,
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					//Text = "Select country"
				};
				lblCountryName.SetBinding(Label.TextProperty, "coutryName");

				var lblCountryDailCode = new Label()
				{
					HorizontalOptions = LayoutOptions.EndAndExpand,
					FontSize = BaseContentPage.screenHeight / 36.8,
					TextColor = Color.Black,
					VerticalTextAlignment = TextAlignment.Center,
					//Text = "Select country"
				};
				lblCountryDailCode.SetBinding(Label.TextProperty, "dailCode");


				Grid gridMain = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Star},
					//new RowDefinition{Height = GridLength.Auto}

				},
					ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
				},
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					//BackgroundColor = Color.Blue
				};

				gridMain.Children.Add(lblCountryName, 0, 0);
				gridMain.Children.Add(lblCountryDailCode, 1, 0);
				gridMain.Padding = new Thickness(10, 0, 20, 0);
				//StackLayout stack = new StackLayout()
				//{
				//	Orientation = StackOrientation.Horizontal,
				//	HorizontalOptions = LayoutOptions.FillAndExpand,
				//	Padding = new Thickness(25, 5, 10, 5),
				//	Children = { lblCountryName, lblCountryDailCode }
				//};
				View = gridMain;
			}
			catch (Exception ex)
			{
			}
			finally
			{
				GC.Collect();
			}
		}
	}
}

