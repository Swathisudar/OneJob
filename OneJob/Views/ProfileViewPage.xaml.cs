﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace OneJob
{
	public partial class ProfileViewPage : BaseContentPage
	{
		DBMethods objDatabase;
		UsersInfo getLocalDB;
		ImageCircle loadImage;
		CustomLabel lblFavouritesValue;
		CustomLabel lblUserName, lblLocationValue, lblcontactAddressValue, lblSecondaryMobileNoValue, lblSecondaryMobileNoValue1, lblLanguagesValue, lblDateofBirthValue, lblGenderValue, lblPrimaryMobileNoValue, lblPrimaryMobileNoValue1;
		CustomLabel AppliedValue, lblNotificationsValue;
		Image ratingIcons1, ratingIcons2, ratingIcons3, ratingIcons4, ratingIcons5;

		protected override void OnAppearing()
		{
			base.OnAppearing();
			objDatabase = new DBMethods();
			getLocalDB = objDatabase.GetUserInfo();
			//UpdateData(getLocalDB);
			GetProfileData();
			GetJoblist();
			PopulatingAppliedJobs();
			GetNotificationlist();
		}

		public ProfileViewPage()
		{
			InitializeComponent();

			objDatabase = new DBMethods();
			getLocalDB = objDatabase.GetUserInfo();

			BackgroundColor = Color.FromHex("#E0E0E0");

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 7) / 100;
			var entryWidth = (width * 80.5) / 100;
			var entry_Width = (width * 75.36) / 100;
			var headerHeight = (height * 7.9) / 100;
			var headerHeightTop = (height * 7.7) / 100;
			var header_Height = (height * 3) / 100;
			var fontSize3 = (height * 3) / 100;
			var fontSize4 = (height * 3.5) / 100;
			var fontSize1 = (height * 2.5) / 100;//2.5;
			var fontSize2 = (height * 2) / 100;//1.85;//1.7;

			#region Page header
			var image_Height = (screenHeight * 6) / 100;
			var imaPicHeight = screenHeight / 6;

			Image menuImage = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				HeightRequest = image_Height,
				WidthRequest = image_Height,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End,
				Margin = new Thickness(entryWidth / 34, 0, 0, entryWidth / 45)
			};

			Image imageOpenList = new Image()
			{
				Source = ImageSource.FromFile("imgEdit.png"),
				HeightRequest = image_Height,
				WidthRequest = image_Height,
				HorizontalOptions = LayoutOptions.End
			};


			#region PopUp

			CustomLabel labelEdit = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Edit Profile",
				TextColor = Color.FromHex("#505050"),
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.Start
			};



			CustomLabel labelArchive = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Log Out",
				TextColor = Color.FromHex("#505050"),
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.Start
			};

			//StackLayout stackPopUp = new StackLayout()
			//{

			//	IsVisible = false,
			//	Padding = new Thickness(15, 10, 0, 10),
			//	Spacing = 15,
			//	BackgroundColor = Color.White,
			//	Children = { labelEdit, labelArchive }
			//};

			//var labelEditTap = new TapGestureRecognizer();
			//labelEditTap.Tapped += (s, e) =>
			//{
			//	stackPopUp.IsVisible = false;
			//	Navigation.PushModalAsync(new EditProfile(null));
			//};
			//labelEdit.GestureRecognizers.Add(labelEditTap);


			//var labelArchiveTap = new TapGestureRecognizer();
			//labelArchiveTap.Tapped += (s, e) =>
			//{
			//	try
			//	{
			//		PageLoading.IsVisible = true;
			//		stackPopUp.IsVisible = false;
			//		objDatabase.DeleteUserInfo();
			//		objDatabase.DeleteWorkType();
			//		objDatabase.DeleteLanguages();
			//		PageLoading.IsVisible = false;
			//		App.Current.MainPage = new Carousel();
			//		//Navigation.PushModalAsync(new UserLogin());
			//	}
			//	catch (Exception ex)
			//	{
			//		var msg = ex.Message;
			//	}
			//	//stackPopUp.IsVisible = false; popupFrame.IsVisible = true;
			//	//Navigation.PushModalAsync(new EditProfile());
			//};
			//labelArchive.GestureRecognizers.Add(labelArchiveTap);
			TapGestureRecognizer menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {
			   //stackPopUp.IsVisible = false;
			   //try
			   //{
			   // var ParentPage = (MasterDetailPage)this.Parent;
			   // ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			   //}
			   //catch (Exception ex)
			   //{
			   // var msg = ex.Message;
			   //}
			   //IGeolocator locator = CrossGeolocator.Current;
			   var ParentPage = (MasterDetailPage)this.Parent;
			   ParentPage.Detail = new HomePage() { BackgroundColor = Color.White };
		   };
			menuImage.GestureRecognizers.Add(menuClicked);



			var imageOpenListTap = new TapGestureRecognizer();
			imageOpenListTap.Tapped += async (s, e) =>
			{
				var naviagate = await DisplayAlert("OneJob", "Do you want to edit your profile ?", "Ok", "Cancel");
				if (naviagate == true)
				{
					try
					{
						Application.Current.Properties["Detail"] = "EditPro";
						Application.Current.Properties["Detail"] = "EditProfileLocation";
						App.Current.MainPage = new LocationsMaterPage();
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					}
				}
				else
				{
				}
				//Navigation.PushModalAsync(new EditProfile(null));
				//stackPopUp.IsVisible = true;
				//popupFrame.IsVisible = true;
			};

			imageOpenList.GestureRecognizers.Add(imageOpenListTap);


			#endregion


			#region for header body
			CustomLabel headerTitle = new CustomLabel()
			{
				Margin = new Thickness(0, 0, 0, entryWidth / 19),
				Text = "Profile",
				//TextColor = AppGlobalVariables.lightGray,
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.End
			};


			loadImage = new ImageCircle()
			{

				HeightRequest = imaPicHeight,
				WidthRequest = imaPicHeight,
				Source = ImageSource.FromFile("Avatar.png"),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};


			//if (getLocalDB != null)
			//{

			//	if (getLocalDB.UserProfilePic != null && getLocalDB.UserProfilePic != "")
			//	{
			//		loadImage.Source = new UriImageSource()
			//		{
			//			Uri = new Uri(getLocalDB.UserProfilePic),
			//			CachingEnabled = false
			//		};
			//	}
			//	else
			//	{
			//		loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			//	}
			//}
			//else
			//{
			//	loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
			//}



			Image cameraPic = new Image()
			{

				Source = ImageSource.FromFile("cameraIcon.png")
			};

			lblUserName = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = getLocalDB.UserFirstName + " " + getLocalDB.UserLastName,
				FontSize = fontSize3,
				//FontSize = Device.GetNamedSize(NamedSize.Large, typeof(CustomLabel)),
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White
			};









			#region for rating stars


			ratingIcons1 = new Image() { };
			ratingIcons2 = new Image() { };
			ratingIcons3 = new Image() { };
			ratingIcons4 = new Image() { };
			ratingIcons5 = new Image() { };

			//var getLocalDB = objDatabase.GetUserInfo();



			Grid ratingHolder = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
				Padding = new Thickness(0, 0, 0, 0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				//WidthRequest = width * 15.62,
				HeightRequest = entryHeight / 3,
				WidthRequest = entryWidth / 4,
				//HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};
			ratingHolder.Children.Add(ratingIcons1, 0, 0);
			ratingHolder.Children.Add(ratingIcons2, 1, 0);
			ratingHolder.Children.Add(ratingIcons3, 2, 0);
			ratingHolder.Children.Add(ratingIcons4, 3, 0);
			ratingHolder.Children.Add(ratingIcons5, 4, 0);

			#endregion


			AbsoluteLayout imageAbsLayout = new AbsoluteLayout()
			{
				//WidthRequest = width / 4,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(loadImage, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(loadImage, AbsoluteLayoutFlags.All);
			imageAbsLayout.Children.Add(loadImage);

			AbsoluteLayout.SetLayoutBounds(cameraPic, new Rectangle(1.4, 1, 0.5, 0.5));
			AbsoluteLayout.SetLayoutFlags(cameraPic, AbsoluteLayoutFlags.All);
			imageAbsLayout.Children.Add(cameraPic);


			var imgback = new StackLayout()
			{
				Children = {
					menuImage,
					headerTitle,
					imageOpenList
				},
				Padding = new Thickness(1, 10, 0, 0),
				Orientation = StackOrientation.Horizontal,
				BackgroundColor = Color.Transparent,
				HeightRequest = headerHeightTop,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};

			var imgbackTap = new TapGestureRecognizer();
			imgbackTap.Tapped += (s, e) =>
			{
				//stackPopUp.IsVisible = false;
			};
			imgback.GestureRecognizers.Add(imgbackTap);

			BoxView FavBox = new BoxView()
			{
				WidthRequest = 1.5,
				HeightRequest = entryHeight / 2.5,
				HorizontalOptions = LayoutOptions.End,
				BackgroundColor = AppGlobalVariables.underLineColor
			};
			lblFavouritesValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "16",
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(CustomLabel)),
			};

			//var FavCount = Application.Current.Properties["FavCount"].ToString();
			//if (FavCount != null)
			//{
			//	lblFavouritesValue.Text = FavCount;
			//}
			//else
			//{
			//	lblFavouritesValue.Text = "0";
			//}




			CustomLabel lblFavourites = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Favourites",
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(CustomLabel)),
				TextColor = Color.White
			};

			BoxView ApplyBox = new BoxView()
			{
				WidthRequest = 1.5,
				HeightRequest = entryHeight / 2.5,
				//HeightRequest = screenHeight / 29.44,
				HorizontalOptions = LayoutOptions.Start,
				BackgroundColor = AppGlobalVariables.underLineColor
			};

			AppliedValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "0",
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(CustomLabel)),
			};



			//var AppliedCount = Application.Current.Properties["AllpiedCount"].ToString();
			//if (AppliedCount != null)
			//{
			//	AppliedValue.Text = AppliedCount;
			//}
			//else
			//{
			//	AppliedValue.Text = "0";
			//}

			CustomLabel Applied = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Applied",
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(CustomLabel)),
			};


			lblNotificationsValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HorizontalOptions = LayoutOptions.Center,
				TextColor = Color.White,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(CustomLabel)),
			};
			CustomLabel lblNotifications = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Notifications",
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(CustomLabel)),
			};

			StackLayout stackFavourites = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.Center,
				Spacing = 0,
				Children = { lblFavouritesValue, lblFavourites }
			};

			StackLayout stackApplied = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.Center,
				Spacing = 0,
				Children = { AppliedValue, Applied }
			};

			StackLayout stackNotifications = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.Center,
				Spacing = 0,
				Children = { lblNotificationsValue, lblNotifications }
			};

			StackLayout Picstack = new StackLayout()
			{
				Spacing = 0,
				Children = { loadImage, lblUserName, ratingHolder }
			};

			//StackLayout stackHeaderBottom = new StackLayout()
			//{
			//	BackgroundColor = Color.Green,
			//	Orientation = StackOrientation.Horizontal,
			//	Padding = new Thickness((width*11)/100, 0, 12, 0),
			//	Spacing = 0,
			//	Children = { stackFavourites, FavBox, stackApplied, ApplyBox, stackNotifications }
			//};

			Grid stackHeaderBottom = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = width,
				Padding = new Thickness(7, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			stackHeaderBottom.Children.Add(stackFavourites, 0, 0);
			stackHeaderBottom.Children.Add(FavBox, 0, 0);
			stackHeaderBottom.Children.Add(stackApplied, 1, 0);
			stackHeaderBottom.Children.Add(ApplyBox, 2, 0);
			stackHeaderBottom.Children.Add(stackNotifications, 2, 0);

			HorizontalGradientStack HeaderLogo = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { Picstack, stackHeaderBottom },
				//Orientation = StackOrientation.Horizontal,
				Spacing = entryWidth / 15,
				Padding = new Thickness(1, 0, 8, 0),
				BackgroundColor = Color.FromHex("#707070"),
				HeightRequest = headerHeight * 4.8,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};


			HorizontalGradientStack HeaderStack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { imgback },
				//Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Padding = new Thickness(1, 0, 10, 0),
				BackgroundColor = Color.Transparent,
				HeightRequest = headerHeight * 1.5 - 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};


			var HeaderStackTap = new TapGestureRecognizer();
			HeaderStackTap.Tapped += (s, e) =>
			{
				//stackPopUp.IsVisible = false;
			};
			HeaderStack.GestureRecognizers.Add(HeaderStackTap);
			#endregion

			//HorizontalGradientStack stackLogo = new HorizontalGradientStack()
			//{
			//	StartColor = AppGlobalVariables.lightMarron,
			//	EndColor = AppGlobalVariables.darkMarron,
			//	Children = { imageAbsLayout },
			//	//Orientation = StackOrientation.Horizontal,
			//	Spacing = 0,
			//	Padding = new Thickness(1, 0, 10, 0),
			//	BackgroundColor = Color.FromHex("#707070"),
			//	HeightRequest = headerHeight * 2.6,
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//	VerticalOptions = LayoutOptions.Start
			//};

			CustomLabel lblPernoalDetals = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Personal Details",
				TextColor = AppGlobalVariables.lightBlue,
				FontFamily = AppGlobalVariables.fontFamily65,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(CustomLabel))
			};


			BoxView FirstNameULine = new BoxView()
			{
				HeightRequest = 2,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start,
			};

			StackLayout stackPersonal = new StackLayout()
			{
				Children = { lblPernoalDetals, FirstNameULine }
			};


			CustomLabel lblDateofBirth = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontLessThick,
				Text = "Date of birth",
				FontSize = fontSize2

			};
			lblDateofBirthValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "22-03-1992",
				//Text = getLocalDB.dateOFBirth,
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};


			StackLayout stackDateOfBirth = new StackLayout()
			{
				Spacing = 0,
				Children = { lblDateofBirth, lblDateofBirthValue }
			};

			CustomLabel lblGender = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Gender:",
				FontSize = fontSize2,
				TextColor = AppGlobalVariables.fontLessThick,
			};

			lblGenderValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "Male",
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};



			StackLayout stackGender = new StackLayout()
			{
				//Orientation = StackOrientation.Horizontal,
				Children = { lblGender, lblGenderValue },
				Spacing = 0
			};


			CustomLabel lblPrimaryMobileNo = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Primary mobile number",
				FontSize = fontSize2,
				TextColor = AppGlobalVariables.fontLessThick,
			};

			lblPrimaryMobileNoValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = getLocalDB.CountryCode,
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};



			lblPrimaryMobileNoValue1 = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = getLocalDB.optionalMobileNumber,
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};



			StackLayout stackPrimaryValue = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { lblPrimaryMobileNoValue, lblPrimaryMobileNoValue1 }
			};

			StackLayout stackPrimary = new StackLayout()
			{
				Spacing = 0,
				Children = { lblPrimaryMobileNo, stackPrimaryValue }
			};


			CustomLabel lblSecondaryMobileNo = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Secondary mobile number",
				FontSize = fontSize2,
				TextColor = AppGlobalVariables.fontLessThick,

			};

			lblSecondaryMobileNoValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = getLocalDB.CountryCode,
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};



			lblSecondaryMobileNoValue1 = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = getLocalDB.optionalMobileNumber,
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick,

			};




			StackLayout stackSecondaryValue = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { lblSecondaryMobileNoValue, lblSecondaryMobileNoValue1 }
			};

			StackLayout stackSecondary = new StackLayout()
			{
				Spacing = 0,
				Children = { lblSecondaryMobileNo, stackSecondaryValue }
			};



			CustomLabel lblLanguages = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Languages you speak",
				FontSize = fontSize2,
				TextColor = AppGlobalVariables.fontLessThick,

			};


			lblLanguagesValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "English, French",
				//Text = landata + ".",
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};






			StackLayout stackLanguages = new StackLayout()
			{
				Spacing = 0,
				Children = { lblLanguages, lblLanguagesValue }
			};


			CustomLabel lblLocation = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				Text = "Location",
				FontSize = fontSize2,
				TextColor = AppGlobalVariables.fontLessThick,
			};

			lblLocationValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "New York",
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};




			StackLayout stackLocation = new StackLayout()
			{
				Spacing = 0,
				Children = { lblLocation, lblLocationValue }
			};


			CustomLabel lblcontactAddress = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = fontSize2,
				Text = "Contact address"
			};

			lblcontactAddressValue = new CustomLabel()
			{
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed dore eiusmod tempor.",
				//Text = getLocalDB.Address,
				FontSize = fontSize1,
				TextColor = AppGlobalVariables.fontVeryThick
			};




			StackLayout stackAddress = new StackLayout()
			{
				Spacing = 0,
				Children = { lblcontactAddress, lblcontactAddressValue }
			};




			Grid bodyGrid = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},

					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}

					//new RowDefinition{Height = GridLength.Auto},
					//new RowDefinition{Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				//RowSpacing = entryWidth / 35,
				RowSpacing = 8,

				Padding = new Thickness(entry_Width / 8, 0, entry_Width / 8, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};



			BoxView spaceFooter = new BoxView()
			{
				HeightRequest = (entryHeight * 2) / 4,
				BackgroundColor = Color.Transparent
			};


			bodyGrid.Children.Add(stackPersonal, 0, 0);

			bodyGrid.Children.Add(stackDateOfBirth, 0, 1);

			bodyGrid.Children.Add(stackGender, 0, 2);

			bodyGrid.Children.Add(stackPrimary, 0, 3);


			bodyGrid.Children.Add(stackSecondary, 0, 4);

			bodyGrid.Children.Add(stackLanguages, 0, 5);


			bodyGrid.Children.Add(stackLocation, 0, 6);

			bodyGrid.Children.Add(stackAddress, 0, 7);

			bodyGrid.Children.Add(spaceFooter, 0, 9);

			Grid MainGrid = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Star}

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = entryWidth,
				RowSpacing = entryWidth / 11,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand
			};
			MainGrid.Children.Add(HeaderLogo, 0, 0);
			MainGrid.Children.Add(bodyGrid, 0, 1);


			#endregion

			StackLayout bodyHolder = new StackLayout()
			{
				Children = { MainGrid },
				Spacing = (entryHeight * 3) / 4,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			var bodyHolderTap = new TapGestureRecognizer();
			bodyHolderTap.Tapped += (s, e) =>
			{
				//stackPopUp.IsVisible = false;
				//popupFrame.IsVisible = true;
			};
			bodyHolder.GestureRecognizers.Add(bodyHolderTap);


			ScrollView scrollHolder = new ScrollView()
			{
				Content = bodyHolder
			};


			StackLayout holder = new StackLayout()
			{
				Children = { HeaderStack, scrollHolder },
				//Spacing = (entryHeight * 3) / 6,
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			var holderTap = new TapGestureRecognizer();
			holderTap.Tapped += (s, e) =>
			{
				//stackPopUp.IsVisible = false;
				//popupFrame.IsVisible = true;
			};

			HeaderStack.GestureRecognizers.Add(holderTap);



			AbsoluteLayout content_Layout = new AbsoluteLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};


			AbsoluteLayout.SetLayoutFlags(holder, AbsoluteLayoutFlags.All);
			AbsoluteLayout.SetLayoutBounds(holder, new Rectangle(0, 0, 1, 1));
			content_Layout.Children.Add(holder);

			//if (Device.OS == TargetPlatform.iOS)
			//{
			//	AbsoluteLayout.SetLayoutFlags(stackPopUp, AbsoluteLayoutFlags.All);
			//	AbsoluteLayout.SetLayoutBounds(stackPopUp, new Rectangle(0.95, 0.06, 0.40, 0.15));
			//	content_Layout.Children.Add(stackPopUp);
			//}
			//else
			//{
			//	AbsoluteLayout.SetLayoutFlags(stackPopUp, AbsoluteLayoutFlags.All);
			//	AbsoluteLayout.SetLayoutBounds(stackPopUp, new Rectangle(0.95, 0.06, 0.40, 0.15));
			//	content_Layout.Children.Add(stackPopUp);
			//}
			PageControlsStackLayout.Children.Add(content_Layout);
		}

		//#region important code for future editings dont delete
		//void UpdateData(UsersInfo getLocalDB)
		//{
		//	try
		//	{
		//		objDatabase = new DBMethods();

		//		if (objDatabase != null)
		//		{
		//			getLocalDB = objDatabase.GetUserInfo();


		//			string landata = "";
		//			var getLanDB = objDatabase.GetLaguages();
		//			if (getLanDB != null)
		//			{
		//				foreach (var item in getLanDB)
		//				{
		//					if (landata == "")
		//					{
		//						landata = item.LanguageName;
		//					}
		//					else
		//					{
		//						landata = landata + "," + item.LanguageName;
		//					}
		//				}
		//			}
		//			else
		//			{

		//			}

		//			if (getLocalDB.Gender == "male" || getLocalDB.Gender == "Male")
		//			{
		//				lblGenderValue.Text = "Male";
		//			}
		//			else
		//			{
		//				lblGenderValue.Text = "Female";
		//			}


		//			if (getLocalDB.UserProfilePic != null && getLocalDB.UserProfilePic != "")
		//			{
		//				loadImage.Source = new UriImageSource()
		//				{
		//					Uri = new Uri(getLocalDB.UserProfilePic),
		//					CachingEnabled = false
		//				};
		//			}
		//			else
		//			{
		//				loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
		//			}

		//			lblUserName.Text = getLocalDB.UserFirstName + " " + getLocalDB.UserLastName;
		//			lblLocationValue.Text = getLocalDB.LocationName;
		//			lblcontactAddressValue.Text = getLocalDB.Address;
		//			lblSecondaryMobileNoValue.Text = getLocalDB.CountryCode;
		//			lblSecondaryMobileNoValue1.Text = getLocalDB.OptionalMobile;
		//			lblLanguagesValue.Text = landata + ".";
		//			lblDateofBirthValue.Text = getLocalDB.dateOFBirth;
		//			lblPrimaryMobileNoValue.Text = getLocalDB.CountryCode;
		//			lblPrimaryMobileNoValue1.Text = getLocalDB.MobileNumber;
		//		}
		//		else
		//		{
		//			loadImage.Source = Device.OnPlatform("Avatar.png", "Avatar.png", "Avatar.png");
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		var msg = ex.Message;
		//	}
		//}
		//#endregion

		private void btnUpdate_Clicked(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new UserProfile());
		}
		string landata = "";
		#region GetProfileData from Service
		public async void GetProfileData()
		{
			try
			{
				GetProfileReq objGetProfioeReq = new GetProfileReq();
				if (getLocalDB != null)
				{
					objGetProfioeReq.seeker_id = getLocalDB.UserID;
				}


				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{
					using (IGetProfileBAL selectWorkTypeList = new GetProfileBAL())
					{
						var objGetProfile = await selectWorkTypeList.GetProfileData(objGetProfioeReq);
						if (objGetProfile != null)
						{
							if (objGetProfile.errorCount == 0)
							{
								PageLoading.IsVisible = false;

								#region Binding data for controls
								if (objGetProfile.userDetails.profilePic != null && objGetProfile.userDetails.profilePic != "")
								{
									if (objGetProfile.userDetails.profilePic != null && objGetProfile.userDetails.profilePic != "")
									{
										loadImage.Source = new UriImageSource()
										{
											Uri = new Uri(getLocalDB.UserProfilePic),
											CachingEnabled = false
										};
									}
									else
									{
										loadImage.Source = ImageSource.FromFile(Device.OnPlatform("imgJob.png", "imgJob.png", "imgJob.png"));
									}

								}
								if (objGetProfile.userDetails.firstName != null && objGetProfile.userDetails.firstName != "")
								{
									lblUserName.Text = objGetProfile.userDetails.firstName + " " + objGetProfile.userDetails.lastName;
								}


								if (objGetProfile.userDetails.dateOfBirth != null && objGetProfile.userDetails.dateOfBirth != "")
								{
									try
									{
										var dOBData = getLocalDB.dateOFBirth.Split('-');
										lblDateofBirthValue.Text = dOBData[2] + "-" + dOBData[1] + "-" + dOBData[0];
									}
									catch (Exception ex)
									{
										lblDateofBirthValue.Text = getLocalDB.dateOFBirth;
										var msg = ex.Message;
									}
								}


								if (objGetProfile.userDetails.seekerRating != null && objGetProfile.userDetails.seekerRating != "")
								{
									#region Rating Logic

									try
									{
										var rate = Double.Parse(getLocalDB.userRating);
										if (rate != null)
										{
											if (rate <= 0.4)
											{
												ratingIcons1.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons2.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons3.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons4.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
											}
											else if (rate == 0.5 || rate <= 1.4)
											{
												ratingIcons1.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons2.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons3.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons4.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
											}
											else if (rate == 1.5 || rate <= 2.4)
											{
												ratingIcons1.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons2.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons3.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons4.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
											}
											else if (rate == 2.5 || rate <= 3.4)
											{
												ratingIcons1.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons2.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons3.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons4.Source = ImageSource.FromFile("imgProfileRateNone");
												ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
											}
											else if (rate == 3.5 || rate <= 4.4)
											{
												ratingIcons1.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons2.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons3.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons4.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
											}
											else if (rate >= 4.5)
											{
												ratingIcons1.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons2.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons3.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons4.Source = ImageSource.FromFile("imgProfileRateDone");
												ratingIcons5.Source = ImageSource.FromFile("imgProfileRateDone");
											}

										}
										else
										{
											ratingIcons1.Source = ImageSource.FromFile("imgProfileRateNone");
											ratingIcons2.Source = ImageSource.FromFile("imgProfileRateNone");
											ratingIcons3.Source = ImageSource.FromFile("imgProfileRateNone");
											ratingIcons4.Source = ImageSource.FromFile("imgProfileRateNone");
											ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
										}

									}
									catch (Exception ex)
									{
										ratingIcons1.Source = ImageSource.FromFile("imgProfileRateNone");
										ratingIcons2.Source = ImageSource.FromFile("imgProfileRateNone");
										ratingIcons3.Source = ImageSource.FromFile("imgProfileRateNone");
										ratingIcons4.Source = ImageSource.FromFile("imgProfileRateNone");
										ratingIcons5.Source = ImageSource.FromFile("imgProfileRateNone");
									}

									#endregion
								}
								if (objGetProfile.userDetails.Gender != null && objGetProfile.userDetails.Gender != "")
								{

									if (objGetProfile.userDetails.Gender != null && objGetProfile.userDetails.Gender != "")
									{
										if (objGetProfile.userDetails.Gender == "male" || objGetProfile.userDetails.Gender == "Male")
										{
											lblGenderValue.Text = "Male";

										}
										else
										{
											lblGenderValue.Text = "Female";
										}
									}
								}


								if (objGetProfile.userDetails.dailCode != null && objGetProfile.userDetails.dailCode != "")
								{
									lblPrimaryMobileNoValue.Text = objGetProfile.userDetails.dailCode;
								}

								if (objGetProfile.userDetails.primaryPhone != null && objGetProfile.userDetails.primaryPhone != "")
								{
									lblPrimaryMobileNoValue1.Text = objGetProfile.userDetails.primaryPhone; ;
								}


								if (objGetProfile.userDetails.optionalCoutryCode != null && objGetProfile.userDetails.optionalCoutryCode != "")
								{
									try
									{
										if (objGetProfile.userDetails.optionalCoutryCode != null && !string.IsNullOrEmpty(objGetProfile.userDetails.optionalCoutryCode))
										{
											lblSecondaryMobileNoValue.Text = objGetProfile.userDetails.optionalCoutryCode;
											//lblSecondaryMobileNoValue.Text = getLocalDB.CountryCode;
										}

									}
									catch (Exception ex)
									{
										var msg = ex.Message;
									}
								}

								if (objGetProfile.userDetails.optionalPhone != null && objGetProfile.userDetails.optionalPhone != "")
								{

									lblSecondaryMobileNoValue1.Text = objGetProfile.userDetails.optionalPhone;
								}


								if (objGetProfile.userDetails.getProfileLanguagesList != null)
								{

									var lanData = objGetProfile.userDetails.getProfileLanguagesList;
									foreach (var item in lanData)
									{
										if (landata == "")
										{
											landata = item.languageName;
										}
										else
										{
											landata = landata + "," + item.languageName;
										}
									}
									if (landata.Length > 1)
									{
										lblLanguagesValue.Text = landata + ".";
									}
								}


								if (objGetProfile.userDetails.Location != null && objGetProfile.userDetails.Location != "")
								{
									lblLocationValue.Text = objGetProfile.userDetails.Location;
								}



								if (objGetProfile.userDetails.Address != null && objGetProfile.userDetails.Address != "")
								{
									lblcontactAddressValue.Text = objGetProfile.userDetails.Address;
								}


								#endregion

							}

						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}
					}
					PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				PageLoading.IsVisible = false;
			}
		}

		#endregion


		#region Count Value


		async void GetNotificationlist()
		{
			try
			{

				var getLocaDB = objDatabase.GetUserInfo();

				NotificationReq obj = new NotificationReq();

				if (getLocaDB != null)
				{
					obj.seeker_id = getLocaDB.UserID;
				}

				//obj.seeker_id = "93";

				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (INotificationBAL JList = new NotificationBAL())
					{
						var joblist = await JList.NotifiList(obj);

						if (joblist != null)
						{
							lblNotificationsValue.Text = joblist.notificationJobs.Count.ToString();
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}

					}
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}



		#region for service to populate Applied jobs ListView
		public async void PopulatingAppliedJobs()
		{
			try
			{
				PageLoading.IsVisible = true;

				if (getLocalDB != null && !string.IsNullOrEmpty(getLocalDB.UserID))
				{
					SeekerRelatedJobsReq SRJobs = new SeekerRelatedJobsReq()
					{
						seeker_id = getLocalDB.UserID,
						latitude = getLocalDB._localLat,
						longitude = getLocalDB._localLog
					};

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (ISeekerRelatedJobsBAL appliedJobs = new SeekerRelatedJobsBAL())
						{
							var jobResponse = await appliedJobs.SeekerRelatedJob(SRJobs);

							if (jobResponse != null)
							{
								AppliedValue.Text = jobResponse.JobDetails.Count.ToString();
								PageLoading.IsVisible = false;
							}
							else
							{
								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								PageLoading.IsVisible = false;
							}

						}


					}
					else
					{
						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
						PageLoading.IsVisible = false;
					}
				}
				else
				{
					await DisplayAlert("OneJob", "Please kindly Login/Register", "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
			PageLoading.IsVisible = false;
		}


		#endregion



		public async void GetJoblist()
		{
			try
			{
				ObservableCollection<JobsListInfo> joblist = new ObservableCollection<JobsListInfo>();
				DBMethods objDB = new DBMethods();
				var getLocalDB = objDB.GetUserInfo();

				if (getLocalDB != null)
				{
					FavJobListReq objFavListReq = new FavJobListReq();

					objFavListReq.seeker_id = getLocalDB.UserID;
					PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (IFavJobListBAL JList = new FavJobListBAL())
						{
							var jobslist = await JList.GetFavJobList(objFavListReq);
							List<JobsListInfo> listcount1 = new List<JobsListInfo>();


							if (jobslist != null)
							{
								foreach (var item in jobslist)
								{
									if (item.Status == "active")
									{
										listcount1.Add(new JobsListInfo
										{
											amountType = item.amountType,
											Favourite = item.Favourite,
											companyName = item.companyName,
											Compensention = item.Compensention,
											createdDate = item.createdDate,
											jobId = item.jobId,
											Status = item.Status,
											Distance = item.Distance,
											endDate = item.endDate,
											Experience = item.Experience,
											FaceBook = item.FaceBook,
											firstName = item.firstName,
											jobAddress = item.jobAddress,
											jobCity = item.jobCity,
											jobCountryId = item.jobCountryId,
											jobDescription = item.jobDescription,
											jobEmail = item.jobEmail,
											jobStateId = item.jobStateId,
											jobTitle = item.jobTitle,
											jobZipCode = item.jobZipCode,
											lastName = item.lastName,
											Latitude = item.Latitude,
											linkesIn = item.linkesIn,
											Longitude = item.Longitude,
											modifiedDate = item.modifiedDate,
											Name = item.Name,
											noOfPositions = item.noOfPositions,
											perHourDay = item.perHourDay,
											profile_pic = item.profile_pic,
											Providerid = item.Providerid,
											providerPhoneNumber = item.providerPhoneNumber,
											ProviderRating = item.ProviderRating,
											startDate = item.startDate,
											Twitter = item.Twitter,
											workTypeID = item.workTypeID,
											workTypeName = item.workTypeName,
											Applied = item.Applied,
											isInterview = item.isInterview,

										});

									}

									if (listcount1.Count == 0)
									{
										lblFavouritesValue.Text = "0";
									}
									else
									{
										lblFavouritesValue.Text = listcount1.Count.ToString();
									}

									PageLoading.IsVisible = false;

								}


							}
							else
							{
								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								PageLoading.IsVisible = false;
							}

						}

						PageLoading.IsVisible = false;

					}
					else
					{
						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
						PageLoading.IsVisible = false;
					}
				}

				else
				{
					await DisplayAlert("OneJob ", "Not yet register?", "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}


		#endregion

	}
}
