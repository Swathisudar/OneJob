﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

/* This page is done by Gopi */

namespace OneJob
{
	public partial class InfoPage : BaseContentPage
	{
		/* global declarations */
		CustomLabel lblAppInfo;
		public InfoPage()
		{
			InitializeComponent();
			BackgroundColor = Color.White;

			GetAppInfo();
			var height = (screenHeight * 1) / 100;
			var headerChildrenHeight = height * 7;
			Image navigationBack = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				HorizontalOptions = LayoutOptions.StartAndExpand,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenHeight,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			TapGestureRecognizer goback = new TapGestureRecognizer();

			goback.Tapped += (object sender, EventArgs e) =>
			{
				//Navigation.PopModalAsync();
				App.Current.MainPage = new HomeMasterPage();
			};
			navigationBack.GestureRecognizers.Add(goback);

			Label pagename = new Label()
			{
				Text = "Info",
				TextColor = Color.White,
				FontSize = screenHeight / 30,
				HeightRequest = headerChildrenHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				//HeightRequest = headerChildrenHeight,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			HorizontalGradientStack headerstack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = AppGlobalVariables.lightMarron,
				Orientation = StackOrientation.Horizontal,
				//HeightRequest = headerHeight,
				HeightRequest = screenHeight / 9.55,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
				Padding = new Thickness(0, 0, 0, 2),
				Children =
				{
					navigationBack,pagename,new BoxView{HorizontalOptions=LayoutOptions.EndAndExpand}
				},

			};

			Image joboneimg = new Image()
			{
				Source = "imgLogoLoginRegister.png",
				HeightRequest = screenHeight / 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			lblAppInfo = new CustomLabel()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalTextAlignment = TextAlignment.Center

			};

			StackLayout bodystack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(20, 0, 20, 0),
				Children =
				{
					lblAppInfo
				},
				//BackgroundColor = Color.Red
			};

			//ScrollView bodyScroll = new ScrollView();
			//{
			//	Content = bodystack;
			//};


			StackLayout stack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,

				Children =
				{
					headerstack,bodystack
				}
			};

			PageControlsStackLayout.Children.Add(stack);
		}

		/* app info service call and response */

		public async void GetAppInfo()
		{
			try
			{
				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{
					using (IAppInfoBAL selectWorkTypeList = new AppInfoBAL())
					{
						var coutriesList = await selectWorkTypeList.GetAppContent();
						if (coutriesList != null)
						{
							lblAppInfo.Text = coutriesList.Appinfocontent;
							PageLoading.IsVisible = false;
						}
					}
					PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				PageLoading.IsVisible = false;
			}
		}

	}
}
