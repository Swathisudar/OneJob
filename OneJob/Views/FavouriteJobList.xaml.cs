﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace OneJob
{
	public partial class FavouriteJobList : BaseContentPage
	{
		ListView detailsList;
		public static Grid tempsLayout;
		public static bool isScrolledAlready;
		public static ScrollView tempsView = new ScrollView();
		public static FavouriteJobList favJobList;
		public static ObservableCollection<JobsListInfo> lstt;
		ObservableCollection<JobsListInfo> joblist;
		Rectangle sbAbsentRect, sbPresentRect;
		public StackLayout searchbarStack;
		AbsoluteLayout searchbarHolder;
		CustomEntry searchentry;
		StackLayout nullDisplayStack;
		BoxView header_HolderLayer;

		Plugin.Geolocator.Abstractions.IGeolocator locator;

		double _mylatitude;

		double _mylongitude;


		public FavouriteJobList()
		{
			lstt = new ObservableCollection<JobsListInfo>();
			var height = (screenHeight * 1) / 100;
			var width = (screenWidth * 1) / 100;
			InitializeComponent();
			GetJoblist();
			favJobList = this;

			#region for header Stack
			var headerHeight = height * 9.9824;//9.4824;
			var titleSize = height * 3;
			var headerChildrenHeight = height * 7;
			var headerChildrenWidth = height * 7;

			header_HolderLayer = new BoxView()
			{
				BackgroundColor = Color.Transparent,
				IsVisible = false,
				//Opacity = 0.1,
				WidthRequest = width * 100,
				HeightRequest = screenHeight / 9.55,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
			};

			Image navigationBack = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};
			Label pageTitle = new Label()
			{
				Text = "Favourite Jobs",
				TextColor = Color.White,
				//BackgroundColor = Color.Green,
				FontSize = titleSize,
				HeightRequest = headerChildrenHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			Image searchListView = new Image()
			{
				Source = ImageSource.FromFile("navigationBarSearch.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				//WidthRequest = headerChildrenHeight,
				WidthRequest = height * 3.5,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};
			TapGestureRecognizer searchImageClicked = new TapGestureRecognizer();
			searchImageClicked.NumberOfTapsRequired = 1;
			searchImageClicked.Tapped += async (object sender, EventArgs e) =>
			{
				searchListView.Opacity = 0.5;
				await Task.Delay(100);
				searchListView.Opacity = 1;
				if (nullDisplayStack.IsVisible == false)
				{
					header_HolderLayer.IsVisible = true;
					CallSearchBar(false);
				}
			};
			searchListView.GestureRecognizers.Add(searchImageClicked);
			Image menuList = new Image()
			{
				Source = ImageSource.FromFile("moreIcon.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};


			var header_Holder = new HorizontalGradientStack() 			{

				StartColor = AppGlobalVariables.lightMarron, 				EndColor = AppGlobalVariables.darkMarron, 				BackgroundColor = AppGlobalVariables.lightMarron, 				Orientation = StackOrientation.Horizontal,
				//HeightRequest = headerHeight,
				HeightRequest = screenHeight / 9.55, 				HorizontalOptions = LayoutOptions.Fill, 				VerticalOptions = LayoutOptions.Start, 				Padding = new Thickness(0, 0, 0, 2), 				Children = { 					navigationBack, 					pageTitle, 					searchListView,
					menuList
				},  			};



			var headerHolder = new AbsoluteLayout()
			{
				HeightRequest = screenHeight / 9.55,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
			};
			AbsoluteLayout.SetLayoutBounds(header_Holder, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(header_Holder, AbsoluteLayoutFlags.All);
			headerHolder.Children.Add(header_Holder);
			AbsoluteLayout.SetLayoutBounds(header_HolderLayer, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(header_HolderLayer, AbsoluteLayoutFlags.All);
			headerHolder.Children.Add(header_HolderLayer);

			#region for search bar
			var search_BarWidth = screenWidth;
			var searchBarWidth = (search_BarWidth * 85) / 100;

			Image closeBar = new Image()
			{
				Source = ImageSource.FromFile("imgCloseRed.png"),
				//Rotation = 180,
				HeightRequest = (width * 6),
				WidthRequest = (width * 6),
				Margin = new Thickness(3, 0, 0, 0),
				HorizontalOptions = LayoutOptions.End
			};


			searchentry = new CustomEntry()
			{
				Placeholder = "Search...",
				BackgroundColor = Color.White,
				PlaceholderColor = AppGlobalVariables.fontLessThick,
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily65,
				WidthRequest = searchBarWidth,
				HorizontalOptions = LayoutOptions.Start
			};
			searchentry.Unfocused += (sender, e) =>
			{
				//searchlistview.IsVisible = false;
			};
			searchentry.TextChanged += searchItem;

			TapGestureRecognizer closeBarClicked = new TapGestureRecognizer();
			closeBarClicked.NumberOfTapsRequired = 1;
			closeBarClicked.Tapped += async (object sender, EventArgs e) =>
			{
				try
				{
					closeBar.Opacity = 0.5;
					await Task.Delay(100);
					closeBar.Opacity = 1;
					searchentry.Text = string.Empty;
					CallSearchBar(true);
				}
				catch (Exception ex)
				{
					PageLoading.IsVisible = false;
					var msg = ex.Message;
				}
			};
			closeBar.GestureRecognizers.Add(closeBarClicked);

			StackLayout searchstack = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { closeBar, searchentry },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 6
			};

			CustomFrame searchframe = new CustomFrame()
			{
				Content = searchstack,
				HeightRequest = headerHeight,
				Padding = new Thickness(screenHeight / 147.2, 0, screenHeight / 35.04, 0),
				HasShadow = false,
			};

			searchbarHolder = new AbsoluteLayout()
			{
				HeightRequest = headerHeight,
				IsVisible = false,
				//HeightRequest = height * 12,
				WidthRequest = search_BarWidth,
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(searchframe, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(searchframe, AbsoluteLayoutFlags.All);
			searchbarHolder.Children.Add(searchframe);

			#endregion

			TapGestureRecognizer closeSearch = new TapGestureRecognizer();
			closeSearch.NumberOfTapsRequired = 1;
			closeSearch.Tapped += (object sender, EventArgs e) =>
			{
				header_HolderLayer.IsVisible = false;
				searchbarHolder.IsVisible = false;
				CallSearchBar(true);
			};
			header_HolderLayer.GestureRecognizers.Add(closeSearch);


			TapGestureRecognizer menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {
			   try
			   {
				   //IGeolocator locator1 = CrossGeolocator.Current;
				   var ParentPage = (MasterDetailPage)this.Parent;
				   ParentPage.Detail = new HomePage() { BackgroundColor = Color.White };
			   }
			   catch (Exception ex)
			   {
				   PageLoading.IsVisible = false;
				   var msg = ex.Message;
			   }
		   };
			navigationBack.GestureRecognizers.Add(menuClicked);

			#endregion

			int rowheight = Convert.ToInt16(screenHeight / 5.4);
			#region for body
			//var borderThick = (width * 5) / 100;
			detailsList = new ListView()
			{
				HasUnevenRows = true,
				//Margin = new Thickness(borderThick, borderThick, borderThick, borderThick),
				BackgroundColor = AppGlobalVariables.lightGray,
				//ItemsSource = jobDetailsMenu,
				SeparatorVisibility = SeparatorVisibility.None,
				RowHeight = rowheight,
				ItemTemplate = new DataTemplate(typeof(FavJobViewCell)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			detailsList.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) =>
			{
				PageLoading.IsVisible = true;
				try
				{
					var itemSelected = e.SelectedItem as JobsListInfo;
					if (itemSelected == null)
					{
						return;
					}
					var showData = await GetDetailed(itemSelected.jobId);
					if (searchbarHolder.IsVisible == true)
					{
						header_HolderLayer.IsVisible = false;
						searchbarHolder.IsVisible = false;
						CallSearchBar(true);
					}
					if (showData != null)
					{
						PageLoading.IsVisible = true;
						await Navigation.PushModalAsync(new FavouriteJobListDetail(showData));
						PageLoading.IsVisible = false;
					}
					else
					{
						//Navigation.PushModalAsync(new HomeDetailsPage(itemSelected));
					}

					//Navigation.PushModalAsync(new FavouriteJobListDetail(itemSelected));
					((ListView)sender).SelectedItem = null;
				}
				catch (Exception ex)
				{
					PageLoading.IsVisible = false;
					var msg = ex.Message;
				}
				PageLoading.IsVisible = false;
			};

			StackLayout bodyHolder = new StackLayout()
			{
				Children = { detailsList },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			#endregion

			#region for holders
			var bodySpacing = height * 1.76;
			StackLayout holderStack = new StackLayout()
			{
				Children = { headerHolder, searchbarHolder },
				Spacing = bodySpacing / 2,
				//Padding = new Thickness(bodySpacing, bodySpacing, bodySpacing, bodySpacing),
				BackgroundColor = AppGlobalVariables.lightGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};
			StackLayout holder1 = new StackLayout()
			{
				Children = { holderStack, bodyHolder },
				Spacing = bodySpacing / 2,
				Padding = new Thickness(0, 0, 0, 0),
				//Padding = new Thickness(0, bodySpacing, 0, 0),
				BackgroundColor = AppGlobalVariables.lightGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			StackLayout holder = new StackLayout()
			{
				Children = { holder1 },
				Padding = new Thickness(0, 0, 0, 0),
				//Padding = new Thickness(0, bodySpacing, 0, 0),
				BackgroundColor = AppGlobalVariables.lightMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			#region NullStack
			CustomLabel nullText = new CustomLabel()
			{
				Text = "No Jobs present here",
				TextColor = AppGlobalVariables.lightGray,
				CustomFontFamily = AppGlobalVariables.fontFamily65,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			StackLayout nullOverLay = new StackLayout()
			{
				Children = { nullText },
				BackgroundColor = Color.FromRgba(0, 0, 0, 0.3),
				HeightRequest = (height * 100),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			nullDisplayStack = new StackLayout()
			{
				Children = { nullOverLay },
				IsVisible = false,
				Spacing = 0,
				BackgroundColor = Color.Transparent,
				HeightRequest = (height * 100),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			#endregion



			AbsoluteLayout totalHolder = new AbsoluteLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(holder, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(holder, AbsoluteLayoutFlags.All);
			totalHolder.Children.Add(holder);

			AbsoluteLayout.SetLayoutBounds(nullDisplayStack, new Rectangle(1, 1, 1, 0.9));
			AbsoluteLayout.SetLayoutFlags(nullDisplayStack, AbsoluteLayoutFlags.All);
			totalHolder.Children.Add(nullDisplayStack);

			//AbsoluteLayout.SetLayoutBounds(searchbarHolder, new Rectangle(0, 0, 1, headerHeight));
			//AbsoluteLayout.SetLayoutFlags(searchbarHolder, AbsoluteLayoutFlags.WidthProportional);
			//totalHolder.Children.Add(searchbarHolder);


			PageControlsStackLayout.Children.Add(totalHolder);

			#endregion
		}

		FavJobListReq objFavListReq;
		JobsListInfo JLInfo;

		#region for to get a detail of a job
		public async void getDetail(JobsListInfo selected_Item)
		{
			try
			{
				var showData = await GetDetailed(selected_Item.jobId);
				if (searchbarHolder.IsVisible == true)
				{
					header_HolderLayer.IsVisible = false;
					searchbarHolder.IsVisible = false;
					CallSearchBar(true);
				}
				if (showData != null)
				{
					await Navigation.PushModalAsync(new FavouriteJobListDetail(showData));
				}
				else
				{
					//Navigation.PushModalAsync(new HomeDetailsPage(itemSelected));
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

		async Task<JobsListInfo> GetDetailed(string jobId)
		{

			try
			{
				PageLoading.IsVisible = true;
				DBMethods objDatabase = new DBMethods();
				if (objDatabase != null)
				{
					var userResp = objDatabase.GetUserInfo();
					JobDetailsReq reqObj = new JobDetailsReq();
					reqObj.jobid = jobId;
					reqObj.seeker_id = userResp.UserID;
					reqObj.latitude = Convert.ToString(_mylatitude);
					reqObj.longitude = Convert.ToString(_mylongitude);


					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (IJobDetailsBAL getJobDetails = new JobDetailsBAL())
						{
							var detailsResponse = await getJobDetails.jobDetailsData(reqObj);
							if (detailsResponse != null)
							{
								if (detailsResponse.Status == 0)
								{
									JLInfo = new JobsListInfo()
									{
										amountType = detailsResponse.JobDetails[0].amountType,
										Favourite = detailsResponse.JobDetails[0].Favourite,
										companyName = detailsResponse.JobDetails[0].companyName,
										Compensention = detailsResponse.JobDetails[0].Compensention,
										createdDate = detailsResponse.JobDetails[0].createdDate,
										jobId = detailsResponse.JobDetails[0].jobId,
										Status = detailsResponse.JobDetails[0].Status,
										Distance = detailsResponse.JobDetails[0].Distance,
										endDate = detailsResponse.JobDetails[0].endDate,
										Experience = detailsResponse.JobDetails[0].Experience,
										FaceBook = detailsResponse.JobDetails[0].FaceBook,
										firstName = detailsResponse.JobDetails[0].firstName,
										jobAddress = detailsResponse.JobDetails[0].jobAddress,
										jobCity = detailsResponse.JobDetails[0].jobCity,
										jobCountryId = detailsResponse.JobDetails[0].jobCountryId,
										jobDescription = detailsResponse.JobDetails[0].jobDescription,
										jobEmail = detailsResponse.JobDetails[0].jobEmail,
										jobStateId = detailsResponse.JobDetails[0].jobStateId,
										jobTitle = detailsResponse.JobDetails[0].jobTitle,
										jobZipCode = detailsResponse.JobDetails[0].jobZipCode,
										lastName = detailsResponse.JobDetails[0].lastName,
										Latitude = detailsResponse.JobDetails[0].Latitude,
										linkesIn = detailsResponse.JobDetails[0].linkesIn,
										Longitude = detailsResponse.JobDetails[0].Longitude,
										modifiedDate = detailsResponse.JobDetails[0].modifiedDate,
										Name = detailsResponse.JobDetails[0].Name,
										noOfPositions = detailsResponse.JobDetails[0].noOfPositions,
										perHourDay = detailsResponse.JobDetails[0].perHourDay,
										profile_pic = detailsResponse.JobDetails[0].profile_pic,
										Providerid = detailsResponse.JobDetails[0].Providerid,
										providerPhoneNumber = detailsResponse.JobDetails[0].providerPhoneNumber,
										ProviderRating = detailsResponse.JobDetails[0].ProviderRating,
										startDate = detailsResponse.JobDetails[0].startDate,
										Twitter = detailsResponse.JobDetails[0].Twitter,
										workTypeID = detailsResponse.JobDetails[0].workTypeID,
										workTypeName = detailsResponse.JobDetails[0].workTypeName,
										Applied = detailsResponse.JobDetails[0].Applied,
										isInterview = detailsResponse.JobDetails[0].isInterview,
										countryName = detailsResponse.JobDetails[0].countryName,
										stateName = detailsResponse.JobDetails[0].stateName
									};
								}
								else
								{
									JLInfo = null;
									//await DisplayAlert("OneJob", "", "ok");
								}
							}
							else 							{
								JLInfo = null; 								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok"); 								PageLoading.IsVisible = false; 							} 
						}
					}
					else
					{
						JLInfo = null; 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					}
				}
				else
				{
					JLInfo = null;
					await DisplayAlert("OneJob", "Please login again", "ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				JLInfo = null;
				await DisplayAlert("OneJob", "Please check your internet access", "ok");
				var msg = ex.Message;
				PageLoading.IsVisible = false;
			}
			PageLoading.IsVisible = false;
			return JLInfo;
		}
		#endregion

		#region to get list of jobs
		async void GetJoblist()
		{
			PageLoading.IsVisible = true;
			try
			{
				locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				if (locator.IsGeolocationEnabled)
				{
					PageLoading.IsVisible = true;
					Plugin.Geolocator.Abstractions.Position myposition = await locator.GetPositionAsync(10000);
					PageLoading.IsVisible = false;
					_mylatitude = myposition.Latitude;
					_mylongitude = myposition.Longitude;
				}
				else
				{
					await DisplayAlert("Message", "Please enable Gps for locations.", "Ok");
				}
			}
			catch (Exception ex)
			{

			}

			try
			{
				joblist = new ObservableCollection<JobsListInfo>();
				DBMethods objDB = new DBMethods();
				var getLocalDB = objDB.GetUserInfo();

				if (getLocalDB != null)
				{
					objFavListReq = new FavJobListReq();

					objFavListReq.seeker_id = getLocalDB.UserID;
					objFavListReq.latitude = Convert.ToString(_mylatitude);
					objFavListReq.longitude = Convert.ToString(_mylongitude);

					PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (IFavJobListBAL JList = new FavJobListBAL())
						{
							PageLoading.IsVisible = true;
							var jobslist = await JList.GetFavJobList(objFavListReq);
							PageLoading.IsVisible = false;


							if (jobslist != null)
							{
								PageLoading.IsVisible = true;

								foreach (var item in jobslist)
								{
									if (item.Status == "active")
									{
										joblist.Add(new JobsListInfo
										{
											amountType = item.amountType,
											Favourite = item.Favourite,
											companyName = item.companyName,
											Compensention = item.Compensention,
											createdDate = item.createdDate,
											jobId = item.jobId,
											Status = item.Status,
											Distance = item.Distance,
											endDate = item.endDate,
											Experience = item.Experience,
											FaceBook = item.FaceBook,
											firstName = item.firstName,
											jobAddress = item.jobAddress,
											jobCity = item.jobCity,
											jobCountryId = item.jobCountryId,
											jobDescription = item.jobDescription,
											jobEmail = item.jobEmail,
											jobStateId = item.jobStateId,
											jobTitle = item.jobTitle,
											jobZipCode = item.jobZipCode,
											lastName = item.lastName,
											Latitude = item.Latitude,
											linkesIn = item.linkesIn,
											Longitude = item.Longitude,
											modifiedDate = item.modifiedDate,
											Name = item.Name,
											noOfPositions = item.noOfPositions,
											perHourDay = item.perHourDay,
											profile_pic = item.profile_pic,
											Providerid = item.Providerid,
											providerPhoneNumber = item.providerPhoneNumber,
											ProviderRating = item.ProviderRating,
											startDate = item.startDate,
											Twitter = item.Twitter,
											workTypeID = item.workTypeID,
											workTypeName = item.workTypeName,
											Applied = item.Applied,
											isInterview = item.isInterview,

										});
									}
								}
								if (joblist.Count <= 0)
								{
									nullDisplayStack.IsVisible = true;
								}
								else
								{
									nullDisplayStack.IsVisible = false;
								}

								detailsList.ItemsSource = joblist;
								detailsList.IsPullToRefreshEnabled = true;
								PageLoading.IsVisible = true;
								lstt = joblist;
								var countFav = joblist.Count;
								detailsList.IsPullToRefreshEnabled = false;
								PageLoading.IsVisible = false;

								Application.Current.Properties["FavCount"] = countFav;
								PageLoading.IsVisible = false;
							} 							else 							{ 								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok"); 								PageLoading.IsVisible = false; 							}

						}

						PageLoading.IsVisible = false;

					}
					else
					{ 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					}
				}

				else
				{
					await DisplayAlert("OneJob ", "Not yet register?", "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
			PageLoading.IsVisible = false;
		}
		#endregion

		#region for unfavourite a job
		public async void DeclineThisjob(JobsListInfo selected_Item)
		{
			try
			{
				PageLoading.IsVisible = true;
				DBMethods objDB = new DBMethods();
				{
					var userInfo = objDB.GetUserInfo();
					FavJobReq objFavJobReq = new FavJobReq();

					objFavJobReq.job_id = selected_Item.jobId;
					objFavJobReq.seeker_id = userInfo.UserID;
					objFavJobReq.status = "inactive";
					//DeclineJobReq declineRequest = new DeclineJobReq()
					//{
					//	jobid = selected_Item.jobId,
					//	provider_id = selected_Item.Providerid,
					//	seeker_id = userInfo.UserID,
					//	status = "decline"
					//};
					if (CheckNetworkAccess.IsNetworkConnected())
					{
						using (IFavJobBAL getFavJobDetails = new FavJobBAL())
						{
							var response = await getFavJobDetails.GetFavJob(objFavJobReq);
							//var response = await declineJob.DeclineThisJob(declineRequest);
							if (response != null)
							{
								GetJoblist();
								await DisplayAlert("Alert", response.Message, "Ok");
							}
							else
							{
								await DisplayAlert("Alert", "Please check your internet Connection", "Ok");
							}
						}
					}
					else
					{
						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
						PageLoading.IsVisible = false;
					}
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
			PageLoading.IsVisible = false;
		}
		#endregion

		#region Search Item
		string thisTab = "closed";
		void CallSearchBar(bool value)
		{
			try
			{
				if (value == true)
				{
					searchentry.Unfocus();
					searchbarHolder.IsVisible = false;
					searchbarStack.Opacity = 1;
					//searchbarStack.LayoutTo(sbAbsentRect, 0, null);
				}
				else
				{
					searchentry.Focus();
					searchbarHolder.IsVisible = true;
					searchbarStack.Opacity = 1;
					//searchbarStack.LayoutTo(sbPresentRect, 0, null);
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

		void searchItem(object sender, TextChangedEventArgs e)
		{
			detailsList.BeginRefresh();
			try
			{
				if (string.IsNullOrWhiteSpace(e.NewTextValue))
				{
					detailsList.ItemsSource = joblist;
				}
				else
				{
					detailsList.ItemsSource = joblist.Where(i => i.jobTitle.ToLower().Contains(e.NewTextValue.ToLower()));
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}

			detailsList.EndRefresh();
		}
		#endregion


	}

	public class FavJobViewCell : ViewCell
	{
		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();

			dynamic c = BindingContext;

			if (c != null)
			{
				Image image = new Image()
				{
					VerticalOptions = LayoutOptions.StartAndExpand,
					HeightRequest = BaseContentPage.screenHeight / 21.02,
					WidthRequest = BaseContentPage.screenWidth / 11.82,
					Margin = new Thickness(0, 5, BaseContentPage.screenHeight / 48.06, 0),
					//Source = "imgJob.png"
				};



				if (c.profile_pic != null && c.profile_pic != "")
				{
					//workIcon.SetBinding(Image.SourceProperty, new Binding("profile_pic"));
					image.Source = new UriImageSource()
					{
						Uri = new Uri(c.profile_pic),
						CachingEnabled = false
					};
				}
				else
				{
					image.Source = Device.OnPlatform("imgJob.png", "imgJob.png", "imgJob.png");
				}


				//image.SetBinding(Image.SourceProperty, new Binding("profile"));

				Label titlelabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					FontSize = BaseContentPage.screenHeight / 38.73,
					TextColor = AppGlobalVariables.lightBlue
				};
				string titleString = c.jobTitle;
				if (titleString.Length >= 17)
				{
					titleString = titleString.Remove(14);
					titleString = titleString + "...";
					titlelabel.Text = titleString;
				}
				else
				{
					titlelabel.SetBinding(Label.TextProperty, new Binding("jobTitle"));
				}
				//titlelabel.SetBinding(Label.TextProperty, new Binding("jobTitle"));

				Label timelabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.End,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray,
					//Text = "yesterday"
				};
				timelabel.SetBinding(Label.TextProperty, new Binding("createdDate"));
				//timelabel.SetBinding(Label.TextProperty, new Binding("createdDate"));

				StackLayout topstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { titlelabel, timelabel },
					Orientation = StackOrientation.Horizontal
				};

				var ratingIcons1 = new Image() { };
				var ratingIcons2 = new Image() { };
				var ratingIcons3 = new Image() { };
				var ratingIcons4 = new Image() { };
				var ratingIcons5 = new Image() { };

				var totalrate = c.ProviderRating;
				if (totalrate != null)
				{
					var rate = Double.Parse(c.ProviderRating);
					if (rate <= 0.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 0.5 || rate <= 1.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 1.5 || rate <= 2.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 2.5 || rate <= 3.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 3.5 || rate <= 4.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate >= 4.5)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingDone");
					}

				}
				else
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");

				}

				Grid ratingHolder = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
					ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
					Padding = new Thickness(0, 0, 0, 0),
					ColumnSpacing = 0,
					RowSpacing = 0,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					HeightRequest = BaseContentPage.screenHeight / 36.8,
				};
				ratingHolder.Children.Add(ratingIcons1, 0, 0);
				ratingHolder.Children.Add(ratingIcons2, 1, 0);
				ratingHolder.Children.Add(ratingIcons3, 2, 0);
				ratingHolder.Children.Add(ratingIcons4, 3, 0);
				ratingHolder.Children.Add(ratingIcons5, 4, 0);

				Label distancelabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.End,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray,
					Text = "Distance: "
				};
				//distancelabel.SetBinding(Label.TextProperty, new Binding("distance"));

				Label mileslabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.End,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Black,
				};




				string input2 = c.Distance;
				if (input2 == null)
				{
					mileslabel.SetBinding(Label.TextProperty, new Binding("Distance"));
				}
				else
				{
					if (input2.Length >= 9)
					{
						string sub = input2.Substring(0, 9);
						mileslabel.Text = sub;
					}
					else
					{
						mileslabel.Text = c.Distance;
					}
				}



				StackLayout middlestack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { ratingHolder, distancelabel, mileslabel },
					Orientation = StackOrientation.Horizontal,
					Spacing = 0
				};

				Label postedlabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.Start,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray,
					Text = "Posted by: "
				};
				//postedlabel.SetBinding(Label.TextProperty, new Binding("Posted"));

				Label postedbylabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray
				};
				postedbylabel.SetBinding(Label.TextProperty, new Binding("companyName"));

				StackLayout bottamstackstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					postedlabel,
					postedbylabel
				},
					Orientation = StackOrientation.Horizontal,
					Spacing = 0
				};

				StackLayout toplabelstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					topstack, middlestack, bottamstackstack
				},
					Spacing = 0
				};

				StackLayout topmainstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					image, toplabelstack
				},
					Spacing = 0,
					Orientation = StackOrientation.Horizontal
				};

				Label discriptionlabel = new Label()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray
				};


				string input = c.jobDescription;
				if (input.Length >= 85)
				{
					string sub = input.Substring(0, 85);
					discriptionlabel.Text = sub + "....";
				}
				else
				{
					discriptionlabel.Text = c.jobDescription;
				}



				StackLayout bottommainstack = new StackLayout()
				{
					//VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					discriptionlabel
				},
					Margin = new Thickness(BaseContentPage.screenHeight / 184, 0, 0, 0)
				};

				StackLayout mainstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					topmainstack,
					bottommainstack
					//gridDes
				},
					Margin = new Thickness(BaseContentPage.screenHeight / 36.8, BaseContentPage.screenHeight / 49.06, 0, Device.OnPlatform(BaseContentPage.screenHeight / 24.53, BaseContentPage.screenHeight / 29.44, BaseContentPage.screenHeight / 24.53))
				};

				Image call = new Image()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					Source = "callButton.png",
					//HeightRequest = BaseContentPage.screenHeight / 21.02,
					//WidthRequest = BaseContentPage.screenWidth / 10,
				};


				StackLayout callstack = new StackLayout()
				{
					Children = { call },
					//WidthRequest = (BaseContentPage.screenWidth * 20) / 100,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
				};

				TapGestureRecognizer calltapGesture = new TapGestureRecognizer();
				calltapGesture.SetBinding(TapGestureRecognizer.CommandParameterProperty, "jobId");

				calltapGesture.Tapped += (object sender, EventArgs e) =>
				{
					var _position = (((TappedEventArgs)e).Parameter).ToString();

					foreach (var i in FavouriteJobList.lstt)
					{
						if (i.jobId == _position)
						{
							if (i.providerPhoneNumber == "")
							{
								App.Current.MainPage.DisplayAlert("OneJob", "Provider Phone Number not available.", "Ok");
							}
							else
							{
								Device.OpenUri(new Uri("tel:" + i.providerPhoneNumber));
							}
						}
					}
				};
				callstack.GestureRecognizers.Add(calltapGesture);

				//TapGestureRecognizer callbtnTap = new TapGestureRecognizer();
				//callbtnTap.NumberOfTapsRequired = 1;
				//callbtnTap.Tapped += (object sender, EventArgs e) =>
				//{
				//	//string strCall;
				//	//if (c != null)
				//	//{
				//	//	strCall = c.providerPhoneNumber;
				//	//	var intNumber = Int32.Parse(strCall);

				//	//	Device.OpenUri(new Uri("tel:intNumber"));
				//	//}
				//};
				//call.GestureRecognizers.Add(callbtnTap);

				//StackLayout allmainstack = new StackLayout()
				//{
				//	VerticalOptions = LayoutOptions.FillAndExpand,
				//	HorizontalOptions = LayoutOptions.FillAndExpand,
				//	Children = { mainstack, callstack },
				//	Orientation = StackOrientation.Horizontal,
				//	BackgroundColor = Color.White,
				//	Margin = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 105.14),
				//	WidthRequest = BaseContentPage.screenWidth
				//};
				BoxView selectBtnClick = new BoxView()
				{
					Color = Color.Transparent,
					WidthRequest = (BaseContentPage.screenHeight * 80) / 100,
					HeightRequest = (BaseContentPage.screenHeight * 17) / 100,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
				TapGestureRecognizer itemClicked = new TapGestureRecognizer();
				itemClicked.NumberOfTapsRequired = 1;
				itemClicked.Tapped += (object sender, EventArgs e) =>
				{
					try
					{
						var item = c as JobsListInfo;
						FavouriteJobList.favJobList.getDetail(item);
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					};
				};
				selectBtnClick.GestureRecognizers.Add(itemClicked);
				Grid allmainstack = new Grid()
				{
					RowDefinitions =
					{
						new RowDefinition{  Height = GridLength.Auto},
					},
					ColumnDefinitions =
					{
						new ColumnDefinition{ Width = new GridLength(4, GridUnitType.Star)},
						new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
					},
					BackgroundColor = Color.White,
					WidthRequest = BaseContentPage.screenWidth,
					Margin = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 105.14),
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand
				};
				allmainstack.Children.Add(mainstack, 0, 0);
				allmainstack.Children.Add(selectBtnClick, 0, 0);
				allmainstack.Children.Add(callstack, 1, 0);

				Image delBtn = new Image()
				{
					Source = ImageSource.FromFile("RemoveListIcon.png"),
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
				Label remove = new Label()
				{
					Text = "Remove",
					TextColor = Color.White,
					FontSize = (BaseContentPage.screenHeight * 2) / 100,
					Margin = new Thickness(0, (BaseContentPage.screenHeight * 4.5) / 100, 0, 0),
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
				//BoxView delBtnClick = new BoxView()
				//{
				//	Color = Color.Transparent,
				//	WidthRequest = (BaseContentPage.screenHeight * 25) / 100,
				//	HeightRequest = (BaseContentPage.screenHeight * 25) / 100,
				//	HorizontalOptions = LayoutOptions.CenterAndExpand,
				//	VerticalOptions = LayoutOptions.CenterAndExpand
				//};

				Grid deleteBtnGrid = new Grid()
				{
					RowDefinitions =
					{
						new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
					},
					ColumnDefinitions =
					{
						new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
					},
					BackgroundColor = AppGlobalVariables.lightMarron,
					WidthRequest = (BaseContentPage.screenHeight * 25) / 100,
					HorizontalOptions = LayoutOptions.End,
					VerticalOptions = LayoutOptions.FillAndExpand
				};
				deleteBtnGrid.Children.Add(delBtn, 0, 0);
				deleteBtnGrid.Children.Add(remove, 0, 0);
				//deleteBtnGrid.Children.Add(delBtnClick, 0, 0);

				TapGestureRecognizer delClicked = new TapGestureRecognizer();
				delClicked.NumberOfTapsRequired = 1;
				delClicked.Tapped += (object sender, EventArgs e) =>
				{
					try
					{
						var item = c as JobsListInfo;
						FavouriteJobList.favJobList.DeclineThisjob(item);
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					};
				};
				deleteBtnGrid.GestureRecognizers.Add(delClicked);


				StackLayout deleteStack = new StackLayout()
				{
					Children = { deleteBtnGrid },
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					WidthRequest = BaseContentPage.screenWidth / 4,
					Margin = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 105.14),
					BackgroundColor = Color.FromRgb(227, 223, 224)
				};



				StackLayout stack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { allmainstack, deleteStack },
					Orientation = StackOrientation.Horizontal,
					Spacing = 0,
					BackgroundColor = Color.FromRgb(227, 223, 224)
				};

				ScrollView container = new ScrollView()
				{
					Content = stack,
					Orientation = ScrollOrientation.Horizontal,
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
				};
				container.ScrollToAsync(allmainstack, ScrollToPosition.Start, false);
				container.Scrolled += (object sender, ScrolledEventArgs e) =>
				{
					try
					{

						if (FavouriteJobList.isScrolledAlready == true)
						{
							if (FavouriteJobList.tempsView != sender as ScrollView)
							{
								var localScroll = FavouriteJobList.tempsView;
								var localStack = FavouriteJobList.tempsLayout;
								FavouriteJobList.tempsView = sender as ScrollView;
								FavouriteJobList.tempsLayout = allmainstack;
								localScroll.ScrollToAsync(localStack, ScrollToPosition.Start, false);
							}
							else
							{

							}
						}
						else
						{
							FavouriteJobList.tempsView = sender as ScrollView;
							FavouriteJobList.tempsLayout = allmainstack;
							var localStack = FavouriteJobList.tempsLayout;
							FavouriteJobList.isScrolledAlready = true;
						}
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					};
				};
				//StackLayout holder = new StackLayout()
				//{
				//	Children = { stack },
				//	VerticalOptions = LayoutOptions.FillAndExpand,
				//	HorizontalOptions = LayoutOptions.FillAndExpand,
				//};

				View = container;
			}
		}
	}
}
