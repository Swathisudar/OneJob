﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Share;
using TK.CustomMap;
using TK.CustomMap.Overlays;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace OneJob
{
	public partial class FavouriteJobListDetail : BaseContentPage
	{
		string pageTest;
		StackLayout ratingImageHolder;
		JobsListInfo homePageData;
		StringBuilder strbuider;
		StringBuilder strbuider1;
		StackLayout popupHolder, popupHolderBackground, applyPopupHolder, applyPopupHolderBackground;
		CustomTimePicker startTimeVal, endTimeVal;
		DBMethods objDatabase;
		UsersInfo getLocalDB;
		string isInterviewed, isApplied;

		Plugin.Geolocator.Abstractions.IGeolocator locator;

		public ObservableCollection<TKCustomMapPin> _Pins { get; set; }
		public ObservableCollection<TKRoute> _Routes { get; set; }

		double _mylatitude;

		double _mylongitude;


		public FavouriteJobListDetail(JobsListInfo homeSelectedDate)
		{
			var height = (screenHeight * 1) / 100;
			var width = (screenWidth * 1) / 100;
			isInterviewed = homeSelectedDate.isInterview;
			isApplied = homeSelectedDate.Applied;

			InitializeComponent();
			homePageData = homeSelectedDate;


			#region for header Stack
			var headerHeight = height * 9.9824;//9.4824;re
			var titleSize = height * 2;
			var headerChildrenHeight = height * 7;
			var headerChildrenWidth = height * 7;

			AbsoluteLayout abholder = new AbsoluteLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			Image navigationBack = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};
			TapGestureRecognizer goback = new TapGestureRecognizer();
			goback.NumberOfTapsRequired = 1;
			goback.Tapped += (object sender, EventArgs e) =>
			{
				Navigation.PopModalAsync(true);
			};
			navigationBack.GestureRecognizers.Add(goback);
			Label pageTitle = new Label()
			{
				//Text = homePageData.jobTitle,
				TextColor = Color.White,
				FontSize = screenHeight / 36.8,
				Margin = new Thickness(0, 15, 0, 0),
				//HeightRequest = headerChildrenHeight,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};


			string input1 = homePageData.jobTitle;
			if (input1 == null)
			{

			}
			else
			{
				if (input1.Length >= 20)
				{
					string sub = input1.Substring(0, 20);
					pageTitle.Text = sub + "...";
				}
				else
				{
					pageTitle.Text = homePageData.jobTitle;
				}
			}


			Image shareThis = new Image()
			{
				Source = ImageSource.FromFile("shareWhiteIcon.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			Image favourite = new Image()
			{
				Source = ImageSource.FromFile("favouriteWhiteIcon.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			TapGestureRecognizer favouriteTap = new TapGestureRecognizer();
			favouriteTap.NumberOfTapsRequired = 1;
			favouriteTap.Tapped += (object sender, EventArgs e) =>
			{
				DisplayAlert("OneJob", "You have already added this as favourite.", "Ok");
			};
			favourite.GestureRecognizers.Add(favouriteTap);

			//var favouriteTap = new TapGestureRecognizer();
			//favouriteTap.Tapped += imgFavTap;
			//favourite.GestureRecognizers.Add(favouriteTap);

			HorizontalGradientStack headerHolder = new HorizontalGradientStack()
			{
				Children = { navigationBack, pageTitle, shareThis, favourite },
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Orientation = StackOrientation.Horizontal,
				//HeightRequest = headerHeight,
				HeightRequest = screenHeight / 9.55,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start
			};
			#endregion






			var shareThisTap = new TapGestureRecognizer();
			shareThisTap.Tapped += async (s, e) =>
			{
				if (homePageData != null)
				{
					var title = homePageData.jobTitle;
					var message = homePageData.jobDescription;
					await CrossShare.Current.Share(title + "\n" + "\n" + "\n" + message, message);
				}
				else
				{
					var title = "OneJob";
					var message = "Lorem ipsum dolor sit amet, conselectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.";
					await CrossShare.Current.Share(title + "\n" + message, message);
				}

			};
			shareThis.GestureRecognizers.Add(shareThisTap);


			#region for brief description Body
			var briefBodyHeight = height * 18.0528;//15.0528;//19.17;
			var briefBHeaderHeight = height * 15.32;
			//var bBHChildrenHeight = (briefBHeaderHeight)/ 100;
			var fontSize1 = height * 2.6;//3;//2.5;
			var fontSize2 = height * 1.5;//1.85;//1.7;

			Image workIcon = new Image()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				//HeightRequest = BaseContentPage.screenHeight / 18.4,
				//WidthRequest = BaseContentPage.screenWidth / 10.35
				HeightRequest = BaseContentPage.screenHeight / 12.26,
				WidthRequest = BaseContentPage.screenWidth / 6.9
				//Margin = new Thickness(0, 10, 0, 0),
				//Source = "imgJobDetail.png"
				//Source = ImageSource.FromFile(item.imageUrl)
			};

			if (homeSelectedDate.profile_pic != null && homeSelectedDate.profile_pic != "")
			{

				workIcon.Source = new UriImageSource()
				{
					Uri = new Uri(homeSelectedDate.profile_pic),
					CachingEnabled = false
				};
			}
			else
			{
				workIcon.Source = Device.OnPlatform("imgJobDetail.png", "imgJobDetail.png", "imgJobDetail.png");
			}

			StackLayout profilepicstack = new StackLayout()
			{
				Children = { workIcon },
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.Start
			};

			Label titleLbl = new Label()
			{
				Text = homePageData.jobTitle,
				TextColor = AppGlobalVariables.lightBlue,
				FontSize = screenHeight / 40.88,
				//HeightRequest = (1.5 * briefBHeaderHeight) / 4.5,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			StackLayout titlestack = new StackLayout()
			{
				Children = { titleLbl },
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				//HeightRequest = screenHeight / 15,
				//BackgroundColor = Color.White
			};

			if (titleLbl.Text.Length >= 22)
			{
				titlestack.HeightRequest = screenHeight / 16;
			}


			#region for rating stars
			var ratingIcons1 = new Image() { };
			var ratingIcons2 = new Image() { };
			var ratingIcons3 = new Image() { };
			var ratingIcons4 = new Image() { };
			var ratingIcons5 = new Image() { };



			#region Rating Logic

			var rate = Double.Parse(homeSelectedDate.ProviderRating);
			if (rate != null)
			{
				if (rate <= 0.4)
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}
				else if (rate == 0.5 || rate <= 1.4)
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}
				else if (rate == 1.5 || rate <= 2.4)
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}
				else if (rate == 2.5 || rate <= 3.4)
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}
				else if (rate == 3.5 || rate <= 4.4)
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}
				else if (rate >= 4.5)
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingDone");
				}

			}
			else
			{
				ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
				ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
			}

			#endregion


			Grid ratingHolder = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
				Padding = new Thickness(0, 0, 0, 0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				//BackgroundColor = AppGlobalVariables.lightMarron,
				//HeightRequest = headerHeight,
				//WidthRequest = width * 15.62,
				HeightRequest = screenHeight / 36.8,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			ratingHolder.Children.Add(ratingIcons1, 0, 0);
			ratingHolder.Children.Add(ratingIcons2, 1, 0);
			ratingHolder.Children.Add(ratingIcons3, 2, 0);
			ratingHolder.Children.Add(ratingIcons4, 3, 0);
			ratingHolder.Children.Add(ratingIcons5, 4, 0);
			#endregion

			StackLayout titletopheadstack = new StackLayout()
			{
				Children = { titlestack, ratingHolder },
				VerticalOptions = LayoutOptions.StartAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};


			Label timeLbl = new Label()
			{
				Text = homePageData.createdDate + ",",
				TextColor = Color.Gray,
				FontSize = screenHeight / 49.06,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.Center
			};

			Label distanceLbl = new Label()
			{
				TextColor = Color.Gray,
				FontSize = screenHeight / 49.06,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.Center
			};

			string input2 = homePageData.Distance;
			if (input2 == null)
			{
				distanceLbl.Text = "Distance: " + homePageData.Distance;
			}
			else
			{
				if (input2.Length >= 9)
				{
					string sub = input2.Substring(0, 9);
					distanceLbl.Text = "Distance: " + sub;
				}
				else
				{
					distanceLbl.Text = "Distance: " + homePageData.Distance;
				}
			}

			StackLayout timedistancestack = new StackLayout()
			{
				Children = { timeLbl, distanceLbl },
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Spacing = 0
			};

			Label postedByLbl = new Label()
			{
				Text = "Posted By: " + homePageData.companyName,
				TextColor = Color.Gray,
				FontSize = screenHeight / 49.06,
				//VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			StackLayout postedbystack = new StackLayout()
			{
				Children = { postedByLbl },
				VerticalOptions = LayoutOptions.EndAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};


			Image phoneIcon = new Image()
			{
				Source = ImageSource.FromFile("callCircleButton.png"),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			StackLayout phoneIconHolder = new StackLayout()
			{
				Children = { phoneIcon },
				//Padding = new Thickness(0, 0, 10, 0),
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(screenHeight / 147.2, 0, 0, 0)
			};

			TapGestureRecognizer calltapGesture = new TapGestureRecognizer();

			calltapGesture.Tapped += (object sender, EventArgs e) =>
								{
									if (homePageData.providerPhoneNumber == "")
									{
										App.Current.MainPage.DisplayAlert("OneJob", "Provider Phone Number not available.", "Ok");
									}
									else
									{
										Device.OpenUri(new Uri("tel:" + homePageData.providerPhoneNumber));
									}


								};
			phoneIconHolder.GestureRecognizers.Add(calltapGesture);


			StackLayout middleHolder = new StackLayout()
			{
				Children = { titletopheadstack, timedistancestack, postedbystack },
				//Padding = new Thickness(0, 0, 10, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				Padding = new Thickness(screenHeight / 49.06, 0, 0, 0)
			};

			#region for brief body footer
			#region for percentage to show job selection status



			#endregion
			StackLayout briefBodyHolder = new StackLayout()
			{
				Children = { profilepicstack, middleHolder, phoneIconHolder },
				Orientation = StackOrientation.Horizontal,

				//Padding = new Thickness(15, 0, 0, 0),
				BackgroundColor = AppGlobalVariables.lightGray,

				//BackgroundColor = Color.Green,
				//HeightRequest = screenHeight / 6,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				Padding = new Thickness(screenHeight / 49.06, screenHeight / 49.06, screenHeight / 49.06, screenHeight / 147.2)
			};

			if (titleLbl.Text.Length >= 22)
			{
				briefBodyHolder.HeightRequest = screenHeight / 6;
			}
			else
			{
				briefBodyHolder.HeightRequest = screenHeight / 7;
			}
			#endregion
			#endregion

			#region for detail description Body
			//var detailBodyHeight = height * 37.694;
			var detailBodyHeight = height * 49.507;//47.007;
			var fontSize3 = height * 2.5;
			var fontSize4 = height * 2;//1.85;//1.7;
			var textingColor = AppGlobalVariables.fontMediumThick;
			var stackDetailSpacing = 3;

			Label jobDescHeading = new Label()
			{
				Text = "Job description",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			Label jobDescription = new Label()
			{
				Text = homePageData.jobDescription,//"consectoretur, adipisicing escolit, sedore desomruats edusimod tempor incididunt ut labore et dolore magna aliqua. Utar enim ad minim veniam",
				FontSize = fontSize4,
				TextColor = textingColor,
				MinimumHeightRequest = detailBodyHeight / 30,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout jobDescriptionStack = new StackLayout()
			{
				Children = { jobDescHeading, jobDescription },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			//ScrollView descriptionScroll = new ScrollView()
			//{
			//	Content = jobDescriptionStack,
			//	Orientation = ScrollOrientation.Vertical,
			//	HeightRequest = detailBodyHeight,
			//	BackgroundColor = Color.White,
			//	HorizontalOptions = LayoutOptions.FillAndExpand
			//};


			Label workTypeHeading = new Label()
			{
				Text = "Work type",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			Label workType = new Label()
			{
				Text = homePageData.workTypeName,//"Electrician",
				FontSize = fontSize3,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout workTypeStack = new StackLayout()
			{
				Children = { workTypeHeading, workType },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			Label workLocationHeading = new Label()
			{
				Text = "Work Location",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			Label workLocation = new Label()
			{
				Text = homePageData.jobAddress,//"San Antonio, TX",
				FontSize = fontSize3,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};



			if (homePageData != null)
			{
				workLocation.Text = homePageData.jobAddress + "," + homePageData.jobCity + "," + homePageData.stateName + "," + homePageData.countryName + ".";
			}

			StackLayout workLocationStack = new StackLayout()
			{
				Children = { workLocationHeading, workLocation },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			Label startDateHeading = new Label()
			{
				Text = "Start Date",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};



			Label startDate = new Label()
			{
				//Text = strbuider.ToString(),//"12-03-2017",
				FontSize = fontSize3,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};

			if (homePageData.startDate != null)
			{
				strbuider = new StringBuilder(homePageData.startDate);
				strbuider.Remove(10, 9);
				startDate.Text = strbuider.ToString();
			}

			StackLayout startDateStack = new StackLayout()
			{
				Children = { startDateHeading, startDate },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			Label expecetdEDateHeading = new Label()
			{
				Text = "Expected end date",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};

			Label expecetdEndDate = new Label()
			{
				//Text = strbuider1.ToString(),//"14-03-2017",
				FontSize = fontSize3,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};

			if (homePageData.endDate != null)
			{
				strbuider1 = new StringBuilder(homePageData.endDate);
				strbuider1.Remove(10, 9);
				expecetdEndDate.Text = strbuider1.ToString();
			}

			StackLayout expecetdEDateStack = new StackLayout()
			{
				Children = { expecetdEDateHeading, expecetdEndDate },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			Label noOfWorkersHeading = new Label()
			{
				Text = "No. of workers",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			Label noOfWorkers = new Label()
			{
				Text = homeSelectedDate.noOfPositions,//"06",
				FontSize = fontSize3,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout noOfWorkersStack = new StackLayout()
			{
				Children = { noOfWorkersHeading, noOfWorkers },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			Label approxAmountHeading = new Label()
			{
				Text = "Approx amount/ worker",
				FontSize = fontSize4,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			Label approxAmount = new Label()
			{
				Text = homePageData.amountType + " " + homePageData.Compensention + "/" + " " + homePageData.perHourDay,//"$ 120",
				FontSize = fontSize3,
				TextColor = textingColor,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout approxAmountStack = new StackLayout()
			{
				Children = { approxAmountHeading, approxAmount },
				Spacing = stackDetailSpacing,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			var detailBodySpacing = width * 10;
			Grid detailBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = GridLength.Auto },
					new RowDefinition{  Height = GridLength.Auto },
					new RowDefinition{  Height = GridLength.Auto },
					new RowDefinition{  Height = GridLength.Auto }
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
				},
				WidthRequest = (width * 100) - 20,
				Padding = new Thickness(detailBodySpacing, detailBodySpacing / 2, detailBodySpacing, detailBodySpacing / 2),
				RowSpacing = width * 4,
				//HeightRequest = detailBodyHeight - 30,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			detailBody.Children.Add(jobDescriptionStack, 0, 2, 0, 1);
			//detailBody.Children.Add(descriptionScroll, 0, 2, 0, 1);
			detailBody.Children.Add(workTypeStack, 0, 1);
			detailBody.Children.Add(workLocationStack, 1, 1);
			detailBody.Children.Add(startDateStack, 0, 2);
			detailBody.Children.Add(expecetdEDateStack, 1, 2);
			detailBody.Children.Add(noOfWorkersStack, 0, 3);
			detailBody.Children.Add(approxAmountStack, 1, 3);

			ScrollView contentScroll = new ScrollView()
			{
				Content = detailBody,

				Orientation = ScrollOrientation.Vertical,
				//HeightRequest = detailBodyHeight,
				BackgroundColor = Color.White,
				Padding = new Thickness(1, 1, 1, 1),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout detailBodyHolder = new StackLayout()
			{
				Children = { contentScroll },
				HeightRequest = detailBodyHeight,
				BackgroundColor = Color.White,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			#endregion

			#region for map layout
			var mapBodyHeight = height * 29.7007 + (height * 5);//height * 27.2007 or 18.574 + (height * 1.0796) + (height * 5);
			var position = new Position(37, -122);
			//var mapSpan = MapSpan.FromCenterAndRadius(position, Distance.FromMiles(7));  //FromCenterAndRadius(, Distance.FromMiles(0.3));
			//Pin locationIdentifier = new Pin()
			//{
			//	Position = position,
			//	Label = "",
			//	Address = "",
			//	Type = PinType.Place
			//};
			//Map workMapLocation = new Map()
			//{
			//	MapType = MapType.Hybrid,
			//	IsShowingUser = true,
			//	//HeightRequest = mapBodyHeight,
			//	//WidthRequest = width*100,
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//	VerticalOptions = LayoutOptions.FillAndExpand
			//};
			//workMapLocation.Pins.Add(locationIdentifier);
			//workMapLocation.MoveToRegion(mapSpan);


			CustomMap mapview = new CustomMap(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(Convert.ToDouble(homePageData.Latitude), Convert.ToDouble(homePageData.Longitude)), Distance.FromMiles(10)));
			mapview.IsShowingUser = true;
			mapview.IsRegionChangeAnimated = true;


			Image navigationimg = new Image()
			{
				Source = "imgNavigation.png"
			};

			_Pins = new ObservableCollection<TKCustomMapPin>();
			_Routes = new ObservableCollection<TKRoute>();

			TapGestureRecognizer navigationimgTap = new TapGestureRecognizer();
			navigationimgTap.Tapped += async (sender, e) =>
			{
				try
				{
					PageLoading.IsVisible = true;
					locator = CrossGeolocator.Current;
					locator.DesiredAccuracy = 50;

					if (locator.IsGeolocationEnabled)
					{
						PageLoading.IsVisible = true;
						Plugin.Geolocator.Abstractions.Position myposition = await locator.GetPositionAsync(10000);
						PageLoading.IsVisible = false;
						_mylatitude = myposition.Latitude;
						_mylongitude = myposition.Longitude;
						//mapview.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(_mylatitude, _mylongitude), Distance.FromMiles(50)));
					}
					else
					{
						PageLoading.IsVisible = false;
						await DisplayAlert("Message", "Please enable Gps for locations.", "Ok");
					}
				}
				catch (Exception ex)
				{
					PageLoading.IsVisible = false;
				}

				var from = new Xamarin.Forms.Maps.Position(_mylatitude, _mylongitude);
				var to = new Xamarin.Forms.Maps.Position(Convert.ToDouble(homePageData.Latitude), Convert.ToDouble(homePageData.Longitude));

				//var route = new TKRoute
				//{
				//	TravelMode = TKRouteTravelMode.Driving,
				//	Source = from,
				//	Destination = to,
				//	Color = Color.Red,
				//};

				//_Pins.Add(new RoutePin
				//{
				//	Route = route,
				//	IsSource = true,
				//	IsDraggable = true,
				//	Position = from,
				//	//Title = "Gopi",
				//	ShowCallout = true,
				//	//DefaultPinColor = Color.Green,
				//	//Subtitle = "Nadh",
				//	//Image = "IMG.png",
				//	Image = "imgsmallLocationpin.png"

				//});

				//_Pins.Add(new RoutePin
				//{
				//	Route = route,
				//	IsSource = false,
				//	IsDraggable = true,
				//	Position = to,
				//	//Title = "Vaka",
				//	//Subtitle = "reddy",
				//	ShowCallout = true,
				//	//DefaultPinColor = Color.Red,
				//	//Image = "IMG.png"
				//	Image = "imgsmallLocationpin.png"
				//});

				//_Routes.Add(route);
				//mapview.Routes = _Routes;
				//mapview.CustomPins = _Pins;

				//if (Device.OS == TargetPlatform.iOS)
				//{
				//	//var request = string.Format("http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "");
				//	var request = string.Format("http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "");
				//	//await	Navigation.PushModalAsync(new ContactWebView(request));
				//	Device.OpenUri(new Uri(request));
				//}

				//if (Device.OS == TargetPlatform.Android)
				//{

				//	//var request = string.Format("http://maps.apple.com/?daddr=" + to.Latitude + "," + to.Longitude + "");
				//	var request = string.Format("http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "");

				//	Device.OpenUri(new Uri(request));
				//}

				if (Device.OS == TargetPlatform.iOS)
				{
					////var request = string.Format("http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "");
					//var request = string.Format("http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "");
					////await	Navigation.PushModalAsync(new ContactWebView(request));
					//Device.OpenUri(new Uri(request));
					PageLoading.IsVisible = true;

					await Navigation.PushModalAsync(new NavigationDetail(to));
					PageLoading.IsVisible = false;
				}

				if (Device.OS == TargetPlatform.Android)
				{
					PageLoading.IsVisible = true;
					//var request = string.Format("http://maps.apple.com/?daddr=" + to.Latitude + "," + to.Longitude + "");
					var request = string.Format("http://maps.google.com/?daddr=" + to.Latitude + "," + to.Longitude + "");

					Device.OpenUri(new Uri(request));
					PageLoading.IsVisible = false;
				}

				//Navigation.PushModalAsync(new NavigationDetail(_mylatitude, _mylongitude, from, to));

			};
			navigationimg.GestureRecognizers.Add(navigationimgTap);

			ObservableCollection<TKCustomMapPin> allPinns = new ObservableCollection<TK.CustomMap.TKCustomMapPin>();


			TKCustomMapPin pinn = new TKCustomMapPin()
			{
				Position = new Xamarin.Forms.Maps.Position(Convert.ToDouble(homePageData.Latitude), Convert.ToDouble(homePageData.Longitude)),
				Image = "imgsmallLocationpin.png",
				ShowCallout = true,
				IsCalloutClickable = true
			};
			allPinns.Add(pinn);
			mapview.CustomPins = allPinns;
			var popupHeight = height * 58;//39.79;
			var popupWidth = width * 75;//62.97;
			var imagePadding = (popupWidth * 11) / 100;

			#region Apply bottom
			ratingImageHolder = new StackLayout()
			{
				//Children = { rstar },
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				//HeightRequest = 60,
				Padding = new Thickness(imagePadding, 0, imagePadding, 0),
				WidthRequest = popupWidth,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			Label rateJob = new Label()
			{
				//Text = "Apply",
				TextColor = Color.White,
				HeightRequest = headerHeight,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			if (homeSelectedDate.Status == "expire")
			{
				rateJob.Text = "Job Expired";
			}
			else
			{
				rateJob.Text = "Apply";
			}
			HorizontalGradientStack rateJobHolder = new HorizontalGradientStack()
			{
				Children = { rateJob },
				HeightRequest = headerHeight,

				//IsVisible = false,
				//IsEnabled = false,
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Opacity = 0.94
				//StartColor = Color.FromRgba(133, 0, 41, 0.3),
				//EndColor = Color.FromRgba(75, 0, 0, 0.3),
			};
			TapGestureRecognizer rateJobHolderTap = new TapGestureRecognizer();
			rateJobHolderTap.NumberOfTapsRequired = 1;
			rateJobHolderTap.Tapped += async (object sender, EventArgs e) =>
			{
				if (rateJob.Text != "Job Expired")
				{
					if (isApplied == "yes")
					{
						await DisplayAlert("OneJob", "You have already applied for this job.", "Ok");
					}
					else
					{
						if (isInterviewed == "yes")
						{
							applyPopupHolder.IsVisible = true;
							applyPopupHolderBackground.IsVisible = true;
						}
						else
						{
							await JobApplied();
						}
					}
				}
			};// btnRateJob_Click;
			rateJobHolder.GestureRecognizers.Add(rateJobHolderTap);


			#endregion

			AbsoluteLayout mapBodyHolder = new AbsoluteLayout()
			{
				//Children = { workMapLocation },
				HeightRequest = mapBodyHeight,
				WidthRequest = width * 100,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			AbsoluteLayout.SetLayoutBounds(mapview, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(mapview, AbsoluteLayoutFlags.All);
			mapBodyHolder.Children.Add(mapview);

			AbsoluteLayout.SetLayoutBounds(rateJobHolder, new Rectangle(0, 1, 1, 0.23));
			AbsoluteLayout.SetLayoutFlags(rateJobHolder, AbsoluteLayoutFlags.All);
			mapBodyHolder.Children.Add(rateJobHolder);

			#endregion

			#region for body holder
			StackLayout holder = new StackLayout()
			{
				Children = { headerHolder, briefBodyHolder, detailBodyHolder, mapBodyHolder },
				BackgroundColor = AppGlobalVariables.lightMarron,
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			//PageControlsStackLayout.Children.Add(holder);


			#region for Popup
			Label popupTitleLbl = new Label()
			{
				Text = "Don't have an account?",
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.Center
			};

			Label popupbodyLbl = new Label()
			{
				Text = "Please Register/Login in order to access this feature.",
				FontFamily = AppGlobalVariables.fontFamily45,
				TextColor = Color.Gray,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(CustomEntry)),
				Margin = 15,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout popupTextHolder = new StackLayout()
			{
				Children = { popupTitleLbl, popupbodyLbl },
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};

			var ratingImageHeight = (popupWidth * 16) / 100;



			//EditorCtrl commentEditor = new EditorCtrl()
			//{
			//	Text = "Leave a comment",
			//	TextColor = Color.Gray,
			//	FontSize = fontSize2,
			//	HeightRequest = popupHeight / 4.5,
			//	WidthRequest = (popupWidth * 90) / 100,//popupWidth - ((popupWidth * 10) / 100),
			//	HorizontalOptions = LayoutOptions.Center,
			//	VerticalOptions = LayoutOptions.Center
			//};
			//EditorFrame commentEditorHolder = new EditorFrame()
			//{
			//	Content = commentEditor,
			//	HeightRequest = popupHeight / 4,
			//	WidthRequest = (popupWidth*90)/100,//popupWidth - ((popupWidth * 10) / 100),
			//	Padding = new Thickness(0, 0, 0, 0),
			//	HorizontalOptions = LayoutOptions.Center,
			//	VerticalOptions = LayoutOptions.Center
			//};
			var popupButtonHeight = (popupWidth * 19) / 100;
			Button popupCancel = new Button()
			{
				Text = "Cancel",
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				BackgroundColor = AppGlobalVariables.lightMarron,
				BorderRadius = 0,
				HeightRequest = popupButtonHeight,//height * 10.211,
				WidthRequest = popupWidth / 2,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End,
				Command = new Command((obj) =>
				{
					popupHolder.IsVisible = false;
					popupHolderBackground.IsVisible = false;
				})
			};
			Button popupSubmit = new Button()
			{
				Text = "Register",
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				//StartColor = AppGlobalVariables.lightMarron,
				//EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = AppGlobalVariables.darkMarron,
				BorderRadius = 0,
				HeightRequest = popupButtonHeight,//height * 10.211,
				WidthRequest = popupWidth / 2,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};

			popupSubmit.Clicked += async (object sender, EventArgs e) =>
			{
				await Navigation.PushModalAsync(new UserRegistration());
			};

			HorizontalGradientGrid popupButtonsHolder = new HorizontalGradientGrid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = GridLength.Auto},
					new ColumnDefinition{ Width = GridLength.Auto}
				},
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = Color.Blue,
				ColumnSpacing = 1,
				//HeightRequest = height * 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			popupButtonsHolder.Children.Add(popupCancel, 0, 0);
			popupButtonsHolder.Children.Add(popupSubmit, 1, 0);


			//HorizontalGradientStack popupButtonsHolder = new HorizontalGradientStack()
			//{
			//	Children = { popupCancel, popupSubmit },
			//	StartColor = AppGlobalVariables.darkMarron,
			//	EndColor = AppGlobalVariables.lightMarron,
			//	//Orientation = StackOrientation.Horizontal,
			//	HeightRequest = height*10,
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//	VerticalOptions = LayoutOptions.End
			//};

			popupHolder = new StackLayout()
			{
				Children = { popupTextHolder, ratingImageHolder, popupButtonsHolder },
				IsVisible = false,
				Padding = new Thickness(0, 10, 0, 0),
				Spacing = (popupHeight * 5) / 100,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			popupHolderBackground = new StackLayout()
			{
				Children = { popupHolder },
				BackgroundColor = Color.FromRgba(0, 0, 0, 0.8),
				IsVisible = false,
			};
			#endregion


			#region for  apply Popup
			var applyPopupHeight = (screenHeight * 54) / 100;// (screenHeight * 58) / 100;//39.79;36.0915493
			var applyPopupWidth = (screenWidth * 68) / 100;//(screenWidth * 75) / 100;//62.97;

			Label applyPopupTitleLbl = new Label()
			{
				Text = "Confirm Date & Time",
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.Center
			};

			Label applyPopupbodyLbl = new Label()
			{
				Text = "Please confirm your availability \r\n time for this particular job \r\n before applying",
				FontFamily = AppGlobalVariables.fontFamily45,
				TextColor = Color.Gray,
				FontSize = height * 2.3,
				Margin = 15,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			BoxView startTimeULine = new BoxView()
			{
				HeightRequest = 1,
				//WidthRequest = boxWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};

			BoxView endTimeULine = new BoxView()
			{
				HeightRequest = 1,
				//WidthRequest = boxWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};

			Label startTimeLbl = new Label()
			{
				Text = "Start time",
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = height * 2,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.Start
			};
			//TimeSpan setTimes = new TimeSpan(0, 0, 0, 0, 0);

			startTimeVal = new CustomTimePicker()
			{
				//Time = setTimes,
				//Time = DateTime.Now.TimeOfDay, //Convert.ToDateTime(time),
				CustomFontSize = 15,
				CustomFontFamily = "Avenir45",
				Format = "HH:mm",
				//BackgroundColor = Color.Green,
				TextColor = AppGlobalVariables.fontVeryThick,
				//HeightRequest = entryHeight,
				//WidthRequest = entry_Width,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			StackLayout startTimeLblStack = new StackLayout()
			{
				Children = { startTimeLbl, startTimeVal, startTimeULine },
				Padding = new Thickness(0, 0, 0, 0),
				//BackgroundColor = Color.Teal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};


			Label endTimeLbl = new Label()
			{
				Text = "End time",
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = height * 2,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.Start
			};

			endTimeVal = new CustomTimePicker()
			{
				//Time = setTimes,
				//Time = DateTime.Now.TimeOfDay, //Convert.ToDateTime(time),
				CustomFontSize = 15,
				CustomFontFamily = "Avenir45",
				Format = "HH:mm",
				//BackgroundColor = Color.Green,
				TextColor = AppGlobalVariables.fontVeryThick,
				//HeightRequest = entryHeight,
				//WidthRequest = entry_Width,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			StackLayout endTimeLblStack = new StackLayout()
			{
				Children = { endTimeLbl, endTimeVal, endTimeULine },
				Padding = new Thickness(0, 0, 0, 0),
				//BackgroundColor = Color.Aqua,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};


			Grid timeHolderGrid = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition { Height = GridLength.Auto},
					//new RowDefinition { Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition { Width = new GridLength( 1, GridUnitType.Star )},
					new ColumnDefinition { Width = new GridLength( 1, GridUnitType.Star )}

				},
				//RowSpacing = 10,
				ColumnSpacing = 10,
				Padding = new Thickness(10, 0, 10, 10),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Center
			};
			//timeHolderGrid.Children.Add(startTimeStack, 0, 0);
			timeHolderGrid.Children.Add(startTimeLblStack, 0, 0);


			//timeHolderGrid.Children.Add(endTimeStack, 1, 0);
			timeHolderGrid.Children.Add(endTimeLblStack, 1, 0);


			var applyPopupButtonHeight = (popupWidth * 19) / 100;
			Button applyPopupCancel = new Button()
			{
				Text = "Cancel",
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				BackgroundColor = AppGlobalVariables.lightMarron,
				BorderRadius = 0,
				HeightRequest = applyPopupButtonHeight,//height * 10.211,
				WidthRequest = applyPopupWidth / 2,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End,
				Command = new Command((obj) =>
				{
					applyPopupHolder.IsVisible = false;
					applyPopupHolderBackground.IsVisible = false;
				})
			};
			Button applyPopupSubmit = new Button()
			{
				Text = "Apply",
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				//StartColor = AppGlobalVariables.lightMarron,
				//EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = AppGlobalVariables.darkMarron,
				BorderRadius = 0,
				HeightRequest = applyPopupButtonHeight,//height * 10.211,
				WidthRequest = applyPopupWidth / 2,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};

			applyPopupSubmit.Clicked += btnRateJob_Clicked;

			HorizontalGradientGrid ApplyPopupButtonsHolder = new HorizontalGradientGrid()
			{
				RowDefinitions =
				{
					new RowDefinition{   Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{  Width = GridLength.Auto},
					new ColumnDefinition{  Width = GridLength.Auto}
				},
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = Color.Blue,
				ColumnSpacing = 1,
				//HeightRequest = height * 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			ApplyPopupButtonsHolder.Children.Add(applyPopupCancel, 0, 0);
			ApplyPopupButtonsHolder.Children.Add(applyPopupSubmit, 1, 0);

			applyPopupHolder = new StackLayout()
			{
				Children = { applyPopupTitleLbl, applyPopupbodyLbl, timeHolderGrid, ApplyPopupButtonsHolder },
				//Children = { ApplyPopupTextHolder, timeHolderGrid, ApplyPopupButtonsHolder },
				IsVisible = false,
				Padding = new Thickness(0, 10, 0, 0),
				Spacing = (popupHeight * 4) / 100,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			applyPopupHolderBackground = new StackLayout()
			{
				Children = { applyPopupHolder },
				BackgroundColor = Color.FromRgba(0, 0, 0, 0.8),
				IsVisible = false,
			};
			#endregion


			AbsoluteLayout holderContent = new AbsoluteLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(holder, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(holder, AbsoluteLayoutFlags.All);
			holderContent.Children.Add(holder);

			AbsoluteLayout.SetLayoutBounds(popupHolderBackground, new Rectangle(0.5, 0.5, 1, 1));
			AbsoluteLayout.SetLayoutFlags(popupHolderBackground, AbsoluteLayoutFlags.All);
			holderContent.Children.Add(popupHolderBackground);
			AbsoluteLayout.SetLayoutBounds(popupHolder, new Rectangle(0.5, 0.47, popupWidth, popupHeight));
			AbsoluteLayout.SetLayoutFlags(popupHolder, AbsoluteLayoutFlags.PositionProportional);
			holderContent.Children.Add(popupHolder);

			AbsoluteLayout.SetLayoutBounds(applyPopupHolderBackground, new Rectangle(0.5, 0.5, 1, 1));
			AbsoluteLayout.SetLayoutFlags(applyPopupHolderBackground, AbsoluteLayoutFlags.All);
			holderContent.Children.Add(applyPopupHolderBackground);
			AbsoluteLayout.SetLayoutBounds(applyPopupHolder, new Rectangle(0.5, 0.47, applyPopupWidth, applyPopupHeight));
			AbsoluteLayout.SetLayoutFlags(applyPopupHolder, AbsoluteLayoutFlags.PositionProportional);
			holderContent.Children.Add(applyPopupHolder);

			AbsoluteLayout.SetLayoutBounds(holderContent, new Rectangle(1, 1, 1, 1));
			AbsoluteLayout.SetLayoutFlags(holderContent, AbsoluteLayoutFlags.All);
			abholder.Children.Add(holderContent);

			AbsoluteLayout.SetLayoutBounds(navigationimg, new Rectangle(0.999, 0.76, 0.10, 0.10));
			AbsoluteLayout.SetLayoutFlags(navigationimg, AbsoluteLayoutFlags.All);
			abholder.Children.Add(navigationimg);

			PageControlsStackLayout.Children.Add(abholder);
			//PageControlsStackLayout.Children.Add(holderContent);

			#endregion
		}

		#region for favourite button clicked service
		private async void imgFavTap(object sender, EventArgs e)
		{
			objDatabase = new DBMethods();
			getLocalDB = objDatabase.GetUserInfo();

			try
			{
				if (getLocalDB != null)
				{
					FavJobReq objFavJobReq = new FavJobReq();

					objFavJobReq.job_id = homePageData.jobId;
					objFavJobReq.seeker_id = getLocalDB.UserID;
					objFavJobReq.status = homePageData.Status;


					PageLoading.IsVisible = true;
					if (CheckNetworkAccess.IsNetworkConnected())
					{
						using (IFavJobBAL getFavJobDetails = new FavJobBAL())
						{
							var objFavJobCheck = await getFavJobDetails.GetFavJob(objFavJobReq);

							if (objFavJobCheck != null)
							{
								if (objFavJobCheck.Status == 1)
								{
									await DisplayAlert("OneJob", objFavJobCheck.Message, "Ok");
									await Navigation.PushModalAsync(new HomeMasterPage());
									PageLoading.IsVisible = false;
								}
								else
								{
									await DisplayAlert("OneJob", objFavJobCheck.Message, "Ok");
									PageLoading.IsVisible = false;
								}
							}
							else 							{ 								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok"); 								PageLoading.IsVisible = false; 							}
						}

					}
					else
					{ 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					}
				}
				else
				{
					popupHolder.IsVisible = true;
					popupHolderBackground.IsVisible = true;
				}

			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
			}
		}
		#endregion


		//private async void btnRateJob_Click(object sender, EventArgs e)
		//{
		//	try
		//	{
		//		objDatabase = new DBMethods();
		//		getLocalDB = objDatabase.GetUserInfo();
		//		if (getLocalDB != null)
		//		{
		//			ApplyJobReq onjApplyJobs = new ApplyJobReq()
		//			{
		//				jobid = homePageData.jobId,
		//				end_time = homePageData.endDate,
		//				provider_id = homePageData.Providerid,
		//				seeker_id = getLocalDB.UserID,
		//				start_time = homePageData.startDate,
		//			};
		//			PageLoading.IsVisible = true;
		//			using (IApplyJobBAL applyJobList = new ApplyJobBAL())
		//			{
		//				var applyCheck = await applyJobList.applyJOB(onjApplyJobs);
		//				if (applyCheck != null)
		//				{
		//					if (applyCheck.Status == 0)
		//					{
		//						await DisplayAlert("OneJob", applyCheck.Message, "Ok");
		//						await Navigation.PushModalAsync(new HomeMasterPage());
		//						PageLoading.IsVisible = false;
		//					}
		//					else
		//					{
		//						await DisplayAlert("OneJob", applyCheck.Message, "Ok");
		//						PageLoading.IsVisible = true;
		//					}
		//				}
		//				else
		//				{
		//					await DisplayAlert("OneJob", "Applied Failed!", "Ok");
		//					PageLoading.IsVisible = true;
		//				}
		//			}
		//			PageLoading.IsVisible = false;
		//		}
		//		else
		//		{
		//			popupHolder.IsVisible = true;
		//			popupHolderBackground.IsVisible = true;
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		var msg = ex.Message;
		//	}
		//}


		#region for popup apply button click
		private async void btnRateJob_Clicked(object sender, EventArgs e)
		{
			try
			{
				if (startTimeVal.Time.Hours >= endTimeVal.Time.Hours)
				{
					await DisplayAlert("OneJob", "End time must be greater than start time", "Ok");
				}
				else
				{
					await JobApplied();
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}
		#endregion

		#region for job Application
		public async Task<bool> JobApplied()
		{

			//if (startTimeVal.Time.Hours >= endTimeVal.Time.Hours)
			//{
			//	await DisplayAlert("OneJob", "End time must be greater than start time", "Ok");
			//}
			//else
			//{
			try
			{
				objDatabase = new DBMethods();
				getLocalDB = objDatabase.GetUserInfo();

				if (getLocalDB != null)
				{
					ApplyJobReq onjApplyJobs = new ApplyJobReq()
					{
						jobid = homePageData.jobId,
						end_time = endTimeVal.Time.Hours.ToString() + ":" + endTimeVal.Time.Minutes.ToString() ?? homePageData.endDate,
						provider_id = homePageData.Providerid,
						seeker_id = getLocalDB.UserID,
						start_time = startTimeVal.Time.Hours.ToString() + ":" + startTimeVal.Time.Minutes.ToString() ?? homePageData.startDate,
					};
					PageLoading.IsVisible = true;


					if (CheckNetworkAccess.IsNetworkConnected())
					{
						using (IApplyJobBAL applyJobList = new ApplyJobBAL())
						{
							var applyCheck = await applyJobList.applyJOB(onjApplyJobs);
							if (applyCheck != null)
							{
								if (applyCheck.Status == 0)
								{

									if (homePageData.providerPhoneNumber != "" && (homePageData.providerPhoneNumber.Length >= 5))
									{
										try
										{
											OTPReq objOtpReq = new OTPReq();

											objOtpReq.otp = "http://www.devrabbit.com/projects/onejob/provider/jobseekerdetails/" + homePageData.jobId + "/" + getLocalDB.UserID;
											objOtpReq.otp_for = "job";
											objOtpReq.seeker = getLocalDB.UserFirstName + " " + getLocalDB.UserLastName;
											objOtpReq.phone_number = homePageData.providerPhoneNumber;// getLocalDB.CountryCode + getLocalDB.MobileNumber;


											PageLoading.IsVisible = true;

											if (CheckNetworkAccess.IsNetworkConnected())
											{

												using (IOtpBAL getOtpDetails = new OtpBAL())
												{
													var otpCheck = await getOtpDetails.mobileOTP(objOtpReq);
													if (otpCheck != null)
													{
														if (otpCheck.Status == 1)
														{
															isApplied = "yes";
															//favourite.Source = "favouriteWhiteIcon.png";
															await DisplayAlert("OneJob", applyCheck.Message, "Ok");
															//await Navigation.PushModalAsync(new HomeMasterPage());
															PageLoading.IsVisible = false;

														}
														else
														{
															await DisplayAlert("OneJob", otpCheck.Message, "Ok");
															PageLoading.IsVisible = false;
														}
													}
													else
													{
														await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
														PageLoading.IsVisible = false;
													}

													PageLoading.IsVisible = false;
												};

											}
											else
											{
												await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
												PageLoading.IsVisible = false;
											}
										}
										catch (Exception ex)
										{
											var msg = ex.Message;
										}
										PageLoading.IsVisible = false;
									}
									else
									{
										isApplied = "yes";
										await DisplayAlert("OneJob", applyCheck.Message, "Ok");
										PageLoading.IsVisible = false;
									}
								}
								else
								{
									await DisplayAlert("OneJob", applyCheck.Message, "Ok");
									PageLoading.IsVisible = true;
								}

							}
							else
							{
								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								PageLoading.IsVisible = false;
							}

						}
						applyPopupHolder.IsVisible = false;
						applyPopupHolderBackground.IsVisible = false;
						PageLoading.IsVisible = false;

					}
					else
					{
						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
						PageLoading.IsVisible = false;
					}

				}
				else
				{
					popupHolder.IsVisible = true;
					popupHolderBackground.IsVisible = true;
				}


			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				PageLoading.IsVisible = false;
			}

			//}
			return true;
		}
		#endregion
	}
}