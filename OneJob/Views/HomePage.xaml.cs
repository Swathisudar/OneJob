﻿using System;
using System.Collections.ObjectModel;
using TK.CustomMap;
using TK.CustomMap.Overlays;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

/* This page is done by Gopi */

namespace OneJob
{
	public partial class HomePage : BaseContentPage
	{
		/* global declarations */
		ObservableCollection<TKCustomMapPin> allPins = new ObservableCollection<TKCustomMapPin>();
		public static HomePage homePages;
		public StackLayout popupHolder, popupHolderBackground, ratingImageHolder, allmainstackpin, mapandliststack;
		Image image1;
		Image ratingIcons11, ratingIcons22, ratingIcons33, ratingIcons44, ratingIcons55;
		Label titlelabel1, timelabel1, mileslabel1, postedbylabel1, discriptionlabel1;
		AbsoluteLayout holderContent;

		public double _latitude;
		public double _longitude;

		public double _latitudeDB;
		public double _longitudeDB;

		public GoogleSearch Arearesponse;
		public LocationRootObject loc;

		public string searchcomplete = null;

		public string _searchkey = null;

		public string _key = "map";

		public ListView listview;

		public Double ratepin = 0;

		public string dispin = null;

		public string titledispin = null;

		public string pinjobid = null;

		public string pinlistmapstring = null;


		CustomEntry searchentry;

		List<JobsListInfo> joblistmap;

		public static List<JobsListInfo> joblistlist;

		public CustomMap mapview1 { get; set; }
		IGeolocator locator;
		public ListView searchlistview;

		public HomePage()
		{
			InitializeComponent();

			homePages = this;
			mylocationTapmethod();

			joblistmap = new List<JobsListInfo>();

			joblistlist = new List<JobsListInfo>();


			AbsoluteLayout ablayout = new AbsoluteLayout();

			Image imgMenu = new Image
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Source = "imgMenu.png",
				HeightRequest = screenHeight / 14.72,
				WidthRequest = screenWidth / 8.28,
				Margin = new Thickness(screenHeight / 73.6, 0, 0, 0)
			};

			Image imgLogo = new Image
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Source = "imgLogo.png",
				HeightRequest = screenHeight / 14.72,
				WidthRequest = screenWidth / 5.91,
				Margin = new Thickness(0, 0, screenHeight / 36.8, 0)
			};

			var imgNotification = new Image
			{
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Source = "imgWhiteNotification.png",
				HeightRequest = screenHeight / 14.72,
				WidthRequest = screenWidth / 8.28,
				Margin = new Thickness(0, 0, 0, 0)
			};

			Image imgFliter = new Image
			{
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Source = "imgfliter.png",
				HeightRequest = screenHeight / 14.72,
				WidthRequest = screenWidth / 8.28,
				Margin = new Thickness(0, 0, screenHeight / 73.6, 0)
			};

			StackLayout imagesstack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Children =
				{
					imgMenu,imgLogo,imgNotification,imgFliter
				},
				Spacing = 0

			};

			HorizontalGradientStack headerstack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				HeightRequest = screenHeight / 9.55,
				Children =
				{
					imagesstack
				}
			};

			CustomLabel mapviewlabel = new CustomLabel()
			{
				Text = "Map view",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.Black,
				FontSize = screenHeight / 36.8,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};

			BoxView mapviewbox = new BoxView()
			{
				BackgroundColor = AppGlobalVariables.lightMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End,
				HeightRequest = screenHeight / 184
			};

			StackLayout mapviewstack = new StackLayout()
			{
				Children = { mapviewlabel, mapviewbox },
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};


			CustomLabel listviewlabel = new CustomLabel()
			{
				Text = "List view",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.Gray,
				FontSize = screenHeight / 36.8,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};

			BoxView listviewbox = new BoxView()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End,
				HeightRequest = screenHeight / 184
			};

			StackLayout listviewstack = new StackLayout()
			{
				Children = { listviewlabel, listviewbox },
				Spacing = 0,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			StackLayout tabstack = new StackLayout()
			{
				Children = { mapviewstack, listviewstack },
				HeightRequest = screenHeight / 16.35,
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};


			mapview1 = new CustomMap(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(_latitude, _longitude), Distance.FromMiles(100000)));


			int rowheight = Convert.ToInt16(screenHeight / 5.4);

			listview = new ListView()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				SeparatorColor = Color.Black,
				SeparatorVisibility = SeparatorVisibility.None,
				Margin = new Thickness(0, screenHeight / 10.98, 0, 0),
				//HasUnevenRows = true,
				RowHeight = rowheight
			};

			listview.ItemTemplate = new DataTemplate(typeof(cell));

			listview.ItemSelected += async (sender, e) =>
			{
				DBMethods gg = new DBMethods();
				var getloca = gg.GetUserInfo();

				if (((ListView)sender).SelectedItem == null)
				{
					return;
				}
				try
				{
					var itemSelected = e.SelectedItem as JobsListInfo;
					if (((ListView)sender).SelectedItem == null)
					{
						return;
					}
					if (Carousel.skip == true)
					{
						popupHolder.IsVisible = true;
						popupHolderBackground.IsVisible = true;
					}
					else
					{
						PageLoading.IsVisible = true;
						var showData = await GetDetailed(itemSelected.jobId);
						if (showData != null)
						{
							PageLoading.IsVisible = true;
							await Navigation.PushModalAsync(new HomeDetailsPage(showData));
							PageLoading.IsVisible = false;
						}
						else
						{
							//Navigation.PushModalAsync(new HomeDetailsPage(itemSelected));
						}
					}
					((ListView)sender).SelectedItem = null;

				}
				catch (Exception ex)
				{
					((ListView)sender).SelectedItem = null;
					var msg = ex.Message;
					PageLoading.IsVisible = false;
				}
				((ListView)sender).SelectedItem = null;
				PageLoading.IsVisible = false;
			};

			StackLayout liststack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { listview },
				BackgroundColor = Color.FromRgb(227, 223, 224)
			};

			mapandliststack = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { mapview1 }
			};

			CustomLabel registerlabel = new CustomLabel()
			{
				Text = "Register",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.White,
				FontSize = screenHeight / 36.8,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				Carousel.skip = false;
				Application.Current.Properties["Detail"] = "Reg";
				App.Current.MainPage = new CountryMasterPage();
				//Navigation.PushModalAsync(new UserRegistration());
			};
			registerlabel.GestureRecognizers.Add(tapGestureRecognizer);


			var tapimgNotification = new TapGestureRecognizer();
			tapimgNotification.Tapped += (s, e) =>
			{
				if (Carousel.skip == false)
				{
					App.Current.MainPage = new NotificationsPage();
				}
				else
				{
					popupHolder.IsVisible = true;
					popupHolderBackground.IsVisible = true;
				}

			};
			imgNotification.GestureRecognizers.Add(tapimgNotification);


			StackLayout registerstack = new StackLayout()
			{
				Children = { registerlabel },
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			BoxView fotterbox = new BoxView()
			{
				VerticalOptions = LayoutOptions.Center,
				BackgroundColor = Color.White,
				WidthRequest = 1,
				HeightRequest = screenHeight / 29.44
			};

			CustomLabel loginlabel = new CustomLabel()
			{
				Text = "Login",
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.White,
				FontSize = screenHeight / 36.8,
				CustomFontFamily = AppGlobalVariables.fontFamily45
			};


			var lblLoginTap = new TapGestureRecognizer();
			lblLoginTap.Tapped += (s, e) =>
			{
				Carousel.skip = false;
				Navigation.PushModalAsync(new UserLogin());
			};
			loginlabel.GestureRecognizers.Add(lblLoginTap);



			StackLayout loginstack = new StackLayout()
			{
				Children = { loginlabel },
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};


			StackLayout mainstack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { headerstack, tabstack, mapandliststack },
				Spacing = 0
			};

			AbsoluteLayout.SetLayoutBounds(mainstack, new Rectangle(1, 1, 1, 1));
			AbsoluteLayout.SetLayoutFlags(mainstack, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(mainstack);


			Image searchiconimg = new Image()
			{
				Source = "imgSearchicon.png",
				HorizontalOptions = LayoutOptions.Start,
				HeightRequest = screenHeight / 18.4,
				WidthRequest = screenHeight / 18.4,

			};

			searchentry = new CustomEntry()
			{
				Placeholder = "Search....",
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			searchlistview = new ListView()
			{
				IsVisible = false,
				SeparatorColor = Color.Black,
				BackgroundColor = Color.White,
				RowHeight = 48
			};

			searchlistview.ItemTemplate = new DataTemplate(typeof(searchcell));

			searchlistview.ItemSelected += async (sender, e) =>
			{
				try
				{
					searchlistview.IsVisible = false;
					searchentry.Unfocus();
					if (((ListView)sender).SelectedItem == null)
					{
						return;
					}
					var item = ((ListView)sender).SelectedItem as Prediction;
					searchcomplete = "ok";
					searchentry.Text = item.description;
					loc = await new JobListBAL().GetAreaLocations(item.place_id);
					_latitude = loc.result.geometry.location.lat;
					_longitude = loc.result.geometry.location.lng;
					GetJobmap();
					GetJoblist();

					((ListView)sender).SelectedItem = null;
				}
				catch (Exception ex)
				{
					searchlistview.IsVisible = false;
				}
				searchlistview.IsVisible = false;
			};


			AbsoluteLayout.SetLayoutBounds(searchlistview, new Rectangle(0.5, Device.OnPlatform(0.35, 0.359, 0.188), 0.9, 0.3));
			AbsoluteLayout.SetLayoutFlags(searchlistview, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(searchlistview);

			searchentry.Unfocused += (sender, e) =>
			{
				searchlistview.IsVisible = false;
			};

			searchentry.Focused += (sender, e) =>
			{
				allmainstackpin.IsVisible = false;
			};


			searchentry.TextChanged += async (object sender, TextChangedEventArgs e) =>
			{
				allmainstackpin.IsVisible = false;
				if (_key == "map")
				{
					try
					{

						if (searchentry.Text == "" && searchentry.Text.Length == 0 && searchentry.Text == string.Empty && searchentry.Placeholder == "Search....")
						{
							searchlistview.IsVisible = false;
						}
						else
						{
							if (searchcomplete != "ok")
							{
								if (CheckNetworkAccess.IsNetworkConnected())
								{
									//PageLoading.IsVisible = true;
									Arearesponse = await new JobListBAL().GetAreaDetails(searchentry.Text);
									//PageLoading.IsVisible = false;
									if (Arearesponse != null)
									{
										searchlistview.ItemsSource = Arearesponse.predictions.ToList();
										searchlistview.IsVisible = true;
									}
									else
									{
										await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
										//PageLoading.IsVisible = false;
									}


								}
								else
								{
									await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
									//PageLoading.IsVisible = false;
								}

							}
							else
							{
								searchlistview.IsVisible = false;
							}
							searchcomplete = null;
						}
					}
					catch (Exception efg)
					{
						//PageLoading.IsVisible = false;
					}
				}
				else
				{
					_searchkey = searchentry.Text;
					GetJoblist();
				}

			};


			StackLayout searchstack = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { searchiconimg, searchentry },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};

			CustomFrame searchframe = new CustomFrame()
			{
				Content = searchstack,
				Padding = new Thickness(screenHeight / 147.2, 0, screenHeight / 35.04, 0),
				HasShadow = false,
			};
			AbsoluteLayout.SetLayoutBounds(searchframe, new Rectangle(0.5, Device.OnPlatform(0.188, 0.195, 0.188), 0.95, 0.07));
			AbsoluteLayout.SetLayoutFlags(searchframe, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(searchframe);

			Image currentlocationiconimg = new Image()
			{
				Source = "imgCurrentLocation.png"
			};

			TapGestureRecognizer mylocationTap = new TapGestureRecognizer();
			mylocationTap.Tapped += async (sender, e) =>
			{
				PageLoading.IsVisible = true;
				await Task.Delay(2000);
				if (pinlistmapstring == "yes")
				{
					//PageLoading.IsVisible = true;
					mylocationTapmethod();
				}
				//PageLoading.IsVisible = false;
			};
			currentlocationiconimg.GestureRecognizers.Add(mylocationTap);

			AbsoluteLayout.SetLayoutBounds(currentlocationiconimg, new Rectangle(0.975, 0.925, 0.12, 0.12));
			AbsoluteLayout.SetLayoutFlags(currentlocationiconimg, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(currentlocationiconimg);


			Image MyProfilelocationimg = new Image()
			{
				Source = "imgMyProfilelocation.png"
			};

			TapGestureRecognizer myprofilelocationTap = new TapGestureRecognizer();
			myprofilelocationTap.Tapped += async (sender, e) =>
			{
				PageLoading.IsVisible = true;
				await Task.Delay(3000);
				//PageLoading.IsVisible = true;
				if (pinlistmapstring == "yes")
				{

					DBMethods obj1 = new DBMethods();
					UsersInfo getLocalDB;

					getLocalDB = obj1.GetUserInfo();

					try
					{
						if (getLocalDB != null)
						{
							_latitudeDB = Convert.ToDouble(getLocalDB._localLat);
							_longitudeDB = Convert.ToDouble(getLocalDB._localLog);
							try
							{
								if (_latitudeDB == 0 && _longitudeDB == 0)
								{
									//_latitudeDB = _latitude;
									//_longitudeDB = _longitude;
									try
									{
										IGeolocator locator2 = CrossGeolocator.Current;
										locator2.DesiredAccuracy = 50;
										if (locator2.IsGeolocationEnabled && locator2.IsGeolocationAvailable)
										{
											PageLoading.IsVisible = true;
											var position = await locator2.GetPositionAsync(timeoutMilliseconds: 10000);
											PageLoading.IsVisible = false;
											var profilelat = position.Latitude;
											var profilelog = position.Longitude;

											_latitudeDB = profilelat;
											_longitudeDB = profilelog;

										}
									}
									catch (Exception ex)
									{
										var msg = ex.Message;
									}
								}
							}
							catch (Exception ex)
							{
								var msg = ex.Message;
							}
						}
					}

					catch (Exception fgg)
					{

					}

					try
					{
						JobListReq obj = new JobListReq();
						obj.latitude = Convert.ToString(_latitudeDB);
						obj.longitude = Convert.ToString(_longitudeDB);
						//obj.latitude = "37.7163057";
						//obj.longitude = "-121.8826824";
						obj.type = "map";
						obj.searchkey = "";

						PageLoading.IsVisible = true;

						if (CheckNetworkAccess.IsNetworkConnected())
						{

							using (IJobListBAL JList = new JobListBAL())
							{
								PageLoading.IsVisible = true;
								var mapjoblist = await JList.GetJobList(obj);
								PageLoading.IsVisible = false;
								if (mapjoblist != null)
								{
									//PageLoading.IsVisible = false;
									joblistmap = mapjoblist;
									try
									{
										allPins.Clear();
									}
									catch (Exception ex)
									{
										var msg = ex.Message;
									}


									foreach (var i in joblistmap)
									{
										allPins.Add(new TKCustomMapPin()
										{
											Position = new Xamarin.Forms.Maps.Position(Convert.ToDouble(i.Latitude), Convert.ToDouble(i.Longitude)),
											Image = "imgsmallLocationpin.png",
											Title = "",
											ID = i.jobId
										});

									}
									mapandliststack.Children.Clear();
									mapview1 = new CustomMap(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(_latitudeDB, _longitudeDB), Distance.FromMiles(50)));
									mapview1.IsShowingUser = true;
									mapview1.CustomPins = allPins;
									//mapview1.MoveToMapRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(_latitude, _longitude), Distance.FromMiles(50)));
									mapandliststack.Children.Add(mapview1);
									mapview1.PinSelected += pinselected;
									PageLoading.IsVisible = false;

								}
								else
								{
									await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
									PageLoading.IsVisible = false;
								}
							}
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
							PageLoading.IsVisible = false;
						}
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					}

					//pinlistmapstring = null;
				}
				PageLoading.IsVisible = false;
			};
			MyProfilelocationimg.GestureRecognizers.Add(myprofilelocationTap);


			HorizontalGradientStack fotterstack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Orientation = StackOrientation.Horizontal,
				Children = { registerstack, fotterbox, loginstack },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				Opacity = 0.94
			};




			AbsoluteLayout.SetLayoutBounds(fotterstack, new Rectangle(1, 1, 1, 0.08));
			AbsoluteLayout.SetLayoutFlags(fotterstack, AbsoluteLayoutFlags.All);
			ablayout.Children.Add(fotterstack);


			TapGestureRecognizer mapviewstacktapGestureRecognizer = new TapGestureRecognizer();
			mapviewstacktapGestureRecognizer.Tapped += async (s, e) =>
			{
				if (Device.OS == TargetPlatform.Android)
				{
					PageLoading.IsVisible = true;
					await Task.Delay(1500);
					PageLoading.IsVisible = false;
				}
				mapviewbox.BackgroundColor = AppGlobalVariables.lightMarron;
				listviewbox.BackgroundColor = Color.White;
				mapviewlabel.TextColor = Color.Black;
				listviewlabel.TextColor = Color.Gray;

				mapandliststack.Children.Clear();
				mapandliststack.Children.Add(mapview1);
				ablayout.Children.Add(currentlocationiconimg);
				if (Carousel.skip == false)
				{
					ablayout.Children.Add(MyProfilelocationimg);

					AbsoluteLayout.SetLayoutBounds(MyProfilelocationimg, new Rectangle(0.01, 1.01, 0.12, 0.12));
					AbsoluteLayout.SetLayoutFlags(MyProfilelocationimg, AbsoluteLayoutFlags.All);
					ablayout.Children.Add(MyProfilelocationimg);
				}


				searchentry.Text = string.Empty;

				mylocationTapmethod();

				_key = "map";

			};
			mapviewstack.GestureRecognizers.Add(mapviewstacktapGestureRecognizer);
			mapviewstacktapGestureRecognizer.NumberOfTapsRequired = 1;


			TapGestureRecognizer listviewstacktapGestureRecognizer = new TapGestureRecognizer();
			listviewstacktapGestureRecognizer.Tapped += async (s, e) =>
			{

				if (pinlistmapstring == "yes")
				{
					if (Device.OS == TargetPlatform.Android)
					{
						PageLoading.IsVisible = true;
						await Task.Delay(1500);
						PageLoading.IsVisible = false;
					}
					listviewbox.BackgroundColor = AppGlobalVariables.lightMarron;
					mapviewbox.BackgroundColor = Color.White;
					listviewlabel.TextColor = Color.Black;
					mapviewlabel.TextColor = Color.Gray;

					mapandliststack.Children.Clear();
					ablayout.Children.Remove(currentlocationiconimg);
					ablayout.Children.Remove(MyProfilelocationimg);
					mapandliststack.Children.Add(liststack);

					searchentry.Text = string.Empty;

					searchlistview.IsVisible = false;

					_key = "listview";

					allmainstackpin.IsVisible = false;

					//pinlistmapstring = null;

				}

			};
			listviewstack.GestureRecognizers.Add(listviewstacktapGestureRecognizer);
			listviewstacktapGestureRecognizer.NumberOfTapsRequired = 1;


			TapGestureRecognizer TgimgMenu = new TapGestureRecognizer();
			TgimgMenu.Tapped += (s, e) =>
			{
				if (Carousel.skip == false)
				{
					try
					{
						HomeMenuPage.opv.GetNotificationlist();
						var ParentPage = (MasterDetailPage)this.Parent;
						ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					}
				}
				else
				{
					popupHolder.IsVisible = true;
					popupHolderBackground.IsVisible = true;
				}
			};
			imgMenu.GestureRecognizers.Add(TgimgMenu);


			if (Carousel.skip == false)
			{
				ablayout.Children.Remove(fotterstack);

				AbsoluteLayout.SetLayoutBounds(currentlocationiconimg, new Rectangle(0.975, 1.01, 0.12, 0.12));
				AbsoluteLayout.SetLayoutFlags(currentlocationiconimg, AbsoluteLayoutFlags.All);
				ablayout.Children.Add(currentlocationiconimg);

				AbsoluteLayout.SetLayoutBounds(MyProfilelocationimg, new Rectangle(0.01, 1.01, 0.12, 0.12));
				AbsoluteLayout.SetLayoutFlags(MyProfilelocationimg, AbsoluteLayoutFlags.All);
				ablayout.Children.Add(MyProfilelocationimg);

			}

			#region for Popup
			var popupHeight = (screenHeight * 58) / 100;//39.79;
			var popupWidth = (screenWidth * 75) / 100;//62.97;

			Label popupTitleLbl = new Label()
			{
				Text = "Don't have an account?",
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				//FontAttributes = FontAttributes.Bold,
				VerticalOptions = LayoutOptions.Center
			};

			Label popupbodyLbl = new Label()
			{
				Text = "Please Register/Login in order to access this feature.",
				FontFamily = AppGlobalVariables.fontFamily45,
				TextColor = Color.Gray,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(CustomEntry)),
				Margin = 15,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout popupTextHolder = new StackLayout()
			{
				Children = { popupTitleLbl, popupbodyLbl },
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};

			var ratingImageHeight = (popupWidth * 16) / 100;
			var popupButtonHeight = (popupWidth * 19) / 100;
			Button popupCancel = new Button()
			{
				Text = "Cancel",
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				BackgroundColor = AppGlobalVariables.lightMarron,
				BorderRadius = 0,
				HeightRequest = popupButtonHeight,//height * 10.211,
				WidthRequest = popupWidth / 2,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End,
				Command = new Command((obj) =>
				{
					popupHolder.IsVisible = false;
					popupHolderBackground.IsVisible = false;
				})
			};
			Button popupSubmit = new Button()
			{
				Text = "Register",
				TextColor = Color.White,
				FontFamily = AppGlobalVariables.fontFamily45,
				//StartColor = AppGlobalVariables.lightMarron,
				//EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = AppGlobalVariables.darkMarron,
				BorderRadius = 0,
				HeightRequest = popupButtonHeight,//height * 10.211,
				WidthRequest = popupWidth / 2,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};

			popupSubmit.Clicked += (object sender, EventArgs e) =>
			{
				Carousel.skip = false;
				Application.Current.Properties["Detail"] = "Reg";
				App.Current.MainPage = new CountryMasterPage();
				//await Navigation.PushModalAsync(new UserRegistration());
			};

			HorizontalGradientGrid popupButtonsHolder = new HorizontalGradientGrid()
			{
				RowDefinitions =
				{
					new RowDefinition{   Height = GridLength.Auto}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{  Width = GridLength.Auto},
					new ColumnDefinition{  Width = GridLength.Auto}
				},
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = Color.Blue,
				ColumnSpacing = 1,
				//HeightRequest = height * 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			popupButtonsHolder.Children.Add(popupCancel, 0, 0);
			popupButtonsHolder.Children.Add(popupSubmit, 1, 0);
			var imagePadding = (popupWidth * 11) / 100;
			ratingImageHolder = new StackLayout()
			{
				//Children = { rstar },
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				//HeightRequest = 60,
				Padding = new Thickness(imagePadding, 0, imagePadding, 0),
				WidthRequest = popupWidth,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			popupHolder = new StackLayout()
			{
				Children = { popupTextHolder, ratingImageHolder, popupButtonsHolder },
				IsVisible = false,
				Padding = new Thickness(0, 10, 0, 0),
				Spacing = (popupHeight * 5) / 100,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			popupHolderBackground = new StackLayout()
			{
				Children = { popupHolder },
				BackgroundColor = Color.FromRgba(0, 0, 0, 0.8),
				IsVisible = false,
			};



			holderContent = new AbsoluteLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			#endregion

			AbsoluteLayout.SetLayoutBounds(ablayout, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(ablayout, AbsoluteLayoutFlags.All);
			holderContent.Children.Add(ablayout);
			AbsoluteLayout.SetLayoutBounds(popupHolderBackground, new Rectangle(0.5, 0.5, 1, 1));
			AbsoluteLayout.SetLayoutFlags(popupHolderBackground, AbsoluteLayoutFlags.All);
			holderContent.Children.Add(popupHolderBackground);
			AbsoluteLayout.SetLayoutBounds(popupHolder, new Rectangle(0.5, 0.47, popupWidth, popupHeight));
			AbsoluteLayout.SetLayoutFlags(popupHolder, AbsoluteLayoutFlags.PositionProportional);
			holderContent.Children.Add(popupHolder);

			#region for pinpopup
			image1 = new Image()
			{
				VerticalOptions = LayoutOptions.StartAndExpand,
				HeightRequest = BaseContentPage.screenHeight / 21.02,
				WidthRequest = BaseContentPage.screenWidth / 11.82,
				Margin = new Thickness(0, 5, BaseContentPage.screenHeight / 49.06, 0),
				//Source = "imgJob.png"
				//Source = "imgPlumbericon.png"
				//Source = "imgJob.png"
			};

			titlelabel1 = new Label()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontSize = BaseContentPage.screenHeight / 38.73,
				TextColor = AppGlobalVariables.lightBlue
			};


			timelabel1 = new Label()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				FontSize = BaseContentPage.screenHeight / 52.57,
				TextColor = Color.Gray,
				//Text = "yesterday",
			};


			StackLayout topstack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { titlelabel1, timelabel1 },
				Orientation = StackOrientation.Horizontal,
			};



			ratingIcons11 = new Image() { };
			ratingIcons22 = new Image() { };
			ratingIcons33 = new Image() { };
			ratingIcons44 = new Image() { };
			ratingIcons55 = new Image() { };


			Grid ratingHolder = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
				Padding = new Thickness(0, 0, 0, 0),
				ColumnSpacing = 0,
				RowSpacing = 0,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				HeightRequest = BaseContentPage.screenHeight / 36.8,
			};
			ratingHolder.Children.Add(ratingIcons11, 0, 0);
			ratingHolder.Children.Add(ratingIcons22, 1, 0);
			ratingHolder.Children.Add(ratingIcons33, 2, 0);
			ratingHolder.Children.Add(ratingIcons44, 3, 0);
			ratingHolder.Children.Add(ratingIcons55, 4, 0);




			Label distancelabel = new Label()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
				FontSize = BaseContentPage.screenHeight / 52.57,
				TextColor = Color.Gray,
				Text = "Distance: "
			};

			mileslabel1 = new Label()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
				FontSize = BaseContentPage.screenHeight / 52.57,
				TextColor = Color.Black,
			};

			StackLayout middlestack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { ratingHolder, distancelabel, mileslabel1 },
				Orientation = StackOrientation.Horizontal,
				Spacing = 0
			};

			Label postedlabel = new Label()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
				FontSize = BaseContentPage.screenHeight / 52.57,
				TextColor = Color.Gray,
				Text = "Posted by: "
			};

			postedbylabel1 = new Label()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontSize = BaseContentPage.screenHeight / 52.57,
				TextColor = Color.Gray
			};

			StackLayout bottamstackstack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					postedlabel,
					postedbylabel1
				},
				Orientation = StackOrientation.Horizontal,
				Spacing = 0
			};

			StackLayout toplabelstack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					topstack, middlestack, bottamstackstack
				},
				Spacing = 0
			};

			StackLayout topmainstack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					image1, toplabelstack
				},
				Spacing = 0,
				Orientation = StackOrientation.Horizontal
			};

			discriptionlabel1 = new Label()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontSize = BaseContentPage.screenHeight / 52.57,
				TextColor = Color.Gray
			};


			string input1 = dispin;
			if (input1 == null)
			{

			}
			else
			{
				if (input1.Length >= 85)
				{
					string sub = input1.Substring(0, 85);
					discriptionlabel1.Text = sub + "....";
				}
				else
				{
					discriptionlabel1.Text = dispin;
				}
			}




			StackLayout bottommainstack = new StackLayout()
			{
				//VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					discriptionlabel1
				},
				Margin = new Thickness(BaseContentPage.screenHeight / 184, 0, 0, 0)
			};

			StackLayout mainstack1 = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					topmainstack,
					bottommainstack
                    //gridDes
                },
				Margin = new Thickness(BaseContentPage.screenHeight / 36.8, BaseContentPage.screenHeight / 49.06, 0, Device.OnPlatform(BaseContentPage.screenHeight / 24.53, BaseContentPage.screenHeight / 29.44, BaseContentPage.screenHeight / 24.53))
			};

			Image imgarrow = new Image()
			{
				HeightRequest = screenHeight / 19.36,
				WidthRequest = screenHeight / 19.36,
				Source = "imgRightArrow.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand
			};

			StackLayout callstack = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.End,
				Children = { imgarrow },
				WidthRequest = BaseContentPage.screenWidth / 5.45

			};

			allmainstackpin = new StackLayout()
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = { mainstack1, callstack },
				Orientation = StackOrientation.Horizontal,
				BackgroundColor = Color.White,
				IsVisible = false
				//Margin = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 105.14)
			};


			TapGestureRecognizer calltapGesture = new TapGestureRecognizer();
			calltapGesture.SetBinding(TapGestureRecognizer.CommandParameterProperty, "jobId");

			calltapGesture.Tapped += async (object sender, EventArgs e) =>
			{
				if (Carousel.skip == true)
				{
					popupHolder.IsVisible = true;
					popupHolderBackground.IsVisible = true;
				}
				else
				{
					var showData = await GetDetailed(pinjobid);
					if (showData != null)
					{
						await Navigation.PushModalAsync(new HomeDetailsPage(showData));
					}
					else
					{
						//Navigation.PushModalAsync(new HomeDetailsPage(itemSelected));
					}
				}

				allmainstackpin.IsVisible = false;

			};
			allmainstackpin.GestureRecognizers.Add(calltapGesture);

			//ablayout.Children.Add(allmainstackpin);

			#endregion

			mapview1.PinSelected += pinselected;
			//mapview1.MapClicked += mapclicked;
			//mapview1.Unfocused += mapclicked1;

			//Content = holderContent;
			//PageLoading.IsVisible = true;
			PageControlsStackLayout.Children.Add(holderContent);
			//PageLoading.IsVisible = false;

		}

		/* selected job details service call and response */
		JobsListInfo JLInfo;
		async Task<JobsListInfo> GetDetailed(string jobId)
		{

			try
			{
				DBMethods objDatabase = new DBMethods();
				if (objDatabase != null)
				{
					var userResp = objDatabase.GetUserInfo();
					JobDetailsReq reqObj = new JobDetailsReq();
					reqObj.jobid = jobId;
					reqObj.seeker_id = userResp.UserID;


					reqObj.latitude = Convert.ToString(_latitude);
					reqObj.longitude = Convert.ToString(_longitude);

					//DBMethods objdb = new DBMethods();
					//UsersInfo getLocaldb;
					//string lat, log;
					//getLocaldb = objdb.GetUserInfo();
					//if (getLocaldb != null)
					//{
					//	lat = getLocaldb._localLat;
					//	log = getLocaldb._localLog;
					//	reqObj.latitude = lat;
					//	reqObj.latitude = log;
					//}


					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (IJobDetailsBAL getJobDetails = new JobDetailsBAL())
						{
							var detailsResponse = await getJobDetails.jobDetailsData(reqObj);
							if (detailsResponse != null)
							{
								if (detailsResponse.Status == 0)
								{
									JLInfo = new JobsListInfo()
									{
										amountType = detailsResponse.JobDetails[0].amountType,
										Favourite = detailsResponse.JobDetails[0].Favourite,
										companyName = detailsResponse.JobDetails[0].companyName,
										Compensention = detailsResponse.JobDetails[0].Compensention,
										createdDate = detailsResponse.JobDetails[0].createdDate,
										jobId = detailsResponse.JobDetails[0].jobId,
										Status = detailsResponse.JobDetails[0].Status,
										Distance = detailsResponse.JobDetails[0].Distance,
										endDate = detailsResponse.JobDetails[0].endDate,
										Experience = detailsResponse.JobDetails[0].Experience,
										FaceBook = detailsResponse.JobDetails[0].FaceBook,
										firstName = detailsResponse.JobDetails[0].firstName,
										jobAddress = detailsResponse.JobDetails[0].jobAddress,
										jobCity = detailsResponse.JobDetails[0].jobCity,
										jobCountryId = detailsResponse.JobDetails[0].jobCountryId,
										jobDescription = detailsResponse.JobDetails[0].jobDescription,
										jobEmail = detailsResponse.JobDetails[0].jobEmail,
										jobStateId = detailsResponse.JobDetails[0].jobStateId,
										jobTitle = detailsResponse.JobDetails[0].jobTitle,
										jobZipCode = detailsResponse.JobDetails[0].jobZipCode,
										lastName = detailsResponse.JobDetails[0].lastName,
										Latitude = detailsResponse.JobDetails[0].Latitude,
										linkesIn = detailsResponse.JobDetails[0].linkesIn,
										Longitude = detailsResponse.JobDetails[0].Longitude,
										modifiedDate = detailsResponse.JobDetails[0].modifiedDate,
										Name = detailsResponse.JobDetails[0].Name,
										noOfPositions = detailsResponse.JobDetails[0].noOfPositions,
										perHourDay = detailsResponse.JobDetails[0].perHourDay,
										profile_pic = detailsResponse.JobDetails[0].profile_pic,
										Providerid = detailsResponse.JobDetails[0].Providerid,
										providerPhoneNumber = detailsResponse.JobDetails[0].providerPhoneNumber,
										ProviderRating = detailsResponse.JobDetails[0].ProviderRating,
										startDate = detailsResponse.JobDetails[0].startDate,
										Twitter = detailsResponse.JobDetails[0].Twitter,
										workTypeID = detailsResponse.JobDetails[0].workTypeID,
										workTypeName = detailsResponse.JobDetails[0].workTypeName,
										Applied = detailsResponse.JobDetails[0].Applied,
										isInterview = detailsResponse.JobDetails[0].isInterview,
										countryName = detailsResponse.JobDetails[0].countryName,
										stateName = detailsResponse.JobDetails[0].stateName
									};
								}
								else
								{
									JLInfo = null;
									//await DisplayAlert("OneJob", "", "ok");
								}
							}
							else
							{
								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								PageLoading.IsVisible = false;
							}

						}

					}
					else
					{
						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
						PageLoading.IsVisible = false;
					}
				}
				else
				{
					JLInfo = null;
					await DisplayAlert("OneJob", "Please login again", "ok");
				}
			}
			catch (Exception ex)
			{
				JLInfo = null;
				await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
				var msg = ex.Message;
			}
			return JLInfo;
		}


		/* for getting user current locations */
		async void mylocationTapmethod()
		{
			PageLoading.IsVisible = true;
			try
			{
				locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;
				if (locator.IsGeolocationEnabled && locator.IsGeolocationAvailable)
				{
					PageLoading.IsVisible = true;
					if (Device.OS == TargetPlatform.Android)
					{
						var position = await locator.GetPositionAsync(timeoutMilliseconds: 25000);
						_latitude = position.Latitude;
						_longitude = position.Longitude;
					}
					else
					{
						var position = await locator.GetPositionAsync(timeoutMilliseconds: 15000);
						_latitude = position.Latitude;
						_longitude = position.Longitude;
					}

					//PageLoading.IsVisible = false;


					PageLoading.IsVisible = false;
					GetJobmap();
					GetJoblist();
				}
				else
				{
					await DisplayAlert("Message", "Please enable Gps for locations.", "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				await DisplayAlert("Message", "Unable to get location, Please tap on  my location icon.", "Ok");
				pinlistmapstring = "yes";
			}
		}

		/* for showing job pins in map */

		async void GetJobmap()
		{
			try
			{
				JobListReq obj = new JobListReq();
				obj.latitude = Convert.ToString(_latitude);
				obj.longitude = Convert.ToString(_longitude);
				//obj.latitude = "37.7163057";
				//obj.longitude = "-121.8826824";
				obj.type = "map";
				obj.searchkey = "";

				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (IJobListBAL JList = new JobListBAL())
					{

						var mapjoblist = await JList.GetJobList(obj);
						if (mapjoblist != null)
						{
							PageLoading.IsVisible = true;
							joblistmap = mapjoblist;
							try
							{
								allPins.Clear();
							}
							catch (Exception ex)
							{
								var msg = ex.Message;
							}


							foreach (var i in joblistmap)
							{
								allPins.Add(new TKCustomMapPin()
								{
									Position = new Xamarin.Forms.Maps.Position(Convert.ToDouble(i.Latitude), Convert.ToDouble(i.Longitude)),
									Image = "imgsmallLocationpin.png",
									Title = "",
									ID = i.jobId
								});

							}
							mapandliststack.Children.Clear();
							mapview1 = new CustomMap(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(_latitude, _longitude), Distance.FromMiles(50)));
							mapview1.IsShowingUser = true;
							mapview1.CustomPins = allPins;
							mapandliststack.Children.Add(mapview1);
							pinlistmapstring = "yes";
							mapview1.PinSelected += pinselected;
							//mapview1.MapClicked += mapclicked;
							//mapview1.Unfocused += mapclicked1;
							PageLoading.IsVisible = false;

						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}
					}
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

		/* job pin selected event handler */
		void pinselected(object sender, TKGenericEventArgs<TKCustomMapPin> e)
		{
			allmainstackpin.IsVisible = false;
			try
			{
				var item = ((TKCustomMap)sender).SelectedPin as TKCustomMapPin;

				foreach (var k in allPins)
				{
					//k.Image = "imgsmallLocationpin.png";
					if (k.ID == item.ID)
					{

						foreach (var m in joblistmap)
						{
							if (item.ID == m.jobId)
							{
								//item.Image = "imglargeLocationpin.png";
								pinjobid = m.jobId;

								if (m.profile_pic == "")
								{
									image1.Source = "imgJob.png";
								}
								else
								{
									image1.Source = m.profile_pic;
								}

								//titlelabel1.Text = m.jobTitle;
								titledispin = m.jobTitle;
								string titledis = titledispin;

								if (titledis == null)
								{

								}
								else
								{
									if (titledis.Length >= 12)
									{
										string sub = titledis.Substring(0, 12);
										titlelabel1.Text = sub + "....";
									}
									else
									{
										titlelabel1.Text = titledispin;
									}
								}

								timelabel1.Text = m.createdDate;
								ratepin = Double.Parse(m.ProviderRating);

								var rate1 = ratepin;
								if (rate1 != null)
								{
									if (rate1 <= 0.4)
									{
										ratingIcons11.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons22.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons33.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons44.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons55.Source = ImageSource.FromFile("imgRatingNone");
									}
									else if (rate1 == 0.5 || rate1 <= 1.4)
									{
										ratingIcons11.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons22.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons33.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons44.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons55.Source = ImageSource.FromFile("imgRatingNone");
									}
									else if (rate1 == 1.5 || rate1 <= 2.4)
									{
										ratingIcons11.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons22.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons33.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons44.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons55.Source = ImageSource.FromFile("imgRatingNone");
									}
									else if (rate1 == 2.5 || rate1 <= 3.4)
									{
										ratingIcons11.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons22.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons33.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons44.Source = ImageSource.FromFile("imgRatingNone");
										ratingIcons55.Source = ImageSource.FromFile("imgRatingNone");
									}
									else if (rate1 == 3.5 || rate1 <= 4.4)
									{
										ratingIcons11.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons22.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons33.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons44.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons55.Source = ImageSource.FromFile("imgRatingNone");
									}
									else if (rate1 >= 4.5)
									{
										ratingIcons11.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons22.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons33.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons44.Source = ImageSource.FromFile("imgRatingDone");
										ratingIcons55.Source = ImageSource.FromFile("imgRatingDone");
									}

								}
								else
								{
									ratingIcons11.Source = ImageSource.FromFile("imgRatingNone");
									ratingIcons22.Source = ImageSource.FromFile("imgRatingNone");
									ratingIcons33.Source = ImageSource.FromFile("imgRatingNone");
									ratingIcons44.Source = ImageSource.FromFile("imgRatingNone");
									ratingIcons55.Source = ImageSource.FromFile("imgRatingNone");
								}
								mileslabel1.Text = m.Distance;

								postedbylabel1.Text = m.companyName;
								discriptionlabel1.Text = m.jobDescription;

								AbsoluteLayout.SetLayoutBounds(allmainstackpin, new Rectangle(1, 1, 1, 0.178));
								AbsoluteLayout.SetLayoutFlags(allmainstackpin, AbsoluteLayoutFlags.All);
								holderContent.Children.Add(allmainstackpin);

							}

						}
					}


				}

				allmainstackpin.IsVisible = true;

				((TKCustomMap)sender).SelectedPin = null;
			}
			catch (Exception ex)
			{

			}
		}


		/* list of jobs service call and response */
		async void GetJoblist()
		{
			listview.ItemsSource = null;
			try
			{
				JobListReq obj = new JobListReq();
				obj.latitude = Convert.ToString(_latitude);
				obj.longitude = Convert.ToString(_longitude);


				obj.type = "list";
				obj.searchkey = _searchkey;

				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{
					using (IJobListBAL JList = new JobListBAL())
					{
						PageLoading.IsVisible = true;
						var joblist = await JList.GetJobList(obj);
						PageLoading.IsVisible = false;

						if (joblist != null)
						{
							listview.ItemsSource = joblist;
							joblistlist = joblist;
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}

					}
					PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

	}


	/* viewcell for location search listview */
	public class searchcell : ViewCell
	{
		protected override void OnBindingContextChanged()
		{


			base.OnBindingContextChanged();

			dynamic searchitem = BindingContext;

			try
			{
				Label addresslabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					FontSize = BaseContentPage.screenHeight / 38.73,
					TextColor = Color.Black,
					Margin = BaseContentPage.screenHeight / 49.06
				};
				//addresslabel.SetBinding(Label.TextProperty, new Binding("description"));


				string input = searchitem.description;
				if (input == null)
				{
					addresslabel.SetBinding(Label.TextProperty, new Binding("description"));
				}
				else
				{
					if (input.Length >= 48)
					{
						string sub = input.Substring(0, 48);
						addresslabel.Text = sub + "...";
					}
					else
					{
						addresslabel.Text = searchitem.description;
					}
				}

				StackLayout stack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { addresslabel }
				};

				View = stack;
			}
			catch (Exception ex)
			{

			}
		}
	}


	/* viewcell for list of jobs listview */
	public class cell : ViewCell
	{
		protected override void OnBindingContextChanged()
		{

			try
			{
				base.OnBindingContextChanged();

				dynamic c = BindingContext;
				Image image = new Image()
				{
					VerticalOptions = LayoutOptions.StartAndExpand,
					HeightRequest = BaseContentPage.screenHeight / 21.02,
					WidthRequest = BaseContentPage.screenWidth / 11.82,
					Margin = new Thickness(0, 5, BaseContentPage.screenHeight / 49.06, 0),
					//Source = "imgPlumbericon.png"
					Source = "imgJob.png"
				};

				if (c.profile_pic != null && c.profile_pic != "")
				{
					image.SetBinding(Image.SourceProperty, new Binding("profile_pic"));
					//image.Source = new UriImageSource()
					//{
					//    Uri = new Uri(c.profile_pic),
					//    CachingEnabled = false
					//};
				}
				else
				{
					image.Source = Device.OnPlatform("imgJob.png", "imgJob.png", "imgJob.png");
				}

				//image.SetBinding(Image.SourceProperty, new Binding("profile"));

				Label titlelabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					FontSize = BaseContentPage.screenHeight / 38.73,
					TextColor = AppGlobalVariables.lightBlue
				};
				string titleString = c.jobTitle;
				if (titleString.Length >= 17)
				{
					titleString = titleString.Remove(14);
					titleString = titleString + "...";
					titlelabel.Text = titleString;
				}
				else
				{
					titlelabel.SetBinding(Label.TextProperty, new Binding("jobTitle"));
				}

				Label timelabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.End,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray,
					//Text = "yesterday",
				};
				timelabel.SetBinding(Label.TextProperty, new Binding("createdDate"));
				//timelabel.SetBinding(Label.TextProperty, new Binding("createdDate"));

				StackLayout topstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { titlelabel, timelabel },
					Orientation = StackOrientation.Horizontal,

				};




				var ratingIcons1 = new Image() { };
				var ratingIcons2 = new Image() { };
				var ratingIcons3 = new Image() { };
				var ratingIcons4 = new Image() { };
				var ratingIcons5 = new Image() { };



				#region Rating Logic

				var rate = Double.Parse(c.ProviderRating);
				if (rate != null)
				{
					if (rate <= 0.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 0.5 || rate <= 1.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 1.5 || rate <= 2.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 2.5 || rate <= 3.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 3.5 || rate <= 4.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate >= 4.5)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingDone");
					}

				}
				else
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}

				#endregion



				Grid ratingHolder = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
					ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
					Padding = new Thickness(0, 0, 0, 0),
					ColumnSpacing = 0,
					RowSpacing = 0,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					HeightRequest = BaseContentPage.screenHeight / 36.8,
				};
				ratingHolder.Children.Add(ratingIcons1, 0, 0);
				ratingHolder.Children.Add(ratingIcons2, 1, 0);
				ratingHolder.Children.Add(ratingIcons3, 2, 0);
				ratingHolder.Children.Add(ratingIcons4, 3, 0);
				ratingHolder.Children.Add(ratingIcons5, 4, 0);




				Label distancelabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.End,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray,
					Text = "Distance: "
				};
				//distancelabel.SetBinding(Label.TextProperty, new Binding("distance"));

				Label mileslabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.End,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Black,
				};
				//mileslabel.SetBinding(Label.TextProperty, new Binding("Distance"));



				string input2 = c.Distance;
				if (input2 == null)
				{
					mileslabel.SetBinding(Label.TextProperty, new Binding("Distance"));
				}
				else
				{
					if (input2.Length >= 9)
					{
						string sub = input2.Substring(0, 9);
						mileslabel.Text = sub;
					}
					else
					{
						mileslabel.Text = c.Distance;
					}
				}



				//mileslabel.SetBinding(Label.TextProperty, new Binding("distance"));

				StackLayout middlestack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { ratingHolder, distancelabel, mileslabel },
					Orientation = StackOrientation.Horizontal,
					Spacing = 0
				};

				Label postedlabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.Start,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray,
					Text = "Posted by: "
				};
				//postedlabel.SetBinding(Label.TextProperty, new Binding("Posted"));

				Label postedbylabel = new Label()
				{
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.StartAndExpand,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray
				};
				postedbylabel.SetBinding(Label.TextProperty, new Binding("companyName"));

				StackLayout bottamstackstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					postedlabel,
					postedbylabel
				},
					Orientation = StackOrientation.Horizontal,
					Spacing = 00
				};

				StackLayout toplabelstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					topstack, middlestack, bottamstackstack
				},
					Spacing = 0
				};

				StackLayout topmainstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					image, toplabelstack
				},
					Spacing = 0,
					Orientation = StackOrientation.Horizontal
				};

				Label discriptionlabel = new Label()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					FontSize = BaseContentPage.screenHeight / 52.57,
					TextColor = Color.Gray
				};


				string input = c.jobDescription;
				if (input == null)
				{
					discriptionlabel.SetBinding(Label.TextProperty, new Binding("jobDescription"));
				}
				else
				{
					if (input.Length >= 80)
					{
						string sub = input.Substring(0, 80);
						discriptionlabel.Text = sub + "....";
					}
					else
					{
						discriptionlabel.Text = c.jobDescription;
					}
				}




				StackLayout bottommainstack = new StackLayout()
				{
					//VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
					discriptionlabel
				},
					//Margin = new Thickness(BaseContentPage.screenHeight / 184, 0, 0, 0)
				};

				StackLayout mainstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					//BackgroundColor = Color.Red,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					WidthRequest = BaseContentPage.screenWidth / 1.4,
					Spacing = 0,
					Children = {
					topmainstack,
					bottommainstack
                    //gridDes
                },
					Margin = new Thickness(BaseContentPage.screenHeight / 36.8, BaseContentPage.screenHeight / 49.06, 0, Device.OnPlatform(BaseContentPage.screenHeight / 24.53, BaseContentPage.screenHeight / 29.44, BaseContentPage.screenHeight / 24.53))
				};

				Image call = new Image()
				{
					VerticalOptions = LayoutOptions.EndAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Source = "callButton.png",
					HeightRequest = BaseContentPage.screenHeight / 21.02,
					WidthRequest = BaseContentPage.screenWidth / 10,
					Margin = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 73.6)
				};

				StackLayout callstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { call },
					//WidthRequest = BaseContentPage.screenWidth / 5.45,
					//Padding = new Thickness(0, 0, 10, 0)

				};

				TapGestureRecognizer calltapGesture = new TapGestureRecognizer();
				calltapGesture.SetBinding(TapGestureRecognizer.CommandParameterProperty, "jobId");

				calltapGesture.Tapped += (object sender, EventArgs e) =>
			{
				if (Carousel.skip == true)
				{
					HomePage.homePages.popupHolder.IsVisible = true;
					HomePage.homePages.popupHolderBackground.IsVisible = true;
				}
				else
				{
					var _position = (((TappedEventArgs)e).Parameter).ToString();

					foreach (var i in HomePage.joblistlist)
					{
						if (i.jobId == _position)
						{
							if (i.providerPhoneNumber == "")
							{
								App.Current.MainPage.DisplayAlert("OneJob", "Provider Phone Number not available.", "Ok");
							}
							else
							{
								Device.OpenUri(new Uri("tel:" + i.providerPhoneNumber));
							}

						}
					}
				}
			};
				callstack.GestureRecognizers.Add(calltapGesture);

				StackLayout allmainstack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.StartAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { mainstack, callstack },
					Orientation = StackOrientation.Horizontal,
					Spacing = 0,
					HeightRequest = BaseContentPage.screenHeight / 8,
					BackgroundColor = Color.White
					//BackgroundColor = Color.Green,
					//Margin = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 105.14)
				};

				BoxView boxx = new BoxView()
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					HeightRequest = BaseContentPage.screenHeight / 105.14,
					//BackgroundColor = Color.Aqua
					BackgroundColor = Color.FromRgb(227, 223, 224),
					//Color = Color.FromRgb(227, 223, 224),
				};

				StackLayout stack = new StackLayout()
				{
					VerticalOptions = LayoutOptions.FillAndExpand,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = { allmainstack, boxx },
					Spacing = 0,
					BackgroundColor = Color.White
				};
				View = stack;

			}
			catch (Exception ex)
			{

			}

		}
	}

	public class RoutePin : TKCustomMapPin
	{
		public bool IsSource { get; set; }
		public TKRoute Route { get; set; }
	}


	public class CustomMap : TKCustomMap
	{
		public CustomMap(MapSpan region) : base(region)
		{

		}
	}

}
