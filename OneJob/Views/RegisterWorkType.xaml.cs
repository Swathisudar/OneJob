﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;

namespace OneJob
{
	public partial class RegisterWorkType : BaseContentPage
	{
		MyListView detailsList;
		ObservableCollection<Details> jobDetailsMenu;
		public List<Details> itemsSelected;
		public static RegisterWorkType rWType;
		string[] userData;

		string getUserID;
		string getCountryCode;
		string editorAddress;
		string dateOfBirth;
		string entryFirstName;
		string entryLastName;
		string entryMobileNo;
		string genderTest;
		List<int> lanugegesdata;
		List<LanguageId> ClassData;
		string pageInfo;
		string profilelocationName;
		string Page;
		public double profileLat;
		public double profileLog;

		public RegisterWorkType(string[] userProfileData, List<int> Languagesdata, string pageType, List<LanguageId> languaguesClassData, double Lat, double Log, string locationName, string data)
		{
			InitializeComponent();
			Page = data;
			profileLat = Lat;
			profileLog = Log;
			profilelocationName = locationName;

			rWType = this;
			itemsSelected = new List<Details>();
			ClassData = languaguesClassData;
			userData = userProfileData;
			pageInfo = pageType;

			if (userData != null)
			{
				getUserID = userData[0];
				getCountryCode = userData[1];
				editorAddress = userData[2];
				dateOfBirth = userData[3];
				entryFirstName = userData[4];
				entryLastName = userData[5];
				entryMobileNo = userData[6];
				genderTest = userData[7];
			}
			lanugegesdata = new List<int>();
			if (Languagesdata != null)
			{
				lanugegesdata = Languagesdata;
			}


			var height = (screenHeight * 1) / 100;
			var width = (screenWidth * 1) / 100;

			#region for populating the listview
			jobDetailsMenu = new ObservableCollection<Details>();
			#endregion

			#region for header Stack
			var headerHeight = height * 9.4824;
			var headerChildrenHeight = height * 7;
			var headerChildrenWidth = height * 7;

			var navigationBack = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			var titleSize = height * 3;
			var pageTitle = new Label()
			{
				Text = "Work Type",
				TextColor = Color.White,
				//BackgroundColor = Color.Green,
				FontSize = titleSize,
				HeightRequest = headerChildrenHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			var headerHolder = new HorizontalGradientStack()
			{
				Padding = new Thickness(0, 10, 0, 5),
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				Children = { navigationBack, pageTitle, new BoxView { HorizontalOptions = LayoutOptions.EndAndExpand } },
				//BackgroundColor = AppGlobalVariables.deepGray,
				Orientation = StackOrientation.Horizontal,
				HeightRequest = headerHeight,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start
			};

			var menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {

			   //var ParentPage = (MasterDetailPage)this.Parent;
			   //ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			   if (Page == "profileTest")
			   {
				   Application.Current.Properties["Detail"] = "ProfileLocation";
				   App.Current.MainPage = new LocationsMaterPage();
			   }
			   else
			   {
				   var ParentPage = (MasterDetailPage)this.Parent;
				   ParentPage.Detail = new HomePage() { BackgroundColor = Color.White };
			   }


		   };
			navigationBack.GestureRecognizers.Add(menuClicked);


			#endregion

			#region for body
			//var borderThick = (width * 5) / 100;
			detailsList = new MyListView()
			{
				HasUnevenRows = true,
				//SelectedItem = null,
				//Margin = new Thickness(borderThick, borderThick, borderThick, borderThick),
				BackgroundColor = AppGlobalVariables.lightGray,
				//SeparatorColor = Color.Maroon,
				//ItemsSource = jobDetailsMenu,
				SeparatorVisibility = SeparatorVisibility.None,
				ItemTemplate = new DataTemplate(typeof(TempLayouts)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			WorkType(detailsList);

			detailsList.ItemSelected += null;
			detailsList.ItemSelected += (object sender, SelectedItemChangedEventArgs e) =>
			{

				//((ListView)sender).SelectedItem = null;
				var item = ((ListView)sender).SelectedItem as Details;
				if (item == null)
				{
					return;
				}

				if (item.IsTick == true)
				{
					item.Tick = false;

				}
				else
				{
					item.Tick = true;
				}

				if (item.Tick == true)
				{

				}
				else
				{
				}

				((ListView)sender).SelectedItem = null;
			};

			var bodyHolder = new StackLayout()
			{
				Children = { detailsList },
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			#endregion

			#region for footer
			var footerHeight = (height * 9.243);//height * 7.394;


			Image btnBackground = new Image()
			{
				Source = "imgLoginButton.png",
				Scale = 1.12,
				HeightRequest = height * 7.394,
				WidthRequest = width * 87.3125,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			CustomLabel imgLogin = new CustomLabel()
			{
				//Text = "Next",
				TextColor = Color.White,
				//FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			var text = App.Current.Properties["WorkFlow"].ToString();
			if (text == "Profile")
			{
				imgLogin.Text = "Next";
			}
			else
			{
				imgLogin.Text = "Done";
			}
			//TapGestureRecognizer nextClicked = new TapGestureRecognizer();
			//nextClicked.NumberOfTapsRequired = 1;
			//nextClicked.Tapped += async (object sender, EventArgs e) =>
			//{
			//	//btnBackground.Opacity = 0.5;
			//	imgLogin.Opacity = 0.5;
			//	await Task.Delay(100);
			//	//btnBackground.Opacity = 1;
			//	imgLogin.Opacity = 1;

			//};
			//btnBackground.GestureRecognizers.Add(nextClicked);
			//imgLogin.GestureRecognizers.Add(nextClicked);

			Grid ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = new GridLength(1, GridUnitType.Star)},
					//new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				//BackgroundColor = Color.Blue,
				//RowSpacing = 10,
				WidthRequest = width * 90.3125,//width * 85.3125,
				HeightRequest = height * 7.394,//(height*9.243),
				Padding = new Thickness(0, 0, 0, 0),

				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			//ImgButton.Children.Add(btnBackgroundStack, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);


			TapGestureRecognizer ImgButtonTap = new TapGestureRecognizer();
			ImgButtonTap.Tapped += btnNext_Click;

			ImgButton.GestureRecognizers.Add(ImgButtonTap);



			StackLayout footerStack = new StackLayout()
			{
				Children = { ImgButton },
				Padding = new Thickness(height * 1.76, 0, height * 1.76, height * 2.113),
				HeightRequest = footerHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			#endregion

			#region for holders
			var bodySpacing = height * 1.76;
			StackLayout holderStack = new StackLayout()
			{
				Children = { bodyHolder },
				Spacing = bodySpacing,
				Padding = new Thickness(bodySpacing, bodySpacing, bodySpacing, bodySpacing),
				BackgroundColor = AppGlobalVariables.lightGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			StackLayout holder = new StackLayout()
			{
				Children = { headerHolder, holderStack, footerStack },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 0),
				//Padding = new Thickness(0, bodySpacing, 0, 0),
				//BackgroundColor = AppGlobalVariables.deepGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			PageControlsStackLayout.Children.Add(holder);

			#endregion
		}


		private async void btnNext_Click(object sender, EventArgs e)
		{
			#region for general data
			if (RegisterWorkType.rWType.itemsSelected.Count > 0)
			{
				try
				{
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}

				DBMethods objDatabase = new DBMethods();
				var getLocalDB = objDatabase.GetUserInfo();
				//Navigation.PushModalAsync(new UploadPicture());
				//App.Current.MainPage = new HomeMasterPage();
				PageLoading.IsVisible = true;


				List<Worktypereq> objworktype = new List<Worktypereq>();
				foreach (var tem in RegisterWorkType.rWType.itemsSelected)
				{
					objworktype.Add(new Worktypereq() { worktype_id = Convert.ToInt32(tem.id), experience = Convert.ToInt32(tem.jobExprnce) });
				}
				objDatabase.DeleteWorkType();
				objDatabase.SaveUserWorkType(objworktype);
				var worktypwe = objDatabase.GetWorktype();

				UpdateProfileReq objUpdateProfileReq = new UpdateProfileReq();
				if (getLocalDB != null)
				{
					objUpdateProfileReq.id = getLocalDB.UserID;
					//objUpdateProfileReq.country_id = getLocalDB.CountryCode;
				}

				#endregion

				if (pageInfo == "HomeMenu")
				{
					try
					{

						var bhh = objDatabase.GetLaguages();
						if (bhh.Count > 0)
						{
							ClassData = new List<LanguageId>();
							lanugegesdata.Clear();
							foreach (var item in bhh)
							{
								ClassData.Add(new LanguageId
								{
									id = item.Id,
									language_name = item.LanguageName
								});
								lanugegesdata.Add(Convert.ToInt32(item.Id));
							}
						}
						else
						{
							//lanugegesdata.Add(0);
						}
						objUpdateProfileReq.optional_dial_code = getLocalDB.OptionalCountryCode;
						objUpdateProfileReq.address = getLocalDB.Address;
						objUpdateProfileReq.date_of_birth = getLocalDB.dateOFBirth;
						objUpdateProfileReq.first_name = getLocalDB.UserFirstName;
						objUpdateProfileReq.last_name = getLocalDB.UserLastName;
						//objUpdateProfileReq.e = "8";
						objUpdateProfileReq.job_description = "";
						objUpdateProfileReq.optional_phone = getLocalDB.OptionalMobile;
						//objUpdateProfileReq.worktype = objworktype;
						objUpdateProfileReq.gender = getLocalDB.Gender;
						objUpdateProfileReq.language_id = lanugegesdata;
						objUpdateProfileReq.worktypereq = objworktype;
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
					}
				}
				else
				{

					try
					{
						objDatabase.DeleteLanguages();

						objUpdateProfileReq.optional_dial_code = getCountryCode;
						objUpdateProfileReq.address = editorAddress;
						objUpdateProfileReq.date_of_birth = dateOfBirth;
						objUpdateProfileReq.first_name = entryFirstName;
						objUpdateProfileReq.last_name = entryLastName;
						//objUpdateProfileReq.e = "8";
						objUpdateProfileReq.job_description = "";
						objUpdateProfileReq.optional_phone = entryMobileNo;
						//objUpdateProfileReq.worktype = objworktype;
						objUpdateProfileReq.gender = genderTest;
						objUpdateProfileReq.language_id = lanugegesdata;
						objUpdateProfileReq.worktypereq = objworktype;
						objUpdateProfileReq.location = profilelocationName;
						objUpdateProfileReq.latitude = profileLat.ToString();
						objUpdateProfileReq.longitude = profileLog.ToString();

						objDatabase.DeleteLanguages();
						objDatabase.SaveUserLanguages(ClassData);
						var bhh = objDatabase.GetLaguages();
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
						PageLoading.IsVisible = false;
					}
				}

				#region for servicecall to update data
				try
				{
					using (IUpdateProfileBAL objUpdateProfile = new UpdateProfileBAL())
					{
						var responeUpdateProfile = await objUpdateProfile.UpdateUserProfile(objUpdateProfileReq);
						if (responeUpdateProfile != null)
						{
							if (responeUpdateProfile.errorCount == 0)
							{

								var saveUserDeatils = new UsersInfo
								{

									Address = responeUpdateProfile.userDetails.Address,
									dateOFBirth = responeUpdateProfile.userDetails.dateOfBirth,
									Gender = responeUpdateProfile.userDetails.Gender,

									OptionalMobile = responeUpdateProfile.userDetails.optionalNumber,
									UserFirstName = responeUpdateProfile.userDetails.firstName,
									UserLastName = responeUpdateProfile.userDetails.lastName,
									UserID = getLocalDB.UserID,
									CountryCode = getLocalDB.CountryCode,

									LocationName = responeUpdateProfile.userDetails.LocationNam,
									_localLog = responeUpdateProfile.userDetails.Longitude,
									_localLat = responeUpdateProfile.userDetails.Latitude,
									UserProfilePic = "",
									SecurityCode = "",
									UserPassword = "",
									jobDescription = " ",
									MobileNumber = getLocalDB.MobileNumber,
									OptionalCountryCode = responeUpdateProfile.userDetails.optionalDialCode// getCountryCode ?? getLocalDB.OptionalCountryCode
								};

								objDatabase.DeleteUserInfo();

								objDatabase.SaveUserInfo(saveUserDeatils);

								var getData = objDatabase.GetUserInfo();

								await DisplayAlert("OneJob", responeUpdateProfile.Message, "Ok");
								if (pageInfo == "HomeMenu")
								{
									try
									{
										var ParentPage = (MasterDetailPage)this.Parent;
										ParentPage.Detail = new HomePage() { BackgroundColor = Color.White };
									}
									catch (Exception ex)
									{
										var msg = ex.Message;
									}
								}
								else
								{
									await Navigation.PushModalAsync(new UploadPicture("worktype"));
								}
							}
							else
							{
								await DisplayAlert("OneJob", responeUpdateProfile.Message, "Ok");
								PageLoading.IsVisible = false;
							}
						}

						else
						{
							await DisplayAlert("OneJob", "Failed!", "Ok");
							PageLoading.IsVisible = false;
						}
					}
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
				#endregion
				PageLoading.IsVisible = false;
			}
			else
			{
				await DisplayAlert("OneJob", "Please select atleast one work type", "Ok");
			}
		}

		#region for service to get respective work Types
		public async void WorkType(ListView lt)
		{
			try
			{
				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{

					using (ISelectWorkTypeBAL selectWorkTypeList = new SelectWorkTypeBAL())
					{
						var coutriesList = await selectWorkTypeList.GetSelectWorkType();

						if (coutriesList != null)
						{
							foreach (var item in coutriesList)
							{
								jobDetailsMenu.Add(new Details() { jobTitle = item.worktypeName, id = item.ID });
							}

							DBMethods userWorksobjDatabase = new DBMethods();
							var userWorks = userWorksobjDatabase.GetWorktype();
							if (userWorks != null)
							{
								//var userWorks = objDatabase.GetWorktype();
								if (userWorks.Count > 0)
								{
									foreach (var item in userWorks)
									{
										foreach (var items in jobDetailsMenu)
										{
											if (item.ID == Convert.ToInt32(items.id))
											{
												items.Tick = true;
												items.jobExprnce = item.Experience;
											}
										}
									}
								}
								PageLoading.IsVisible = false;
							}

							lt.ItemsSource = jobDetailsMenu;
							PageLoading.IsVisible = false;
						}
						else
						{
							await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
							PageLoading.IsVisible = false;
						}

						//PageLoading.IsVisible = false;
					}
					//PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}
		#endregion

	}

	public class TempLayouts : ViewCell
	{
		public int width = (BaseContentPage.screenWidth * 1) / 100;
		public int height = (BaseContentPage.screenHeight * 1) / 100;
		public List<string> expNumbs;
		Label selectedExperience;

		protected override void OnBindingContextChanged()
		{
			//itemSelected = new List<Details>();
			base.OnBindingContextChanged();
			dynamic item = BindingContext as Details;
			var items = ((Details)item);

			var cellheight1 = height * 11.53;//9.771;//(height * 10.14) / 100;
			var cellheight2 = height * 23.98;//21.214;

			#region for body

			#region for toplayer
			var listSpacing = (height * 1.76) / 2;
			var cellHeadHeight = height * 9.771;
			Image jobIcon = new Image()
			{

			};
			Label jobName = new Label()
			{
				//Text = item.jobTitle,
				TextColor = Color.Black,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Center
			};
			jobName.SetBinding(Label.TextProperty, "jobTitle");

			Image checkimg = new Image()
			{
				Source = "imgCheckBox.png",
			};
			checkimg.SetBinding(Image.IsVisibleProperty, new Binding("true"));

			Image whitecheckimg = new Image()
			{
				Source = "imgCheckBoxWhite.png",
			};
			whitecheckimg.SetBinding(Image.IsVisibleProperty, new Binding("Tick"));

			var imgCheckTap = new TapGestureRecognizer();
			imgCheckTap.Tapped += (object sender, EventArgs e) =>
			{

			};
			checkimg.GestureRecognizers.Add(imgCheckTap);


			Image jobSelected = new Image()
			{
				Source = ImageSource.FromFile("imgCheckBoxWhite.png"),
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout jobSelectedHolder = new StackLayout()
			{
				Children = { jobSelected },
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.Center
			};
			//jobSelected.SetBinding(Switch.IsToggledProperty, new Binding(item.isSelected));
			Grid topLayer = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition { Height = new GridLength(1, GridUnitType.Star) }
				},
				ColumnDefinitions =
				{
					new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(6, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) }
				},
				Padding = new Thickness(listSpacing, 0, listSpacing, 0),
				HeightRequest = cellHeadHeight,
				BackgroundColor = Color.White,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start
			};
			//topLayer.Children.Add(jobIcon, 0, 0);
			topLayer.Children.Add(jobName, 0, 2, 0, 1);
			topLayer.Children.Add(jobSelectedHolder, 2, 0);
			//topLayer.Children.Add(checkimg, 2, 0);
			//topLayer.Children.Add(whitecheckimg, 2, 0);
			#endregion


			#region for bottom Layer
			var expLabelHeight = height * 3.697;
			var expNumHeight = height * 8.7464;

			Label expText = new Label()
			{
				Text = "Experience in years",
				TextColor = Color.Black,
				FontSize = height * 2.1,
				HorizontalOptions = LayoutOptions.Start,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalOptions = LayoutOptions.Center
			};
			StackLayout expTextStack = new StackLayout()
			{
				Children = { expText },
				Padding = new Thickness(10, 0, 0, 0),
				//IsVisible = false,
				BackgroundColor = Color.White,
				//BackgroundColor = Color.White,
				HeightRequest = expLabelHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			#region for experience labels
			Label expNumber0 = new Label()
			{
				Text = "0",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber1 = new Label()
			{
				Text = "1",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber2 = new Label()
			{
				Text = "2",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber3 = new Label()
			{
				Text = "3",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber4 = new Label()
			{
				Text = "4",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber5 = new Label()
			{
				Text = "5",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber6 = new Label()
			{
				Text = "6",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber7 = new Label()
			{
				Text = "7",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber8 = new Label()
			{
				Text = "8",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber9 = new Label()
			{
				Text = "9",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber10 = new Label()
			{
				Text = "10",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			Label expNumber11 = new Label()
			{
				Text = "10+",
				TextColor = Color.Black,
				BackgroundColor = Color.White,
				HeightRequest = expNumHeight,
				WidthRequest = expNumHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			#endregion
			#region for selected Experience Value
			selectedExperience = new Label();
			var selectedExp = new TapGestureRecognizer();
			selectedExp.NumberOfTapsRequired = 1;
			selectedExp.Tapped += (object sender, EventArgs e) =>
			{
				try
				{
					selectedExperience.BackgroundColor = Color.White;
					selectedExperience.TextColor = Color.Black;
					selectedExperience = (Label)sender;
					selectedExperience.BackgroundColor = AppGlobalVariables.lightBlue;
					selectedExperience.TextColor = Color.White;
					var dataSelected = item;
					//var labelText = selectedExperience.Text;

					//RegisterWorkType.rWType.itemsSelected.Where(x => x.id == item.id).FirstOrDefault().
					var selectedIndex = RegisterWorkType.rWType.itemsSelected.FindIndex(x => x.id == item.id);
					RegisterWorkType.rWType.itemsSelected[selectedIndex].jobExprnce = selectedExperience.Text;
					//if (item.IsTick == true)
					//{
					//	RegisterWorkType.rWType.itemsSelected.Add(new Details
					//	{
					//		id = ((Details)item).id,
					//		jobTitle = ((Details)item).jobTitle,
					//		jobExprnce = selectedExperience.Text
					//	});
					//}
					//else
					//{
					//	RegisterWorkType.rWType.itemsSelected.Add(new Details
					//	{
					//		id = ((Details)item).id,
					//		jobTitle = ((Details)item).jobTitle,
					//		jobExprnce = selectedExperience.Text
					//	});
					//}
					//selectedExperience = null;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			expNumber0.GestureRecognizers.Add(selectedExp);
			expNumber1.GestureRecognizers.Add(selectedExp);
			expNumber2.GestureRecognizers.Add(selectedExp);
			expNumber3.GestureRecognizers.Add(selectedExp);
			expNumber4.GestureRecognizers.Add(selectedExp);
			expNumber5.GestureRecognizers.Add(selectedExp);
			expNumber6.GestureRecognizers.Add(selectedExp);
			expNumber7.GestureRecognizers.Add(selectedExp);
			expNumber8.GestureRecognizers.Add(selectedExp);
			expNumber9.GestureRecognizers.Add(selectedExp);
			expNumber10.GestureRecognizers.Add(selectedExp);
			expNumber11.GestureRecognizers.Add(selectedExp);

			var expNumberStack = new StackLayout()
			{
				Children = { expNumber0, expNumber1, expNumber2, expNumber3, expNumber4, expNumber5, expNumber6, expNumber7, expNumber8, expNumber9, expNumber10 },
				//Children = { expNumber0, expNumber1, expNumber2, expNumber3, expNumber4, expNumber5, expNumber6, expNumber7, expNumber8, expNumber9, expNumber10, expNumber11 },
				//Padding = new Thickness( 5, 5, 5, 0),
				//BackgroundColor = Color.Maroon,
				Spacing = 0,
				Orientation = StackOrientation.Horizontal,
				HeightRequest = expNumHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			#endregion

			var expNumberStackHolder = new MyScrollBar()
			{
				//IsVisible = false,
				//BackgroundColor = Color.Maroon,
				BackgroundColor = Color.White,
				Orientation = ScrollOrientation.Horizontal,
				HeightRequest = expNumHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			expNumberStackHolder.Content = expNumberStack;

			if (Device.OS == TargetPlatform.iOS)
			{
				expTextStack.IsVisible = false;
				expNumberStackHolder.IsVisible = false;
			}
			else
			{
				expTextStack.Opacity = 0;
				expNumberStackHolder.Opacity = 0;
			}

			#endregion

			#endregion

			Grid bodyHolderGrid = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto }
					//new RowDefinition { Height = new GridLength(1, GridUnitType.Star)},
					//new RowDefinition { Height = new GridLength(0.5, GridUnitType.Star) },
					//new RowDefinition { Height = new GridLength(1, GridUnitType.Star)}
				},
				ColumnDefinitions =
				{
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
				},
				//HeightRequest = cellheight2,
				RowSpacing = 0,
				//Padding = new Thickness(0, 0, 0, listSpacing),
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start
			};
			bodyHolderGrid.Children.Add(topLayer, 0, 0);
			bodyHolderGrid.Children.Add(expTextStack, 0, 1);
			bodyHolderGrid.Children.Add(expNumberStackHolder, 0, 2);

			#region for holder
			StackLayout listHolder = new StackLayout()
			{
				Children = { bodyHolderGrid },
				//Padding = new Thickness(0,0,0,listSpacing),
				BackgroundColor = AppGlobalVariables.lightGray,
				HeightRequest = cellheight1,//cellheight2,//cellheight1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			if (items.IsTick == true)
			{
				jobSelected.Source = ImageSource.FromFile("Checked.png");
				listHolder.HeightRequest = cellheight2;
				this.ForceUpdateSize();
				//await Task.Delay(10);
				//expTextStack.IsVisible = true;
				//expNumberStackHolder.IsVisible = true;
				if (Device.OS == TargetPlatform.iOS)
				{
					expTextStack.IsVisible = true;
					expNumberStackHolder.IsVisible = true;
				}
				else
				{
					expTextStack.Opacity = 1;
					expNumberStackHolder.Opacity = 1;
				}

				switch (items.jobExprnce)
				{
					case "1":
						selectedExperience = expNumber1;
						break;
					case "2":
						selectedExperience = expNumber2;
						break;
					case "3":
						selectedExperience = expNumber3;
						break;
					case "4":
						selectedExperience = expNumber4;
						break;
					case "5":
						selectedExperience = expNumber5;
						break;
					case "6":
						selectedExperience = expNumber6;
						break;
					case "7":
						selectedExperience = expNumber7;
						break;
					case "8":
						selectedExperience = expNumber8;
						break;
					case "9":
						selectedExperience = expNumber9;
						break;
					case "10":
						selectedExperience = expNumber10;
						break;
					default:
						selectedExperience = expNumber0;
						break;
				}
				selectedExperience.BackgroundColor = AppGlobalVariables.lightBlue;
				RegisterWorkType.rWType.itemsSelected.Add(items);
			}
			else
			{
				jobSelected.Source = ImageSource.FromFile("imgCheckBoxWhite.png");
				listHolder.HeightRequest = cellheight1;
				this.ForceUpdateSize();
				//await Task.Delay(10);
				//expTextStack.IsVisible = false;
				//expNumberStackHolder.IsVisible = false;
				if (Device.OS == TargetPlatform.iOS)
				{
					expTextStack.IsVisible = false;
					expNumberStackHolder.IsVisible = false;
				}
				else
				{
					expTextStack.Opacity = 0;
					expNumberStackHolder.Opacity = 0;
				}
			}

			TapGestureRecognizer jobselecting = new TapGestureRecognizer();
			jobselecting.NumberOfTapsRequired = 1;
			jobselecting.Tapped += (object sender, EventArgs e) =>
			{
				try
				{
					selectedExperience.BackgroundColor = Color.White;
					selectedExperience.TextColor = Color.Black;
					if (item.IsTick == true)
					{
						item.IsTick = false;
						jobSelected.Source = ImageSource.FromFile("imgCheckBoxWhite.png");
						listHolder.HeightRequest = cellheight1;
						this.ForceUpdateSize();
						//await Task.Delay(10);
						//expTextStack.IsVisible = false;
						//expNumberStackHolder.IsVisible = false;
						if (Device.OS == TargetPlatform.iOS)
						{
							expTextStack.IsVisible = false;
							expNumberStackHolder.IsVisible = false;
						}
						else
						{
							expTextStack.Opacity = 0;
							expNumberStackHolder.Opacity = 0;
						}
						RegisterWorkType.rWType.itemsSelected.Remove(items);
					}
					else
					{
						item.IsTick = true;
						jobSelected.Source = ImageSource.FromFile("Checked.png");
						listHolder.HeightRequest = cellheight2;
						this.ForceUpdateSize();
						//await Task.Delay(10);
						//expTextStack.IsVisible = true;
						//expNumberStackHolder.IsVisible = true;
						if (Device.OS == TargetPlatform.iOS)
						{
							expTextStack.IsVisible = true;
							expNumberStackHolder.IsVisible = true;
						}
						else
						{
							expTextStack.Opacity = 1;
							expNumberStackHolder.Opacity = 1;
						}
						items.jobExprnce = "0";
						RegisterWorkType.rWType.itemsSelected.Add(items);
					}
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}

				//selectedExperience.BackgroundColor = Color.White;
				//selectedExperience.TextColor = Color.Black;
				//if (((Switch)sender).IsToggled == true)
				//{
				//	listHolder.HeightRequest = cellheight2;
				//	this.ForceUpdateSize();
				//	//await Task.Delay(10);
				//	//expTextStack.IsVisible = true;
				//	//expNumberStackHolder.IsVisible = true;
				//	if (Device.OS == TargetPlatform.iOS)
				//	{
				//		expTextStack.IsVisible = true;
				//		expNumberStackHolder.IsVisible = true;
				//	}
				//	else
				//	{
				//		expTextStack.Opacity = 1;
				//		expNumberStackHolder.Opacity = 1;
				//	}
				//}
				//else
				//{
				//	listHolder.HeightRequest = cellheight1;
				//	this.ForceUpdateSize();
				//	//await Task.Delay(10);
				//	//expTextStack.IsVisible = false;
				//	//expNumberStackHolder.IsVisible = false;
				//	if (Device.OS == TargetPlatform.iOS)
				//	{
				//		expTextStack.IsVisible = false;
				//		expNumberStackHolder.IsVisible = false;
				//	}
				//	else
				//	{
				//		expTextStack.Opacity = 0;
				//		expNumberStackHolder.Opacity = 0;
				//	}
				//}
			};
			jobSelected.GestureRecognizers.Add(jobselecting);

			//jobSelected.Tapped += (object sender, ToggledEventArgs e) =>
			//{
			//	selectedExperience.BackgroundColor = Color.White;
			//	selectedExperience.TextColor = Color.Black;
			//	if (((Switch)sender).IsToggled == true)
			//	{
			//		listHolder.HeightRequest = cellheight2;
			//		this.ForceUpdateSize();
			//		//await Task.Delay(10);
			//		//expTextStack.IsVisible = true;
			//		//expNumberStackHolder.IsVisible = true;
			//		if (Device.OS == TargetPlatform.iOS)
			//		{
			//			expTextStack.IsVisible = true;
			//			expNumberStackHolder.IsVisible = true;
			//		}
			//		else
			//		{
			//			expTextStack.Opacity = 1;
			//			expNumberStackHolder.Opacity = 1;
			//		}
			//	}
			//	else
			//	{
			//		listHolder.HeightRequest = cellheight1;
			//		this.ForceUpdateSize();
			//		//await Task.Delay(10);
			//		//expTextStack.IsVisible = false;
			//		//expNumberStackHolder.IsVisible = false;
			//		if (Device.OS == TargetPlatform.iOS)
			//		{
			//			expTextStack.IsVisible = false;
			//			expNumberStackHolder.IsVisible = false;
			//		}
			//		else
			//		{
			//			expTextStack.Opacity = 0;
			//			expNumberStackHolder.Opacity = 0;
			//		}
			//	}
			//};

			View = listHolder;
			#endregion

		}
	}


	public class Details : INotifyPropertyChanged
	{
		public string id { get; set; }

		public string jobTitle { get; set; }

		public string jobExprnce { get; set; }

		public bool isSelected { get; set; }

		public string imageUrl { get; set; }

		public bool IsTick = false;

		public bool Tick
		{
			get
			{
				return IsTick;
			}
			set
			{
				if (IsTick != value)
				{
					IsTick = value;
					PropertyChanged(this, new PropertyChangedEventArgs("Tick"));
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };
	}
}
