﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;

namespace OneJob
{
	public partial class UserRegistration : BaseContentPage
	{

		#region for Declaring User Variables
		// class level variables declaration
		CustomLabel haveAccount, signIn;
		CustomEntry mobileNo, password, confirmPassword;
		CustomLabel mobileNoLbl, passwordLbl, confirmPasswordLbl, pwdMatch;
		public static string updateimg;
		public static UserRegistration uRegister;
		public static CustomLabel regCountryCode;
		#endregion


		protected override bool OnBackButtonPressed()
		{


			App.Current.MainPage = new Carousel();
			return true;
		}

		public UserRegistration()
		{
			//InitializeComponent();
			BackgroundColor = Color.FromHex("#FFFFFF");
			uRegister = this;

			int height = screenHeight; //568;
			int width = screenWidth; //320;

			var entryHeight = (height * 8) / 100;

			//var widthEntry = (width * 76.36) / 100; 

			var entryWidth = (width * 81) / 100;


			regCountryCode = new CustomLabel()
			{
				Text = "+91",
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				TextColor = AppGlobalVariables.fontVeryThick,
				VerticalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Margin = new Thickness((entryWidth) / 12, 0, 0, 0),
				VerticalTextAlignment = TextAlignment.Center
			};

			//var Ccdoe = Application.Current.Properties["SelectedCountry"];
			//if (Ccdoe == null)
			//{
			//	regCountryCode.Text = "+91";
			//}
			//else
			//{
			//	regCountryCode.Text = "+" + Ccdoe.ToString();
			//}


			var regCountryCodeTap = new TapGestureRecognizer();
			regCountryCodeTap.Tapped += (s, e) =>
			{
				try
				{
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			regCountryCode.GestureRecognizers.Add(regCountryCodeTap);


			mobileNo = new CustomEntry()
			{
				Placeholder = " Mobile Number",
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				Keyboard = Keyboard.Numeric,
				HeightRequest = entryHeight,
				WidthRequest = (entryWidth * 2) / 2.2,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			if (Device.OS == TargetPlatform.Android)
			{
				mobileNo.Margin = new Thickness(0, 3, 0, 0);
			}


			Image imgCountryCode = new Image()
			{
				Source = "imgPicker.png"
			};

			var imgCountryCodeTap = new TapGestureRecognizer();
			imgCountryCodeTap.Tapped += (s, e) =>
			{
				try
				{
					var ParentPage = (MasterDetailPage)this.Parent;
					ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			imgCountryCode.GestureRecognizers.Add(imgCountryCodeTap);


			mobileNoLbl = new CustomLabel()
			{
				Text = "Mobile number",
				TextColor = AppGlobalVariables.fontLessThick,
				//TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			mobileNo.TextChanged += mobileNoChanged;

			mobileNo.Focused += (object sender, FocusEventArgs e) =>
			{
				//mobileNoLbl.FontSize = 10;
				//mobileNoLbl.IsVisible = true;
				//var textValue = mobileNo.Text;
				//if (textValue.Length == 10 || textValue.Length == 0)
				//{
				mobileNoLbl.IsVisible = true;
				mobileNoLbl.TextColor = AppGlobalVariables.fontLessThick;
				mobileNoLbl.Text = "Mobile number";
				//}
				//else {
				//    mobileNoLbl.TextColor = Color.Red;
				//    mobileNoLbl.Text = "Mobile number must be 10 digits";
				//}

				//mobileNoLbl.Text = "Mobile number";
				mobileNo.PlaceholderColor = Color.Transparent;
				//mobileNo.Placeholder = string.Empty;
			};
			mobileNo.Unfocused += (object sender, FocusEventArgs e) =>
			{
				mobileNoLbl.IsVisible = true;

				if (!string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					//passwordLbl.FontSize = 10;


					var textValue = mobileNo.Text;
					if (textValue.Length >= 10 || textValue.Length == 0)
					{
						mobileNoLbl.IsVisible = true;
						mobileNoLbl.TextColor = AppGlobalVariables.fontLessThick;
						mobileNoLbl.Text = "Mobile number";
					}
					else
					{
						mobileNoLbl.IsVisible = true;
						mobileNoLbl.TextColor = Color.Red;
						mobileNoLbl.Text = "Mobile number must be 10 digits";
					}


					mobileNo.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					mobileNoLbl.IsVisible = false;
					mobileNoLbl.Text = "Mobile number";
					mobileNo.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					mobileNo.Placeholder = "Mobile number";
				}
				if (string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					mobileNo.Placeholder = "Mobile number";
				}
			};



			password = new CustomEntry()
			{
				Placeholder = "Password",
				//CustomFontSize = fontsize,//15,
				IsCustomPassword = true,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				IsPassword = true,
				Keyboard = Keyboard.Email,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};

			password.TextChanged += pwdChanged;

			passwordLbl = new CustomLabel()
			{
				Text = "Password",
				//TextColor = AppGlobalVariables.fontLessThick,
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			password.Focused += (object sender, FocusEventArgs e) =>
			{

				//var textValue = password.Text;
				//if (textValue.Length >= 8)
				//{
				//    passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				//    passwordLbl.Text = "Password";
				//}
				//else {
				passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				passwordLbl.Text = "Password";
				//}

				passwordLbl.IsVisible = true;
				password.PlaceholderColor = Color.Transparent;
				//password.Placeholder = string.Empty;
			};
			password.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(password.Text))
				{
					//passwordLbl.FontSize = 10;
					passwordLbl.IsVisible = true;

					var textValue = password.Text;
					if (textValue.Length >= 8 || textValue.Length == 0)
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
						passwordLbl.Text = "Password";
					}
					else
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = Color.Red;
						passwordLbl.Text = "Password must be 8 characters";
					}


					password.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					passwordLbl.IsVisible = false;
					passwordLbl.Text = "Password";
					password.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					password.Placeholder = "Password";
				}
				if (string.IsNullOrWhiteSpace(password.Text))
				{
					password.Placeholder = "Password";
				}
			};



			confirmPassword = new CustomEntry()
			{
				Placeholder = "Confirm Password",
				//CustomFontSize = fontsize,//15,
				IsCustomPassword = true,
				TextColor = AppGlobalVariables.fontVeryThick,
				PlaceholderColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(CustomEntry)),
				IsPassword = true,
				Keyboard = Keyboard.Email,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End,
			};


			confirmPassword.TextChanged += confirmPwdChanged;

			confirmPasswordLbl = new CustomLabel()
			{
				Text = "Confirm Password",
				//TextColor = AppGlobalVariables.fontLessThick,
				TextColor = AppGlobalVariables.fontLessThick,
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(CustomLabel)),
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				IsVisible = false,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Start
			};
			confirmPassword.Focused += (object sender, FocusEventArgs e) =>
			{

				//var textValue = password.Text;
				//if (textValue.Length >= 8)
				//{
				//    passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				//    passwordLbl.Text = "Password";
				//}
				//else {
				confirmPasswordLbl.TextColor = AppGlobalVariables.fontLessThick;
				confirmPasswordLbl.Text = "Confirm Password";
				//}

				confirmPasswordLbl.IsVisible = true;
				confirmPassword.PlaceholderColor = Color.Transparent;
				//password.Placeholder = string.Empty;
			};
			confirmPassword.Unfocused += (object sender, FocusEventArgs e) =>
			{
				if (!string.IsNullOrWhiteSpace(password.Text))
				{
					//passwordLbl.FontSize = 10;
					confirmPasswordLbl.IsVisible = true;

					var textValue = password.Text;
					if (textValue.Length >= 8 || textValue.Length == 0)
					{
						confirmPasswordLbl.IsVisible = true;
						confirmPasswordLbl.TextColor = AppGlobalVariables.fontLessThick;
						confirmPasswordLbl.Text = "Confirm Password";
					}
					else
					{
						confirmPasswordLbl.IsVisible = true;
						confirmPasswordLbl.TextColor = Color.Red;
						confirmPasswordLbl.Text = "Confirm Password must be 8 characters";
					}


					confirmPassword.PlaceholderColor = Color.Transparent;
					//password.Placeholder = string.Empty;
				}
				else
				{
					//passwordLbl.FontSize = 15;ppGlobalVariables.fontLessThick
					confirmPasswordLbl.IsVisible = false;
					confirmPasswordLbl.Text = "confirm Password";
					confirmPassword.PlaceholderColor = AppGlobalVariables.fontVeryThick;
					confirmPassword.Placeholder = "confirm Password";
				}
				if (string.IsNullOrWhiteSpace(confirmPassword.Text))
				{
					confirmPassword.Placeholder = "confirm Password";
				}
			};
			var btnHeight = (height * 7) / 100;



			//HorizontalGradientButton btnRegister = new HorizontalGradientButton()
			//{
			//    StartColor = AppGlobalVariables.lightMarron,
			//    EndColor = AppGlobalVariables.darkMarron,
			//    Text = "Register",
			//    BorderRadius = 3,
			//    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
			//    HeightRequest = btnHeight,
			//    TextColor = Color.FromHex("#FFFFFF"),
			//    FontFamily = AppGlobalVariables.fontFamily45,
			//    HorizontalOptions = LayoutOptions.Center,
			//    BackgroundColor = AppGlobalVariables.lightMarron,
			//    WidthRequest = entryWidth,
			//    VerticalOptions = LayoutOptions.End

			//};

			//btnRegister.Clicked += btnRegister_Click;


			//BoxView entityNameULine = new BoxView()
			//{
			//    HeightRequest = boxHeight,
			//    WidthRequest = boxWidth,
			//    Color = AppGlobalVariables.underLineColor,
			//    BackgroundColor = AppGlobalVariables.underLineColor,
			//    HorizontalOptions = LayoutOptions.Center,
			//    VerticalOptions = LayoutOptions.EndAndExpand
			//};


			BoxView mobileNoULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout mobileNoULineStack = new StackLayout()
			{
				Children = { mobileNoULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView passwordULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout passwordULineStack = new StackLayout()
			{
				Children = { passwordULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView confirmPassULine = new BoxView()
			{
				HeightRequest = 0.8,
				WidthRequest = entryWidth,
				Color = AppGlobalVariables.underLineColor,
				BackgroundColor = AppGlobalVariables.underLineColor,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			StackLayout confirmPassULineStack = new StackLayout()
			{
				Children = { confirmPassULine },
				Spacing = 0,
				Padding = new Thickness(0, 0, 0, 5),
				HeightRequest = entryHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.End
			};

			BoxView space = new BoxView()
			{
				Opacity = 0,
				//HeightRequest = (entryHeight * 2) / 9,
				HeightRequest = 6,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};



			Grid pageBody = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto},
					new RowDefinition{Height = GridLength.Auto}

				},

				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				RowSpacing = entryHeight / 10,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};



			Image btnBackground = new Image()
			{
				Source = "imgLoginButton.png",
				HeightRequest = entryHeight,
				WidthRequest = entryWidth,
				//Margin = new Thickness(0, screenHeight / 13, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			CustomLabel imgLogin = new CustomLabel()
			{
				Text = "Register",
				TextColor = Color.White,
				FontAttributes = FontAttributes.Bold,
				HorizontalOptions = LayoutOptions.Center,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				//VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Grid ImgButton = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},
				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}

				},
				//BackgroundColor = Color.Blue,
				//RowSpacing = 10,
				WidthRequest = entryWidth,
				////Padding = new Thickness(20, 20, 10, 0),
				////HorizontalOptions = LayoutOptions.FillAndExpand,
				////VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			ImgButton.Children.Add(btnBackground, 0, 0);
			ImgButton.Children.Add(imgLogin, 0, 0);

			TapGestureRecognizer btnLoginTap = new TapGestureRecognizer();
			btnLoginTap.Tapped += btnRegister_Click;

			ImgButton.GestureRecognizers.Add(btnLoginTap);


			Grid entry = new Grid()
			{
				RowDefinitions =
				{
					new RowDefinition{Height = GridLength.Auto},

				},
				ColumnDefinitions =
				{
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Auto)},
					new ColumnDefinition{Width = new GridLength(1, GridUnitType.Star)}
				},
				WidthRequest = entryWidth,
				ColumnSpacing = 8,
				//Padding = new Thickness(20, 20, 10, 0),
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				//VerticalOptions = LayoutOptions.Start
				//Padding = new Thickness(10, 20, 10, 5),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand
			};
			entry.Children.Add(regCountryCode, 0, 0);
			entry.Children.Add(imgCountryCode, 1, 0);
			entry.Children.Add(mobileNo, 2, 0);


			pageBody.Children.Add(mobileNoLbl, 0, 0);
			pageBody.Children.Add(mobileNoULineStack, 0, 0);
			pageBody.Children.Add(entry, 0, 0);
			pageBody.Children.Add(passwordLbl, 0, 1);
			pageBody.Children.Add(passwordULineStack, 0, 1);
			pageBody.Children.Add(password, 0, 1);
			pageBody.Children.Add(confirmPasswordLbl, 0, 2);
			pageBody.Children.Add(confirmPassULineStack, 0, 2);
			pageBody.Children.Add(confirmPassword, 0, 2);
			pageBody.Children.Add(space, 0, 3);
			pageBody.Children.Add(ImgButton, 0, 4);

			StackLayout bodyHolder = new StackLayout()
			{
				Children = { pageBody },
				Spacing = (entryHeight * 3) / 4,
				Padding = new Thickness(0, 0, 0, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand
			};

			//ScrollView scrollHolder = new ScrollView()
			//{
			//    Content = bodyHolder,
			//    //VerticalOptions = LayoutOptions.End

			//};


			#region PageFooter

			CustomLabel lblAlreadyRegister = new CustomLabel()
			{
				Text = "Already registered?",
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = AppGlobalVariables.fontLessThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
			};

			CustomLabel lblLogin = new CustomLabel()
			{
				Text = "Login",
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = AppGlobalVariables.fontVeryThick,
				CustomFontFamily = AppGlobalVariables.fontFamily45,
				//FontAttributes = FontAttributes.Bold
			};

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += (s, e) =>
			{
				//Application.Current.Properties["Detail"] = "Log";
				//App.Current.MainPage = new CountryMasterPage();
				Navigation.PushModalAsync(new UserLogin());
			};
			lblLogin.GestureRecognizers.Add(tapGestureRecognizer);

			BoxView spaceFooter = new BoxView()
			{
				HeightRequest = entryHeight / 4,
				BackgroundColor = Color.Transparent
			};

			StackLayout stackFooter = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { lblAlreadyRegister, lblLogin },
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.EndAndExpand
			};

			HorizontalGradientStack iosstatusbarstack = new HorizontalGradientStack()
			{
				StartColor = AppGlobalVariables.lightMarron,
				EndColor = AppGlobalVariables.darkMarron,
				BackgroundColor = Color.FromHex("#4B0000"),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = screenHeight / 28.30,
				IsVisible = Device.OnPlatform(true, false, false)
			};


			Image joboneimg = new Image()
			{
				Source = "imgLogoLoginRegister.png",
				HeightRequest = screenHeight / 10,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			BoxView imgTopspace = new BoxView()
			{
				Opacity = 0,
				HeightRequest = screenHeight / 13,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};


			BoxView spaceButton = new BoxView()
			{
				Opacity = 0,
				HeightRequest = entryWidth / 8.5,
				WidthRequest = entryWidth,
				Color = Color.Transparent,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.End
			};


			#endregion

			StackLayout stackMain = new StackLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = { iosstatusbarstack, imgTopspace, joboneimg, spaceButton, bodyHolder, stackFooter, spaceFooter }
			};


			PageControlsStackLayout.Children.Add(stackMain);


		}

		void mobileNoChanged(object sender, TextChangedEventArgs e)
		{
			var mobileObject = (Entry)sender;
			var textValue = mobileObject.Text;
			if (textValue.Length > 10)
			{
				mobileNo.Text = textValue.Remove(textValue.Length - 1);
			}
			else if (textValue.Length == 10)
			{
				mobileNoLbl.TextColor = AppGlobalVariables.fontLessThick;
				mobileNoLbl.Text = "Mobile number";
			}
			else
			{
				mobileNoLbl.TextColor = Color.Red;
				mobileNoLbl.Text = "Mobile number must be 10 digits";
			}
		}

		void pwdChanged(object sender, TextChangedEventArgs e)
		{
			var textValue = password.Text;
			if (textValue.Length >= 8)
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = AppGlobalVariables.fontLessThick;
				passwordLbl.Text = "Password";
			}
			else
			{
				passwordLbl.IsVisible = true;
				passwordLbl.TextColor = Color.Red;
				passwordLbl.Text = "Password must be 8 characters";
			}

		}

		void confirmPwdChanged(object sender, TextChangedEventArgs e)
		{
			var textValue = confirmPassword.Text;
			if (textValue.Length >= 8)
			{
				confirmPasswordLbl.IsVisible = true;
				confirmPasswordLbl.TextColor = AppGlobalVariables.fontLessThick;
				confirmPasswordLbl.Text = "Confirm Password";
			}
			else
			{
				confirmPasswordLbl.IsVisible = true;
				confirmPasswordLbl.TextColor = Color.Red;
				confirmPasswordLbl.Text = "Confirm Password must be 8 characters";
			}

		}

		#region to get data from another pages
		public void changeCountryCode(string cCode)
		{
			//regCountryCode1.Text = cCode;
		}
		#endregion

		private async void btnRegister_Click(object sender, EventArgs e)
		{
			mobileNo.Unfocus();
			password.Unfocus();
			confirmPassword.Unfocus();

			try
			{
				if (string.IsNullOrWhiteSpace(mobileNo.Text) && string.IsNullOrWhiteSpace(password.Text))
				{
					passwordLbl.IsVisible = true;
					mobileNoLbl.IsVisible = true;
					confirmPasswordLbl.IsVisible = true;

					mobileNoLbl.Text = "Please enter Mobile number";
					passwordLbl.Text = "Please enter password";
					confirmPasswordLbl.Text = "Please enter confirm password";

					mobileNoLbl.TextColor = Color.Red;
					confirmPasswordLbl.TextColor = Color.Red;
					passwordLbl.TextColor = Color.Red;
				}
				else if (string.IsNullOrWhiteSpace(mobileNo.Text))
				{
					mobileNoLbl.IsVisible = true;
					mobileNoLbl.TextColor = Color.Red;
					mobileNoLbl.Text = "Please enter Mobile number";
				}

				else if (mobileNo.Text.Length != 10)
				{
					mobileNoLbl.IsVisible = true;
					mobileNoLbl.TextColor = Color.Red;
					mobileNoLbl.Text = "Mobile number must be 10 digits";
				}
				else if (!Regex.IsMatch(mobileNo.Text, @"^([1-9])([0-9]*)$"))
				{
					mobileNoLbl.IsVisible = true;
					mobileNoLbl.TextColor = Color.Red;
					mobileNoLbl.Text = "Mobile number must be 10 digits";
				}
				else if (string.IsNullOrWhiteSpace(password.Text))
				{
					passwordLbl.IsVisible = true;
					passwordLbl.TextColor = Color.Red;
					passwordLbl.Text = "Please enter password";
				}

				else if (string.IsNullOrWhiteSpace(confirmPassword.Text))
				{
					confirmPasswordLbl.IsVisible = true;
					confirmPasswordLbl.TextColor = Color.Red;
					confirmPasswordLbl.Text = "Please enter confirm password";
				}
				else if (password.Text.Length < 8 || confirmPassword.Text.Length < 8)
				{
					if (password.Text.Length < 8)
					{
						passwordLbl.IsVisible = true;
						passwordLbl.TextColor = Color.Red;
						passwordLbl.Text = "Password must be 8 characters";
					}
					else
					{
						confirmPasswordLbl.IsVisible = true;
						confirmPasswordLbl.TextColor = Color.Red;
						confirmPasswordLbl.Text = "Password must be 8 characters";
					}
				}
				else if (confirmPassword.Text != password.Text)
				{
					await Task.Delay(100);
					var textValue = confirmPassword.Text;

					if (textValue.Length >= 8 || confirmPassword.Text != password.Text)
					{
						confirmPasswordLbl.IsVisible = true;
						confirmPasswordLbl.TextColor = Color.Red;
						confirmPasswordLbl.Text = "Password's did't match";
					}

					confirmPasswordLbl.IsVisible = true;
				}
				else
				{


					PageLoading.IsVisible = true;

					CheckSeekerReq csRequest = new CheckSeekerReq();
					csRequest.phone = mobileNo.Text;

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (ICheckSeekerBAL checkUser = new CheckSeekerBAL())
						{
							var checkUserResponse = await checkUser.CheckingSeeker(csRequest);
							if (checkUserResponse != null)
							{
								if (checkUserResponse.Status == 1)
								{
									//await DisplayAlert("OneJob", checkUserResponse.Message, "Ok");
									OTPReq objOtpReq = new OTPReq();

									Random generator = new Random();
									String randomNumber = generator.Next(100000, 999999).ToString("D6");
									objOtpReq.otp = randomNumber;

									string str = regCountryCode.Text + mobileNo.Text;

									objOtpReq.phone_number = str;

									objOtpReq.otp_for = "Registration";

									string pageInfo = "registration";

									string[] registerdata = new string[] { pageInfo, randomNumber, mobileNo.Text, password.Text, regCountryCode.Text };
									PageLoading.IsVisible = true;


									if (CheckNetworkAccess.IsNetworkConnected())
									{

										using (IOtpBAL getOtpDetails = new OtpBAL())
										{
											var otpCheck = await getOtpDetails.mobileOTP(objOtpReq);

											if (otpCheck != null)
											{
												if (otpCheck.Status == 1)
												{
													await DisplayAlert("OneJob", "Please check your mobile for OTP.", "Ok");
													//    await Navigation.PushModalAsync(new OTPVerification());
													await Navigation.PushModalAsync(new OTPVerification(registerdata));

													PageLoading.IsVisible = false;
												}
												else
												{
													await DisplayAlert("OneJob", otpCheck.Message, "Ok");
													PageLoading.IsVisible = false;

												}
											}
											else
											{
												//await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
												await DisplayAlert("OneJob", "Please check country code.", "Ok");
												PageLoading.IsVisible = false;

											}
										}
									}
									else
									{
										await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
										PageLoading.IsVisible = false;
									}
									PageLoading.IsVisible = false;
								}
								else
								{
									//await DisplayAlert("OneJob", checkUserResponse.Message, "Ok");
									await DisplayAlert("OneJob", checkUserResponse.Message, "Ok");
									PageLoading.IsVisible = false;

								}
							}
							else
							{
								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok");
								PageLoading.IsVisible = false;
							}
						}

					}
					else
					{
						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
						PageLoading.IsVisible = false;
					}
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}
	}
}