﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using Plugin.Geolocator;

namespace OneJob
{
	public partial class JobCompleted : BaseContentPage
	{
		//ObservableCollection<AppliedJobsList> jobDetailsMenu { get; set; }
		ListView detailsList;
		CustomEntry searchentry;
		Rectangle sbAbsentRect, sbPresentRect;
		StackLayout nullDisplayStack;
		ObservableCollection<SeekerRelatedJobsList> AppliedJobsMenu;

		public StackLayout searchbarStack;
		AbsoluteLayout searchbarHolder;

		Plugin.Geolocator.Abstractions.IGeolocator locator;

		double _mylatitude;

		double _mylongitude;
		public JobCompleted()
		{
			var height = (screenHeight * 1) / 100;
			var width = (screenWidth * 1) / 100;
			InitializeComponent();

			SeekerJobsList();


			#region for header Stack
			var headerHeight = height * 9.4824;//9.4824;
			var titleSize = height * 3.5;
			var headerChildrenHeight = height * 8;
			var headerChildrenWidth = height * 8;

			var header_HolderLayer = new BoxView()
			{
				BackgroundColor = Color.Transparent,
				IsVisible = false,
				//Opacity = 0.1,
				WidthRequest = width * 100,
				HeightRequest = screenHeight / 9.55,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
			};

			Image navigationBack = new Image()
			{
				Source = ImageSource.FromFile("navigationBarBack.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.End
			};
			Label pageTitle = new Label()
			{
				Text = "Job Completed",
				TextColor = Color.White,
				//BackgroundColor = Color.Green,
				FontSize = titleSize,
				HeightRequest = headerChildrenHeight,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.End
			};
			Image searchListView = new Image()
			{
				Source = ImageSource.FromFile("navigationBarSearch.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				//WidthRequest = headerChildrenHeight,
				WidthRequest = height * 4,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};
			TapGestureRecognizer searchImageClicked = new TapGestureRecognizer();
			searchImageClicked.NumberOfTapsRequired = 1;
			searchImageClicked.Tapped += async (object sender, EventArgs e) =>
			{
				searchListView.Opacity = 0.5;
				await Task.Delay(100);
				searchListView.Opacity = 1;
				if (nullDisplayStack.IsVisible == false)
				{
					header_HolderLayer.IsVisible = true;
					CallSearchBar(false);
				}
			};
			searchListView.GestureRecognizers.Add(searchImageClicked);
			Image menuList = new Image()
			{
				Source = ImageSource.FromFile("moreIcon.png"),
				//BackgroundColor = Color.Green,
				HeightRequest = headerChildrenHeight,
				WidthRequest = headerChildrenWidth,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End
			};


			//HorizontalGradientStack headerHolder = new HorizontalGradientStack()
			//{
			//	Children = { navigationBack, pageTitle, searchListView, menuList },
			//	StartColor = AppGlobalVariables.lightMarron,
			//	EndColor = AppGlobalVariables.darkMarron,
			//	BackgroundColor = AppGlobalVariables.lightMarron,
			//	Orientation = StackOrientation.Horizontal,
			//	HeightRequest = headerHeight,
			//	HorizontalOptions = LayoutOptions.Fill,
			//	VerticalOptions = LayoutOptions.Start
			//};




			HorizontalGradientStack header_Holder = new HorizontalGradientStack() 			{ 				Children = { navigationBack, pageTitle, searchListView, menuList }, 				StartColor = AppGlobalVariables.lightMarron, 				EndColor = AppGlobalVariables.darkMarron, 				BackgroundColor = AppGlobalVariables.lightMarron, 				Orientation = StackOrientation.Horizontal,
				//HeightRequest = headerHeight,
				HeightRequest = screenHeight / 9.55, 				HorizontalOptions = LayoutOptions.Fill, 				VerticalOptions = LayoutOptions.Start 			};

			var headerHolder = new AbsoluteLayout()
			{
				HeightRequest = screenHeight / 9.55,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
			};
			AbsoluteLayout.SetLayoutBounds(header_Holder, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(header_Holder, AbsoluteLayoutFlags.All);
			headerHolder.Children.Add(header_Holder);
			AbsoluteLayout.SetLayoutBounds(header_HolderLayer, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(header_HolderLayer, AbsoluteLayoutFlags.All);
			headerHolder.Children.Add(header_HolderLayer);


			TapGestureRecognizer menuClicked = new TapGestureRecognizer();
			menuClicked.NumberOfTapsRequired = 1;
			menuClicked.Tapped += (object sender, EventArgs e) =>
		   {
			   //try
			   //{
			   // var ParentPage = (MasterDetailPage)this.Parent;
			   // ParentPage.IsPresented = (ParentPage.IsPresented == false) ? true : false;
			   //}
			   //catch (Exception ex)
			   //{
			   // var msg = ex.Message;
			   //}
			   var ParentPage = (MasterDetailPage)this.Parent;
			   ParentPage.Detail = new HomePage() { BackgroundColor = Color.White };
		   };
			navigationBack.GestureRecognizers.Add(menuClicked);


			#region for search bar
			var search_BarWidth = screenWidth;
			var searchBarWidth = (search_BarWidth * 85) / 100;

			Image closeBar = new Image()
			{
				Source = ImageSource.FromFile("imgCloseRed.png"),
				//Rotation = 180,
				HeightRequest = (width * 6),
				WidthRequest = (width * 6),
				Margin = new Thickness(3, 0, 0, 0),
				HorizontalOptions = LayoutOptions.End
			};


			searchentry = new CustomEntry()
			{
				Placeholder = "Search...",
				BackgroundColor = Color.White,
				PlaceholderColor = AppGlobalVariables.fontLessThick,
				TextColor = AppGlobalVariables.fontVeryThick,
				FontFamily = AppGlobalVariables.fontFamily65,
				WidthRequest = searchBarWidth,
				HorizontalOptions = LayoutOptions.Start
			};
			TapGestureRecognizer closeBarClicked = new TapGestureRecognizer();
			closeBarClicked.NumberOfTapsRequired = 1;
			closeBarClicked.Tapped += async (object sender, EventArgs e) =>
			{
				try
				{
					closeBar.Opacity = 0.5;
					await Task.Delay(100);
					closeBar.Opacity = 1;
					searchentry.Text = string.Empty;
					header_HolderLayer.IsVisible = false;
					CallSearchBar(true);
				}
				catch (Exception ex)
				{
					var msg = ex.Message;
				}
			};
			closeBar.GestureRecognizers.Add(closeBarClicked);

			/*
			//searchlistview = new ListView()
			//{
			//	//VerticalOptions = LayoutOptions.FillAndExpand,
			//	//HorizontalOptions = LayoutOptions.FillAndExpand,
			//	//SeparatorVisibility = SeparatorVisibility.None,
			//	IsVisible = false,
			//	SeparatorColor = Color.Black,
			//	BackgroundColor = Color.White
			//	//BackgroundColor = Color.Red
			//	//Margin = new Thickness(0, screenHeight / 10.98, 0, 0),
			//	//RowHeight = rowheight
			//};

			//searchlistview.ItemTemplate = new DataTemplate(typeof(searchcell));

			//searchlistview.ItemSelected += async (sender, e) =>
			//{
			//	try
			//	{
			//		searchlistview.IsVisible = false;
			//		if (((ListView)sender).SelectedItem == null)
			//		{
			//			return;
			//		}
			//		var item = ((ListView)sender).SelectedItem as Prediction;
			//		searchcomplete = "ok";
			//		searchentry.Text = item.description;
			//		loc = await new JobListBAL().GetAreaLocations(item.place_id);
			//		_latitude = loc.result.geometry.location.lat;
			//		_longitude = loc.result.geometry.location.lng;
			//		GetJobmap();
			//		GetJoblist();

			//		((ListView)sender).SelectedItem = null;
			//	}
			//	catch (Exception ex)
			//	{

			//	}
			//};


			//AbsoluteLayout.SetLayoutBounds(searchlistview, new Rectangle(0.5, Device.OnPlatform(0.35, 0.359, 0.188), 0.9, 0.3));
			//AbsoluteLayout.SetLayoutFlags(searchlistview, AbsoluteLayoutFlags.All);
			//ablayout.Children.Add(searchlistview);
			*/

			searchentry.Unfocused += (sender, e) =>
			{
				//searchlistview.IsVisible = false;
			};

			searchentry.TextChanged += searchItem;
			/*(object sender, TextChangedEventArgs e) =>
			{
				allmainstackpin.IsVisible = false;
				if (_key == "map")
				{
					try
					{

						if (searchentry.Text == string.Empty)
						{
							searchlistview.IsVisible = false;
						}
						else
						{
							if (searchcomplete != "ok")
							{
								Arearesponse = await new JobListBAL().GetAreaDetails(searchentry.Text);
								searchlistview.ItemsSource = Arearesponse.predictions.ToList();
								searchlistview.IsVisible = true;

							}
							else
							{
								searchlistview.IsVisible = false;
							}
							searchcomplete = null;
						}
					}
					catch (Exception efg)
					{

					}
				}
				else
				{
					_searchkey = searchentry.Text;
					GetJoblist();
				}

			};*/


			StackLayout searchstack = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = { closeBar, searchentry },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 6
			};

			CustomFrame searchframe = new CustomFrame()
			{
				Content = searchstack,
				HeightRequest = headerHeight,
				Padding = new Thickness(screenHeight / 147.2, 0, screenHeight / 35.04, 0),
				HasShadow = false,
			};

			searchbarHolder = new AbsoluteLayout()
			{
				HeightRequest = headerHeight,
				IsVisible = false,
				//HeightRequest = height * 12,
				WidthRequest = search_BarWidth,
				VerticalOptions = LayoutOptions.Start,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(searchframe, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(searchframe, AbsoluteLayoutFlags.All);
			searchbarHolder.Children.Add(searchframe);

			TapGestureRecognizer closeSearch = new TapGestureRecognizer();
			closeSearch.NumberOfTapsRequired = 1;
			closeSearch.Tapped += (object sender, EventArgs e) =>
			{
				header_HolderLayer.IsVisible = false;
				searchbarHolder.IsVisible = false;
				CallSearchBar(true);
			};
			header_HolderLayer.GestureRecognizers.Add(closeSearch);


			#endregion

			#endregion

			#region for body
			//var borderThick = (width * 5) / 100;
			detailsList = new ListView()
			{
				HasUnevenRows = true,
				//Margin = new Thickness(borderThick, borderThick, borderThick, borderThick),
				BackgroundColor = AppGlobalVariables.lightGray,
				//ItemsSource = jobDetailsMenu,
				SeparatorVisibility = SeparatorVisibility.None,
				ItemTemplate = new DataTemplate(typeof(JobCompletedViewCell)),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			detailsList.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) =>
			 {
				 PageLoading.IsVisible = true;
				 try
				 {
					 var itemSelected = e.SelectedItem as SeekerRelatedJobsList;
					 if (itemSelected == null)
					 {
						 return;
					 }
					 var showData = await GetDetailed(itemSelected.Id);
					 if (searchbarHolder.IsVisible == true)
					 {
						 header_HolderLayer.IsVisible = false;
						 searchbarHolder.IsVisible = false;
						 CallSearchBar(true);
					 }
					 if (showData != null)
					 {
						 PageLoading.IsVisible = true;
						 await Navigation.PushModalAsync(new JobCompletedDetail(showData));
						 PageLoading.IsVisible = false;
					 }
					 else
					 {
						 //Navigation.PushModalAsync(new HomeDetailsPage(itemSelected));
					 }

			//Navigation.PushModalAsync(new FavouriteJobListDetail(itemSelected));
			((ListView)sender).SelectedItem = null;
				 }
				 catch (Exception ex)
				 {
					 var msg = ex.Message;
				 }
				 PageLoading.IsVisible = false;
			 };


			#endregion

			#region NullStack
			CustomLabel nullText = new CustomLabel()
			{
				Text = "No jobs ",
				TextColor = AppGlobalVariables.lightGray,
				CustomFontFamily = AppGlobalVariables.fontFamily65,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			StackLayout nullOverLay = new StackLayout()
			{
				Children = { nullText },
				BackgroundColor = Color.FromRgba(0, 0, 0, 0.3),
				HeightRequest = (height * 100),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			nullDisplayStack = new StackLayout()
			{
				Children = { nullOverLay },
				IsVisible = false,
				Spacing = 0,
				BackgroundColor = Color.Transparent,
				HeightRequest = (height * 100),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			#endregion


			StackLayout display_Stack = new StackLayout()
			{
				Children = { detailsList },
				Spacing = 0,
				//HeightRequest = (height * 74.94),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};


			//AbsoluteLayout displayStack = new AbsoluteLayout()
			//{
			//	//HeightRequest = (height * 74.94),
			//	HorizontalOptions = LayoutOptions.FillAndExpand,
			//};
			//AbsoluteLayout.SetLayoutBounds(display_Stack, new Rectangle(0, 0, 1, 1));
			//AbsoluteLayout.SetLayoutFlags(display_Stack, AbsoluteLayoutFlags.All);
			//displayStack.Children.Add(display_Stack);

			#region for holders
			var bodySpacing = height * 1.76;
			StackLayout holderStack = new StackLayout()
			{
				Children = { headerHolder, searchbarHolder },
				Spacing = bodySpacing / 2,
				//Padding = new Thickness(bodySpacing, bodySpacing, bodySpacing, bodySpacing),
				BackgroundColor = AppGlobalVariables.lightGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Start
			};


			StackLayout bodyHolder = new StackLayout()
			{
				Children = { display_Stack },
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			StackLayout holder1 = new StackLayout()
			{
				Children = { holderStack, bodyHolder },
				Spacing = bodySpacing / 2,
				Padding = new Thickness(0, 0, 0, 0),
				//Padding = new Thickness(0, bodySpacing, 0, 0),
				BackgroundColor = AppGlobalVariables.lightGray,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			StackLayout holder = new StackLayout()
			{
				Children = { holder1 },
				Padding = new Thickness(0, 0, 0, 0),
				//Padding = new Thickness(0, bodySpacing, 0, 0),
				BackgroundColor = AppGlobalVariables.lightMarron,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};



			AbsoluteLayout totalHolder1 = new AbsoluteLayout()
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			AbsoluteLayout.SetLayoutBounds(holder, new Rectangle(0, 0, 1, 1));
			AbsoluteLayout.SetLayoutFlags(holder, AbsoluteLayoutFlags.All);
			totalHolder1.Children.Add(holder);

			AbsoluteLayout.SetLayoutBounds(nullDisplayStack, new Rectangle(1, 1, 1, 0.89));
			AbsoluteLayout.SetLayoutFlags(nullDisplayStack, AbsoluteLayoutFlags.All);
			totalHolder1.Children.Add(nullDisplayStack);

			//AbsoluteLayout.SetLayoutBounds(searchbarHolder, new Rectangle(0, 0, 1, screenHeight / 9.55));
			//AbsoluteLayout.SetLayoutFlags(searchbarHolder, AbsoluteLayoutFlags.WidthProportional);
			//totalHolder1.Children.Add(searchbarHolder);


			PageControlsStackLayout.Children.Add(totalHolder1);

			#endregion
		}

		FavJobListReq objFavListReq;
		JobsListInfo JLInfo;

		async Task<JobsListInfo> GetDetailed(string jobId)
		{

			try
			{
				DBMethods objDatabase = new DBMethods();
				if (objDatabase != null)
				{
					var userResp = objDatabase.GetUserInfo();
					JobDetailsReq reqObj = new JobDetailsReq();
					reqObj.jobid = jobId;
					reqObj.seeker_id = userResp.UserID;
					reqObj.latitude = Convert.ToString(_mylatitude);
					reqObj.longitude = Convert.ToString(_mylongitude);

					PageLoading.IsVisible = true;

					if (CheckNetworkAccess.IsNetworkConnected())
					{

						using (IJobDetailsBAL getJobDetails = new JobDetailsBAL())
						{
							var detailsResponse = await getJobDetails.jobDetailsData(reqObj);
							if (detailsResponse != null)
							{
								if (detailsResponse.Status == 0)
								{
									JLInfo = new JobsListInfo()
									{
										amountType = detailsResponse.JobDetails[0].amountType,
										Favourite = detailsResponse.JobDetails[0].Favourite,
										companyName = detailsResponse.JobDetails[0].companyName,
										Compensention = detailsResponse.JobDetails[0].Compensention,
										createdDate = detailsResponse.JobDetails[0].createdDate,
										jobId = detailsResponse.JobDetails[0].jobId,
										Status = detailsResponse.JobDetails[0].Status,
										Distance = detailsResponse.JobDetails[0].Distance,
										endDate = detailsResponse.JobDetails[0].endDate,
										Experience = detailsResponse.JobDetails[0].Experience,
										FaceBook = detailsResponse.JobDetails[0].FaceBook,
										firstName = detailsResponse.JobDetails[0].firstName,
										jobAddress = detailsResponse.JobDetails[0].jobAddress,
										jobCity = detailsResponse.JobDetails[0].jobCity,
										jobCountryId = detailsResponse.JobDetails[0].jobCountryId,
										jobDescription = detailsResponse.JobDetails[0].jobDescription,
										jobEmail = detailsResponse.JobDetails[0].jobEmail,
										jobStateId = detailsResponse.JobDetails[0].jobStateId,
										jobTitle = detailsResponse.JobDetails[0].jobTitle,
										jobZipCode = detailsResponse.JobDetails[0].jobZipCode,
										lastName = detailsResponse.JobDetails[0].lastName,
										Latitude = detailsResponse.JobDetails[0].Latitude,
										linkesIn = detailsResponse.JobDetails[0].linkesIn,
										Longitude = detailsResponse.JobDetails[0].Longitude,
										modifiedDate = detailsResponse.JobDetails[0].modifiedDate,
										Name = detailsResponse.JobDetails[0].Name,
										noOfPositions = detailsResponse.JobDetails[0].noOfPositions,
										perHourDay = detailsResponse.JobDetails[0].perHourDay,
										profile_pic = detailsResponse.JobDetails[0].profile_pic,
										Providerid = detailsResponse.JobDetails[0].Providerid,
										providerPhoneNumber = detailsResponse.JobDetails[0].providerPhoneNumber,
										ProviderRating = detailsResponse.JobDetails[0].ProviderRating,
										startDate = detailsResponse.JobDetails[0].startDate,
										Twitter = detailsResponse.JobDetails[0].Twitter,
										workTypeID = detailsResponse.JobDetails[0].workTypeID,
										workTypeName = detailsResponse.JobDetails[0].workTypeName,
										Applied = detailsResponse.JobDetails[0].Applied,
										countryName = detailsResponse.JobDetails[0].countryName,
										stateName = detailsResponse.JobDetails[0].stateName,
										isInterview = detailsResponse.JobDetails[0].isInterview,
										RatingTest = detailsResponse.JobDetails[0].RatingTest
									};
									PageLoading.IsVisible = false;
								}
								else
								{
									JLInfo = null;
									//await DisplayAlert("OneJob", "", "ok");
								}
							} 							else 							{ 								await DisplayAlert("OneJob", AppGlobalVariables._annonymousMessage, "Ok"); 								PageLoading.IsVisible = false; 							}

						}
					}
					else
					{ 						await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok"); 						PageLoading.IsVisible = false; 					} 
				}
				else
				{
					JLInfo = null;
					await DisplayAlert("OneJob", "Please login again", "ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				JLInfo = null;
				await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
			return JLInfo;
		}



		SeekerRelatedJobs coutriesList;
		public async void SeekerJobsList()
		{
			PageLoading.IsVisible = true;
			try
			{
				locator = CrossGeolocator.Current;
				locator.DesiredAccuracy = 50;

				if (locator.IsGeolocationEnabled)
				{
					PageLoading.IsVisible = true;
					Plugin.Geolocator.Abstractions.Position myposition = await locator.GetPositionAsync(10000);
					PageLoading.IsVisible = false;
					_mylatitude = myposition.Latitude;
					_mylongitude = myposition.Longitude;
				}
				else
				{
					await DisplayAlert("Message", "Please enable Gps for locations.", "Ok");
				}
			}
			catch (Exception ex)
			{

			}


			try
			{

				DBMethods objDatabase = new DBMethods();
				var getLocalDB = objDatabase.GetUserInfo();
				SeekerRelatedJobsReq SRJobs = new SeekerRelatedJobsReq();
				if (getLocalDB != null)
				{
					SRJobs.seeker_id = getLocalDB.UserID;
					SRJobs.latitude = Convert.ToString(_mylatitude);
					SRJobs.longitude = Convert.ToString(_mylongitude);
				}

				PageLoading.IsVisible = true;

				if (CheckNetworkAccess.IsNetworkConnected())
				{
					using (ISeekerRelatedJobsBAL seekerTypeJobList = new SeekerRelatedJobsBAL())
					{
						coutriesList = await seekerTypeJobList.SeekerRelatedJob(SRJobs);
						if (coutriesList != null)
						{
							AppliedJobsMenu = new ObservableCollection<SeekerRelatedJobsList>();
							if (coutriesList.Status == 0)
							{
								foreach (var item in coutriesList.JobDetails)
								{
									if (item.Status == "closed" || item.Status == "completed")
									{

										AppliedJobsMenu.Add(new SeekerRelatedJobsList
										{
											AmountType = item.AmountType,
											CompanyName = item.CompanyName,
											Status = item.Status,
											Compensation = item.Compensation,
											CreatedDate = item.CreatedDate,
											EndDate = item.EndDate,
											Experience = item.Experience,
											Facebook = item.Facebook,
											Id = item.Id,
											JobAddress = item.JobAddress,
											JobCity = item.JobCity,
											JobCountryId = item.JobCountryId,
											JobDescription = item.JobDescription,
											JobEmail = item.JobEmail,
											JobStateId = item.JobStateId,
											JobTitle = item.JobTitle,
											JobZipcode = item.JobZipcode,
											Latitude = item.Latitude,
											Linkedin = item.Linkedin,
											Longitude = item.Longitude,
											ModifiedDate = item.ModifiedDate,
											NoOfPositions = item.NoOfPositions,
											PerHourDay = item.PerHourDay,
											ProviderId = item.ProviderId,
											StartDate = item.StartDate,
											Twitter = item.Twitter,
											WorktypeId = item.WorktypeId,
											WorktypeName = item.WorktypeName,
											firstName = item.firstName,
											lastName = item.lastName,
											providerPhone = item.providerPhone,
											providerRating = item.providerRating,
											seekerStatus = item.seekerStatus,
											userProfile = item.userProfile,
											Distance = item.Distance

										});
										//if (coutriesList.JobDetails.Count == 0)
										//{
										//	nullDisplayStack.IsVisible = true;
										//}
										//else
										//{
										//	nullDisplayStack.IsVisible = false;
										//}
										//PageLoading.IsVisible = false;
									}
								}

								if (AppliedJobsMenu.Count <= 0)
								{
									nullDisplayStack.IsVisible = true;
								}
								else
								{
									nullDisplayStack.IsVisible = false;
								}

								detailsList.ItemsSource = AppliedJobsMenu;

								PageLoading.IsVisible = false;
							}
							PageLoading.IsVisible = false;
						}
					}
					PageLoading.IsVisible = false;
				}
				else
				{
					await DisplayAlert("OneJob", AppGlobalVariables.NetWorkMsg, "Ok");
					PageLoading.IsVisible = false;
				}
			}
			catch (Exception ex)
			{
				PageLoading.IsVisible = false;
				var msg = ex.Message;
			}
		}


		string thisTab = "closed";
		void CallSearchBar(bool value)
		{
			try
			{
				if (value == true)
				{
					searchentry.Unfocus();
					searchbarHolder.IsVisible = false;
					searchbarStack.Opacity = 1;
					//searchbarStack.LayoutTo(sbAbsentRect, 0, null);
				}
				else
				{
					searchentry.Focus();
					searchbarHolder.IsVisible = true;
					searchbarStack.Opacity = 1;
					//searchbarStack.LayoutTo(sbPresentRect, 0, null);
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}
		}

		void searchItem(object sender, TextChangedEventArgs e)
		{
			detailsList.BeginRefresh();
			try
			{

				if (string.IsNullOrWhiteSpace(e.NewTextValue))
				{
					detailsList.ItemsSource = AppliedJobsMenu;
				}
				else
				{
					detailsList.ItemsSource = AppliedJobsMenu.Where(i => i.JobTitle.ToLower().Contains(e.NewTextValue.ToLower()));
				}

				/*
				switch (thisTab)
				{
					case "closed":
						if (string.IsNullOrWhiteSpace(e.NewTextValue))
						{
							detailsList.ItemsSource = AppliedJobsMenu;

						}
						else {
							detailsList.ItemsSource = AppliedJobsMenu.Where(i => i.JobTitle.ToLower().Contains(e.NewTextValue.ToLower()));
						}
						break;
				}
				*/
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
			}

			detailsList.EndRefresh();

		}

	}

	public class JobCompletedViewCell : ViewCell
	{
		dynamic i1tem;
		protected override void OnBindingContextChanged()
		{
			double width = (BaseContentPage.screenWidth * 1) / 100;
			double height = (BaseContentPage.screenHeight * 1) / 100;
			base.OnBindingContextChanged();
			i1tem = BindingContext;

			if (i1tem != null)
			{
				var item = i1tem as SeekerRelatedJobsList;

				#region for populating data
				var fontSize1 = height * 2.582;
				var fontSize2 = height * 1.8022;
				var callImgHeight = height * 4;
				Image workIcon = new Image()
				{
					VerticalOptions = LayoutOptions.StartAndExpand,
					HeightRequest = BaseContentPage.screenHeight / 21.02,
					WidthRequest = BaseContentPage.screenWidth / 11.82,
					Margin = new Thickness(BaseContentPage.screenHeight / 48.06, 5, BaseContentPage.screenHeight / 48.06, 0),
					//Source = "imgJob.png"
				};

				if (i1tem.userProfile != null && i1tem.userProfile != "")
				{
					//workIcon.SetBinding(Image.SourceProperty, new Binding("profile_pic"));
					workIcon.Source = new UriImageSource()
					{
						Uri = new Uri(i1tem.userProfile),
						CachingEnabled = false
					};
				}
				else
				{
					workIcon.Source = Device.OnPlatform("imgJob.png", "imgJob.png", "imgJob.png");
				}

				//if (item.userProfile == null)
				//{
				//	workIcon.Source = "imgJob.png";
				//}
				//else
				//{
				//	workIcon.SetBinding(Image.SourceProperty, new Binding("userProfile"));

				//}

				StackLayout workIconholder = new StackLayout()
				{
					Children = { workIcon },
					//Padding = new Thickness(10, 0, 10, 0),
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
				Image phone_Star_Icon = new Image()
				{
					//Source = ImageSource.FromFile(item.image2Url),
					HeightRequest = callImgHeight,
					WidthRequest = callImgHeight,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.Center
				};
				Label shallRateLbl = new Label()
				{
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.End
				};
				//if (item.image2Url == "imgRatingNone.png")
				//{
				//	shallRateLbl.Text = "Rate this job";
				//}
				//else if (item.image2Url == "imgRatingDone.png")
				//{
				//	shallRateLbl.SetBinding(Label.TextProperty, new Binding("rating"));
				//}
				//else
				//{
				//	//"callButton"
				//	shallRateLbl.IsVisible = false;
				//}
				StackLayout phoneImageholder = new StackLayout()
				{
					Children = { phone_Star_Icon, shallRateLbl },
					Padding = new Thickness(10, 0, 10, 0),
					HorizontalOptions = LayoutOptions.Center,
					VerticalOptions = LayoutOptions.CenterAndExpand
				};
				phone_Star_Icon.SetBinding(Image.SourceProperty, new Binding("image2Url"));
				Label titleLbl = new Label()
				{
					TextColor = Color.Black,
					FontSize = fontSize1,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.End
				};
				//titleLbl.SetBinding(Label.TextProperty, "JobTitle");


				string titleString = i1tem.JobTitle;
				if (titleString.Length >= 20)
				{
					titleString = titleString.Remove(17);
					titleString = titleString + "...";
					titleLbl.Text = titleString;
				}
				else
				{
					titleLbl.SetBinding(Label.TextProperty, new Binding("JobTitle"));
				}


				Label timeLbl = new Label()
				{
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalOptions = LayoutOptions.End,
					VerticalOptions = LayoutOptions.End
				};
				timeLbl.SetBinding(Label.TextProperty, new Binding("createdDate"));
				//timeLbl.SetBinding(Label.TextProperty, "time");



				#region for rating stars

				var ratingIcons1 = new Image() { };
				var ratingIcons2 = new Image() { };
				var ratingIcons3 = new Image() { };
				var ratingIcons4 = new Image() { };
				var ratingIcons5 = new Image() { };



				#region Rating Logic

				var rate = Double.Parse(i1tem.providerRating);
				if (rate != null)
				{
					if (rate <= 0.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 0.5 || rate <= 1.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 1.5 || rate <= 2.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 2.5 || rate <= 3.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate == 3.5 || rate <= 4.4)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
					}
					else if (rate >= 4.5)
					{
						ratingIcons1.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons2.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons3.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons4.Source = ImageSource.FromFile("imgRatingDone");
						ratingIcons5.Source = ImageSource.FromFile("imgRatingDone");
					}

				}
				else
				{
					ratingIcons1.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons2.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons3.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons4.Source = ImageSource.FromFile("imgRatingNone");
					ratingIcons5.Source = ImageSource.FromFile("imgRatingNone");
				}

				#endregion



				Grid ratingHolder = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1, GridUnitType.Star)},
				},
					ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star)}
				},
					Padding = new Thickness(0, 0, 0, 0),
					ColumnSpacing = 0,
					RowSpacing = 0,
					WidthRequest = width * 15.62,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Center
				};
				ratingHolder.Children.Add(ratingIcons1, 0, 0);
				ratingHolder.Children.Add(ratingIcons2, 1, 0);
				ratingHolder.Children.Add(ratingIcons3, 2, 0);
				ratingHolder.Children.Add(ratingIcons4, 3, 0);
				ratingHolder.Children.Add(ratingIcons5, 4, 0);
				#endregion

				Label distance_Lbl = new Label()
				{
					Text = "Distance: ",
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalOptions = LayoutOptions.End,
					VerticalOptions = LayoutOptions.End
				};
				Label distanceLbl = new Label()
				{
					//Text = "3.2 miles",
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalOptions = LayoutOptions.End,
					VerticalOptions = LayoutOptions.Start
				};




				string input2 = i1tem.Distance;
				if (input2 == null)
				{
					distanceLbl.SetBinding(Label.TextProperty, new Binding("Distance"));
				}
				else
				{
					if (input2.Length >= 9)
					{
						string sub = input2.Substring(0, 9);
						distanceLbl.Text = sub;
					}
					else
					{
						distanceLbl.Text = i1tem.Distance;
					}
				}


				//distanceLbl.SetBinding(Label.TextProperty, "distance");
				Grid rati_dist_Stack = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{  Height = GridLength.Auto},
					new RowDefinition{  Height = GridLength.Auto}
				},
					ColumnDefinitions =
				{
					new ColumnDefinition{ Width = GridLength.Auto}
				},
					ColumnSpacing = 0,
					RowSpacing = 0,//cellSpacing/3,
					HorizontalOptions = LayoutOptions.End,
					VerticalOptions = LayoutOptions.End
				};
				rati_dist_Stack.Children.Add(distance_Lbl, 0, 0);
				rati_dist_Stack.Children.Add(distanceLbl, 1, 0);

				Label postedBy_Lbl = new Label()
				{
					Text = "Posted By: ",
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.End
				};
				Label postedByLbl = new Label()
				{
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.End
				};
				postedByLbl.SetBinding(Label.TextProperty, "CompanyName");
				Grid postedLbLGrid = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{  Height = GridLength.Auto},
					new RowDefinition{  Height = GridLength.Auto}
				},
					ColumnDefinitions =
				{
					new ColumnDefinition{ Width = GridLength.Auto}
				},
					ColumnSpacing = 0,
					RowSpacing = 0,//cellSpacing/3,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.End
				};
				postedLbLGrid.Children.Add(postedBy_Lbl, 0, 0);
				postedLbLGrid.Children.Add(postedByLbl, 1, 0);

				Label descriptionLabel = new Label()
				{
					Text = item.JobDescription,
					TextColor = Color.Gray,
					FontSize = fontSize2,
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Start
				};

				//descriptionLabel.SetBinding(Label.TextProperty, "JobDescription");


				string input = i1tem.JobDescription;
				if (input == null)
				{
					descriptionLabel.SetBinding(Label.TextProperty, new Binding("JobDescription"));
				}
				else
				{
					if (input.Length >= 85)
					{
						string sub = input.Substring(0, 85);
						descriptionLabel.Text = sub + "....";
					}
					else
					{
						descriptionLabel.Text = i1tem.JobDescription;
					}
				}

				StackLayout descriptionLabelHolder = new StackLayout()
				{
					Children = { descriptionLabel },
					Padding = new Thickness(15, 0, 0, 0),
					HorizontalOptions = LayoutOptions.Start,
					VerticalOptions = LayoutOptions.Start
				};
				#endregion

				#region for cell holder
				var cellHeight = height * 17.606;
				var cellSpacing = (height * 0.704) / 2;
				Grid holderGrid = new Grid()
				{
					RowDefinitions =
				{
					new RowDefinition{  Height = new GridLength(1.5, GridUnitType.Star)},
					new RowDefinition{  Height = new GridLength(0.8, GridUnitType.Star)},
					new RowDefinition{  Height = new GridLength(0.8, GridUnitType.Star)},
					new RowDefinition{  Height = new GridLength(2, GridUnitType.Star)}
				},
					ColumnDefinitions =
				{
					new ColumnDefinition{ Width = new GridLength(1.2, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(5, GridUnitType.Star)},
					new ColumnDefinition{ Width = new GridLength(1.5, GridUnitType.Star)}
				},
					HeightRequest = cellHeight - (cellSpacing * 2),
					//WidthRequest = width*100,
					RowSpacing = 0,//cellSpacing/3,
					BackgroundColor = Color.White,
					Padding = new Thickness(0, cellSpacing * 1.5, 0, cellSpacing * 4),
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.Center
				};
				holderGrid.Children.Add(workIconholder, 0, 1, 0, 3);
				holderGrid.Children.Add(titleLbl, 1, 0);
				holderGrid.Children.Add(timeLbl, 1, 0);
				holderGrid.Children.Add(phoneImageholder, 2, 3, 0, 4);
				holderGrid.Children.Add(rati_dist_Stack, 1, 1);
				holderGrid.Children.Add(ratingHolder, 1, 1);
				holderGrid.Children.Add(postedLbLGrid, 1, 2);
				holderGrid.Children.Add(descriptionLabelHolder, 0, 2, 3, 4);
				#endregion

				//Label deleteMe = new Label()
				//{
				//	Text = "delete Me",
				//	TextColor = Color.White,
				//	HorizontalOptions = LayoutOptions.Center,
				//	VerticalOptions = LayoutOptions.Center
				//};

				//StackLayout removeHolder = new StackLayout()
				//{
				//	Children = { deleteMe },
				//	HeightRequest = cellHeight - (cellSpacing * 2),
				//	WidthRequest = cellHeight - (cellSpacing * 2),
				//	BackgroundColor = AppGlobalVariables.lightMarron,
				//	Padding = new Thickness(0, cellSpacing, 0, cellSpacing),
				//	HorizontalOptions = LayoutOptions.End,
				//	VerticalOptions = LayoutOptions.Center
				//};

				StackLayout holder = new StackLayout()
				{
					Children = { holderGrid },
					Orientation = StackOrientation.Horizontal,
					BackgroundColor = Color.FromRgb(227, 223, 224),
					Padding = new Thickness(0, 0, 0, BaseContentPage.screenHeight / 105),
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.Center,
				};

				//ScrollView holder1 = new ScrollView()
				//{
				//	Content = holder,
				//	Orientation = ScrollOrientation.Horizontal,
				//	HorizontalOptions = LayoutOptions.FillAndExpand,
				//	VerticalOptions = LayoutOptions.Center
				//};

				View = holder;
			}
		}
	}
}
