﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class VerticalGradientGrid : Grid
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
