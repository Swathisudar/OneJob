﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class HorizontalGradientStack : StackLayout
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
