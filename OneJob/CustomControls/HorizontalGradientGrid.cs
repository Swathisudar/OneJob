﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class HorizontalGradientGrid : Grid
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
