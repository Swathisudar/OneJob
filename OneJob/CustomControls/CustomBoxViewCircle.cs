﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class CustomBoxViewCircle : BoxView
	{
		public static readonly BindableProperty CornerRadiusProperty =
			BindableProperty.Create(nameof(CornerRadius), typeof(double), typeof(CustomBoxViewCircle), 0.0);

		public double CornerRadius
		{
			get { return (double)GetValue(CornerRadiusProperty); }
			set { SetValue(CornerRadiusProperty, value); }
		}
	}
}
