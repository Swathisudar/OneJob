﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class HorizontalGradientButton : Button
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
