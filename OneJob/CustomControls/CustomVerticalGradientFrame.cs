﻿using System;
using Xamarin.Forms;
namespace OneJob
{
	public class CustomVerticalGradientFrame : Frame
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
