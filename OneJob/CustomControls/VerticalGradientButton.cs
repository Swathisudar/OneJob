﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class VerticalGradientButton : Button
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
