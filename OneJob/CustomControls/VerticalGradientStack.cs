﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class VerticalGradientStack : StackLayout
	{
		public Color StartColor { get; set; }
		public Color EndColor { get; set; }
	}
}
