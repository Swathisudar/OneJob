﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	#region Selected Languages JSON to C# classes 

	/* Request Class*/

	public class Laguage
	{
		public string id { get; set; }
		public string language_name { get; set; }
		public string language_code { get; set; }
		public string status { get; set; }
	}

	public class LaguageList
	{
		public List<Laguage> laguages { get; set; }
	}

	#endregion

}
