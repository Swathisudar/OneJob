﻿using System;
namespace OneJob
{

	#region SignUp JSON to C# classes 

	/* Request Class*/

	public class SignUpReq
	{
		public string phone { get; set; }
		public string dial_code { get; set; }
		public string password { get; set; }
		public string udid { get; set; }
		public string device_type { get; set; }
		public string device_info { get; set; }
	}


	/* Response Class*/

	public class SignUpRes
	{
		public string error_count { get; set; }
		public string success_message { get; set; }
		public int register_id { get; set; }
		public string dial_code { get; set; }
	}

	#endregion

}
