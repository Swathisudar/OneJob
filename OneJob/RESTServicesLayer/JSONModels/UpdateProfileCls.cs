﻿using System;
using System.Collections.Generic;

namespace OneJob
{

	#region UpdateProfile JSON to C# classes */


	/* Request Class*/



	public class Worktypereq
	{
		public int worktype_id { get; set; }
		public int experience { get; set; }
	}
	public class WorkTypeList
	{
		public List<Worktypereq> worktype { get; set; }
	}

	public class UpdateProfileReq
	{
		public string id { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string date_of_birth { get; set; }
		public string optional_dial_code { get; set; }
		public string optional_phone { get; set; }
		public string address { get; set; }
		public List<int> language_id { get; set; }
		public string gender { get; set; }
		public string location { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public List<Worktypereq> worktypereq { get; set; }
		public string job_description { get; set; }
	}






	//public class Worktype
	//{
	//	public int worktype_id { get; set; }
	//	public int experience { get; set; }
	//	public string id { get; set; }
	//	public string worktype_name { get; set; }
	//	public string status { get; set; }
	//	public string created_date { get; set; }
	//	public object modified_date { get; set; }

	//}



	//public class UpdateProfileReq
	//{
	//	public string id { get; set; }
	//	public string first_name { get; set; }
	//	public string last_name { get; set; }
	//	public string date_of_birth { get; set; }
	//	public string optional_phone { get; set; }
	//	public string address { get; set; }
	//	public string country_id { get; set; }
	//	public List<int> language_id { get; set; }
	//	public string gender { get; set; }
	//	public List<Worktype> worktype { get; set; }
	//	public string job_description { get; set; }
	//}

	//response
	public class LanguageId
	{
		public string id { get; set; }
		public string language_name { get; set; }
		public string language_code { get; set; }
		public string status { get; set; }
	}

	public class Worktype
	{
		public string worktype_id { get; set; }
		public string worktype_name { get; set; }
		public int experience { get; set; }
	}

	public class UserDetails
	{
		public string id { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string date_of_birth { get; set; }
		public string optional_phone { get; set; }
		public string optional_dial_code { get; set; }
		public string address { get; set; }
		public List<LanguageId> language_id { get; set; }
		public string gender { get; set; }
		public string job_description { get; set; }
		public string location { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string modified_date { get; set; }
		public List<Worktype> worktype { get; set; }
	}

	public class UpdateProfileRes
	{
		public int error_count { get; set; }
		public string message { get; set; }
		public UserDetails user_details { get; set; }
	}


	//public class LanguageId
	//{
	//	public string id { get; set; }
	//	public string language_name { get; set; }
	//	public string language_code { get; set; }
	//	public string status { get; set; }
	//}


	//public class UserDetails
	//{
	//	public string id { get; set; }
	//	public string first_name { get; set; }
	//	public string last_name { get; set; }
	//	public string date_of_birth { get; set; }
	//	public string optional_phone { get; set; }
	//	public string address { get; set; }
	//	public string country_id { get; set; }
	//	public List<LanguageId> language_id { get; set; }
	//	public string gender { get; set; }
	//	public string job_description { get; set; }
	//	public string modified_date { get; set; }
	//	public List<Worktype> worktype { get; set; }
	//}

	//public class UpdateProfileRes
	//{
	//	public int error_count { get; set; }
	//	public string message { get; set; }
	//	public UserDetails user_details { get; set; }
	//}
	#endregion
}