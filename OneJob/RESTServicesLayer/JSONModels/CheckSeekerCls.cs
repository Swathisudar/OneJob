﻿using System;
namespace OneJob
{
	public class CheckSeekerCls
	{
		public CheckSeekerCls(){}
	}

	//www.devrabbit.com/projects/onejob/seeker/checkseekerexistornot
	//request
	public class CheckSeekerReq
	{
		public string phone { get; set; }
	}

	//response
	public class CheckSeekerRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}

	//response data
}
