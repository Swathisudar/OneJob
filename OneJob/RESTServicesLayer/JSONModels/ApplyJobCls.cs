﻿using System;
namespace OneJob
{
	public class ApplyJobReq
	{
		public string jobid { get; set; }
		public string provider_id { get; set; }
		public string seeker_id { get; set; }
		public string start_time { get; set; }
		public string end_time { get; set; }
	}

	public class ApplyJobRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}
}
