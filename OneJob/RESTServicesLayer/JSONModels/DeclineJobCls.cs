﻿using System;
namespace OneJob
{
	public class DeclineJobCls
	{
		public DeclineJobCls(){}
	}

	//http://www.devrabbit.com/projects/onejob/seeker/declinejob
	//request
	public class DeclineJobReq
	{
		public string jobid { get; set; }
		public string provider_id { get; set; }
		public string seeker_id { get; set; }
		public string status { get; set; }
	}

	//response
	public class DeclineJobRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}

	//response data
}
