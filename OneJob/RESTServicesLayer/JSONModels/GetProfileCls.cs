﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace OneJob
{

	public class GetProfileReq
	{
		public string seeker_id { get; set; }
	}

	public class GetprofileLanguageid
	{
		public string id { get; set; }
		public string language_name { get; set; }
		public string language_code { get; set; }
		public string status { get; set; }
	}

	public class GetprofileWorktype
	{
		public string worktype_id { get; set; }
		public string worktype_name { get; set; }
		public string experience { get; set; }
	}

	public class GetprofileDetails
	{
		public string id { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string date_of_birth { get; set; }
		public string email { get; set; }
		public string dial_code { get; set; }
		public string optional_dial_code { get; set; }
		public string phone { get; set; }
		public string optional_phone { get; set; }
		public string password { get; set; }
		public string address { get; set; }
		public string location { get; set; }
		public string language_id { get; set; }
		public string city { get; set; }
		public string state_id { get; set; }
		public string country_id { get; set; }
		public string zipcode { get; set; }
		public string gender { get; set; }
		public string job_description { get; set; }
		public string profile_pic { get; set; }
		public string udid { get; set; }
		public string device_type { get; set; }
		public string device_info { get; set; }
		public string created_date { get; set; }
		public string modified_date { get; set; }
		public string seeker_status { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string seeker_rating { get; set; }
		public List<GetprofileLanguageid> getprofile_languageids { get; set; }
		public List<GetprofileWorktype> getprofile_worktype { get; set; }
	}

	public class GetProfileResData
	{
		public int error_count { get; set; }
		public GetprofileDetails getprofile_details { get; set; }
	}

}

