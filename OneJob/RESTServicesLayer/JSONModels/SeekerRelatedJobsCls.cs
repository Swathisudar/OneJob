﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class SeekerRelatedJobsCls
	{
		public SeekerRelatedJobsCls() { }
	}

	//http://www.devrabbit.com/projects/onejob/seeker/seekerrelatedjobs
	//request
	public class SeekerRelatedJobsReq
	{
		public string seeker_id { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
	}


	//response

	public class Jobdetail
	{
		public string id { get; set; }
		public string provider_id { get; set; }
		public string job_title { get; set; }
		public string worktype_id { get; set; }
		public string experience { get; set; }
		public string no_of_positions { get; set; }
		public string compensation { get; set; }
		public string per_hour_day { get; set; }
		public string amount_type { get; set; }
		public string job_description { get; set; }
		public string job_address { get; set; }
		public string job_city { get; set; }
		public string job_state_id { get; set; }
		public string job_country_id { get; set; }
		public string job_zipcode { get; set; }
		public string status { get; set; }
		public string job_email { get; set; }
		public string linkedin { get; set; }
		public string twitter { get; set; }
		public string facebook { get; set; }
		public string created_date { get; set; }
		public object modified_date { get; set; }
		public string start_date { get; set; }
		public string end_date { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string is_interview { get; set; }
		public string seeker_status { get; set; }
		public string worktype_name { get; set; }
		public string company_name { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string profile_pic { get; set; }
		public string provider_phone { get; set; }
		public string distance { get; set; }
		public string provider_rating { get; set; }
	}

	public class SeekerRelatedJobsRes
	{
		public int status { get; set; }
		public List<Jobdetail> jobdetails { get; set; }
	}

}

