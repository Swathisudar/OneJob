﻿using System;
namespace OneJob
{

	#region ForgetPassword JSON to C# classes 

	/* Request Class*/

	public class ForgetPwdReq
	{
		public string phone { get; set; }
		public string password { get; set; }
	}

	/* Response Class*/

	public class ForgetPwdRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}

	#endregion

}
