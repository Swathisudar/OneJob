﻿using System;
namespace OneJob
{
	public class SeekerRatingCls
	{
		public SeekerRatingCls(){}
	}

	//www.devrabbit.com/projects/onejob/seeker/seekerrating
	//request
	public class SeekerRatingReq
	{
		public string seeker_id { get; set; }
		public string rating { get; set; }
		public string job_id { get; set; }
		public string provider_id { get; set; }
		public string type { get; set; }
	}

	//response
	public class SeekerRatingRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}

	//response data
}
