﻿using System;
namespace OneJob
{
	#region OTP JSON to C# classes 

	/* Request Class*/

	public class OTPReq
	{
		public string phone_number { get; set; }
		public string otp_for { get; set; }
		public string seeker { get; set; }
		public string otp { get; set; }
	}


	/* Response Class*/

	public class OTPRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}

	#endregion

}
