﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class NotificationCls
	{
		public int status { get; set; }
		public List<Notificationjob> notificationjobs { get; set; }
	}
	public class Notificationjob
	{
		public string notification_id { get; set; }
		public string seeker_id { get; set; }
		public string job_id { get; set; }
		public string message { get; set; }
		public string status { get; set; }
		public string created_date { get; set; }
	}

	public class NotificationReq
	{
		public string seeker_id { get; set; }
	}

}
