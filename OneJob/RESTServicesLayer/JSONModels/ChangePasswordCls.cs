﻿using System;
namespace OneJob
{

	#region Change Password JSON to C# classes 

	/* Request Class*/

	public class ChangePwdReq
	{
		public string id { get; set; }
		public string password { get; set; }
	}

	/* Response Class*/

	public class ChangePwdRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}

	#endregion

}
