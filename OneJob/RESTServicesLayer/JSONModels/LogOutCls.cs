﻿using System;
namespace OneJob
{
	public class LogOutReq
	{
		public string seeker_id { get; set; }
	}

	public class LogOutRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}
}
