﻿using System;
using System.Collections.Generic;

namespace OneJob
{

	public class FavJobListReq 	{
		public string seeker_id { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; } 	}  	public class Job 	{ 		public string id { get; set; }
		public string provider_id { get; set; }
		public string job_title { get; set; }
		public string worktype_id { get; set; }
		public string experience { get; set; }
		public string no_of_positions { get; set; }
		public string compensation { get; set; }
		public string per_hour_day { get; set; }
		public string amount_type { get; set; }
		public string job_description { get; set; }
		public string job_address { get; set; }
		public string job_city { get; set; }
		public string job_state_id { get; set; }
		public string job_country_id { get; set; }
		public string job_zipcode { get; set; }
		public string status { get; set; }
		public string job_email { get; set; }
		public string linkedin { get; set; }
		public string twitter { get; set; }
		public string facebook { get; set; }
		public string created_date { get; set; }
		public object modified_date { get; set; }
		public string start_date { get; set; }
		public string end_date { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string worktype_name { get; set; }
		public string company_name { get; set; }
		public string profile_pic { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string name { get; set; }
		public string provider_phone { get; set; }
		public string distance { get; set; }
		public string provider_rating { get; set; }
		public string favourite { get; set; } 	}  	public class JobList 	{ 		public int status { get; set; } 		public List<Job> jobs { get; set; } 		public string type { get; set; } 	}


	#region FavJobs List     public class JobListReq 	{ 		public string latitude { get; set; } 		public string longitude { get; set; } 		public string searchkey { get; set; } 		public string type { get; set; } 	}  	public class Favouritejob 	{ 		public string id { get; set; }
		public string provider_id { get; set; }
		public string job_title { get; set; }
		public string worktype_id { get; set; }
		public string experience { get; set; }
		public string no_of_positions { get; set; }
		public string compensation { get; set; }
		public string per_hour_day { get; set; }
		public string amount_type { get; set; }
		public string job_description { get; set; }
		public string job_address { get; set; }
		public string job_city { get; set; }
		public string job_state_id { get; set; }
		public string job_country_id { get; set; }
		public string job_zipcode { get; set; }
		public string status { get; set; }
		public string job_email { get; set; }
		public string linkedin { get; set; }
		public string twitter { get; set; }
		public string facebook { get; set; }
		public string created_date { get; set; }
		public object modified_date { get; set; }
		public string start_date { get; set; }
		public string end_date { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string is_interview { get; set; }
		public string worktype_name { get; set; }
		public string phone { get; set; }
		public string company_name { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string profile_pic { get; set; }
		public string distance { get; set; }
		public string provider_rating { get; set; } 	}  	public class FavJobList 	{ 		public int status { get; set; } 		public List<Favouritejob> favouritejobs { get; set; } 	}


	#endregion 




	public class GoogleSearch
	{
		public List<Prediction> predictions { get; set; }
		public string status { get; set; }
	}
	public class MatchedSubstring
	{
		public int length { get; set; }
		public int offset { get; set; }
	}

	public class Term
	{
		public int offset { get; set; }
		public string value { get; set; }
	}

	public class Prediction
	{
		public string description { get; set; }
		public string id { get; set; }
		public List<MatchedSubstring> matched_substrings { get; set; }
		public string place_id { get; set; }
		public string reference { get; set; }
		public List<Term> terms { get; set; }
		public List<string> types { get; set; }
	}


	public class AddressComponent
	{
		public string long_name { get; set; }
		public string short_name { get; set; }
		public List<string> types { get; set; }
	}

	public class Location
	{
		public double lat { get; set; }
		public double lng { get; set; }
	}

	public class Northeast
	{
		public double lat { get; set; }
		public double lng { get; set; }
	}

	public class Southwest
	{
		public double lat { get; set; }
		public double lng { get; set; }
	}

	public class Viewport
	{
		public Northeast northeast { get; set; }
		public Southwest southwest { get; set; }
	}

	public class Geometry
	{
		public Location location { get; set; }
		public Viewport viewport { get; set; }
	}

	public class Photo
	{
		public int height { get; set; }
		public List<string> html_attributions { get; set; }
		public string photo_reference { get; set; }
		public int width { get; set; }
	}

	public class Result
	{
		public List<AddressComponent> address_components { get; set; }
		public string adr_address { get; set; }
		public string formatted_address { get; set; }
		public Geometry geometry { get; set; }
		public string icon { get; set; }
		public string id { get; set; }
		public string name { get; set; }
		public List<Photo> photos { get; set; }
		public string place_id { get; set; }
		public string reference { get; set; }
		public string scope { get; set; }
		public List<string> types { get; set; }
		public string url { get; set; }
		public int utc_offset { get; set; }
		public string vicinity { get; set; }
	}

	public class LocationRootObject
	{
		public List<object> html_attributions { get; set; }
		public Result result { get; set; }
		public string status { get; set; }
	}
}
