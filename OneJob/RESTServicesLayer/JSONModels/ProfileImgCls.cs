﻿using System;
namespace OneJob
{
	public class ProfileImgReq
	{
		public string seeker_id { get; set; }
		public string seeker_image { get; set; }

	}

	public class ProfileImgRes

	{
		public int status { get; set; }
		public string message { get; set; }
		public string profile_url { get; set; }
	}
}
