﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	#region Selected Countries JSON to C# classes 

	/* Request Class*/

	public class Country
	{
		public string country_id { get; set; }
		public string country_name { get; set; }
		public string country_with_dial_code { get; set; }
		public string dial_code { get; set; }
		public string country_flag { get; set; }
		public string iso2 { get; set; }
	}

	public class CountryList
	{
		public List<Country> countries { get; set; }
	}


	#endregion

}
