﻿using System;
namespace OneJob
{
	public class FavJobReq
	{
		public string job_id { get; set; }
		public string seeker_id { get; set; }
		public string status { get; set; }
	}

	public class FavJobRes
	{
		public int status { get; set; }
		public string message { get; set; }
	}
}