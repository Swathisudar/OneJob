﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OneJob
{
	public class HttpClientSource<T> where T : class
	{
		static HttpClient client;

		static HttpClientSource()
		{
			client = new HttpClient();
			client.BaseAddress = new Uri(Constants.Apiurl);
			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Add("X-Parse-Application-Id", Constants.ApplicationID);
			client.DefaultRequestHeaders.Add("X-Parse-REST-API-Key", Constants.ApiKey);
			//client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(new UTF8Encoding().GetBytes(Constants.Username + ":" + Constants.Password)));
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			//client.DefaultRequestHeaders.Add("secret", Constants.ServerSecretCode);
			//client.DefaultRequestHeaders.Add("securitycode", Constants.ServerSecurityCode);
		}

        internal static Task CreateOrUpdateItemWithPostAsync(string v, object request)
        {
            throw new NotImplementedException();
        }

        public static async Task<string> CreateOrUpdateItemWithPostAsync(string methodName, T t)
		{
			var postData = new StringContent(JsonConvert.SerializeObject(t));
			string returnVal = "";
			try
			{
				//var postData = new StringContent(JsonConvert.SerializeObject(t));

				HttpResponseMessage response = await client.PostAsync(methodName, postData);

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					returnVal = await response.Content.ReadAsStringAsync();
					//t = JsonConvert.DeserializeObject<T>(str);
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				returnVal = null;
			}
			return returnVal;
		}

		private HttpWebRequest GetHttpWebRequestPost(string MethodType, string Url, string ContentType)
		{
			HttpWebRequest endpointRequest = (HttpWebRequest)HttpWebRequest.Create(Url);
			endpointRequest.Method = MethodType;
			endpointRequest.ContentType = ContentType;
			endpointRequest.Headers["APIKey"] = Constants.ApplicationID;
			return endpointRequest;
		}

		private HttpWebRequest GetHttpWebRequestGet(string MethodType, string Url)
		{
			HttpWebRequest endpointRequest = (HttpWebRequest)HttpWebRequest.Create(Url);
			endpointRequest.Method = MethodType;
			endpointRequest.Accept = "application/x-www-form-urlencoded";
			endpointRequest.Headers["APIKey"] = Constants.ApplicationID;
			return endpointRequest;
		}

		public async Task<GoogleSearch> GetAreaDetailsList(string JsonPostData, string MethodType)
		{
			try
			{
				string responsestring = "";
				string Url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + JsonPostData + "&types=geocode&language=en&key=AIzaSyDkrJg4DeV8dDrSxaJjD8_t1EZVHD7RmU0";
				HttpWebRequest endpointRequest = GetHttpWebRequestPost(MethodType, Url, "application/json");
				byte[] byteArray = Encoding.UTF8.GetBytes(JsonPostData);
				using (var stream = await Task.Factory.FromAsync<Stream>(endpointRequest.BeginGetRequestStream, endpointRequest.EndGetRequestStream, null))
				{
					stream.Write(byteArray, 0, byteArray.Length);
					stream.Dispose();
				}
				using (var endpointResponse = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(endpointRequest.BeginGetResponse, endpointRequest.EndGetResponse, null)))
				{
					if (endpointResponse.StatusCode == HttpStatusCode.OK || endpointResponse.StatusCode == HttpStatusCode.Created)
					{
						StreamReader loResponseStream = new StreamReader(endpointResponse.GetResponseStream());
						responsestring = loResponseStream.ReadToEnd();
					}
				}
				var list = (GoogleSearch)JsonConvert.DeserializeObject(responsestring, typeof(GoogleSearch));
				return list;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}


		public async Task<LocationRootObject> GetAreaDetailsLocation(string JsonPostData, string MethodType)
		{
			try
			{
				string Url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + JsonPostData + "&key=AIzaSyDkrJg4DeV8dDrSxaJjD8_t1EZVHD7RmU0";
				HttpWebRequest endpointRequest = GetHttpWebRequestGet("GET", Url);
				string responsestring = "";
				using (var endpointResponse = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(endpointRequest.BeginGetResponse, endpointRequest.EndGetResponse, null)))
				{

					StreamReader loResponseStream = new StreamReader(endpointResponse.GetResponseStream());
					responsestring = loResponseStream.ReadToEnd();
					endpointResponse.Dispose();
					endpointRequest.Abort();
				}

				var list = (LocationRootObject)JsonConvert.DeserializeObject(responsestring, typeof(LocationRootObject));
				return list;

			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}




		public static async Task<T> CreateOrUpdateItemWithPostAsync1(string methodName, T t)
		{
			var postData = new StringContent(JsonConvert.SerializeObject(t));
			string returnVal = "";
			try
			{
				//var postData = new StringContent(JsonConvert.SerializeObject(t));

				HttpResponseMessage response = await client.PostAsync(methodName, postData);

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					returnVal = await response.Content.ReadAsStringAsync();
					t = JsonConvert.DeserializeObject<T>(returnVal);
				}
			}
			catch (Exception ex)
			{
				var msg = ex.Message;
				returnVal = null;
			}
			return t;
		}


		public static async Task<T> RetriveDataWithPostAsync(string methodName)
		{
			//var jsonString = JsonConvert.SerializeObject("{}"); 
			var postData = new StringContent("");
			T t = null;
			HttpResponseMessage response = await client.PostAsync(methodName, postData);
			response.EnsureSuccessStatusCode();
			if (response.IsSuccessStatusCode)
			{
				var str = await response.Content.ReadAsStringAsync();
				t = JsonConvert.DeserializeObject<T>(str);
			}
			return t;
		}

		public static async Task<T> RetriveDataWithGetAsync(string methodName)
		{
			T t = null;
			HttpResponseMessage response = await client.GetAsync(methodName);
			if (response.IsSuccessStatusCode)
			{
				var str = await response.Content.ReadAsStringAsync();
				t = JsonConvert.DeserializeObject<T>(str);
			}
			return t;
		}

		public static async Task<string> UpdateItemWithPutAsync(T t, string methodNameWithIdParam)
		{
			var postData = new StringContent(JsonConvert.SerializeObject(t));
			var status = "";
			//HttpResponseMessage response = await client.PutAsync($"api/products/{Id}", postData);
			HttpResponseMessage response = await client.PutAsync(methodNameWithIdParam, postData);
			response.EnsureSuccessStatusCode();
			if (response.IsSuccessStatusCode)
			{
				status = "Success";
			}
			else
			{
				status = "Failed";
			}
			return status;
		}

		public static async Task<string> DestroyItemWithDeleteAsync(string methodNameWithIdParam)
		{
			var status = "";
			HttpResponseMessage response = await client.DeleteAsync(methodNameWithIdParam);
			if (response.StatusCode == HttpStatusCode.OK)
			{
				status = "Success";
			}
			else
			{
				status = "Failed";
			}
			return status;
		}
	}
}
