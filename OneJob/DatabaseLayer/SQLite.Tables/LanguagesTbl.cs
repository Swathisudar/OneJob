﻿using System;
using SQLite.Net.Attributes;

namespace OneJob
{

	[Table("Languages_Tbl")]
	public class Languages_Tbl
	{
		[PrimaryKey]
		public string Id
		{
			get;
			set;
		}

		public string LanguageName
		{
			get;
			set;
		}
	}
}




//public class AllSportsList
//		{
//			public List<SportItem> SportsList
//			{
//				get;
//				set;
//			}
//		}
//		public class SportItem
//		{
//			public int SportNumber
//			{
//				get;
//				set;
//			}
//			public string Id
//			{
//				get;
//				set;
//			}

//			public string SportName
//			{
//				get;
//				set;
//			}

//			public string SportSymbol
//			{
//				get;
//				set;
//			}

//			public string SportSymbolFav
//			{
//				get;
//				set;
//			}

//			public bool IsTeamFavorite
//			{
//				get;
//				set;
//			}

//			public bool IsEventFavorite
//			{
//				get;
//				set;
//			}

//			public bool IsFavorite
//			{
//				get;
//				set;
//			}

//		}
//	}
//}
