﻿using System;
using SQLite.Net.Attributes;

namespace OneJob
{
	[Table("DeviceTokenInfo")]
	public class DeviceTokenInfo
	{
		public string DeviceToken
		{
			get;
			set;
		}
		public string DeviceUDID
		{
			get;
			set;
		}
	}
}
