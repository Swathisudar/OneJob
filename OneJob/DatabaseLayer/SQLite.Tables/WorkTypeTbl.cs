﻿using System;
using SQLite.Net.Attributes;

namespace OneJob
{

	[Table("WorkType_Tbl")]
	public class WorkType_Tbl
	{
		[PrimaryKey]
		public int ID
		{
			get;
			set;
		}
		public string WorkTypeName
		{
			get;
			set;
		}

		public string Experience
		{
			get;
			set;
		}

		public string CreatedDate
		{
			get;
			set;
		}
		//public object ModifiedDate
		//{
		//	get;
		//	set;
		//}

		public bool IsTeamFavorite
		{
			get;
			set;
		}

		public bool IsEventFavorite
		{
			get;
			set;
		}

		public bool IsFavorite
		{
			get;
			set;
		}

	}
}

