﻿using System;
using System.Collections.Generic;
using SQLite.Net.Attributes;

namespace OneJob
{
	[Table("UsersInfo")]
	public class UsersInfo
	{
		[PrimaryKey]
		public string UserID
		{
			get;
			set;
		}



		public string CountryCode
		{
			get;
			set;
		}

		public string OptionalCountryCode
		{
			get;
			set;
		}

		public string LocationName
		{
			get;
			set;
		}


		public string userRating
		{
			get;
			set;
		}

		public string UserProfilePic
		{
			get;
			set;
		}

		public string SecurityCode
		{
			get;
			set;
		}
		public string UserPassword
		{
			get;
			set;
		}

		public string _localLat
		{
			get;
			set;
		}
		public string _localLog
		{
			get;
			set;
		}





		/* Create Profile */


		public string UserFirstName { get; set; }
		public string UserLastName { get; set; }
		public string dateOFBirth { get; set; }
		public string MobileNumber { get; set; }
		public string OptionalMobile { get; set; }
		public string Address { get; set; }
		//public List<int> languageId { get; set; }
		public string Gender { get; set; }
		//public List<Worktype> WorkType { get; set; }
		public string jobDescription { get; set; }






	}
}
