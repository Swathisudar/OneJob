﻿using System;
using System.Collections.Generic;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;

namespace OneJob
{
	public class DBMethods : IDBMethodes, IDisposable
	{
		static SQLiteConnection sqliteconnection;
		public DBMethods()
		{
			sqliteconnection = DependencyService.Get<ISQLite>().GetConnection();
			sqliteconnection.CreateTable<DeviceTokenInfo>();
			sqliteconnection.CreateTable<UsersInfo>();
			sqliteconnection.CreateTable<WorkType_Tbl>();
			sqliteconnection.CreateTable<Languages_Tbl>();

		}

		public void SaveUserWorkType(List<Worktypereq> workTypeListItems)
		{
			try
			{
				sqliteconnection.Query<WorkType_Tbl>("delete from WorkType_Tbl");

				foreach (var item in workTypeListItems)
				{
					sqliteconnection.Insert(new WorkType_Tbl
					{
						ID = item.worktype_id,
						Experience = item.experience.ToString(),
						//ModifiedDate = item.modified_date,
					});
				}
			}
			catch (Exception ex)
			{
				//LogInfo.ReportErrorInfo(ex.Message, ex.StackTrace, "DatabaseMethods-SaveUserFavSports");
			}
		}


		public void SaveUserLanguages(List<LanguageId> LanguagesListItems)
		{
			try
			{
				sqliteconnection.Query<Languages_Tbl>("delete from Languages_Tbl");

				foreach (var item in LanguagesListItems)
				{
					sqliteconnection.Insert(new Languages_Tbl
					{
						Id = item.id,
						LanguageName = item.language_name,

					});
				}
			}
			catch (Exception ex)
			{
				//LogInfo.ReportErrorInfo(ex.Message, ex.StackTrace, "DatabaseMethods-SaveUserFavSports");
			}
		}



		public void SaveDeviceToken(string deviceToken, string deviceUDID)
		{
			try
			{
				sqliteconnection.Query<DeviceTokenInfo>("delete from DeviceTokenInfo");
				sqliteconnection.Insert(new DeviceTokenInfo
				{
					DeviceToken = deviceToken,
					DeviceUDID = deviceUDID
				});

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public DeviceTokenInfo GetDeviceToken()
		{
			return sqliteconnection.Table<DeviceTokenInfo>().FirstOrDefault();
		}



		public List<WorkType_Tbl> GetWorktype()
		{
			var datas = (from data in sqliteconnection.Table<WorkType_Tbl>() select data).ToList();
			return datas;
		}

		//public Languages_Tbl GetLaguages()
		//{
		//	return sqliteconnection.Table<Languages_Tbl>().GetEnumerator().Current;
		//}

		public List<Languages_Tbl> GetLaguages()
		{
			var datas = (from data in sqliteconnection.Table<Languages_Tbl>() select data).ToList();
			return datas;
		}
		/// <summary>
		/// Create User Info in local database
		/// </summary>
		/// 

		public void SaveUserInfo(UsersInfo objUserInfo)
		{
			try
			{
				//sqliteconnection.Query<UsersInfo> ("delete from UsersInfo");
				var UserInfo = sqliteconnection.Table<UsersInfo>().Where(x => x.UserID == (objUserInfo.UserID)).FirstOrDefault();

				if (UserInfo == null)
				{
					sqliteconnection.Insert(new UsersInfo
					{
						UserID = objUserInfo.UserID,
						Address = objUserInfo.Address,
						dateOFBirth = objUserInfo.dateOFBirth,
						Gender = objUserInfo.Gender,
						jobDescription = objUserInfo.jobDescription,
						//languageId = objUserInfo.languageId,
						OptionalMobile = objUserInfo.OptionalMobile,
						MobileNumber = objUserInfo.MobileNumber,
						UserFirstName = objUserInfo.UserFirstName,
						UserLastName = objUserInfo.UserLastName,
						CountryCode = objUserInfo.CountryCode,
						//WorkType = objUserInfo.WorkType,
						UserProfilePic = objUserInfo.UserProfilePic,
						UserPassword = objUserInfo.UserPassword,
						LocationName = objUserInfo.LocationName,
						userRating = objUserInfo.userRating,
						_localLat = objUserInfo._localLat,
						_localLog = objUserInfo._localLog,
						OptionalCountryCode = objUserInfo.OptionalCountryCode,
						SecurityCode = ""
					});
				}
				else
				{
					//UserInfo.UserID = objUserInfo.UserId;
					//UserInfo.UserName = objUserInfo.Name;
					//UserInfo.UserPhoneNumber = objUserInfo.MobileNumber;
					//UserInfo.SecurityCode = objUserInfo.SecurityCode;
					//UserInfo.UserProfilePic = objUserInfo.UserProfileImageUrl;
					//UserInfo.UserPassword = objUserInfo.OldPassword;
					//sqliteconnection.Update(UserInfo);
				}

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}


		public UsersInfo GetUserInfo()
		{
			try
			{

				var userInfo = sqliteconnection.Table<UsersInfo>().FirstOrDefault();
				if (userInfo != null)
				{

					UsersInfo objUserInfo = new UsersInfo()
					{
						UserID = userInfo.UserID.ToString(),
						CountryCode = userInfo.CountryCode,
						Address = userInfo.Address,
						dateOFBirth = userInfo.dateOFBirth,
						Gender = userInfo.Gender,
						jobDescription = userInfo.jobDescription,
						//languageId = userInfo.languageId,
						OptionalMobile = userInfo.OptionalMobile,
						MobileNumber = userInfo.MobileNumber,
						UserFirstName = userInfo.UserFirstName,
						UserLastName = userInfo.UserLastName,
						LocationName = userInfo.LocationName,
						//WorkType = userInfo.WorkType,
						userRating = userInfo.userRating,
						UserProfilePic = userInfo.UserProfilePic,
						UserPassword = userInfo.UserPassword,
						_localLat = userInfo._localLat,
						_localLog = userInfo._localLog,
						OptionalCountryCode = userInfo.OptionalCountryCode,
						SecurityCode = ""
						//UserName = userInfo.UserName,
						//UserPhoneNumber = userInfo.UserPhoneNumber,
						//SecurityCode = userInfo.SecurityCode,
						//UserProfilePic = userInfo.UserProfilePic,
						//UserPassword = userInfo.UserPassword,

					};
					return objUserInfo;
				}
				else
				{
					return null;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}



		public void DeleteWorkType()
		{
			try
			{

				sqliteconnection.Query<WorkType_Tbl>("delete from WorkType_Tbl");
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void DeleteLanguages()
		{
			try
			{

				sqliteconnection.Query<Languages_Tbl>("delete from Languages_Tbl");
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public void DeleteDeviceTokens()
		{
			try
			{

				sqliteconnection.Query<DeviceTokenInfo>("delete from DeviceTokenInfo");
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}








		public void UpdateUserInfo(UsersInfo objUserInfo)
		{
			var UserInfo = sqliteconnection.Table<UsersInfo>().FirstOrDefault();

			UserInfo.UserID = objUserInfo.UserID;
			//UserInfo.UserName = objUserInfo.UserName;
			//UserInfo.UserPhoneNumber = objUserInfo.UserPhoneNumber;
			//UserInfo.SecurityCode = objUserInfo.SecurityCode;
			//UserInfo.UserPassword = objUserInfo.UserPassword;
			//UserInfo.UserProfilePic = objUserInfo.UserProfilePic; ;
			sqliteconnection.Update(UserInfo);
		}

		public void DeleteUserInfo()
		{
			try
			{

				sqliteconnection.Query<UsersInfo>("delete from UsersInfo");
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~DatabaseMethods() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}


		#endregion
	}
}
