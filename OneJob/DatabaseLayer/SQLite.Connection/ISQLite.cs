﻿using System;
using SQLite.Net;

namespace OneJob
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}
