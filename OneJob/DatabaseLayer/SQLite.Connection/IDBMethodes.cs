﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public interface IDBMethodes
	{
		void SaveDeviceToken(string deviceToken, string deviceUDID);
		DeviceTokenInfo GetDeviceToken();

		void SaveUserInfo(UsersInfo objUserInfo);
		UsersInfo GetUserInfo();

		void UpdateUserInfo(UsersInfo objUserInfo);
		void DeleteUserInfo();

		void SaveUserWorkType(List<Worktypereq> workTypeListItems);
		List<WorkType_Tbl> GetWorktype();
		void DeleteWorkType();

		void SaveUserLanguages(List<LanguageId> sportsListItems);
		List<Languages_Tbl> GetLaguages();
		void DeleteLanguages();
	}
}
