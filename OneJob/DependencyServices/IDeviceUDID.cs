﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneJob
{
	public interface IDeviceUDID
	{
		string GetDeviceUDID();
		void CancelID(int id);
	}
}
