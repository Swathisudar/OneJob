﻿using System;
namespace OneJob
{
	public interface ICrop
	{
		void GetPhotoRegister(byte[] img2, string path, UploadPicture imgUpload, string test);
		void GetPhotoEditProfile(byte[] img2, string path, EditProfile imgUpload, string test);
	}
}
