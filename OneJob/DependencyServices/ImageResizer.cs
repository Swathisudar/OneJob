﻿using System;
namespace OneJob
{
	public interface ImageResizer
	{
		byte[] ResizeImage(byte[] imageData, float width, float height);
	}
}
