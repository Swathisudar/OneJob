﻿using System;
using Xamarin.Forms;

namespace OneJob
{
	public class AppGlobalVariables
	{
		public static string fontFamily65 = "Avenir LT Std, 65 Medium";
		public static string fontFamily45 = "Avenir LT Std, 45 Book";


		public static Color lightMarron = Color.FromHex("##850029");
		public static Color darkMarron = Color.FromHex("#4B0000");
		public static Color viewlightMarron = Color.FromHex("#6D0018");

		public static Color lightBlue = Color.FromHex("#004491");

		public static Color fontVeryThick = Color.FromHex("#303030");
		public static Color fontMediumThick = Color.FromHex("#505050");
		public static Color fontLessThick = Color.FromHex("#707070");

		public static Color lightGray = Color.FromHex("#F5F5F5");
		public static Color underLineColor = Color.FromHex("#999999");

		public static string NetWorkMsg = "Please check your network access";
		public static string _annonymousMessage = "Server error please try again";

	}
}
