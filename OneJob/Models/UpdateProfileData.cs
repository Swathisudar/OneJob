﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class UpdateProfileData
	{
		public string UserID { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string dateOfBirth { get; set; }
		public string optionalDialCode { get; set; }
		public string optionalNumber { get; set; }
		public string Address { get; set; }
		//public string countryId { get; set; }
		public List<LanguageId> languagesId { get; set; }
		public string Latitude { get; set; }
		public string LocationNam { get; set; }
		public string Longitude { get; set; }
		public string Gender { get; set; }
		public string jobDescription { get; set; }
		public string modifiedDate { get; set; }
		public List<Worktype> WorkType { get; set; }

	}

	public class UpdateInfo
	{
		public int errorCount { get; set; }
		public string Message { get; set; }
		public UpdateProfileData userDetails { get; set; }

	}
}
