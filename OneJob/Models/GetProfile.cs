﻿using System;
using System.Collections.Generic;

namespace OneJob
{

	public class GetProileLanguagesList
	{
		public string languageId { get; set; }
		public string languageName { get; set; }
		public string languageCode { get; set; }
		public string Status { get; set; }
	}

	public class GetprofileWorktypeList
	{
		public string worktypeId { get; set; }
		public string worktyeName { get; set; }
		public string Experience { get; set; }
	}

	public class GetProfileData
	{
		public string id { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string dateOfBirth { get; set; }
		public string Email { get; set; }
		public string dailCode { get; set; }
		public string optionalCoutryCode { get; set; }
		public string primaryPhone { get; set; }
		public string optionalPhone { get; set; }
		public string Password { get; set; }
		public string Address { get; set; }
		public string Location { get; set; }
		public string languageId { get; set; }
		public string City { get; set; }
		public string stateId { get; set; }
		public string countryId { get; set; }
		public string zipCode { get; set; }
		public string Gender { get; set; }
		public string jobDescription { get; set; }
		public string profilePic { get; set; }
		public string UDID { get; set; }
		public string deviceType { get; set; }
		public string DeviceInfo { get; set; }
		public string createdDate { get; set; }
		public string modifiedDate { get; set; }
		public string seekerStatus { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public string seekerRating { get; set; }

		public List<GetProileLanguagesList> getProfileLanguagesList { get; set; }
		public List<GetprofileWorktypeList> getprofileWorkTypeList { get; set; }

	}

	public class GetProfileInfoList
	{
		public int errorCount { get; set; }
		public string Message { get; set; }
		public GetProfileData userDetails { get; set; }

	}
}
