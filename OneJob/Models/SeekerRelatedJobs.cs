﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class SeekerRelatedJobs
	{
		public int Status { get; set; }
		public List<SeekerRelatedJobsList> JobDetails { get; set; }
	}


	public class SeekerRelatedJobsList
	{
		public string Id { get; set; }
		public string ProviderId { get; set; }
		public string JobTitle { get; set; }
		public string WorktypeId { get; set; }
		public string Experience { get; set; }
		public string NoOfPositions { get; set; }
		public string Compensation { get; set; }
		public string PerHourDay { get; set; }
		public string AmountType { get; set; }
		public string JobDescription { get; set; }
		public string JobAddress { get; set; }
		public string JobCity { get; set; }
		public string JobStateId { get; set; }
		public string JobCountryId { get; set; }
		public string JobZipcode { get; set; }
		public string Status { get; set; }
		public string JobEmail { get; set; }
		public string Linkedin { get; set; }
		public string Twitter { get; set; }
		public string Facebook { get; set; }
		public string CreatedDate { get; set; }
		public object ModifiedDate { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public string WorktypeName { get; set; }
		public string CompanyName { get; set; }
		public string firstName { get; set; } 		public string lastName { get; set; } 		public string userProfile { get; set; } 		public string providerPhone { get; set; } 		public string providerRating { get; set; }
		public string seekerStatus { get; set; }
		public string Distance { get; set; } 
	}
}
