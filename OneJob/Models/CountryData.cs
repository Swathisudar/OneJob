﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class CountryData
	{
		public string countryID { get; set; }
		public string coutryName { get; set; }
		public string countryDailCode { get; set; }
		public string dailCode { get; set; }
		public string coutryFlag { get; set; }
		public string iso2 { get; set; }
	}
}
