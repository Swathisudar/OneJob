﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class NotificationsJob
	{
		public string NotificationID { get; set; }
		public string seekerId { get; set; }
		public string jobId { get; set; }
		public string Message { get; set; }
		public string Status { get; set; }
		public string createdDate { get; set; }
	}


	public class NotificationListData
	{
		public int Status { get; set; }
		public List<NotificationsJob> notificationJobs { get; set; }
	}
}
