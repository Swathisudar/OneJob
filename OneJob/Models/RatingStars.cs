﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace OneJob
{
	public class RatingStars : ContentView
	{
		private Label ReviewsLabel { get; set; }
		private List<Image> StarImages { get; set; }
		string ratingstaron = "", ratingstarhalf = "", ratingstaroff = "";

		bool testUserRating;
		public RatingStars(bool StarTest)
		{

			testUserRating = StarTest;
			if (testUserRating == true)
			{
				ratingstaron = "imgUserRatingMaroon.png"; ratingstarhalf = "ratingstarhalf.png"; ratingstaroff = "imgUserRatingWhite.png";
			}
			else
			{
				ratingstaron = "imgRatingDone.png"; ratingstarhalf = "ratingstarhalf.png"; ratingstaroff = "imgRatingNone.png";
			}


			GenerateDisplay(testUserRating);


		}
		private void GenerateDisplay(bool RatingUser)
		{

			//Creates Review Count Label 
			ReviewsLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label))
			};

			//Create Star Image Placeholders 
			StarImages = new List<Image>();

			#region Rating show
			//for (int i = 1; i <= 5; i++)
			//	StarImages.Add(new Image());

			#endregion

			#region User Rating

			if (RatingUser == true)
			{
				for (int i = 1; i <= 5; i++)
				{
					Image ObjImage = new Image()
					{
						HeightRequest = BaseContentPage.screenHeight / 25,
						WidthRequest = BaseContentPage.screenHeight / 25
					};

					ObjImage.StyleId = i.ToString();
					var tapGestureRecognizer = new TapGestureRecognizer();
					tapGestureRecognizer.Tapped += (s, e) =>
					{
						Image ObjSelectedImage = (Image)s;
						if (ObjSelectedImage.StyleId != "")
						{
							Rating = int.Parse(ObjSelectedImage.StyleId);

							MessagingCenter.Send<RatingStars, string>(this, "Hi", Rating.ToString());
						}
						// handle the tap
					};
					ObjImage.GestureRecognizers.Add(tapGestureRecognizer);
					StarImages.Add(ObjImage);
				}
			}
			else {

				//Image ObjImage = new Image()
				//{
				//	HeightRequest = BaseContentPage.screenHeight / 30,
				//	WidthRequest = BaseContentPage.screenHeight / 30
				//};
				for (int i = 1; i <= 5; i++)
				{
					StarImages.Add(new Image());
				}

			}



			#endregion
			// StarImages[0].Source = GetStarFileName(2);
			//Create Horizontal Stack containing stars and review count label 
			StackLayout starsStack = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Start,
				Padding = 0,
				Spacing = 0,
				Children = {
					StarImages[0],
					StarImages[1],
					StarImages[2],
					StarImages[3],
					StarImages[4],
					ReviewsLabel
				}
			};

			updateReviewsDisplay();

			updateStarsDisplay();

			this.Content = starsStack;
		}

		//Set the Display of the Reviews Label 
		public void updateReviewsDisplay()
		{
			ReviewsLabel.Text = Reviews > 0 ? " (" + Convert.ToString(Reviews) + ")" : "";
		}

		//Set the correct images for the stars based on the rating 
		public void updateStarsDisplay()
		{
			for (int i = 1; i <= StarImages.Count; i++)
			{
				StarImages[i - 1].Source = GetStarFileName(i);
			}
		}

		//uses zero based position for stars 
		private string GetStarFileName(int position)
		{

			//Rating is out of 10 
			//int currentStarMaxRating = (position + 1) * 2;

			//if (Rating >= currentStarMaxRating)
			//{
			//	return ratingstaron;
			//}
			//else if (Rating >= currentStarMaxRating - 1)
			//{
			//	return ratingstarhalf;
			//}
			//else
			//{
			//	return ratingstaroff;
			//}


			int currentStarMaxRating = (position);

			if (Rating >= currentStarMaxRating)
			{
				return ratingstaron;
			}
			else if (Rating >= currentStarMaxRating)
			{
				return ratingstarhalf;
			}
			else
			{
				return ratingstaroff;
			}

		}

		//Add in configurable "Rating" double property from XAML, for setting the rating stars
		public static BindableProperty RatingProperty =
			BindableProperty.Create<RatingStars, int>(ctrl => ctrl.Rating,
				defaultValue: 0,
				defaultBindingMode: BindingMode.OneWay,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var ratingStars = (RatingStars)bindable;
					ratingStars.updateStarsDisplay();
				}
			);

		//Rating is out of 10 
		public int Rating
		{
			get { return (int)GetValue(RatingProperty); }
			set { SetValue(RatingProperty, value); }
		}

		//Add in configurable "Rating" double property from XAML, for setting the rating stars
		public static BindableProperty ReviewsProperty =
			BindableProperty.Create<RatingStars, int>(ctrl => ctrl.Reviews,
				defaultValue: 0,
				defaultBindingMode: BindingMode.OneWay,
				propertyChanged: (bindable, oldValue, newValue) =>
				{
					var ratingStars = (RatingStars)bindable;
					ratingStars.updateReviewsDisplay();
				}
			);

		//Count of the total number of reviews 
		public int Reviews
		{
			get { return (int)GetValue(ReviewsProperty); }
			set { SetValue(ReviewsProperty, value); }
		}

	}
}
