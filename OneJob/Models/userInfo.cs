﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class User
	{
		public string errorCount { get; set; }
		public string successMessage { get; set; }
		public List<UserInfo> responseInfo { get; set; }
		public string register_id { get; set; }
		public string dailID { get; set; }
	}

	public class UserInfo
	{
		public string UserID { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string dateOfBirth { get; set; }
		public string emailID { get; set; }
		public string phoneNumber { get; set; }
		public string optionalPhoneNumber { get; set; }
		public string Password { get; set; }
		public string Adddress { get; set; }
		public string languageId { get; set; }
		public string cityName { get; set; }
		public string stateID { get; set; }
		public string countryId { get; set; }
		public string zipCode { get; set; }
		public string Gender { get; set; }
		public string Location { get; set; }
		public string seekerRating { get; set; }

		//public string workTypeId { get; set; }
		//public string Experience { get; set; }
		public string jobDescription { get; set; }
		public string profilePic { get; set; }
		public string dailCode { get; set; }
		public string optionalDialCode { get; set; }
		public string UDID { get; set; }
		public string deviceType { get; set; }
		public string deviceInfo { get; set; }
		public string createdDate { get; set; }
		public object modifiedDate { get; set; }
		public string seekerStatus { get; set; }
		public object Latitude { get; set; }
		public object Longitude { get; set; }

		public List<WorkTypeDetails> workType { get; set; }
	}

	public class WorkTypeDetails
	{
		public string WorkTypeId { get; set; }
		public string WorkTypeName { get; set; }
		public string Experience { get; set; }
	}
}
