﻿using System;
using System.Collections.Generic;

namespace OneJob
{

	// Resapone Jobs List and JobDeatails

	public class JobsListInfo
	{
		public string jobId { get; set; }
		public string Providerid { get; set; }
		public string jobTitle { get; set; }
		public string workTypeID { get; set; }
		public string Experience { get; set; }
		public string noOfPositions { get; set; }
		public string Compensention { get; set; }
		public string perHourDay { get; set; }
		public string amountType { get; set; }
		public string jobDescription { get; set; }
		public string jobAddress { get; set; }
		public string jobCity { get; set; }
		public string jobStateId { get; set; }
		public string jobCountryId { get; set; }
		public string jobZipCode { get; set; }
		public string Status { get; set; }
		public string jobEmail { get; set; }
		//public string profile_pic { get; set; }
		public string linkesIn { get; set; }
		public string Twitter { get; set; }
		public string FaceBook { get; set; }
		public string createdDate { get; set; }
		public object modifiedDate { get; set; }
		public string startDate { get; set; }
		public string endDate { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public string workTypeName { get; set; }
		public string Favourite { get; set; }
		public string seekerStatus { get; set; }
		public string companyName { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
		public string Name { get; set; }
		public string stateName { get; set; }
		public string countryName { get; set; }
		public string Distance { get; set; }
		public string statusMessage { get; set; }
		public string providerPhoneNumber { get; set; }
		public string profile_pic { get; set; } 		public string ProviderRating { get; set; }
		public string Applied { get; set; } 		public string isInterview { get; set; }
		public string RatingTest { get; set; }

	}

	public class JobDetailsList
	{
		public int Status { get; set; }

		public List<JobsListInfo> JobDetails { get; set; }
	}
}
