﻿using System;
using System.Collections.Generic;

namespace OneJob
{
	public class SelectWorkType
	{
		public string ID { get; set; }
		public string worktypeName { get; set; }
		public string Status { get; set; }
		public string createsDate { get; set; }
		public object modifiedDate { get; set; }
	}

	//public class Worktype
	//{
	//	public string id { get; set; }
	//	public string worktype_name { get; set; }
	//	public string status { get; set; }
	//	public string created_date { get; set; }
	//	public object modified_date { get; set; }
	//}

	//public class SelectedWorks
	//{
	//	public List<SelectWorkType> worktype { get; set; }
	//}

	public class Worktypeget
	{
		public string id { get; set; }
		public string worktype_name { get; set; }
		public string status { get; set; }
		public string created_date { get; set; }
		public object modified_date { get; set; }
	}

	public class SelectedWorks
	{
		public List<Worktypeget> worktypeget { get; set; }
	}
}
