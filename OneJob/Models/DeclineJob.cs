﻿using System;
namespace OneJob
{
	public class DeclineJob
	{
		public int Status { get; set; }
		public string Message { get; set; }
	}
}
