﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace OneJob
{
	public class SelectedLanguages<T> : INotifyPropertyChanged
	{

		public bool IsTick = false;
		public bool Tick
		{
			get
			{
				return IsTick;
			}
			set
			{
				if (IsTick != value)
				{
					IsTick = value;
					PropertyChanged(this, new PropertyChangedEventArgs("Tick"));
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };
		public string ID { get; set; }
		public string Status { get; set; }
		public string LanguageName { get; set; }
		public string languagesCode { get; set; }
	}
}
